/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_ChatterPostController
 * Description: Controller to create/display chatter feeds
 * Created By: Rohit Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update                                           
 =====================================================================*/
public without sharing class EDGE_ChatterPostController {

    static apexLogHandler.apexLog logCreateNewChatterFeed = new apexLogHandler.apexLog('ChatterPostCtrl','createNewChatterFeed');
    
    public String parentId{get;set;}
    public String content{get;set;}
    public map<Id,User> chatterUser{get;set;}
    public String selectedFeed{get;set;}
    public String feedCommentString{get;set;}
    public String deleteFeedId{get;set;}
    public String deleteFeedCommentId{get;set;}
    
    public void createNewChatterFeed() {
        logCreateNewChatterFeed.logMessage('>>inside createNewChatterFeed method ...>> ');
        if(!String.isEmpty(parentId) && !String.isEmpty(content)) {
            FeedItem feeds = new FeedItem();
            
            system.debug('>>>content 1>>> ' + content);
            logCreateNewChatterFeed.logDebug('>>>content 1>>> ' + content);
            String c1 = content.replace('<strong>','<b>');
            String c2 = c1.replace('</strong>','</b>');
            String c3 = c2.replace('<em>','<i>');
            String c4 = c3.replace('</em>','</i>');
            system.debug('>>>content 2>>> ' + c4);
            logCreateNewChatterFeed.logDebug('>>>content 2>>> ' + c4);
            feeds.Body = c4;
            feeds.Type ='TextPost';
            feeds.Visibility = 'AllUsers';
            feeds.ParentId = parentId;
            feeds.IsRichText = true;
            insert feeds;
            logCreateNewChatterFeed.logDebug('' + feeds);
            logCreateNewChatterFeed.saveLogs();
        }
    }
    
    public List<FeedItem> getChatterFeeds() {
    	List<FeedItem> feedItems = new List<FeedItem>();
    	chatterUser = new map<Id,User>();
    	for(FeedItem feedItem :[SELECT ID, CreatedDate, CreatedById, CreatedBy.FirstName, 
    								CreatedBy.LastName, Body, Type,
    								(SELECT CommentBody,InsertedById,InsertedBy.FirstName,InsertedBy.LastName FROM FeedComments),
 								    (SELECT ID, FieldName, OldValue, NewValue FROM FeedTrackedChanges) 
 									FROM FeedItem WHERE ParentId =:parentId AND Type IN ('TextPost','TrackedChange') 
 									ORDER BY CreatedDate DESC]) {
 			feedItems.add(feedItem);							
 			chatterUser.put(feedItem.CreatedById, null);
 			if(feedItem.FeedComments.size()>0) {
 			    for(FeedComment feedsC : feedItem.FeedComments) {
 			        chatterUser.put(feedsC.InsertedById, null);
 			    }
 			}
 		}
 		for(User user : [SELECT Id,SmallPhotoUrl FROM User WHERE ID IN:chatterUser.KeySet()]) {
 			chatterUser.put(user.Id, user);
 		}
 		return feedItems;
    }
    
    public void addFeedComment() {
        if(!String.isEmpty(feedCommentString) && !String.isEmpty(selectedFeed)) {
            FeedComment feedCommentObj = new FeedComment();
            feedCommentObj.CommentBody = feedCommentString;
            feedCommentObj.CommentType = 'TextComment';
            feedCommentObj.FeedItemId = selectedFeed;
            insert feedCommentObj;
            feedCommentString='';
            selectedFeed='';
        }
    }
    
    public void deleteFeed() {
    	if(!String.isEmpty(deleteFeedId)) {
    		delete (new FeedItem(Id=deleteFeedId));
    	}	
    }
    
    public void deleteFeedComment() {
    	if(!String.isEmpty(deleteFeedCommentId)) {
    		delete (new FeedComment(Id=deleteFeedCommentId));
    	}	
    }
}