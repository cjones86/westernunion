/**=====================================================================
 * Name: CMP_MyActionsCtrl
 * Description: Controller class for CMP_MyActions component
 * Created Date: 8th Feb'16
 * Created By: Nikhil Sharma
 =====================================================================*/
public without sharing Class CMP_MyActionsCtrl{
    
    static apexLogHandler.apexLog logRegisterInterest = new apexLogHandler.apexLog('CMP_MyActionsCtrl','RegisterInterest');
    
    public User currentUser {get;set;}
    

	Public boolean selfReg{
		get{
			return Utility.loggedInUser.Contact.Account.RecordType.DeveloperName == 'EDGE_Self_Registered';
		}
	}	
    Public boolean canMakeColdPayments{
        get{
            if(Utility_Security.canMakeStandardColdPayments)
                return true;
            else
                return false;
        }set;}

     Public boolean canSubmitInvoice{
        get{
            if(Utility_Security.canSubmitInvoices)
                return true;
            else
                return false;
        }set;}

    public CMP_MyActionsCtrl(){
        currentUser = Utility.loggedInUser;
    }
    
    @RemoteAction
	public static String RegisterInterest(String Interest, String AccountId){
	    logRegisterInterest.logMessage('>>inside RegisterInterest method ...>> ');
	    
		System.Debug('FeatName'+Interest);
		logRegisterInterest.logDebug('FeatName'+Interest);
		System.Debug('AccId'+AccountId);
		logRegisterInterest.logDebug('AccId'+AccountId);
		Account userAccount = [Select id, EDGE_Make_Payment_Interest__c,EDGE_Make_Payment_Interest_Date__c,EDGE_Manage_Holding_Interest__c,EDGE_Manage_Holding_Interest_Date__c,EDGE_Submit_Invoice_Interest__c,EDGE_Submit_Invoice_Interest_Date__c From Account Where Id = :AccountId Limit 1];
		if(Interest == 'SubmitInvoice' && !userAccount.EDGE_Submit_Invoice_Interest__c){
			userAccount.EDGE_Submit_Invoice_Interest__c = true;
			userAccount.EDGE_Submit_Invoice_Interest_Date__c = System.Now();
			update useraccount;
		}
		else if(Interest == 'MakePayment' && !userAccount.EDGE_Make_Payment_Interest__c){
			userAccount.EDGE_Make_Payment_Interest__c = true;
			userAccount.EDGE_Make_Payment_Interest_Date__c = System.Now();
			update useraccount;
		}
		else if(Interest == 'ManageHolding' && !userAccount.EDGE_Manage_Holding_Interest__c){
			userAccount.EDGE_Manage_Holding_Interest__c = true;
			userAccount.EDGE_Manage_Holding_Interest_Date__c = System.Now();
			update useraccount;
		}
		logRegisterInterest.saveLogs();
		return '';
	}
}