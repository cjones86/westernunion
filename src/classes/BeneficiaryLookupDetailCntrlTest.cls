/**=======================================================================================
 * Name: BeneficiaryLookupDetailCntrlTest
 * Description: Test Class of BeneficiaryLookupDetailCntrl Class
 * Created Date: Apr 29, 2015
 * Created By: Rohit B. (Appirio)
 * 
 * Date Modified                Modified By                 	Description of the update
 * [Date]						[Developer Name]				[Short Description]
 ==========================================================================================*/

@isTest
private class BeneficiaryLookupDetailCntrlTest {
	
	@isTest 
	static void test_method_one() {
        Input_Beneficiary__c ib = new Input_Beneficiary__c(Name = 'Test 1');
        insert ib;

        System.currentPageReference().getParameters().put('id', ib.id);
        BeneficiaryLookupDetailCntrl bldc = new BeneficiaryLookupDetailCntrl();

        Input_Beneficiary__c ib1 = bldc.inputRecord;
        System.assertNotEquals(ib1, Null);
	}
	
}