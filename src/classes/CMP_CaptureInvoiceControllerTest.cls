/**=====================================================================
 * Appirio, Inc
 * Name: CMP_CaptureInvoiceControllerTest
 * Description: Test class for Capture Invoice and related Document upload functionality.
 * Created Date: 08 Feb' 2016
 * Created By: Rohit Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update                                           
 =====================================================================*/
@isTest
private class CMP_CaptureInvoiceControllerTest {

    static testMethod void captureInvoice() {
        test_Utility.createCMPAdministration();
		test_Utility.createCMPAlert();
		test_Utility.currencyISOMapping();
		CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        insert beneAccount;
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact;
        Account beneAccount1 = test_Utility.createAccount(false);
        beneAccount1.RecordTypeId=accountRecordType;
        beneAccount1.ExternalId__c='12345';
        beneAccount1.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        insert beneAccount1;
        Contact newContact1 = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact1;
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
		commUser.timezonesidkey='America/Indiana/Indianapolis';
		commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        commUser.CommunityNickname = commUser.alias;
        insert commUser;
        System.RunAs(commUser){
        	Test.startTest();
        	CMP_CaptureInvoiceController cmpCI = new CMP_CaptureInvoiceController();
        	cmpCI.save();
        	cmpCI.saveNew();
        	cmpCI.showBenePopUp();
        	cmpCI.closeBenePopUp();
        	cmpCI.invoice.Custom_Currency__c  = 'USD';
        	//cmpCI.invoice.Name = 'INVTest-123';
	        cmpCI.invoice.Status__c = 'Pending Approval (Open)';
	        cmpCI.invoice.Initiated_By__c='Seller';
	        cmpCI.invoice.Due_Date__c = Date.today().addDays(30);
	        cmpCI.invoice.Amount__c = 2000;
	        cmpCI.invoice.Account__c = Utility.currentAccount;
	        cmpCI.invoice.Buyer__c = UserInfo.getUserId();
	        cmpCI.invoice.Beneficiary_GP_ID__c = '12345';
	        cmpCI.saveNew();
	        cmpCI.invoice.Custom_Currency__c  = 'USD';
        	//cmpCI.invoice.Name = 'INVTest-123';
	        cmpCI.invoice.Status__c = 'Pending Approval (Open)';
	        cmpCI.invoice.Initiated_By__c='Seller';
	        cmpCI.invoice.Due_Date__c = Date.today().addDays(30);
	        cmpCI.invoice.Amount__c = 2000;
	        cmpCI.invoice.Account__c = Utility.currentAccount;
	        cmpCI.invoice.Buyer__c = UserInfo.getUserId();
	        
	        cmpCI.invoice.Beneficiary_GP_ID__c = '12345';
	        cmpCI.uploadedAttachment.ContentFileName = 'Test.pdf';
	        Blob myBlob = Blob.valueof('Test data for pdf file');	        
	        cmpCI.uploadedAttachment.ContentData = myBlob;
	        //cmpCI.attachmentUpdate();
	        //cmpCI.uploadedAttachment.Name = 'Test1.pdf';
	        //cmpCI.uploadedAttachment.Body = myBlob;
	        //cmpCI.attachmentUpdate();
	        //system.assertEquals(cmpCI.posts.size(),2);
	        //cmpCI.deleteIndex = 1;
	        //cmpCI.delAttachment();
	        cmpCI.save();
        	Test.stopTest();
        }
    }
}