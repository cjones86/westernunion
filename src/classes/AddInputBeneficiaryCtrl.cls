/**=====================================================================
 * Name: AddInputBeneficiaryCtrl
 * Description: Related to AddInputBeneficiary in InvoicedandPaymentsTab Page
 * Created Date: Feb 06, 2015
 * Created By: Nishant Bansal (JDC)
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
public without sharing class AddInputBeneficiaryCtrl {

	public Input_Beneficiary__c inputBene{get;set;}
	public String benefComId{get;set;}
	public String accId {get;set;}
	public AddInputBeneficiaryCtrl(){
		inputBene = new Input_Beneficiary__c();
		accId = ApexPages.currentPage().getParameters().get('accId');
		if(!String.isEmpty(ApexPages.CurrentPage().getParameters().get('beneId'))) {
			String beneId = ApexPages.CurrentPage().getParameters().get('beneId');
			 inputBene = [SELECT Id,Name,Buyer_Supplier_Number__c FROM Input_Beneficiary__c WHERE Id=:beneId];
		}
	}

// To save the input beneficiary record
	public PageReference save(){
		try{
			User userLoggedIn = Utility.loggedInUser;
			if(String.isEmpty(accId)){
        		accId = Utility.currentAccount;
			}
            if(inputBene.Id==null) {
    			Integer count = [SELECT Count() FROM Input_Beneficiary__c WHERE Name =: inputBene.Name AND Account__c =: accId];
    			if (count > 0) {
    				ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CM_Alert_ABeneSameNameError));
    				return null;
    			}
            } else {
                Integer count = [SELECT Count() FROM Input_Beneficiary__c WHERE Name =: inputBene.Name AND Account__c =: accId AND Id!=:inputBene.Id];
    			if (count > 0) {
    				ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CM_Alert_ABeneSameNameError));
    				return null;
    			}
            }
			if(String.isEmpty(inputBene.Name)){
				ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CM_HelpText_Section_MyProfile_PleaseFillOutThisField));
				return null;
			}
			if(inputBene.Id==null) {
				inputBene.Account__c = accId;
			}
			upsert inputBene;
			benefComId = ApexPages.CurrentPage().getParameters().get('benefId') == null ? '' : ApexPages.CurrentPage().getParameters().get('benefId');
			//ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Input Saved :'));
            //pagereference pageref = new pagereference('/apex/InvoicesAndPayments);
            //return pageRef;
		} catch (exception ex) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Unable to save Input Beneficiary. Error Detail :'+ex.getMessage()));
            //return null;
		}
		return null;
	}

// to cancel the creation of save record
	public PageReference cancel(){
		pagereference pageref = new pagereference('/apex/EDGE_InvoicesAndPayments');
        return pageRef;
	}

}