/**=====================================================================
 * Name: NetCashFlowCtrl_test
 * Description: Test class for NetCashFlowCtrl.cls
 * Created Date: Mar 16, 2015
 * Created By: Aashita Sharma 
 =====================================================================*/

@isTest
private class NetCashFlowCtrl_test {
    
    // creating test user
    private static User testUser; 
    static User portalUser;
	private static User commUser;
    private static User commUser1;
    private static Account beneAccount;
    
    // method  to cover NetCashFlowCtrl class
    static testMethod void testNetCashFlowCtrl() {
       
        createTestData();
        
        System.runAs(commUser){
            system.currentPageReference().getParameters().put('currency','INR');
            system.currentPageReference().getParameters().put('month','3');
            
            Test.startTest();
            NetCashFlowCtrl netController = new NetCashFlowCtrl();
            netController.Search();
            netController.getaccountTradedCurrency();
            NetCashFlowCtrl.loadAllTimeData();
            netController.getaccountNetcashCurrency();
            System.assert((netController.wrapperList).size() > 0);
            //System.assertEquals(NetCashFlowCtrl.aggResult.size(), 1);
            Test.stopTest();
        }
    }
     
     // creating test data for coverage
    /*public static void  createTestData(){ 
        testUser = test_Utility.createTestUser(true);
        test_Utility.createWUEdgeSharingAdmin();
        System.runAs(testUser){
            Invoice__c invoice = test_Utility.createInvoice(true);
            Input__c input = test_Utility.createInput(false);
            input.Type__c = 'Forecast Inflow';
            input.Type__c = 'Purchase Order';
            insert input;
        }
    }*/
    
        static void createTestData() {
    	//portalUser = [Select   Id, ContactId, DefaultCurrencyIsoCode, Name, Contact.AccountId, IsPrmSuperUser, CurrencyIsoCode FROM  User WHERE  Id =: UserInfo.getUserId() LIMIT 1];

    	test_Utility.createCMPAdministration();
    	test_Utility.createWUEdgeSharingAdmin();
        CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.CCT_Client_ID__c = '3232112';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount.BillingStreet = 'testStreet';
        beneAccount.BillingCity = 'city';
        beneAccount.BillingState = 'state';
        beneAccount.BillingCountry = 'USA';
        beneAccount.BillingPostalCode = '231231';
        
        Account beneAccount1 = test_Utility.createAccount(false);
        beneAccount1.RecordTypeId=accountRecordType;
        beneAccount1.ExternalId__c='11111145';
        beneAccount.CCT_Client_ID__c = '32321122131';
        beneAccount1.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount1.CMP_Enabled__c = true;
        List<Account> listAccount = new List<Account>{beneAccount, beneAccount1};
        insert listAccount;
        
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        Contact newContact1 = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        List<Contact> listContacts = new List<Contact>{newContact,newContact1};
        insert listContacts;
        
        commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
		commUser.timezonesidkey='America/Indiana/Indianapolis';
		commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        
        commUser1 = test_Utility.createCommUser(newContact1.Id, false);
        commUser1.ProfileId = profileId;
        commUser1.emailencodingkey='UTF-8';
        commUser1.localesidkey='en_US';
		commUser1.timezonesidkey='America/Indiana/Indianapolis';
		commUser1.CMP_Enabled__c=true;
        commUser1.UserName = newContact1.Email+'.cmp1';
        
        list<User> listUsers = new List<User>{commUser,commUser1};
        insert listUsers;
        
     	List<Input__c> inputList = new List<Input__c>();
    	inputList.add(test_Utility.createInput(false));
    	inputList.add(test_Utility.createInput(false));
    	inputList.add(test_Utility.createInput(false));
    	inputList.add(test_Utility.createInput(false));
    	inputList.add(test_Utility.createInput(false));
    	inputList.add(test_Utility.createInput(false));
    	inputList.add(test_Utility.createInput(false));
    	inputList.add(test_Utility.createInput(false));
    	inputList.add(test_Utility.createInput(false));
    	inputList.add(test_Utility.createInput(false));
    	
    	//inputList.get(1).Input_Type__c = 'Forecast Outgoing';
    	inputList.get(1).Parent_account__c = beneAccount.id;
    	inputList.get(1).Input_Type__c  = 'Forecast Inflow';
    	inputList.get(1).Transaction_Date__c = Date.today();
    	inputList.get(1).paid__c = false;
    	
    	inputList.get(2).Input_Type__c = 'Forecast Outflow';
    	inputList.get(2).Parent_account__c = beneAccount.id;
    	inputList.get(2).Transaction_Date__c = Date.today();
    	inputList.get(2).paid__c = false;
    	
    	inputList.get(3).Input_Type__c = 'Option - Other';
    	inputList.get(4).Input_Type__c = 'Forecast Inflow';
    	
    	insert inputList;
    	
    	Invoice__c invoice = test_Utility.createInvoice('GP Invoice - Active', beneAccount.Id, commUser.Id,'Pending Approval (Open)', false);
        invoice.Due_Date__c = date.today();
        invoice.Custom_Currency__c = 'INR';
        insert invoice;
        test_Utility.createForwardContracts(beneAccount.Id, true);
        test_Utility.createHoldingBalance(beneAccount.Id, true);
    }
    
    
}