/**=====================================================================
 * Appirio, Inc
 * Name: Edge_Activity_MyInvoicesController
 * Description: Controller to display user related invoices based on it's type Buyer/Beneficiary.
 * Created Date: 01 Fab' 2016
 * Created By: Rohit Sharma (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 26 Feb 2016          Ranjeet Singh       Removed CMP by Edge and used with new UI component
 =====================================================================*/
public without sharing class Edge_Activity_MyInvoicesController {

  static apexLogHandler.apexLog logShowPay = new apexLogHandler.apexLog('MyInvoicesController','showPay');
  static apexLogHandler.apexLog logResetAllFilter = new apexLogHandler.apexLog('MyInvoicesController','resetAllFilter');

  public Boolean displayPopup {get; set;}
  public String popupURL{get;set;}
  public String selectedInvoice{get;set;}
  public integer allInvoices{get;set;}
  public integer allInvoicesBene{get;set;}
  public transient List<InvoiceUIWrapper> buyerInvoices{get;set;}
  public transient List<InvoiceUIWrapper> beneficiaryInvoices{get;set;}
  public List<SelectOption> statusList{get;set;}
  public String status{get;set;}
  public List<SelectOption> currencyList{get;set;}
  public String currencyString{get;set;}
  public integer dueDate{get;set;}
  private List<String> gPId;
  public decimal invoicePay{get;set;}
  public decimal invoiceReceive{get;set;}
  public Date startDate{get;set;}
  public Boolean createTransactionPermForSP{get;set;}
  public Boolean createTransactionPermForH2H{get;set;}
  public String currentAccount{get;set;}
  public String userDefaultCurrency{get;set;}
  private List<String> splitStatus;

  public String localeDate{
        get{
            Utility utility = new Utility();
            return utility.localeDate;
        }
    }

  public String localeDateRegx{
        get{
            System.debug('Hello'+localeDate);
            String dtStr = localeDate.replace('yyyy', '(\\d{2,4})');
                        System.debug('Hello'+dtStr);

            dtStr = dtStr.replace('mm', '(\\d{1,2})\\');
                        System.debug('Hello'+dtStr);

            dtStr = dtStr.replace('dd', '(\\d{1,2})\\');
                        System.debug('Hello'+dtStr);
            System.debug('Hello'+'/'+dtStr+'( (\\d{1,2}):(\\d{1,2}))? ?(am|pm|AM|PM|Am|Pm)?/');

            return '/'+dtStr+'( (\\d{1,2}):(\\d{1,2}))? ?(am|pm|AM|PM|Am|Pm)?/';
        }
    }

  public Edge_Activity_MyInvoicesController() {
    status = '';
    currencyString = '';
    dueDate = 0;
    //statusList = getInvoiceStatus();
    currencyList =  getInvoiceCurrency();
    currentAccount = Utility.currentAccount;
    userDefaultCurrency = UserInfo.getDefaultCurrency();
    createTransactionPermForSP = Utility_Security.canPayInvoices_SP;
    createTransactionPermForH2H = Utility_Security.canPayInvoices_H2H;
    gPId = new List<String>();
    for(Global_Pay_ID_Management__c gpRecord : [SELECT Global_Pay_ID__c FROM Global_Pay_ID_Management__c WHERE User_Beneficiary__c=:UserInfo.getUserId()]) {
      gPId.add(gpRecord.Global_Pay_ID__c);
    }
    refreshInvoices();


  }

    public List<String> getInvoiceStatus() {
         //List<SelectOption> options = new List<SelectOption>();
         List<String> options = new List<String>();
         Map<String,String> optionsMap = new Map<String,String>();

         //options.add(new SelectOption('',Label.CM_EDGE_Select));
         Schema.DescribeFieldResult fieldResult = Invoice__c.Status__c.getDescribe();
         List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
         system.debug('###'+Utility_Security.canCreateTransaction);
         if(Utility_Security.canCreateTransaction){
             for( Schema.PicklistEntry f : ple) {
                 if(f.getValue()!='Pending Approval (Open)' && f.getValue()!='Approved' && f.getvalue() != 'Paid' && f.getvalue() !='Void' && f.getValue() != 'Full Payment Submitted'){
                  //options.add(new SelectOption(f.getValue(), f.getLabel()));
                  system.debug('>>>value'+f.getLabel());
                  //optionsMap.put(f.getValue(), f.getLabel());
                  options.add(f.getLabel());
                 }
             }

             system.debug('>>>List'+options);
         }
         else{
             for( Schema.PicklistEntry f : ple) {
                 if(f.getValue()=='Pending Approval (Open)' || f.getValue()=='Approved' || f.getvalue() == 'Paid' || f.getvalue() == 'Void'){
                  //options.add(new SelectOption(f.getValue(), f.getLabel()));
                  //optionsMap.put(f.getValue(), f.getLabel());
                  options.add(f.getLabel());
                 }
             }
         }
         //return optionsMap;
         return options;
    }


  public List<SelectOption> getInvoiceCurrency() {
          String strQuery = 'Select count(Id), Custom_Currency__c FROM Invoice__c' ;
          String currentAccId = Utility.currentAccount;
          strQuery +=' WHERE Account__c=\''+currentAccId+'\' or ((Seller_Account__c=\''+currentAccId+'\' ' + 'OR (Supplier__c != null AND Supplier__r.Supplier__c=\''+currentAccId+'\')))  group by  Custom_Currency__c';
      List<SelectOption>payCurrencyOptions = new List<SelectOption>();
        set<String> currencySet = new set<String>();
        payCurrencyOptions.add(new SelectOption('',Label.CM_EDGE_Select));
        system.debug('****strQuery>>'+strQuery);
        for(AggregateResult Invoice: database.query(strQuery)){
            String currencyCode = (String) Invoice.get('Custom_Currency__c');
            if(!String.isBlank(currencyCode) && !currencySet.Contains(currencyCode)) {
                currencySet.add(currencyCode);
                  payCurrencyOptions.add(new SelectOption(currencyCode,currencyCode));
            }
        }
        return payCurrencyOptions;
  }

    public void closePopup() {
      displayPopup = false;
    }

    public PageReference showPay() {
      logShowPay.logMessage('>>inside showPay method ...>> ');
      popupURL = CMP_Administration__c.getInstance().Base_Invite_URL__c+'/apex/GPAppTest?app=payBeneficiary&id='+selectedInvoice;
      displayPopup = true;
      logShowPay.logDebug(popupURL);
      logShowPay.saveLogs();
      return null;
    }

  private String generatefilterQuery() {

    String query = '';
    if(!String.isBlank(status)) {
      //query = 'Status__c=\''+status+'\'';
      if(status.contains(',')){
         splitStatus = status.split(',');
          //query = 'Status__c IN: \''+splitStatus+'\'';
         query = 'Status__c IN: splitStatus ';
      }else{
         query = 'Status__c= \''+status+'\'';
      }


    }
    if(!String.isBlank(currencyString)) {
      query = query+(String.isBlank(query)?'':' AND ')+'Custom_Currency__c=\''+currencyString+'\'';
    }

    if(dueDate>0 && duedate <= 60) {
      startDate = Date.today().addDays(dueDate);
      /*if(dueDate>60) {
        startDate = Date.today().addDays(60);
      }*/
     // Issue fix I-228749
     //query = query+(String.isBlank(query)?'':' AND ')+'Due_Date__c'+(dueDate>60 ? '>:startDate' :'>=TODAY AND Due_Date__c<=:startDate');

          query = query+(String.isBlank(query)?'':' AND ')+'Due_Date__c'+(dueDate>60 ? '>:startDate' :'<=:startDate');
    }
    return query;
  }

  public void resetAllFilter() {
    logResetAllFilter.logMessage('>>inside resetAllFilter method ...>> ');
    status = '';
    currencyString = '';
    dueDate = 0;
    refreshInvoices();
    logResetAllFilter.saveLogs();
  }

  public void refreshInvoices() {
    System.debug('****dueDate:: '+  dueDate + '  :: >> '+ currencyString+' .. '+status);
    String filterQuery = generatefilterQuery();
    if(buyerInvoices!=null) {
      buyerInvoices.clear();
    }
    if(beneficiaryInvoices!=null) {
      beneficiaryInvoices.clear();
    }
    buyerInvoices = getPayableInvoice(filterQuery);
    beneficiaryInvoices = getReceivables(filterQuery);
    String selectedCurrency = !String.isEmpty(currencyString) ? currencyString : Utility.userCurrency;
    Decimal payableAmt=0.0;
    invoicePay = 0.0;
    for(InvoiceUIWrapper inv: buyerInvoices){
			if(inv!=null){
      payableAmt = Utility.getSettlementCurrencyAmount(inv.Invoice.Amount__c,inv.Invoice.Custom_Currency__c,selectedCurrency);
      invoicePay += payableAmt;
      System.debug('invAmt:: '+  inv.Invoice.Balance__c + '  >> conversion:: >> '+ payableAmt);
			}
    }

    payableAmt=0.0;
    invoiceReceive = 0.0;
    for(InvoiceUIWrapper inv: beneficiaryInvoices){
			if(inv!=null){
      payableAmt = Utility.getSettlementCurrencyAmount(inv.Invoice.Balance__c,inv.Invoice.Custom_Currency__c,selectedCurrency);
      invoiceReceive += payableAmt;
      System.debug('invAmt:: '+  inv.Invoice.Balance__c + '  >> conversion:: >> '+ payableAmt);
			}
    }
  }

  public List<InvoiceUIWrapper> getPayableInvoice(String filterQuery) {

    String userId = UserInfo.getUserId();
    String currentAccId = Utility.currentAccount;
    invoicePay = 0.0;
    List<InvoiceUIWrapper> latestInvoices = new List<InvoiceUIWrapper>();
    InvoiceUIWrapper invWrapper;
    for(Invoice__c Invoice: database.query('SELECT Name,Account__c,Initiated_By__c,Balance__c,Account__r.Name,Reference_Number__c,RecordTypeId,RecordType.Name,CreatedBy.Contact.Account.Name, Custom_Currency__c,Delivery_Method__c,Type__c, TypeTrans__c,Due_Date__c,Amount__c,Status__c,Beneficiary_Name__c,Beneficiary_GP_Name__c,CreatedBy.Name,Id,Buyer__r.Contact.AccountId,Beneficiary_GP_ID__c,Invoice_Number__c,Supplier__c FROM Invoice__c WHERE Account__c=:currentAccId'+(String.isBlank(filterQuery)?'':' AND ')+filterQuery+' ORDER BY Due_Date__c DESC')){
      if(Invoice.RecordTypeId != null && !('Void'.equalsIgnoreCase(Invoice.Status__c) || 'Full Payment Submitted'.equalsIgnoreCase(Invoice.Status__c) || 'Paid'.equalsIgnoreCase(Invoice.Status__c)))
      
      invWrapper  = new InvoiceUIWrapper(Invoice);
      //encryption
      //system.debug('invoice Id >>>'+invWrapper.encodedInvoiceId);
      //String encodedVal = EncryptionManager.doEncrypt(Invoice.Id);
      //system.debug('encoded val >>>'+encodedVal);
      //invWrapper.encodedInvoiceId = encodedVal;
      latestInvoices.add(invWrapper);
    }
    allInvoices = database.countQuery('select count() FROM Invoice__c WHERE Buyer__c=:userId'+(String.isBlank(filterQuery)?'':' AND ')+filterQuery);
    return latestInvoices;
  }

  public List<InvoiceUIWrapper> getReceivables(String filterQuery) {
    invoiceReceive = 0.0;
    String userId = UserInfo.getUserId();
    String currentAccId = Utility.currentAccount;
    String accountString = '(Seller_Account__c=\''+currentAccId+'\' ' + 'OR (Supplier__c != null AND Supplier__r.Supplier__c=\''+currentAccId+'\'))';
    List<InvoiceUIWrapper> latestInvoices = new List<InvoiceUIWrapper>();
    InvoiceUIWrapper invWrapper;

    for(Invoice__c Invoice: database.query('SELECT Name,Initiated_By__c,Account__c,Balance__c,Reference_Number__c,RecordTypeId,RecordType.Name,Custom_Currency__c,Delivery_Method__c,Type__c, TypeTrans__c, Due_Date__c,Amount__c,Status__c,Beneficiary_Name__c,Beneficiary_GP_Name__c,Buyer__c,Buyer__r.Name,CreatedBy.Name,Id, Invoice_Number__c, Account__r.Name, Supplier__c FROM Invoice__c Where '+ accountString +(String.isBlank(filterQuery)?'':' AND ')+filterQuery+' ORDER BY Due_Date__c DESC')){
       if(Invoice.RecordTypeId != null && !('Void'.equalsIgnoreCase(Invoice.Status__c) || 'Full Payment Submitted'.equalsIgnoreCase(Invoice.Status__c) || 'Paid'.equalsIgnoreCase(Invoice.Status__c))){

              invWrapper = new InvoiceUIWrapper(Invoice);
          if(Invoice.TypeTrans__c.equalsIgnoreCase(Label.CM_SectionHeading_InvoiceDetail_Payables)){
             // System.assert(Boolean condition, Object opt_msg)
              invWrapper.getCategoryType = Label.CM_EDGE_Receivables_Others;
          }

          latestInvoices.add(invWrapper);
        }
    }
      /*NS: 15th Feb - Now calculating invoiceReceive in refreshInvoices method with different logic
      for(AggregateResult groupedResults : database.query('SELECT SUM(Amount__c)amount FROM Invoice__c WHERE Beneficiary_GP_ID__c IN :gPId'+(String.isBlank(filterQuery)?'':' AND ')+filterQuery)) {
        invoiceReceive = (decimal)groupedResults.get('amount');
        break;
      }
      */
    return latestInvoices;
  }
}