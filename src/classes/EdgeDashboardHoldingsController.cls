public with sharing class EdgeDashboardHoldingsController {

    Public Map<String,Decimal> mapCurrAndAvlBalance{get;set;}
    Public Map<String,Decimal> mapCurrAndBookedBalance{get;set;}
    Public List<EdgeGpJsonUtils.HoldingBalancesResponseWrpr> listResponse {get;set;}
    Public Map<String,Global_Pay_H2H_Currencies__c> mapGPCurrenciesAndColor{get;set;}
    Public Map<String,String> mapCurrencyAndColor{get;set;}
    Public Map<String,String> mapCurrencyAndFlag{get;set;}
    Public Boolean isError{get;set;}
    Public String errMsg{get;set;}

    public Boolean canShowHoldingBalance{
        get{
            return Utility_Security.canShowHoldingBalance;
        }
    }

    Public EdgeDashboardHoldingsController(){
        listResponse = new List<EdgeGpJsonUtils.HoldingBalancesResponseWrpr>();
        String customerId = Utility.cctClientID;
        String calloutResponseResult = '';
        String calloutResponseStatus = '';
        isError = false;
        errMsg = '';
        mapCurrencyAndFlag = new Map<String,String>();
        mapCurrencyAndColor = new Map<String,String>();
        mapGPCurrenciesAndColor = Global_Pay_H2H_Currencies__c.getAll();

      //  customerId = '617217';

        if(String.isBlank(customerId)){
            //isError = true;
            //errMsg = Label.CM_Alert_WUBSDisabled;
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.CM_Alert_WUBSDisabled));
        }

        try{
            EdgeGpTransctionAPI.isMockupResponse = true;
            EdgeGpTransctionAPI apiCall = new EdgeGpTransctionAPI();
            
            System.debug('>>>api call>>> '+ apiCall.getHoldingBalances(customerId));
            if(apiCall.getHoldingBalances(customerId)){
                calloutResponseResult = apiCall.calloutResponseResult;
                calloutResponseStatus = apiCall.calloutResponseStatus;

                System.debug('>> calloutResponseResult >> '+ calloutResponseResult);
                System.debug('>> calloutResponseStatus >> '+ calloutResponseStatus);

                if(calloutResponseStatus.containsIgnoreCase('200: Detail : OK')){
                    if(String.isNotBlank(calloutResponseResult)){
                        EdgeGpJsonUtils jsonUtil = new EdgeGpJsonUtils();
                        listResponse = jsonUtil.getHoldingBalances(calloutResponseResult);
                    }
                }
                else{
                    isError = true;
                    errMsg = Label.CM_Alert_WUBSDisabled;
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.CM_Alert_WUBSDisabled));
                }

                System.debug('>> listResponse >> '+ listResponse);
            }

            for(EdgeGpJsonUtils.HoldingBalancesResponseWrpr res : listResponse){
                if(mapGPCurrenciesAndColor.containsKey(res.Currency1)){
                    String flagIcon = mapGPCurrenciesAndColor.get(res.Currency1).Flag_Icon__c;
                    String borderColor = mapGPCurrenciesAndColor.get(res.Currency1).Holding_Currency_Color__c;
                    mapCurrencyAndFlag.put(res.Currency1,flagIcon);
                    mapCurrencyAndColor.put(res.Currency1,borderColor);
                }
                else{
                    System.debug('>> 3 >> ');
                    mapCurrencyAndFlag.put(res.Currency1,'UNK.ico');
                    mapCurrencyAndColor.put(res.Currency1,'#828282');
                }
            }
       }
        catch(Exception Ex){
            isError = true;
            errMsg = Label.CM_Alert_WUBSDisabled;
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.CM_Alert_WUBSDisabled));
            //ApexPages.addMessages(Ex);
        }

    }
}