/**=====================================================================
 * Name: EDGE_MarketplaceComponentCtrl
 * Description: The page should show a list of all companies that have a Marketplace profile created
 * Created Date: May 10, 2016
 * Created By: Priyanka Kumar
 * Task      : T-501551
 * Modified By : Priyanka Kumar  26th May'16  T-504984
 * Modified By : Priyanka Kumar  6th June'16  T-508920
 =====================================================================*/

public without sharing class EDGE_MarketplaceComponentCtrl {
    
    static apexLogHandler.apexLog logInviteUser = new apexLogHandler.apexLog('MarketplaceComponentCtrl','inviteUser');
    
    public string searchTxt{get;set;}
    public Set<Id> accSet = new Set<Id>();
    String searchQuery;
    public List<Marketplace_profile__c> profileList {get;set;}
    public boolean isNetworked {get;set;}
    //public list<AccountWrapper> listAccounts{get;set;}
    public list<ProfileWrapper> listProfiles{get;set;}
    public string accountId{get;set;}
    public boolean isShowSuccessMsg{get;set;}
    public boolean isShowErrorMsg{get;set;}
    
    //T-508920
    public String companyName{get;set;}
    public String goodsBuy{get;set;}
    public String goodsSell{get;set;}
    public String source{get;set;}
    public String supply{get;set;}
    public String industry{get;set;}
    public boolean H2HCapable{get;set;}
    public boolean H2HCapableCheck{get;set;}
    public String networkStatus{get;set;}
    public List<List<String>> sourceSIC {get;set;}
    public string jsonsourceSIC{get;set;}
    public List<SICWrapper> LetsTry{get;set;}
    
    public List<SelectOption> IndustryCodeWithName{get;set;}
    public  Map<String , String> mapExtendedValue{get;set;}
    

    
    private Map<String,String> getCountryValues(){
        Map<String,String> options = new Map<String,String>();            
         Schema.DescribeFieldResult fieldResult =
	     Marketplace_profile__c.Where_do_they_source_from__c.getDescribe();
	     List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();	          
	     for( Schema.PicklistEntry f : ple)	     {
	        options.put(f.getLabel(), f.getValue());
	     }       
	     return options;
    }

  
  public Map<String,String> getIndustryValues(){
    Map<String,String> options = new Map<String,String>();
          
     Schema.DescribeFieldResult fieldResult =
     Marketplace_profile__c.What_Industry_do_they_belong_to__c.getDescribe();
     List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
          
     for( Schema.PicklistEntry f : ple)
     {
        options.put(f.getLabel(), f.getValue());
     }       
     return options;
  }
    
    public EDGE_MarketplaceComponentCtrl(){
        LetsTry = new List<SICWrapper>();
        searchTxt = '';
        isNetworked = false;
        isShowSuccessMsg = false;
        isShowErrorMsg = false;
        H2HCapableCheck = true;
        //fetchMarketPlaceProfiles();
        fetchRecords();
        Integer x = 0;
        sourceSIC = new List<List<String>>();
        List<String> imList = new  List<String>();
        IndustryCodeWithName = new List<SelectOption>();
        mapExtendedValue = new Map<String , String>();
        
        

        /*
        for(CM_EDGE_SIC_CODE__mdt cm : [Select IndustryType__c,SIC_Code__c from CM_EDGE_SIC_CODE__mdt]){
             IndustryCodeWithName.add(new SelectOption(cm.SIC_Code__c,cm.IndustryType__c ));
             mapExtendedValue.put(cm.IndustryType__c,cm.SIC_Code__c );
        }*/
        
        Schema.DescribeFieldResult fieldResult = Marketplace_profile__c.Industry_Sic_Code__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        System.debug(ple);
        for( Schema.PicklistEntry f : ple){
                    if(f.getLabel() !=  'N/A')
            IndustryCodeWithName.add(new SelectOption(f.getValue(), f.getLabel()));
            mapExtendedValue.put(f.getLabel(),f.getValue() );
        }     
        
        

        
    }
    
    public void fetchRecords(){
        
            Id currentAccount = Utility.currentAccount;     
            searchQuery  = 'Select Id,Name,Account__c,Account__r.Id,Account__r.Name,Account__r.Description,Account__r.Industry,Account__r.CMP_Holding_Enabled__c,';
            searchQuery += 'Account__r.BillingStreet,Account__r.BillingState,Account__r.BillingCountry,Account__r.BillingPostalCode,Account__r.BillingCity,';
            searchQuery += 'Account__r.Account_Status_GBP__c FROM Marketplace_profile__c'; 
            searchQuery += ' where Account__c != null AND Account__c != :currentAccount  And Account__r.WU_EDGE_Flag__c!=true';
            
            Map<String,String> mapCountryLabelAndValue = getCountryValues();
             
            if(!String.isBlank(companyName)){
                searchQuery += ' AND Account__r.Name like \'%'+companyName+'%\'';
            }
            
            if(!String.isBlank(goodsBuy)){
              searchQuery += ' AND Goods_Services_Buy_Visibility__c = true AND What_goods_services_do_they_Buy__c like \'%'+goodsBuy+'%\'';
            }
            
            if(!String.isBlank(goodsSell)){
              searchQuery += ' AND Goods_Services_Sell_Visibility__c = true AND What_goods_services_do_they_Sell__c like \'%'+goodsSell+'%\'';
            }
            
            if(!String.isBlank(source)){
              String srcFilter = source;
              if(mapCountryLabelAndValue.containsKey(source))
                srcFilter = mapCountryLabelAndValue.get(source);
              searchQuery += ' AND Source_From_Visibility__c = true AND Where_do_they_source_from__c INCLUDES( \''+srcFilter+'\')';
            }
            
            if(!String.isBlank(supply)){
              String supFilter = supply;
              if(mapCountryLabelAndValue.containsKey(supply))
                supFilter = mapCountryLabelAndValue.get(supply);
              searchQuery += ' AND What_Supplied_Visibility__c = true AND Where_do_they_supply_to__c INCLUDES( \''+supFilter+'\')';
            }
            
            if(!String.isBlank(industry) ){
                if(!(industry.equalsIgnoreCase('None'))){
                    System.debug('Hello'  + industry);
                    searchQuery += ' AND Which_Industry_Visibility__c = true AND Industry_Sic_Code__c = \''+industry+'\'';
                }
            }
            
            if(H2HCapable != null){
              searchQuery += ' AND Account__r.CMP_Holding_Enabled__c = '+H2HCapable;
            }
            
            system.debug('searchQuery >>'+searchQuery);
            profileList = Database.query(searchQuery);
            system.debug('profileList >>'+profileList);
            fetchNetworkStatus();  
            if(networkStatus != null)
            {
                fetchNetworkStatusFilterRecords();
            }
    }
    
    /*public void fetchMarketPlaceProfiles(){
        Id currentAccount = Utility.currentAccount;
        if(String.isBlank(searchTxt)){
            
            searchQuery  = 'Select Id,Name,Account__c,Account__r.Id,Account__r.Name,Account__r.Description,Account__r.Industry,';
            searchQuery += 'Account__r.Account_Status_GBP__c FROM Marketplace_profile__c'; 
            searchQuery += ' where Account__c != null AND Account__c != :currentAccount LIMIT 1000';
            
        }else{
            searchQuery  = 'Select Id,Name,Account__c,Account__r.Id,Account__r.Name,Account__r.Description,Account__r.Industry,';
            searchQuery += 'Account__r.Account_Status_GBP__c FROM Marketplace_profile__c'; 
            searchQuery += ' where Account__c != null AND Account__c != :currentAccount';
            searchQuery += ' AND (Account__r.Name like \'%'+searchTxt+'%\'';
            searchQuery += ' OR Account__r.BillingStreet like \'%'+searchTxt+'%\'';
            searchQuery += ' OR Account__r.BillingState like \'%'+searchTxt+'%\'';
            searchQuery += ' OR Account__r.BillingPostalCode like \'%'+searchTxt+'%\'';
            searchQuery += ' OR Account__r.BillingCity like \'%'+searchTxt+'%\'';
            searchQuery += ' OR Account__r.BillingCountry like \'%'+searchTxt+'%\'';
            searchQuery += ' OR (Goods_Services_Buy_Visibility__c = true AND What_goods_services_do_they_Buy__c like \'%'+searchTxt+'%\')';
            searchQuery += ' OR (Goods_Services_Sell_Visibility__c = true AND What_goods_services_do_they_Sell__c like \'%'+searchTxt+'%\')';
            searchQuery += ' OR (Source_From_Visibility__c = true AND Where_do_they_source_from__c INCLUDES( \'%'+searchTxt+'%\'))';
            searchQuery += ' OR (What_Supplied_Visibility__c = true AND Where_do_they_supply_to__c INCLUDES( \'%'+searchTxt+'%\'))';
            searchQuery += ' OR (Which_Industry_Visibility__c = true AND What_Industry_do_they_belong_to__c like \'%'+searchTxt+'%\')) LIMIT 1000';
        }
 
        system.debug('searchQuery >>'+searchQuery);
        profileList = Database.query(searchQuery);
        system.debug('profileList >>'+profileList);
        fetchNetworkStatus(); 
        system.debug('listProfiles >>'+listProfiles);  
    }*/
    
 
    public void fetchNetworkStatusFilterRecords(){
        System.debug(Label.Edge_All + 'Hello '+ networkStatus);
        //fetchRecords();
        set<Id> networkedAccountIds = new set<Id>(); 
        List<Network__c> networkList = new List<Network__c>();
        for(Marketplace_profile__c prof : profileList){
             accSet.add(prof.Account__c);
        }
        List<Account> accList = [Select Id,Name,Description,Industry,Account_Status_GBP__c FROM Account Where Id IN :accSet];    
             
        for(Network__c net : [Select Id, Name, Account_Invitee__c,Account_Inviter__c,Status__c
                                From Network__c
                                Where ((Account_Invitee__c = :Utility.currentAccount And Account_Inviter__c IN :accSet) OR
                                (Account_Inviter__c = :Utility.currentAccount And Account_Invitee__c IN :accSet)) /*AND Status__c = '3 - Active (Accepted)'*/]){
             if(net.Account_Invitee__c == Utility.currentAccount)
                networkedAccountIds.add(net.Account_Inviter__c);
             if(net.Account_Inviter__c == Utility.currentAccount)
                networkedAccountIds.add(net.Account_Invitee__c);
             networkList.add(net);   
        }
        listProfiles = new List<ProfileWrapper>();
        if(networkStatus == Label.Edge_Connected){
                for(Marketplace_profile__c prof : profileList){
                    if(networkedAccountIds.contains(prof.Account__r.Id))
                        listProfiles.add(new ProfileWrapper(prof,true));
                }
        }else if(networkStatus == Label.Edge_Not_Connected){
              for(Marketplace_profile__c prof : profileList){
                if(!networkedAccountIds.contains(prof.Account__r.Id))
                    listProfiles.add(new ProfileWrapper(prof,false));
            }
        }else if(networkStatus == Label.Edge_All){
              for(Marketplace_profile__c prof : profileList){
                if(networkedAccountIds.contains(prof.Account__r.Id))
                    listProfiles.add(new ProfileWrapper(prof,true));
                else
                    listProfiles.add(new ProfileWrapper(prof,false));
            }
        }
    }
    
    private void fetchNetworkStatus(){
        set<Id> networkedAccountIds = new set<Id>(); 
        List<Network__c> networkList = new List<Network__c>();
        for(Marketplace_profile__c prof : profileList){
             accSet.add(prof.Account__c);
        }
        List<Account> accList = [Select Id,Name,Description,Industry,Account_Status_GBP__c FROM Account Where Id IN :accSet];    
        for(Network__c net : [Select Id, Name, Account_Invitee__c,Account_Inviter__c,Status__c
                                From Network__c
                                Where ((Account_Invitee__c = :Utility.currentAccount And Account_Inviter__c IN :accSet) OR
                                (Account_Inviter__c = :Utility.currentAccount And Account_Invitee__c IN :accSet))  /*AND Status__c = '3 - Active (Accepted)'*/]){
             if(net.Account_Invitee__c == Utility.currentAccount)
                networkedAccountIds.add(net.Account_Inviter__c);
             if(net.Account_Inviter__c == Utility.currentAccount)
                networkedAccountIds.add(net.Account_Invitee__c);
             networkList.add(net);   
        }
        
        listProfiles = new List<ProfileWrapper>();
        for(Marketplace_profile__c prof : profileList){
            if(networkedAccountIds.contains(prof.Account__r.Id))
                listProfiles.add(new ProfileWrapper(prof,true));
            else
                listProfiles.add(new ProfileWrapper(prof,false));
        }
        isNetworked = false;
        system.debug('listProfiles inside>>'+listProfiles);  
    }
    
    public void inviteUserTONetwork() {
        logInviteUser.logMessage('>>inside inviteUserTONetwork method ...>> ');
        Id accId = ApexPages.CurrentPage().getParameters().get('accId');
        Id cntId;
        for(User us: [Select Id, ContactId From User Where IsActive = True And IsPortalEnabled = True
                                And ContactId!=null And Contact.AccountId!=null And Contact.AccountId=:accountId Limit 1]){
            cntId = us.ContactId;
        }
        
        Network__c relatedNetwork = null;
        for(Network__c net: [Select Id From Network__c Where Account_Invitee__c=:accountId 
                                            And Account_Inviter__c=:Utility.currentAccount
                                            And Status__c != '3 - Active (Accepted)']){
            relatedNetwork = net;
        }
        if(accountId != null && cntId != null && relatedNetwork == null){
            Network__c newNetwork = new Network__c(Inviter_User__c=UserInfo.getUserId(), 
                                              Invitee_Contact__c = cntId,
                                              Status__c='1 - Pending (Sent)',Status_DateTime__c = system.now(),
                                              Account_Invitee__c=accountId,
                                              Account_Inviter__c=Utility.currentAccount);
            try{
                //System.assert(false, 'helo');
                insert newNetwork;
                logInviteUser.logDebug('newNetwork inserted ' + newNetwork);
                isShowSuccessMsg = true;
                isNetworked = true;
                fetchNetworkStatus();
            }Catch(exception ex){
                logInviteUser.logDebug('Exception ' + ex.getMessage());
                isShowErrorMsg = true;
            }
        }else if(accountId != null && cntId != null && relatedNetwork != null){
          relatedNetwork.Status__c = '0 - Resend';
          update relatedNetwork;
          logInviteUser.logDebug('relatedNetwork updated ' + relatedNetwork);
          isShowSuccessMsg = true;
          fetchNetworkStatus();
          isNetworked = true;
        }else{
            isShowErrorMsg = true;
            fetchNetworkStatus();
            isNetworked = true;
        }
        logInviteUser.saveLogs();
    }
    
    public class ProfileWrapper {
        public Marketplace_profile__c prof{get;set;}
        public Boolean isNetworked{get;set;}
        public Boolean isWUVerified{get;set;}
        public String address{get;set;}
        public String netId{get;set;}
        public String encodedAccountId{get;set;}
                
        public ProfileWrapper(Marketplace_profile__c prof, Boolean isNetworked){
            this.prof= prof;
            this.isNetworked = isNetworked;
            if(String.isNotBlank(prof.account__r.Account_Status_GBP__c) && 
                (prof.account__r.Account_Status_GBP__c.equalsIgnoreCase('Active') || prof.account__r.Account_Status_GBP__c.equalsIgnoreCase('Yet to Trade')))
                this.isWUVerified = true;
            else
                this.isWUVerified = false;
                
           this.address = getFullAddress(prof.account__r.BillingStreet,prof.account__r.BillingCity,prof.account__r.BillingState,prof.account__r.BillingCountry,prof.account__r.BillingPostalCode);     
           this.encodedAccountId = Utility.doEncryption(prof.account__r.id);
           if(encodedAccountId.contains('+')){
              encodedAccountId = encodedAccountId.replace('+','%2B');
           }
        }
        
       public string getFullAddress(String addline1,String city,String state,String country, String postalCode){
            String address = '';
            if(String.isNotBlank(addline1))
             address = addline1 + ', ';
            if(String.isNotBlank(city))
             address += city + ', ';
            if(String.isNotBlank(state))
             address += state + ', ';
            if(String.isNotBlank(country))
             address += country + ', ';
            if(String.isNotBlank(postalCode))
             address += postalCode;
            return address;
        }
    }
    
    public void searchProfile(){ 
        //fetchMarketPlaceProfiles();
    } 
    public void refresh(){
        System.debug('inside debug');
        companyName = null;
        goodsBuy = null;
        goodsSell = null;
        source = null;
        supply = null;
        industry = 'None';
        H2HCapable = null;
        networkStatus = null;
         H2HCapableCheck = true;
       // industry = null;
        fetchRecords();
    }
    public class SICWrapper{
        public String label {get;set;}
        public String value {get;set;}
        public SICWrapper(String label,String value){
            this.label  = label;
            this.value  = value;
        }
    }

}