/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_MakePaymentStep2Ctrl_Test
 * Description: Test class for EDGE_MakePaymentStep2Ctrl
 * Created Date: 17 May' 2016
 * Created By: Nikhil Sharma (Appirio)
 =====================================================================*/
 
@isTest
private class EDGE_MakePaymentStep2Ctrl_Test {

	private static testMethod void test() {
        test_Utility.createCMPAdministration();
		test_Utility.createCMPAlert();
		CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.CCT_Client_Id__c = '312321112312';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        insert beneAccount;
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact;
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
				commUser.timezonesidkey='America/Indiana/Indianapolis';
				commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        commUser.CommunityNickname = commUser.alias;
        insert commUser;
        
        Supplier__c sup = new Supplier__c(Buyer__c=beneAccount.Id,Address_line_1__c='test1',Address_Line_2__c='test2',
                                City__c='test',State_Province__c='test',Country__c='AND',Post_Code__c='123',Supplier__c=beneAccount.Id);
        insert sup;
   
        
        Global_Pay_ID_Management__c gPIM = new Global_Pay_ID_Management__c(Global_Pay_ID__c='12345',
                                                                               User_Beneficiary__c=commUser.Id,
                                                                               Creation_Type__c='Automatic Email Match');
           insert gPIM;
        
        System.runAs(commUser){
            //Test For H2H payment
            ApexPages.currentPage().getParameters().put('deliveryMethod','H2H');
            ApexPages.currentPage().getParameters().put('SuppAccount',sup.Id);
            ApexPages.currentPage().getParameters().put('isModal','true');
            EDGE_MakePaymentStep2Ctrl ctrl = new EDGE_MakePaymentStep2Ctrl();
            ctrl.invWrp.inv.Amount__c = 213213;
            ctrl.invWrp.inv.Custom_Currency__c = 'USD';
            ctrl.invWrp.inv.Beneficiary_GP_Email_Address__c = 'test@test.com';
            ctrl.invWrp.inv.Invite_to_my_network__c = true;
            
            ctrl.payInvoice();
            ctrl.payH2HInvoice();
            ctrl.nextToSelectBene();
            
            List<Invoice__c> listH2HInvoice = [Select Id From Invoice__c Where Beneficiary_GP_ID__c = '312321112312'];
            //System.assertEquals(1,listH2HInvoice.size());
            
            //Test For Standard payment
            ApexPages.currentPage().getParameters().put('deliveryMethod','StdPayment');
            ApexPages.currentPage().getParameters().put('SuppAccount',sup.Id);
            EDGE_MakePaymentStep2Ctrl ctrl1 = new EDGE_MakePaymentStep2Ctrl();
            ctrl1.invWrp.inv.Amount__c = 213213;
            ctrl1.invWrp.inv.Custom_Currency__c = 'USD';
            ctrl1.invWrp.inv.Reference_Number__c = '12345';
            ctrl1.invWrp.inv.Beneficiary_GP_Email_Address__c = 'test@test.com';               
            ctrl.invWrp.inv.Invite_to_my_network__c = true;

            ctrl1.payInvoice();
            List<Invoice__c> listSPInvoice = [Select Id From Invoice__c Where Beneficiary_GP_ID__c = '12345'];
            
            
           
                
        Invoice__c submitInvoice = new Invoice__c(RecordTypeId=Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('GP Invoice - Active').getRecordTypeId());
       submitInvoice.Status__c = 'Invoice Submitted';
       submitInvoice.Initiated_By__c='Seller';
       submitInvoice.Account__c = beneAccount.Id;
       submitInvoice.Buyer__c = commUser.Id;
       submitInvoice.Beneficiary_GP_ID__c = '12345';
       insert submitInvoice;
                  ApexPages.currentPage().getParameters().put('invoiceId',submitInvoice.id);

       
           gPIM = new Global_Pay_ID_Management__c(Global_Pay_ID__c='12345',
                                                                               User_Beneficiary__c=commUser.Id,
                                                                               Source_Invoice_ID__c = submitInvoice.id,
                                                                               Creation_Type__c='Automatic Email Match');
       
                  ctrl1 = new EDGE_MakePaymentStep2Ctrl();
                  ctrl1.beneName =  'TEST';
                  ctrl1.displayPopup = true;
                  ctrl1.popupURL = 'Test';


           

            //System.assertEquals(1,listH2HInvoice.size());
        }
	}
	
	private static testMethod void test2(){
		EDGE_MakePaymentStep2Ctrl ctrl1 = new EDGE_MakePaymentStep2Ctrl();
		ctrl1.payH2HInvoice();
		ctrl1.nextToSelectBene();
		
	}

}