public without sharing class EDGE_Network_Invite_UsersCtrl{

    public String beneEmail{get;set;}
    public boolean isUserExists{get;set;}
    public boolean isUserAlreadyExists{get;set;}
    public Set<Id> userContactId;
    
    public PageReference searchUser() {
        isUserExists = false;
        isUserAlreadyExists = false;
        userContactId = new Set<Id>();
        Set<Id> existingcontact = new Set<Id>();
        for(Network__c network : [SELECT Invitee_Contact__c FROM Network__c WHERE Inviter_User__c=:UserInfo.getUserId()]) {
            existingcontact.add(network.Invitee_Contact__c);
        }
        String searchUsers =  existingcontact.size()>0 ? 'AND ContactId NOT IN:existingcontact':'';
        for(User user : Database.query('SELECT Id,ContactId FROM User WHERE Email=:beneEmail AND IsActive=true AND CMP_Enabled__c=true Limit 1')) {
            if(existingcontact.contains(user.ContactId)){
                isUserAlreadyExists = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,Label.CM_PageMessage_UserAlreadyExists));
                return null;
            }
            userContactId.add(user.ContactId);
        }
        if(userContactId.size()>0) {
            isUserExists = true;
        }
        if(userContactId.size()==0) {
            inviteNewUserTOCMP();
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Invited User to CMP'));
        }
        return null;
    }
    
    public void inviteNewUserTOCMP() {
        Invites__c invite = new Invites__c(Invitation_Sent_Count__c=1,Benne_Email__c=beneEmail,Inviter_User__c=UserInfo.getUserId(),
                                           Status__c = 'Sent',Invitation_Token__c=GPIntegrationUtility.generateInvitesTOKEN('Invitation_Token__c'),
                                           Last_Sent_By_User__c=UserInfo.getUserId(),Status_DateTime__c = system.now());
        insert invite;
    }

    public PageReference inviteUserTONetwork() {
        List<Network__c> newNetworks = new List<Network__c>();
        for(Id cntId: userContactId) {
            newNetworks.add(new Network__c(Invitee_Contact__c=cntId,
                                           Inviter_User__c=UserInfo.getUserId(),
                                           Status__c='1 - Pending (Sent)',Status_DateTime__c = system.now()));
        }
        insert newNetworks;
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Invite has been sent to the email address provided'));
        return null;
    }
}