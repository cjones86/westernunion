public without sharing class AttachmentTriggerManager {

    static apexLogHandler.apexLog logAlertToDealer = new apexLogHandler.apexLog('AttchTriggerManager','alertToDealer');
    static apexLogHandler.apexLog logEmailToDealer = new apexLogHandler.apexLog('AttchTriggerManager','emailToDealer');
    static apexLogHandler.apexLog logEmailToCMPUser = new apexLogHandler.apexLog('AttchTriggerManager','emailToCMPUser');

	public static Map<Id, List<Case_Document__c>> accountDocIdCaseDocListMap = new Map<Id, List<Case_Document__c>>();
	public static void sendAlertToDealer(List<Attachment> newList){
	    logAlertToDealer.logMessage('>>inside sendAlertToDealer method ...>> ');
		Map<Id, List<Attachment>> documentIdAttachmentListMap = new Map<Id, List<Attachment>>();
		Map<Id, Id> attachmentIdOwnerId = new Map<Id, Id>();
		Map<Id,User> userIdMap;
		Map<Id, List<Account_Document__c>> accountIdDocumentListMap = new Map<Id, List<Account_Document__c>>();
		Map<Id, List<Account_Document__c>> dealerAccountIdDocumentListMap = new Map<Id, List<Account_Document__c>>();
		for(Attachment atch : newList){
			String documentId = atch.ParentId;
			if(documentId.startsWith(Label.CM_Community_AccountDocument_InitialCode)){
				if(!documentIdAttachmentListMap.containsKey(atch.ParentId)){
					documentIdAttachmentListMap.put(atch.ParentId,new List<Attachment>());
				}
				documentIdAttachmentListMap.get(atch.ParentId).add(atch);
				attachmentIdOwnerId.put(atch.Id, atch.CreatedById);
			}
		}
		if(documentIdAttachmentListMap.size() > 0){
			userIdMap = new Map<Id,User>([SELECT Id, Name, Email, ContactId FROM User Where Id IN : attachmentIdOwnerId.values()]);
			for(Account_Document__c accDoc : [SELECT Id, Name,Name__c, Account__c, OwnerId FROM Account_Document__c
														WHERE Id IN :documentIdAttachmentListMap.keySet()]){
				for(Attachment attach : documentIdAttachmentListMap.get(accDoc.Id)){
					if(userIdMap.containsKey(attachmentIdOwnerId.get(attach.Id))){
						User tUser = userIdMap.get(attachmentIdOwnerId.get(attach.Id));
						if(!String.isEmpty(tUser.ContactId)){
							if(!accountIdDocumentListMap.containskey(accDoc.Account__c)){
								accountIdDocumentListMap.put(accDoc.Account__c, new List<Account_Document__c>());
							}
							accountIdDocumentListMap.get(accDoc.Account__c).add(accDoc);
						}
						else{
							if(!dealerAccountIdDocumentListMap.containskey(accDoc.Account__c)){
								dealerAccountIdDocumentListMap.put(accDoc.Account__c, new List<Account_Document__c>());
							}
							dealerAccountIdDocumentListMap.get(accDoc.Account__c).add(accDoc);
						}
					}
				}
			}
			for(Case_Document__c caseDoc : [SELECT Id, ID__c, Case__c, Case__r.OwnerId, Case__r.Owner.Email, Case__r.Owner.Name FROM Case_Document__c WHERE ID__c IN :documentIdAttachmentListMap.keySet()]){
				if(!accountDocIdCaseDocListMap.containsKey(caseDoc.ID__c)){
					accountDocIdCaseDocListMap.put(caseDoc.ID__c, new List<Case_Document__c>());
				}
				accountDocIdCaseDocListMap.get(caseDoc.ID__c).add(caseDoc);
			}
			System.Debug('###'+dealerAccountIdDocumentListMap);
			logAlertToDealer.logDebug('###'+dealerAccountIdDocumentListMap);
			if(accountIdDocumentListMap.size() > 0){

				sendEmailToDealer(accountIdDocumentListMap, documentIdAttachmentListMap, userIdMap);
			}
			if(dealerAccountIdDocumentListMap.size() > 0){

				sendEmailToCMPUser(dealerAccountIdDocumentListMap);
			}
		}
		logAlertToDealer.saveLogs();
	}

	private static void sendEmailToDealer(Map<Id, List<Account_Document__c>> accountIdDocumentListMap,
											Map<Id, List<Attachment>> documentIdAttachmentListMap, Map<Id,User> userIdMap){
		logEmailToDealer.logMessage('>>inside sendEmailToDealer method ...>> ');
		List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage emailMsj;
        String siteURL = '';
        EmailTemplate et = [SELECT Id FROM EmailTemplate WHERE Name = 'Email to Account Owner When Document is Created'];
		for(Account acc : [SELECT Id, OwnerId, Client_Manager__c, Client_Manager__r.User__c, Owner.Email, Owner.Name, Client_Manager__r.User__r.Name, Client_Manager__r.User__r.Email,
									Client_Support_Coordinator__c, Client_Support_Coordinator__r.Email, Client_Support_Coordinator__r.Name FROM Account
									WHERE Id IN: accountIdDocumentListMap.keySet()]){

			for(Account_Document__c accDocObj : accountIdDocumentListMap.get(acc.Id)){
				siteURL = CMP_Administration__c.getInstance().CM_Dealer_Url__c +'apex/AccountDocumentView?Id='+accDocObj.Id+'&aId='+acc.Id;
				for(Attachment attachObj : documentIdAttachmentListMap.get(accDocObj.Id)){
					emailMsj = new Messaging.SingleEmailMessage();
	                emailMsj.setHtmlBody('Dear '+acc.Client_Support_Coordinator__r.Name+'<br/><br/>A new file has been uploaded; '+accDocObj.Name__c+', by '+userIdMap.get(attachObj.OwnerId).Name
	                +'. Below is the detail link:<br /><br />'+siteURL);

	                emailMsj.setSubject(Label.CM_DocumentNotification_Subject);
	                emailMsj.setWhatId(accDocObj.Id);
					if(acc.Client_Support_Coordinator__c != null){
						List<String> userEmailTempList = new List<String>();
						userEmailTempList = Label.CM_Community_CollaborationCenter_ClientSupportExist.split(',');
						userEmailTempList.add(acc.Client_Support_Coordinator__r.Email);
		                emailMsj.setToAddresses(userEmailTempList);
					}else{
						List<String> userEmailTempList = new List<String>();
						userEmailTempList = Label.CM_Community_CollaborationCenter_ClientSupportCoordinator.split(',');
						emailMsj.setToAddresses(userEmailTempList);
					}

	                emailMsj.setSaveAsActivity(false);
	                emailList.add(emailMsj);

	                if(acc.Client_Manager__c != null && acc.Client_Manager__r.User__c != null){
			            emailMsj = new Messaging.SingleEmailMessage();
		                emailMsj.setHtmlBody('Dear '+acc.Client_Manager__r.User__r.Name+'<br/><br/>A new file has been uploaded; '+accDocObj.Name__c+', by '+userIdMap.get(attachObj.OwnerId).Name
		                +'. Below is the detail link:<br /><br />'+siteURL);
		                emailMsj.setSubject(Label.CM_DocumentNotification_Subject);
		                emailMsj.setWhatId(accDocObj.Id);
		                emailMsj.setToAddresses(new List<String>{acc.Client_Manager__r.User__r.Email});
		                emailMsj.setSaveAsActivity(false);
		                emailList.add(emailMsj);
					}

					if(acc.OwnerId != null){
			            emailMsj = new Messaging.SingleEmailMessage();
		                emailMsj.setHtmlBody('Dear '+acc.Owner.Name+'<br/><br/>A new file has been uploaded; '+accDocObj.Name__c+', by '+userIdMap.get(attachObj.OwnerId).Name
		                +'. Below is the detail link:<br /><br />'+siteURL);
		                emailMsj.setSubject(Label.CM_DocumentNotification_Subject);
		                emailMsj.setWhatId(accDocObj.Id);
		                emailMsj.setToAddresses(new List<String>{acc.Owner.Email});
		                emailMsj.setSaveAsActivity(false);
		                emailList.add(emailMsj);
					}
					if(accountDocIdCaseDocListMap.containsKey(accDocObj.Id)){
						for(Case_Document__c caseD : accountDocIdCaseDocListMap.get(accDocObj.Id)){
							emailMsj = new Messaging.SingleEmailMessage();
			                emailMsj.setHtmlBody('Dear '+caseD.Case__r.Owner.Name+'<br/><br/>A new file has been uploaded; '+accDocObj.Name__c+', by '+userIdMap.get(attachObj.OwnerId).Name
			                +'. Below is the detail link:<br /><br />'+siteURL);
			                emailMsj.setSubject(Label.CM_DocumentNotification_Subject);
			                emailMsj.setWhatId(accDocObj.Id);
			                emailMsj.setToAddresses(new List<String>{caseD.Case__r.Owner.Email});
			                emailMsj.setSaveAsActivity(false);
			                emailList.add(emailMsj);
						}
					}

					/*emailMsj = new Messaging.SingleEmailMessage();
			            emailMsj.setTemplateId(et.Id);
			            emailMsj.setWhatId(accDocObj.Id);
			            emailMsj.setTargetObjectId(acc.OwnerId);
			            emailMsj.setSaveAsActivity(false);*/
				}
            }
        }
        if(emailList.size() > 0){
            try{
                Messaging.sendEmail(emailList);
                logEmailToDealer.logDebug('Message Send');
            }catch(Exception ex){
                logEmailToDealer.logDebug('Exception ' + ex.getMessage());
                system.debug('-----ashish-----------------'+ex);
            }
        }
        logEmailToDealer.saveLogs();
	}


	private static void sendEmailToCMPUser(Map<Id, List<Account_Document__c>> dealerAccountIdDocumentListMap){
	    logEmailToCMPUser.logMessage('>>inside sendEmailToCMPUser method ...>> ');
		List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage emailMsj;
        String siteURL = Site.getBaseUrl();
        EmailTemplate et = [SELECT Id FROM EmailTemplate WHERE Name = 'Document Notification Template'];
        OrgWideEmailAddress emailAdd = [SELECT Id, Address FROM OrgWideEmailAddress WHERE Address = 'donotreply@wuedge.com'];
		for(User tUser : [SELECT Id, Email, Name, ContactId, Contact.AccountId FROM User
											WHERE Contact.AccountId IN: dealerAccountIdDocumentListMap.keySet()
											AND CMP_Document_Notification__c = true
											AND Collaboration_Center__c = true
											AND isActive = true]){
			for(Account_Document__c accDocObj : dealerAccountIdDocumentListMap.get(tUser.Contact.AccountId)){
                /*
                emailMsj = new Messaging.SingleEmailMessage();
	            emailMsj.setTemplateId(et.Id);
	            emailMsj.setWhatId(accDocObj.Id);
	            emailMsj.setTargetObjectId(tUser.Id);
	            emailMsj.setSaveAsActivity(false);*/
	            emailList.add(createEmailBody(et.Id, tUser.Id, accDocObj.Id,emailAdd.id));
            }
        }

        if(emailList.size() > 0){
            try{
                Messaging.sendEmail(emailList);
                logEmailToCMPUser.logDebug('Email Sent');
            }catch(Exception ex){
                logEmailToCMPUser.logDebug('Exception ' + ex.getMessage());
                system.debug('-----ashish-----------------'+ex);
            }
        }
        logEmailToCMPUser.saveLogs();
	}


	private static Messaging.SingleEmailMessage createEmailBody(String emailTemplateId, String userId, String tragetObjId, String OrgWideEmailAddress){
		Messaging.SingleEmailMessage emailMsj = new Messaging.SingleEmailMessage();
        emailMsj.setTemplateId(emailTemplateId);
        emailMsj.setWhatId(tragetObjId);
        emailMsj.setTargetObjectId(userId);
        emailMsj.setSaveAsActivity(false);
        emailMsj.setOrgWideEmailAddressId(OrgWideEmailAddress);
        emailMsj.setUseSignature(false);
        return emailMsj;
	}

}