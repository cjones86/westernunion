/*	----------------------------------------------------------------
	Created by:		Timothy Fabros
	Date Created:	04 / 02 / 2014
	----------------------------------------------------------------
	CLASS: Global
	
	Global class for the purpose of running manual sharing rules
	to share Input__c records.
	----------------------------------------------------------------
*/
global class InputManualSharing {
	
	/*	----------------------------------------------------------------
		FUTURE METHOD: shareInputRecords
		----------------------------------------------------------------
		INPUTS: String 
		RETURN: void
		
		Gets the input record to be shared and inserts a sharing rule.
		----------------------------------------------------------------
	*/
	@future
	public static void shareInputRecords(List<String> inputIds, String currentUser) {
		system.debug('INSIDE THE @FUTURE METHOD RUN FROM THE TRIGGER ------------------------------------------------------');
		List<Input__c> 		inputs 			= new List<Input__c>();
		Set<Id>             accountIdSet	= New Set<Id>();
		map<Id, List<User>> accountIdUsersMap   = New map<Id, List<User>>();
		
		List<Input_Beneficiary__Share>  inputBeneShareList      = New List<Input_Beneficiary__Share>();
		List<Input__Share>  inputShareList      = New List<Input__Share>();
		
		inputs = [Select Id, Input_Beneficiary__c, Parent_Account__c From Input__c Where Id In: inputIds];
		system.debug('These are the inputs that have been queried: ' + inputs);		
		for (Input__c input : inputs) {
			if (input.Parent_Account__c != null)
				accountIdSet.add(input.Parent_Account__c);
		}
		system.debug('account ids Values: ' + accountIdSet);
		for (User u : [Select Id, Name, ContactId, Contact.AccountId from User where Contact.AccountId in: accountIdSet And Id !=: currentUser]) {
			if (!accountIdUsersMap.containsKey(u.Contact.AccountId))
				accountIdUsersMap.put(u.Contact.AccountId, new List<User>{u});
			else 
				accountIdUsersMap.get(u.Contact.AccountId).add(u);
		}
		system.debug('account ids map values: ' + accountIdUsersMap);
		for (Input__c input : inputs) {
			if (input.Parent_Account__c != null) {
				if (accountIdUsersMap.containsKey(input.Parent_Account__c)) {
					for (User u : accountIdUsersMap.get(input.Parent_Account__c)) {
						inputShareList.add(new Input__Share(ParentId = input.Id, UserOrGroupId = u.Id, AccessLevel = 'Edit'));
					}
				}
			}
		}

		// shares the input beneficiary - added by tim.
		for (Input__c input : inputs) {
			if (input.Parent_Account__c != null) {
				if (accountIdUsersMap.containsKey(input.Parent_Account__c)) {
					if (input.Input_Beneficiary__c != null) {
						for (User u : accountIdUsersMap.get(input.Parent_Account__c)) {
							inputBeneShareList.add(new Input_Beneficiary__Share(ParentId = input.Input_Beneficiary__c, UserOrGroupId = u.Id, AccessLevel = 'Edit'));
						}
					}
				}
			}
		}
		system.debug('inputBeneShareList ids map values: ' + inputBeneShareList);
		system.debug('inputShareList ids map values: ' + inputShareList);
		
		try {
			insert inputBeneShareList;
			insert inputShareList;
		}
		catch (Exception e) {
			System.debug('Problem occurred trying to share the Input__c records in InputManualSharing.cls: ' + e);
		}
	}
}