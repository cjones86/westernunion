global class DragDropDocumentUploadController {

     
    public String parentId{get;set;} 
    public String typeInput{get;set;}
    public String sourcePageCtl{get;set;}
    
    public String recordTypeId{
    	get{
    		String recordTypeNameId = '';
    		String transTypeName = '';
    		if(Site.getSiteId()==null){
    			transTypeName = 'WUBS Generated';
    		}else{
    			transTypeName = 'Customer Generated';
    		}
    		recordTypeNameId = Schema.SObjectType.Account_Document__c.getRecordTypeInfosByName().get(transTypeName).getRecordTypeId();
    		return recordTypeNameId;
    	}
    	set;
    }
    
    public String documentType{
    	get{
    		if(Site.getSiteId()==null){
    			return 'WUBS Generated';
    		}else{
    			return 'Customer Generated';
    		}
    	}
    	set;
    }
    
    
    public String documentName {get; set;}
    public String accountObjId {
    	get{
    		return Utility.currentAccount;
    	} 
    	set;
    }
    public Attachment attachment {
        get{
            if (attachment == null)
            attachment = new Attachment();
            return attachment;
        }set;
    }
    public Pagereference uploadFile(){
            attachment.ParentId=parentId;
            insert attachment;
            return null;
    }
    
    @RemoteAction
    global static String uploadFile(String parentId, String docName, String transType){
        String accountId = Utility.currentAccount;
        try{
        	String recordTypeId = Schema.SObjectType.Account_Document__c.getRecordTypeInfosByName().get(transType).getRecordTypeId();    
        	system.debug('------------------s-');    
        	Account_Document__c accDocu = new Account_Document__c(Name__c = docName, Account__c = parentId, Document_Type__c = transType, RecordTypeId = recordTypeId);
        	insert accDocu;
        	return accDocu.id;
        }catch(Exception ex){
        	system.debug('exception is created--------'+ex);
        	return '';
        }
    }
    
    @RemoteAction
    global static String updateDoc(String accDocuId){
        try{
        	Account_Document__c accDoc = [SELECT Id, OwnerId, Account__c, Account__r.OwnerId FROM Account_Document__c WHERE Id =:accDocuId];
            accDoc.OwnerId = accDoc.Account__r.OwnerId;
            update accDoc;
            return accDoc.Id;
        }
        catch(Exception ex){
        	system.debug('exception is created--------'+ex);
        	return '';
        }
    }
    
    
    @RemoteAction
    global static String deleteDoc(String accDocuId){
        try{
        	system.debug('exception is created----accDocuId-');
        	delete new Account_Document__c(Id = accDocuId);
            return '';
        }
        catch(Exception ex){
        	system.debug('exception is created----accDocuId----'+ex);
        	return accDocuId;
        }
    }
    
    
    @RemoteAction
    global static String checkDocumentName(String docName, String parentId){
    	List<Account_Document__c> docList = new List<Account_Document__c>([SELECT Id FROM Account_Document__c WHERE Name__c =:docName AND Account__c =:parentId]);
    	if(docList.size() > 0){
    		return 'fail';
    	}else{
    		return '';
    	}
    }
    
    
    
    //Get cuttent page name
    private String getPageName(){
        if(String.isEmpty(sourcePageCtl)){
            return Utility.getVFPageName();
        }
        return sourcePageCtl;
    }
// To upload the file in InvoiceAndPayment Section Using the browse button    
      public PageReference upload1() {
          String accId = Utility.currentAccount;
          
        attachment.OwnerId = Utility.loggedInUser.id;
        attachment.ParentId = parentId; // the record the file is attached to
        attachment.IsPrivate = false;
        attachment.contentType= 'text;charset=UTF-8';   
        
        try {
            insert attachment;
        }catch (DMLException e) {
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
          return null;
        } finally {
        }
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
        PageReference pr =  new PageReference('/apex/ParseAndDisplayCSVInvoicePage?attid='+attachment.id + '&aId='+accId+'&sourcePage='+getPageName());
        pr.setredirect(true);
        return pr;
      }
      
      
// To upload the file in InsightsAndInputs Section Using the browse button        
    public PageReference uploadInput() {
        System.debug('====+Anjali Attachment name==' + attachment+'------typeInput---------'+typeInput);
        String SitePrefix = Site.getPathPrefix();
        String accId = Utility.currentAccount;
        if(Site.getSiteId()!=null){
        	typeInput = 'Customer Generated';
        }else{
        	typeInput = 'WUBS Generated';
        }
        String recordTypeName = '';
        if(typeInput.indexOf('/') != -1 || typeInput.indexOf(' ') != -1){
        	recordTypeName = typeInput.replaceAll('/', '_');
        	recordTypeName = recordTypeName.replaceAll(' ', '_');
        }
        
		String recordTypeId = Schema.SObjectType.Account_Document__c.getRecordTypeInfosByName().get(typeInput).getRecordTypeId();
        
        Account_Document__c accDocu = new Account_Document__c(Name__c = attachment.Name, Account__c = accId, Document_Type__c = typeInput, RecordTypeId = recordTypeId);
        system.debug('--------------documentName--------------'+documentName);
        if(!String.isEmpty(documentName)){
        	accDocu.Name__c = documentName;
        }
        insert accDocu;
        
        attachment.OwnerId = UserInfo.getUserId(); 
        attachment.ParentId = accDocu.Id; // the record the file is attached to
        attachment.IsPrivate = false;
       //  attachment.contentType= 'text;charset=UTF-8'; 
    
        try {
            insert attachment;
            Account accOnj = [SELECT Id, OwnerId from Account Where Id =: accDocu.Account__c];
            accDocu.OwnerId = accOnj.OwnerId;
            update accDocu;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Account Document is Created'));
        }catch (DMLException e) {
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
          return null;
        } 
        
        PageReference pr;
        if(Site.getSiteId()==null){
        	pr =  new PageReference('/apex/AccountDocument?aId='+accId+'&tab=doc');
        }else{
        	pr =  new PageReference('/EDGE_Document_Collaboration?tab=docOut');
        }
        pr.setredirect(true);
        return pr;
    }
    
    public PageReference docRedirect() {
        String SitePrefix = Site.getPathPrefix();
        String accId = Utility.currentAccount;
        PageReference pr;
        if(Site.getSiteId()==null){
        	pr =  new PageReference('/apex/AccountDocument?aId='+accId+'&tab=doc');
        }else{
        	pr =  new PageReference('/EDGE_Document_Collaboration?tab=docOut');
        }
        pr.setredirect(true);
        return pr;
    }

}