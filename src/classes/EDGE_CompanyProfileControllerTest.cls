/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_CompanyProfileControllerTest
 * Description: Controller Test class for EDGE_CompanyProfileController
 * Created By: Priyanka Kumar (Appirio)
 * 
 * Date Modified : 26th May'16               Modified By: Nikhil Sharma 
 =====================================================================*/
 
@isTest
private class EDGE_CompanyProfileControllerTest {
    
    private static User commUser;
    private static User commUser1;
    private static Contact newContact;
    private static Account beneAccount;
    
    static testMethod void testMarketProfiles() {
        createTestData();
        Account accRecord = [Select Id From Account where ExternalId__c='11111145'];// account record for commUser1 
        
        // See commUser1 profile as commUser
        System.runAs(commUser){
            test.startTest();
                Apexpages.currentpage().getparameters().put('id',accRecord.Id);
                EDGE_CompanyProfileController con = new EDGE_CompanyProfileController();
                con.getProfile();
                con.edit();
                con.save();
                con.cancel();
                System.assertNotEquals(null,con.profile);
                System.assertEquals(false,con.isEditVisible);//comm user should not be able to edit other market profile.
            test.stopTest();
        }
    }
    
    static testMethod void testLoggedInUserProfile() {
       createTestData(); 
       System.runAs(commUser){
           //set up market profile for logged in user
            test.startTest();
                EDGE_CompanyProfileController con = new EDGE_CompanyProfileController();
                con.getProfile();
                con.edit();
                con.save();
                con.cancel();
                con.getSourceValues();
                con.getSupplyValues();
                con.inviteUserTONetwork();
                // check whether new profile got created for commUser or not as no market profile existed already.
                System.assertNotEquals(null,con.profile);
                System.assertEquals(true,con.isEditVisible); //comm user should be able to edit his own profile.
            test.stopTest();
        }
    }
    
   static testMethod void testNetworkedUser() {
       createTestData(); 
       Account accRecord = [Select Id From Account where ExternalId__c='11111145'];// account record for commUser1 
       Network__c newNetwork = test_utility.createNetwork(newContact.Id, commUser.Id, accRecord.Id, beneAccount.Id,true);
       
       System.runAs(commUser){
       	   Apexpages.currentpage().getparameters().put('id',accRecord.Id);
           //set up market profile for logged in user
            test.startTest();
                EDGE_CompanyProfileController con = new EDGE_CompanyProfileController();
                con.getProfile();
                con.edit();
                con.save();
                con.cancel();
                con.getSourceValues();
                con.getSupplyValues();
            //    con.inviteUserTONetwork();
            test.stopTest();
        }
    }
    
    //set up two community users.
    private static void createTestData(){
        test_Utility.createCMPAdministration();
		CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.CCT_Client_ID__c = '3232112';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        /*
        beneAccount.BillingStreet = 'testStreet';
        beneAccount.BillingCity = 'city';
        beneAccount.BillingState = 'state';
        beneAccount.BillingCountry = 'USA';
        beneAccount.BillingPostalCode = '231231';
        */
        
        Account beneAccount1 = test_Utility.createAccount(false);
        beneAccount1.RecordTypeId=accountRecordType;
        beneAccount1.ExternalId__c='11111145';
        beneAccount.CCT_Client_ID__c = '32321122131';
        beneAccount1.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount1.CMP_Enabled__c = true;
        List<Account> listAccount = new List<Account>{beneAccount, beneAccount1};
        insert listAccount;
        
        newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        Contact newContact1 = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        List<Contact> listContacts = new List<Contact>{newContact,newContact1};
        insert listContacts;
        
        commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
		commUser.timezonesidkey='America/Indiana/Indianapolis';
		commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        
        commUser1 = test_Utility.createCommUser(newContact1.Id, false);
        commUser1.ProfileId = profileId;
        commUser1.emailencodingkey='UTF-8';
        commUser1.localesidkey='en_US';
				commUser1.timezonesidkey='America/Indiana/Indianapolis';
				commUser1.CMP_Enabled__c=true;
        commUser1.UserName = newContact1.Email+'.cmp1';
        
        list<User> listUsers = new List<User>{commUser,commUser1};
        insert listUsers;
        
        //set up market profile records for both accounts
        Marketplace_profile__c mp1 = test_Utility.createMarketProfile(false, beneAccount1.Id);
        mp1.Where_do_they_source_from__c = 'AFG;ALB;DZA';
        Marketplace_profile__c mp2 = test_Utility.createMarketProfile(false, beneAccount1.Id);
        mp2.Where_do_they_supply_to__c = 'AFG;ALB;DZA';
        List<Marketplace_profile__c> listMarketProfiles = new List<Marketplace_profile__c>{mp1,mp2};
        insert listMarketProfiles;
        
        //Network__c newNetwork = test_utility.createNetwork(newContact.Id, commUser.Id, beneAccount.Id, beneAccount1.Id,false);
        //newNetwork.Status__c='3 - Active (Accepted)';
        //insert newNetwork;
    }
}