@isTest
private class Edge_SearchSupplierModal_Test{
    static Account account;
    static Supplier__c supp;
    
    @isTest
	private static void testMethodForSupplierModal() {
	   	User commUser = createData();
        test_Utility.createWUEdgeSharingAdmin(); 
		System.RunAs(commUser){
	    Edge_SearchSupplierModal ssp = new Edge_SearchSupplierModal();
	    System.assertEquals(1, ssp.listSuppliers.size());
		}
	    
	    

	}	
	private static User createData() {
		test_Utility.createCMPAdministration();
		test_Utility.createCMPAlert();
		CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        insert beneAccount;
        account = beneAccount;
        supp = test_Utility.createSupplier(beneAccount.Id,beneAccount.id,false);
        supp.Supplier_Name__c = 'Test';
        supp.Address_line_1__c = 'Test';
        supp.Address_Line_2__c = 'Test';
        supp.City__c = 'Test';
        supp.State_Province__c = 'Test';
        //supp.Country__c = 'ANDORRA';
        supp.Post_Code__c = 'Test';
        insert supp;
        
        
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact;
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
		commUser.timezonesidkey='America/Indiana/Indianapolis';
		commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        commUser.CommunityNickname = commUser.alias;
        insert commUser;     
        if (commUser.Id != null) { 
            Global_Pay_ID_Management__c gPIM = new Global_Pay_ID_Management__c(Global_Pay_ID__c='12345',
                                                                               User_Beneficiary__c=commUser.Id,
                                                                               Creation_Type__c='Automatic Email Match');
           insert gPIM;
        }
        return commUser;
	}

}