/**================================================================      
* Appirio, Inc
* Name: EDGE_BatchService
* Description: Utility methods related to current batch
* Created Date: 20-Oct-2016
* Created By: Raghu Rankawat (Appirio)
*
* Date Modified      Modified By      Description of the update
*
==================================================================*/

public class EDGE_BatchService {
    
    public static boolean showWarning{get;set;}
    public static String lastSubmittedUser{get;set;}
    public static DateTime lastSubmittedDateTime{get;set;}
    public static String confirmationMsg {get;set;}
    
   
   /**************************************************
    Get current active batch if any or create new batch
   ***************************************************/
   public static Batch__c getActiveBatchForCurrentContact(){
       User currentUser = Utility.loggedInUser;
       //Find currentBatch for current contact if any
       List<Batch__c> currentBatch = [SELECT Id, Name, Batch_Submitted__c, GP2_Last_Order_Submitted_Date_Time__c, status__c, GP2_Last_Submitted_By__c, Contact__c 
                FROM Batch__c 
                WHERE Contact__c = :currentUser.ContactId
                AND Batch_Submitted__c != null 
                AND status__c = 'Active'
                ORDER BY Batch_Submitted__c DESC
                LIMIT 1];
       //If no current batch then create a new batch
       if(currentBatch.isEmpty()){
            Batch__c newBatch = new Batch__c(Batch_Submitted__c=System.Now(), 
                                        Contact__c = currentUser.ContactId,
                                        Status__c='Active',
                                        GP2_Last_Order_Submitted_Date_Time__c=null,
                                        GP2_Last_Submitted_By__c=null);
            insert newBatch;
            currentBatch.add(newBatch);
                                        
       }
       return currentBatch.get(0); 
   }
   
   
   /**************************************************
    Submit batch order to payment API
   ***************************************************/
   public static PageReference submitBatchToGP2(Batch__c bh){
       User currentUser = Utility.loggedInUser;
     
       try{
        
           List<Invoice__c> invList = [SELECT Id, Name, Account__c, GP2_Last_Order_Submitted_Date_Time__c, GP2_Last_Submitted_By__c
                                       FROM Invoice__c
                                       WHERE Account__c = :currentUser.Contact.AccountId
                                       AND Batch_Id__c = :bh.Id]; 

           bh.GP2_Last_Order_Submitted_Date_Time__c = System.Now();
           bh.GP2_Last_Submitted_By__c = currentUser.Id;
           update bh;
           
           for(Invoice__c inv : invList){
               inv.GP2_Last_Order_Submitted_Date_Time__c = System.Now();
               inv.GP2_Last_Submitted_By__c = currentUser.Id; 
           }
           if(invList.size() > 0)
               update invList;
           
       }catch(Exception e){
           System.debug('Exception submitBatchToGP2 ' + e.getMessage());
           return null;
       }
       
       PageReference pg = new PageReference('/EDGE_MultiLineOrderPayGP2?batchId='+bh.Id);
       String retURL = CMP_Administration__c.getInstance().CM_Community_BaseURL__c+'/EDGE_MultiLineOrderStep1?step=1';
       pg.getParameters().put('retUrl', retURL);
       pg.setRedirect(true);
       return pg;
       //Payment posting to GP: logic to be updated
   }
   
   /**************************************************
    Add selected invoices to batch
   ***************************************************/
   public static String addInvoicesToBatch(Id batchId,List<InvoiceUIWrapper_MultiLine> invoices,boolean canProcessBatch){
        
        boolean canProceed = false;
          
        List<Invoice__c> listInvoiceToUpdateWithBatchId = new List<Invoice__c>();
        if(batchId != null){
         Decimal softLockReleaseTime = CMP_Administration__c.getInstance().Soft_Lock_Release_Threshold_in_minutes__c;
            for(InvoiceUIWrapper_MultiLine inv: invoices) {
               if(inv.selected && inv.Invoice.Batch_Id__c != null && inv.Invoice.Batch_Id__c != batchId && inv.Invoice.GP2_Last_Order_Submitted_Date_Time__c != null &&
                     inv.Invoice.GP2_Last_Submitted_By__c != null && canProcessBatch == false){
                        //this is already associated with a batch                       
                        //check if it is within 20 mins
                        long no_of_Min = (System.now().getTime() - (inv.Invoice.GP2_Last_Order_Submitted_Date_Time__c).getTime())/60000;
                        
                        //if different user is trying again to add the batch
                        if(inv.Invoice.GP2_Last_Submitted_By__c != userInfo.getUserId()){
                            if(no_of_Min < softLockReleaseTime){
                              //show warning message
                              //showWarning = true;
                              lastSubmittedUser = inv.Invoice.GP2_Last_Submitted_By__r.Name;
                              lastSubmittedDateTime = inv.Invoice.GP2_Last_Order_Submitted_Date_Time__c;
                              confirmationMsg = lastSubmittedUser +' has already started the process of submitting this Invoice/Batch for payment at ' + lastSubmittedDateTime + ' Do you wish to proceed with payment?';
                              return confirmationMsg;
                            }
                        } 
                        //this means that either same user is trying to submit the batch or different user but timeframe is > than 20 mins
                        canProceed = true;                  
                    }
                    
                    if(inv.Selected && (inv.Invoice.Batch_Id__c == null || inv.Invoice.Batch_Id__c == batchId || canProcessBatch || canProceed)){
                      //if canprocessbatch is true , that means invoice was related to other batch still user wants to proceed.
                      Invoice__c invoice = new Invoice__c(Id = inv.Invoice.Id);
                      invoice.Batch_Id__c = batchId;
                      invoice.GP2_Last_Order_Submitted_Date_Time__c = System.Now();
                      invoice.GP2_Last_Submitted_By__c = UserInfo.getUserId();
                      listInvoiceToUpdateWithBatchId.add(invoice);
              } 
              }
            if(listInvoiceToUpdateWithBatchId.size() > 0){
              try{
                update listInvoiceToUpdateWithBatchId;
              }Catch(exception ex){
                System.debug('>>> error in update batch invoices >>> '+ ex.getMessage());
                
              }
              
            }
         }
        return null;
   
   }
    
}