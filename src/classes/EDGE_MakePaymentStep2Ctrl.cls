/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_MakePaymentStep2Ctrl
 * Description: Controller class for EDGE_MakePaymentStep2 page
 * Created Date: 11 May' 2016
 * Created By: Nikhil Sharma (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
public without sharing class EDGE_MakePaymentStep2Ctrl {

    static apexLogHandler.apexLog logPayInvoice = new apexLogHandler.apexLog('EDGE_MakePaymentStep2Ctrl','payInvoice');
    
    Public String delMethod{get;set;}
    Public String suppAccount{get;set;}
    Public InvoiceWrapper invWrp {get;set;}
    public List<SelectOption> H2Hcurrencies {get;set;}
    public String hbCurrencyISO{get;set;}
    Public boolean isSaved{get;set;}
    public Boolean displayPopup {get; set;}
    public String popupURL{get;set;}
    public String selectedInvoice{get;set;}
    public String beneName{get;set;}
    public String sellerAccountId{get;set;}
    public boolean noNetwork{get;set;}
    public String suppAccountId{get;set;}
    public String errorMessage{get;set;}
    public String deltype;
    public boolean isEmailBlank;
    public String retInvoiceId{get;set;}

    public EDGE_MakePaymentStep2Ctrl(){
        deltype = ApexPages.currentPage().getParameters().get('deliveryMethod');
        delMethod = '';
        noNetwork = false;
        errorMessage = '';
        isEmailBlank = false;

        if(String.isNotBlank(deltype)){
            if(deltype.equalsIgnoreCase('H2H'))
                delMethod = 'Holding-to-Holding';
            if(deltype.equalsIgnoreCase('StdPayment'))
                delMethod = 'Standard Payment';
        }
        if(ApexPages.currentPage().getParameters().get('SuppAccount') != null)
        suppAccountId = EncryptionManager.doDecrypt(ApexPages.currentPage().getParameters().get('SuppAccount'));
        
        if(ApexPages.currentPage().getParameters().get('invoiceId') != null)
        retInvoiceId = EncryptionManager.doDecrypt(ApexPages.currentPage().getParameters().get('invoiceId'));
        for(Supplier__c supp: [Select Id, Name, Supplier__c,Network_Status__c,Supplier_Name__c,email_address__c From Supplier__c Where Id=:suppAccountId]){

        	 if(String.isBlank(supp.email_address__c)){
        	 	 isEmailBlank = true;
        	 }

            suppAccount = supp.Supplier_Name__c;
            sellerAccountId = supp.Supplier__c;
            if(supp.Network_Status__c.containsIgnoreCase('Not Currently Networked'))
                noNetwork = true;
        }
        Invoice__c inv;
        if(retInvoiceId != null){
        	inv = new Invoice__c(Id=retInvoiceId);
        	inv = [Select Id,Reference_Number__c,Amount__c,Custom_Currency__c,RecordTypeId,
        	       Delivery_Method__c,Supplier__c,Buyer__c,
        	       Balance__c,Invite_to_my_network__c
        	       FROM Invoice__c WHERE Id=:retInvoiceId];
        	       
          if(delMethod == 'Holding-to-Holding'){
          	hbCurrencyISO = inv.Custom_Currency__c;
          }	       
        	       
        }else{
          inv = new Invoice__c();
        }
        inv.RecordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('Cold Payment - Active').getRecordTypeId();
        inv.Delivery_Method__c = delMethod;
        inv.Supplier__c = suppAccountId;
        inv.Buyer__c = UserInfo.getUserId();
        inv.Balance__c = inv.Amount__c;
        inv.Invite_to_my_network__c = false;
        if(delMethod == 'Holding-to-Holding'){
            EdgeGpJsonUtils jsonUtil = new EdgeGpJsonUtils(false);
	 	    H2Hcurrencies = jsonUtil.getHoldingCurrencies();
	 	    
        }
        invWrp = new InvoiceWrapper(inv);
    }

    public pageReference payH2HInvoice(){
    	payInvoice();
    	String isModal = ApexPages.currentPage().getParameters().get('isModal');
    	if(isModal!='true')
    		isModal='false';
    	if(isSaved){
    	    String encodedInvoiceId = Utility.doEncryption(selectedInvoice);
    		PageReference pg = new PageReference('/EDGE_Pay?invoiceId='+encodedInvoiceId+'&isModal='+isModal);
        String retURL = CMP_Administration__c.getInstance().CM_Community_BaseURL__c+'/EDGE_MakePaymentStep2?SuppAccount='+suppAccountId + '&invoiceId='+selectedInvoice +'&deliveryMethod='+delType;
        System.Debug('###'+returl);
    		pg.getParameters().put('retUrl', retURL);
    		pg.setRedirect(true);
    		return pg;
    	}
    	return null;
    }

    public pageReference nextToSelectBene(){
    	errorMessage='';
      System.Debug('###'+isEmailBlank);
      System.Debug('###'+invWrp.inv.Invite_to_my_network__c);
    	if(isEmailBlank == true && invWrp.inv.Invite_to_my_network__c){
    		errorMessage += (String.isNotBlank(errorMessage)?'<br/>':'')+System.Label.CM_EDGE_Bene_Email_Error;
    		return null;
    	}
    	payInvoice();
    	String isModal = ApexPages.currentPage().getParameters().get('isModal');
    	if(isModal!='true')
    		isModal='false';
    	if(isSaved){
    	    String encodedInvoiceId = Utility.doEncryption(selectedInvoice);
    		PageReference pg = new PageReference('/EDGE_SelectBeneficiary?invoiceId='+encodedInvoiceId+'&isModal='+isModal);
    		String retURL = CMP_Administration__c.getInstance().CM_Community_BaseURL__c+'/EDGE_MakePaymentStep2?SuppAccount='+suppAccountId +'&invoiceId='+selectedInvoice +'&deliveryMethod='+delType;
    		pg.getParameters().put('retUrl', retURL);
    		pg.setRedirect(true);
    		return pg;
    	}
    	return null;
    }

    Public void payInvoice(){
        
        logPayInvoice.logMessage('>>inside payInvoice method ...>> ');
    	errorMessage='';
         try{
         	if(invWrp.inv.Reference_Number__c!= null && !Utility.matchPattern(invWrp.inv.Reference_Number__c)){
            	isSaved = false;
	        	errorMessage = System.Label.CM_EDGE_SWIFTCharactersOnly;
	            return;
	        }

            invWrp.inv.Status__c = 'Draft';
            invWrp.inv.Account__c = Utility.currentAccount;
            invWrp.inv.Beneficiary_GP_Name__c = suppAccount; // supplier name

            if(delMethod == 'Holding-to-Holding'){
                for(Account HBAccount : [Select id, Name, CCt_Client_Id__c, (Select Id, Name, Email From Contacts) From Account Where id=:sellerAccountId]) {
                    invWrp.inv.Beneficiary_GP_Email_Address__c = HBAccount.Contacts[0].Email;
                    invWrp.inv.Seller_Account__c = sellerAccountId;
                    invWrp.inv.Beneficiary_GP_ID__c = HBAccount.CCt_Client_Id__c;
                    break;
                }
                invWrp.inv.Custom_Currency__c = hbCurrencyISO;
             }else if(delMethod == 'Standard Payment'){
                 String currentAccId = (String)Utility.currentAccount;
                 List<Global_Pay_ID_Management__c> listGPIdm = new List<Global_Pay_ID_Management__c>();
                 for(Global_Pay_ID_Management__c gpIdm : [Select Id, Global_Pay_Id__c,Supplier__c From Global_Pay_ID_Management__c Where
                                                                Account_Beneficiary__c=:sellerAccountId
                                                                And Source_Invoice_Buyer_Account__c=:currentAccId.substring(0,15) limit 1]){
                     invWrp.inv.Seller_Account__c = sellerAccountId;
                     invWrp.inv.Beneficiary_GP_ID__c = gpIdm.Global_Pay_Id__c;
                     if(gpIdm.Supplier__c != suppAccountId){
                         gpIdm.Supplier__c = suppAccountId;
                         listGPIdm.add(gpIdm);
                     }
                 }
                 if(listGPIdm.size() > 0){
                    update listGPIdm;
                    logPayInvoice.logDebug('listGPIdm ' + listGPIdm);
                 }
             }
			 if(invWrp.inv.Due_date__c==null){
             	invWrp.inv.Due_date__c = Datetime.now().date();
             } 
             upsert invWrp.inv;
             logPayInvoice.logDebug('invWrp.inv ' + invWrp.inv);
             isSaved = true;
             selectedInvoice = String.valueOf(invWrp.inv.Id);
         } catch(DMLException ex) {
              isSaved = false;
              errorMessage += (String.isNotBlank(errorMessage)?'<br/>':'') + System.Label.CM_Alert_UnableToSaveInvoice + ex.getdmlMessage(0);
              logPayInvoice.logDebug('Exception ' + ex.getMessage());
         } catch (Exception ex) {
         	  isSaved = false;
              errorMessage += (String.isNotBlank(errorMessage)?'<br/>':'') + System.Label.CM_Alert_UnableToSaveInvoice + ex.getMessage();
              logPayInvoice.logDebug('Exception ' + ex.getMessage());
         }
         logPayInvoice.saveLogs();
     }

    Public Class InvoiceWrapper{
         Public Invoice__c inv{get;set;}
         Public InvoiceWrapper(Invoice__c invoice){
             inv = invoice;
         }
    }

}