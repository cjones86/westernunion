/**=====================================================================
 * Name: EDGE_MultiLineOrder_Review_Controller
 * Description: T-520492
 * Created Date: Aug 08, 2016
 * Created By: Priyanka Kumar (JDC)
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
public without sharing class EDGE_MultiLineOrder_Review_Controller {
	
	 static apexLogHandler.apexLog logInvoice = new apexLogHandler.apexLog('MultiLineOrder_Review_Cont','search');
	
	  public String getLocaleDate() {
        Utility utility = new Utility();
        return utility.localeDate;
    }
    
    public String localeDate{ 
          get{
              Utility utility = new Utility();
              return utility.localeDate;
          }
      }
      public String localeDateRegx{ 
          get{
              String dtStr = localeDate.replace('yyyy', '(\\d{2,4})');
          dtStr = dtStr.replace('mm', '(\\d{1,2})\\');
          dtStr = dtStr.replace('dd', '(\\d{1,2})\\');
              return '/'+dtStr+'( (\\d{1,2}):(\\d{1,2}))? ?(am|pm|AM|PM|Am|Pm)?/';
          }
      }



    //public List<InvoiceUIWrapper_MultiLine> buyerInvoices{get;set;}
    //public List<InvoiceUIWrapper_MultiLine> beneficiaryInvoices{get;set;}
    //public List<SelectOption> statusList{get;set;}
    public String status{get;set;}
    //public List<SelectOption> currencyList{get;set;}
    public String currencyString{get;set;}
    //public Boolean createTransactionPermForSP{get;set;}
    //public Boolean createTransactionPermForH2H{get;set;}
    public string currentAccount{get;set;}
    public String userDefaultCurrency{get;set;}
    public String userCurrency{get;set;}
    private List<String> gPId;

    public List<InvoiceUIWrapper_MultiLine> invoices{get;set;}
    public List<String> selectedStatusSet{get;set;}
    public String currencyS{get;set;}
    public String requestType{get;set;}
    public String xlsType{get;set;}
    public integer loadCurrency{get;set;}
    public String orderId{get;set;}
	public string batchName{get;set;}
	
    public EDGE_MultiLineOrder_Review_Controller() {
        
        step2 = false;
        
        //status = '';
        currencyString = '';
        //statusList = getInvoiceStatus();
        //currencyList =  getInvoiceCurrency();
        currentAccount = Utility.currentAccount;
        userDefaultCurrency = UserInfo.getDefaultCurrency();
        //createTransactionPermForSP = Utility_Security.canPayInvoices_SP;
        //createTransactionPermForH2H = Utility_Security.canPayInvoices_H2H;
        //gPId = new List<String>();
        userCurrency = Utility.userCurrency;
        /*for(Global_Pay_ID_Management__c gpRecord : [SELECT Global_Pay_ID__c FROM Global_Pay_ID_Management__c WHERE User_Beneficiary__c=:UserInfo.getUserId()]) {
            gPId.add(gpRecord.Global_Pay_ID__c);
        }*/
        
        if(!String.isEmpty(ApexPages.currentPage().getParameters().get('orderId'))) {
        	orderId = ApexPages.currentPage().getParameters().get('orderId');
        	searchInvoice();
        }
        
        
        //if(ApexPages.currentPage() != null && ApexPages.currentPage().getParameters().containsKey('xlsType')) {
        /*if(!String.isEmpty(ApexPages.currentPage().getParameters().get('xlsType'))) {
            xlsType = ApexPages.currentPage().getParameters().get('xlsType');
            requestType = xlsType;
            status = !String.isEmpty(ApexPages.currentPage().getParameters().get('status')) ? ApexPages.currentPage().getParameters().get('status') :'';
            currencyString = !String.isEmpty(ApexPages.currentPage().getParameters().get('currencyString')) ? ApexPages.currentPage().getParameters().get('currencyString') : '';
            searchInvoice();
        //}
        } else {
            conversionRate = 0.0;
            aggregateTotal = 0.0;
            hedgedTotal = 0.0;
            unHedgedTotal = 0.0;
            totalInHomeCurrency = 0.0;
            invoices = new List<InvoiceUIWrapper_MultiLine>();
        }   
        System.Debug('###'+currencyString);*/
    }        

    public Pagereference refreshInvoices() {
        //loadCurrency=1;
        toggleSort();
        return null;
    }

    public Pagereference resetInvoices() {
        status = '';
        currencyString = '';
        //loadCurrency=1;
        toggleSort();
        return null;
    }

    /*public List<SelectOption> payCurrencyOptions{
        get{
            
            String strQuery = 'Select count(Id), Custom_Currency__c FROM Invoice__c' ;
            String accountString = 'Account__c=\''+currentAccount+'\'';
            if(requestType=='Receivables') {
            accountString = '(Seller_Account__c=\''+currentAccount+'\' ' + 'OR (Supplier__c != null AND Supplier__r.Supplier__c=\''+currentAccount+'\'))';
            }
            strQuery +=' WHERE '+accountString+'  group by  Custom_Currency__c';
                    
            if(payCurrencyOptions==null){
                payCurrencyOptions = new List<SelectOption>();
                set<String> currencySet = new set<String>();
                payCurrencyOptions.add(new SelectOption('',Label.CM_EDGE_Select));
                for(AggregateResult Invoice: database.query(strQuery)){
                    String currencyCode = (String) Invoice.get('Custom_Currency__c');
                    if(!String.isBlank(currencyCode) && !currencySet.Contains(currencyCode)) {
                        currencySet.add(currencyCode);
                        payCurrencyOptions.add(new SelectOption(currencyCode,currencyCode));
                    }
                }
            }
            return payCurrencyOptions;
        }
        set;
    }

    public String currentUserLocale{
        get{
            String currentUserLocaleVal = UserInfo.getLocale();
            if(currentUserLocaleVal.contains('_')){
                currentUserLocaleVal = currentUserLocaleVal.substring(0, currentUserLocaleVal.indexOf('_'));
            }
            return currentUserLocaleVal;
        }
        set;
    }

    public String aggregateTotalFormat{
        get{
            String aggregateTotalVal = aggregateTotal.format();
            if(!aggregateTotalVal.contains(Utility.getDecimalChar)){
                aggregateTotalVal = aggregateTotal.format() + Utility.getDecimalChar+'00';
            }
            return aggregateTotalVal;
        }
        set;
    }

    public String hedgedTotalFormat{
        get{
            String hedgedTotalVal = hedgedTotal.format();
            if(!hedgedTotalVal.contains(Utility.getDecimalChar)){
                hedgedTotalVal = hedgedTotal.format() + Utility.getDecimalChar+'00';
            }
            return hedgedTotalVal;
        }
        set;
    }

    public String defaultValueFormat{
        get{
            Integer aTemp = 0;
            String efaultValue = aTemp.format();
            if(!efaultValue.contains(Utility.getDecimalChar)){
                efaultValue = aTemp.format() + Utility.getDecimalChar+'00';
            }
            return efaultValue;
        }
        set;
    }

    public String unHedgedTotalFormat{
        get{
            String unHedgedTotalVal = unHedgedTotal.format();
            if(!unHedgedTotalVal.contains(Utility.getDecimalChar)){
                unHedgedTotalVal = unHedgedTotal.format() + Utility.getDecimalChar+'00';
            }
            return unHedgedTotalVal;
        }
        set;
    }

    public String unHedgedTotalSettlementFormat{
        get{
            String unHedgedTotalSettlementVal = unHedgedTotalSettlement.format();
            if(!unHedgedTotalSettlementVal.contains(Utility.getDecimalChar)){
                unHedgedTotalSettlementVal = unHedgedTotalSettlement.format() + Utility.getDecimalChar+'00';
            }
            return unHedgedTotalSettlementVal;
        }
        set;
    }

    public Decimal aggregateTotalSettlement{
        get{
            return Utility.getSettlementCurrencyAmount(aggregateTotal, currencyString, UserInfo.getDefaultCurrency());
        }
    }
    public String aggregateTotalSettlementFormat{
        get{
            String aggregateTotalSettlementVal = aggregateTotalSettlement.format();
            if(!aggregateTotalSettlementVal.contains(Utility.getDecimalChar)){
                aggregateTotalSettlementVal = aggregateTotalSettlement.format() + Utility.getDecimalChar+'00';
            }
            return aggregateTotalSettlementVal;
        }
        set;
    }
    public Decimal hedgedTotalSettlement{
        get{
            return Utility.getSettlementCurrencyAmount(hedgedTotal, currencyString, Utility.userCurrency);
        }
    }
    public Decimal unHedgedTotalSettlement {
        get{
            return Utility.getSettlementCurrencyAmount(unHedgedTotal, currencyString, Utility.userCurrency);
        }
    }

    public List<String> getInvoiceStatus() {
         //List<SelectOption> options = new List<SelectOption>();
         //Map<String,String> optionsMap = new Map<String,String>();
         List<String> options = new List<String>();
         
         //optionsMap.put('',Label.CM_EDGE_Select);
         Schema.DescribeFieldResult fieldResult = Invoice__c.Status__c.getDescribe();
         List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
         if(Utility_Security.canCreateTransaction){
             for( Schema.PicklistEntry f : ple) {
                 if(f.getValue()!='Pending Approval (Open)' && f.getValue()!='Approved' && f.getvalue() != 'Paid'){
                  //options.add(new SelectOption(f.getValue(), f.getLabel()));
                  //optionsMap.put(f.getValue(), f.getLabel());
                  options.add(f.getLabel());
                 }
             }
         }
         else{
             for( Schema.PicklistEntry f : ple) {
                 if(f.getValue()=='Pending Approval (Open)' || f.getValue()=='Approved' || f.getvalue() == 'Paid' || f.getvalue() == 'Void'){
                  //options.add(new SelectOption(f.getValue(), f.getLabel()));
                  //optionsMap.put(f.getValue(), f.getLabel());
                  options.add(f.getLabel());
                 }
             }
         }
         return options;
    }

    public List<SelectOption> getInvoiceCurrency() {
         List<SelectOption> options = new List<SelectOption>();
         options.add(new SelectOption('',Label.CM_EDGE_Select));
         Schema.DescribeFieldResult fieldResult = Invoice__c.Custom_Currency__c.getDescribe();
         List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
         for( Schema.PicklistEntry f : ple) {
              options.add(new SelectOption(f.getLabel(), f.getValue()==currencyString ? 'Currenct('+currencyString+')':f.getValue()));
         }
         return options;
    }*/
    
    public void toggleSortAction() {
        loadCurrency = 0;
        toggleSort();
    }

    public void toggleSort() {
        // simply toggle the direction
        System.debug('1>> '+ loadCurrency);
        searchInvoice();
        //populateAmountField();
    }
    
    public Boolean step2{get;set;}
    //Review & Pay
    /*public PageReference reviewToPay() {
        if(!step2) {
            step2 = true;
            ApexPages.currentPage().getParameters().put('step','2');
            List<InvoiceUIWrapper_MultiLine> tempInvs = new List<InvoiceUIWrapper_MultiLine>();
            
            for(InvoiceUIWrapper_MultiLine inv: invoices) {
                if(inv.selected) tempInvs.add(inv);
            }
            invoices.clear();
            invoices.addAll(tempInvs);
        } else {
            PageReference pg = new PageReference('/EDGE_MultiLineOrderPayGP2?invoiceIds='+String.join(invoices, ','));
            String retURL = ----Use custom setting <CM_Site_Url>---+'/EDGE_MultiLineOrderStep1?step=1';
            pg.getParameters().put('retUrl', retURL);
            pg.setRedirect(true);
            return pg;
            //Payment posting to GP: logic to be updated
        }     
        return null;
    }
    
    public PageReference backToStep1() {
        step2 = false;
        ApexPages.currentPage().getParameters().put('step','1');
        searchInvoice();
        populateAmountField();
        return null;
    }*/
    
    public void searchInvoice(){        
        
        
            logInvoice.logMessage('>>inside searchInvoice method ...>> ');
            String strQuery = 'Select Name,Account__c,RecordType.Name,Custom_Currency__c,Delivery_Method__c,Type__c,Due_Date__c,Amount__c,Beneficiary_GP_ID__c,Beneficiary_Name__c,Beneficiary_GP_Name__c,Batch_Id__r.Name,'+
                              'Status__c,Buyer__c,Buyer__r.Name,TypeTrans__c,Buyer__r.Contact.AccountId,CreatedBy.Contact.Account.Name,CreatedBy.Name,Id, Invoice_Number__c, Account__r.Name, Reference_Number__c FROM Invoice__c' ;
            selectedStatusSet = new List<String>();
 
            String accountString = 'Account__c=\''+currentAccount+'\'';

            
            String selectedOrderId = '';
            if(!String.isEmpty(orderId)){
                selectedOrderId = ' AND Batch_Id__c =\''+orderId+'\' ';
            }
            
            strQuery +=' WHERE '+accountString+selectedOrderId+'  ORDER BY Due_Date__c DESC';
            System.debug('>> chkpoint 3>> '+ strQuery);
           //logInvoice.logDebug('requestType : '+strQuery);
            invoices = new List<InvoiceUIWrapper_MultiLine>();
            if(!String.isEmpty(orderId)){
	            for(Invoice__c Invoice: database.query(strQuery)){
	                if(Invoice.RecordTypeId != null)
	                    invoices.add(new InvoiceUIWrapper_MultiLine(Invoice));
	            }
            }
            
            if(invoices.size() > 0)
            	batchName = invoices[0].Invoice.Batch_Id__r.Name;
            
           // logInvoice.logDebug('requestType : '+invoices.size());
           // logInvoice.saveLogs();
            
    }
    
    public PageReference emptyAllInvoices(){
    	//this method will remove all the invoices from the batch.
    	List<Invoice__c> deleteBatchFromInvoiceList = new List<Invoice__c>();
    	for(InvoiceUIWrapper_MultiLine multiWrapper : invoices){
    		Invoice__c inv = multiWrapper.Invoice;
    		inv.Batch_Id__c = null;
    		deleteBatchFromInvoiceList.add(inv);
    	}
    	
    	if(deleteBatchFromInvoiceList.size() > 0){
    		update deleteBatchFromInvoiceList;
    	}
    	
    	searchInvoice();
    	
    	return new PageReference('/EDGE_MultiLine_Order_Review?orderId='+orderId);
    	
    }
    
    public PageReference editInvoices(){
    	return new PageReference('/EDGE_MultiLineOrderStep1?step=1');
    }
    
    public PageReference proceedToPay(){
    	Batch__c bh = [Select Id,GP2_Last_Order_Submitted_Date_Time__c,GP2_Last_Submitted_By__c  FROM Batch__c where Id = :orderId ];
      return EDGE_BatchService.submitBatchToGP2(bh);
    }
    
      public PageReference removeSelectedInvoices() {
        List<InvoiceUIWrapper_MultiLine> tempInvs = new List<InvoiceUIWrapper_MultiLine>();
        List<Invoice__c> removeSelectedInvoices = new List<Invoice__c>();
        
        for(InvoiceUIWrapper_MultiLine inv: invoices) {
            if(inv.selected == true){
            	 Invoice__c invoiceVar = inv.Invoice;
               invoiceVar.Batch_Id__c = null;
               removeSelectedInvoices.add(invoiceVar);
            }
        }
        
	      if(removeSelectedInvoices.size() > 0){
	        update removeSelectedInvoices;
	      }
      
        searchInvoice();
      
      return new PageReference('/EDGE_MultiLine_Order_Review?orderId='+orderId);
    } 

    /*public Pagereference populateAmountField() {
        aggregateTotal = 0.0;
        hedgedTotal = 0.0;
        unHedgedTotal = 0.0;
        totalInHomeCurrency = 0.0;
        Decimal hedgeAccount = 0.0;
        Decimal marketRate = 0.0;
        Decimal holdingBalance = 0.0;
        conversionRate = 0.0;
        currencyS = currencyString;
        if(currencyS == null ||  currencyS == '') {
            currencyS = userDefaultCurrency;
            //return null;
        }
        //conversionRate = [SELECT conversionrate FROM currencytype WHERE isocode =: selectedCurrency LIMIT 1].conversionRate;
        // Get Market rates for the selected currency
        for(Market_Rate__c rate : [SELECT Id, Currency_Code__c, Currency_Value__c
                                   FROM Market_Rate__c
                                   WHERE Currency_Code__c =:currencyS]) {
            conversionRate = rate.Currency_Value__c;
        }
        for(AggregateResult aggregateResult : [SELECT sum(FX_Balance__c) settlementAmount,
                                               avg(Base_USD__c) marketRate
                                               FROM Forward_Contracts__c
                                               WHERE Account_ID__c =: currentAccount
                                               AND Custom_Currency__c =: currencyS]) {

            if(aggregateResult.get('settlementAmount') != null)
            hedgeAccount = (Decimal)    aggregateResult.get('settlementAmount');
            if(aggregateResult.get('marketRate') != null)
            marketRate = (Decimal)    aggregateResult.get('marketRate');

        }
        //conversionRate = marketRate;
        for(AggregateResult aggregateResult : [SELECT sum(Amount__c) holdingBalance
                                               FROM Holding_Balance__c
                                               WHERE Account__c =: currentAccount
                                               AND Custom_Currency__c =: currencyS]) {

           if(aggregateResult.get('holdingBalance') != null)
           holdingBalance = (Decimal)   aggregateResult.get('holdingBalance');
         }
         hedgedTotal = hedgeAccount + holdingBalance;
         for(InvoiceUIWrapper_MultiLine invoiceRec: invoices){
            if(invoiceRec.Invoice.Custom_Currency__c == currencyS && invoiceRec.Invoice.Amount__c != null) {
                    aggregateTotal += invoiceRec.Invoice.Amount__c;
                }
         }
         if(conversionRate==0 || conversionRate<0){
            //Conversion market not found for selected currency.
            totalInHomeCurrency = aggregateTotal;
         }else{
            totalInHomeCurrency = aggregateTotal / conversionRate;
         }
         unHedgedTotal = aggregateTotal - hedgedTotal;
         return null;
    }*/

}