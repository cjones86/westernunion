@isTest
private class portalCashflowReportTest {

	static testMethod void testPortalCashflowReport (){	
		TestDataCreation.createTestData();

		PageReference pageRef = new PageReference('portalCashFlowReport');
		Test.setCurrentPage(pageRef);

		// Add parameters to page URL
		ApexPages.currentPage().getParameters().put('cId', TestDataCreation.con.Id);

		// **************************
		// *** Run code
		Test.startTest();
		
		portalCashflowReport psfTest = new portalCashflowReport();
		psfTest.getUserCurrency();
		psfTest.getUserName();
		psfTest.populateCurrencies();
		psfTest.currentCurrency();
		psfTest.getTabbedView();
		psfTest.exportToCSV();
		psfTest.disclaimerLink();
		
		Test.stopTest();
	}
}