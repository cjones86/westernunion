// 
// (c) 2014 Appirio, Inc.
//
// BeneficiaryEditController
// Used as Page Controller to show the details of Input Beneficiary object record
//
// 07 Apr, 2015    Ashish Goyal  Original (Ref. T-375697)
//
public without sharing class BeneficiaryEditController {
	
	public Input_Beneficiary__c inputRecord {get;set;}	
	public boolean escapeAssociation = false;
	public String closeUserPopup{get;set;}
	
	public BeneficiaryEditController(){
		closeUserPopup = '';
		if(ApexPages.currentPage().getParameters().get('Id') != null && ApexPages.currentPage().getParameters().get('Id') != ''){
	 		for(Input_Beneficiary__c tmpInv : getInputBene(ApexPages.currentPage().getParameters().get('Id')) ){
	 		    this.inputRecord = tmpInv;
	        }
		}
	}	 
	 
	 // GetInvoice with all the fields in the fieldset
    private List<Input_Beneficiary__c> getInputBene(String id) {
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : this.getFields()) {
            query += f.getFieldPath() + ', ';
        }
        String account = Utility.currentAccount;
        query += 'Id, OwnerId FROM Input_Beneficiary__c Where Id =: id LIMIT 1';
        return Database.query(query);
    }
    
    
    // Get fields from fieldset
	public List<Schema.FieldSetMember> getFields() {
        return SObjectType.Input_Beneficiary__c.FieldSets.Detail.getFields();
    }
    
    
    public map<String, string> LabelDetailMaps{
  		get{
  			//String apiName = Utility.changeTransactionType(selectedTranscation);
  				try{
	  				return Utility.getFieldLabel('Input_Beneficiary__c', 'Detail');
  				}catch(Exception Ex){
  					
  				}
  			return new map<String, string>();
  		}
  	}
  	
  	public void save(){
  		try{
  			update inputRecord;
  			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, System.Label.CM_Alert_BuyerSupplierDetailsUpdated));
         	closeUserPopup = 'self.close();';
  		}
  		catch(Exception ex){
  			ApexPages.addMessages(ex);
  		}
  	}

}