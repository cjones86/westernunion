public with sharing class EDGE_NetExposureChartsController {
	
	  public String currentAccount{get;set;} 
	  public String currentUserCurrency ;
	  LIST<Invoice__c> invList = new LIST<Invoice__c>();
	  Set<String> currenciesSet = new Set<String>();
	  public Map<String,String> mapCurrenyNetExposure = new Map<String,String>();
	  public Map<String,String> mapCurrenytotalHegding = new Map<String,String>();
	  public Map<String,String> mapCurrenytotalHegdingInstuments = new Map<String,String>();
	  
	  transient   public  LIST<Double>   outflowTotal{get;private set;}
    transient   public  LIST<Double>   inflowTotal{get;private set;}
    transient   public  LIST<Double>   hedgeTotalObligation{get;private set;}
    transient   public  LIST<Double>   hedgeTotalProtection{get;private set;}
    transient   public  LIST<Double>   hedgeTotal{get;private set;}
    transient   public  LIST<Double>   netHedgeTotal{get;private set;}
	
	
	
	public EDGE_NetExposureChartsController(){
		currentAccount = Utility.currentAccount;
    for(Invoice__c inv : [Select Id, Name,RecordType.Name, Account__c, Amount__c, Custom_Currency__c, Due_Date__c
			         From Invoice__c where Account__c =: currentAccount
			         And Due_Date__c = THIS_MONTH
              ]){
              	currenciesSet.add(inv.Custom_Currency__c);
              }
              
	 }
	
	public void fetchChartData(){
	      CashFlowComponentClone cashFlowCln = new CashFlowComponentClone();
	      for(String curr : currenciesSet){
	        cashFlowCln.cur = curr;
	        CashFlowComponentClone.selMonth = 1;
	        LIST<String> months = cashFlowCln.getMonths();
	        mapCurrenyNetExposure.put(curr,cashFlowCln.ztnetOperatingFXCashFlow[0]);
	        mapCurrenytotalHegding.put(curr,cashFlowCln.zthedgeTotal[0]);
	        mapCurrenytotalHegdingInstuments.put(curr,cashFlowCln.ztnetHedgeTotal[0]);
	      }
        
	}
	
	/*public void fetchChartData(){
		        currentAccount = Utility.currentAccount;
		        currentUserCurrency = Utility.userCurrency;
		        LIST<Invoice__c> invList = new LIST<Invoice__c>();//used as a temporary storage list to process the value into their own lists

            system.debug('Searching based on the account. Account has been passed in Invoice.');
            invList = [Select Id, Name,RecordType.Name, Account__c, Amount__c, Custom_Currency__c, Due_Date__c,
                        Status__c, Seller__c, Product__c
                        From Invoice__c where Account__c =: currentAccount
                        And Status__c not in 
                        (:Label.CM_Community_Text_FieldFilter_InvoiceStatus_Item_Void, :Label.CM_Community_Text_FieldFilter_InvoiceStatus_Item_Paid , :Label.CM_Edge_Partial_Payment,:Label.CM_Edge_Full_Payment,:Label.CM_Edge_Invoice_Submitted)
                        And Due_Date__c = THIS_MONTH
                        ];
                        
            for(Invoice__c i:invList)
              {
              addToInvoice(i, system.Today());
            }            
                        
	}
	
	    private void addToInvoice(Invoice__c i, Date od){
        Double c;
        integer mi;
        if(currentAccount != null)
        {
            User uc = Utility.loggedInUser;//classUser;
            system.debug('Adding Invoices based on the current account details '+od+ 'invoice due date >>>'+i.Due_Date__c);
            c = Double.valueOf(convert(double.valueOf(i.Amount__c), i.Custom_Currency__c, currentUserCurrency ));
            mi = od.monthsBetween(i.Due_Date__c);
        }
 
        if( i.Status__c!= Label.CM_Community_Text_FieldFilter_InvoiceStatus_Item_Void) {//Discussed with Ceri 21/June/16 : i.Status__c!= Label.CM_Edge_Draft &&
            
            //if(i.Status__c!= Label.CM_Edge_Draft && i.Status__c!= Label.CM_Community_Text_FieldFilter_InvoiceStatus_Item_Void) 

            //we want only for the current month
            outflowTotal[mi] += c;
            netHedgeTotal[mi] -= c;
            //netOperatingFXCashFlow[mi] -= c;
            //netFXPositionProtection[mi] -= c;
            //netFXPositionObligation[mi] -= c;

            //add to total

            //outflowTotal[totalCol] += c;
            //netHedgeTotal[totalCol] -= c;
            //netOperatingFXCashFlow[totalCol] -= c;
            //netFXPositionProtection[totalCol] -= c;
            //netFXPositionObligation[totalCol] -= c;
      }
    }
    
    private double convert(Double d, String c, String ic){
        return convert(d, '', c, ic);
    }
    private double convert(Double d, String convert, String c, String ic)
    {
        if((convert== Label.CM_Edge_Convert) &&  c != currentUserCurrency ){//Userinfo.getDefaultCurrency()
            d = Utility.getSettlementCurrencyAmount(d, c, ic);
        }
        return d;
    }*/
	

}