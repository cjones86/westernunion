public with sharing  class componentReport {

	public string cur {get;set;}
	public DateTime dt{get;set;}
	public MAP<String,MAP<Integer,double>> reportMap{get;set;}
	public MAP<Integer, String> monthMap{get;set;}
	public date td;
	public date oldestDate;
	public String currentCurrency{get;set;}
	public MAP<String, Double>mrMap;//Map of Market Rates
	
	//Breaking the maps out into types for better reporting
	public MAP<String,MAP<Integer,double>> outgoingMap{get;set;}
	public MAP<String,MAP<Integer,double>> incomingMap{get;set;}
	public MAP<String,MAP<Integer,double>> hedgeMap{get;set;}
	
	public LIST<String> months{get;set;}//Used to create the month columns in the report page.
	transient public LIST<Double> outflowTotal{get;private set;}
	transient public LIST<Double> inflowTotal{get;set;}
	transient public LIST<Double> hedgeTotalObligation{get;set;}
	transient public LIST<Double> hedgeTotalProtection{get;set;}
	transient public LIST<Double> netFXPositionProtection{get;set;}
	transient public LIST<Double> netFXPositionObligation{get;set;}
	transient public LIST<Double> netOperatingFXCashFlow{get;set;}
	public LIST<String> outgoingKey{get;set;}

	public componentReport()
	{
		updateMarketRate();
		//Initiate Lists
		outflowTotal = new LIST<Double>();
		inflowTotal = new LIST<Double>();
		hedgeTotalObligation = new LIST<Double>();
		hedgeTotalProtection = new LIST<Double>();
		netFXPositionProtection = new LIST<Double>();
		netFXPositionObligation = new LIST<Double>();
		netOperatingFXCashFlow = new LIST<Double>();
		
		
		//Fire functions to populate maps
		theMonthMap();
		dt = dateTime.now();
		oldestDate = theMonthMap(); //get the oldest date that needs to be displayed.
		monthSearch(oldestDate);
	}
	
	//Find the oldest month
	public Date theMonthMap()
	{
		monthMap = new MAP<Integer, String>();
		for(Input__c inp:[SELECT 	Id, Name, Amount__c, Type__c, Transaction_Date__c, CurrencyIsoCode, Paid__c, OwnerId
							FROM 	Input__c
							WHERE	Paid__c = false AND OwnerId =: UserInfo.getUserId()])
		{
			if(td == null)
			{
				td = inp.Transaction_Date__c;
			}
			else if(td > inp.Transaction_Date__c)
			{
				td = inp.Transaction_Date__c;
			}
		}
		return td;
	}
	
	//this function returns the List of Month Name to be displayed in the table. It is fired by a single number which is the month of the oldest date 
	public void monthSearch(Date m)
	{
		months = new LIST<String>();
		
		monthMap = new MAP<Integer, String>();
		monthMap.put(1,'Jan');
		monthMap.put(2,'Feb');
		monthMap.put(3,'Mar');
		monthMap.put(4,'Apr');
		monthMap.put(5,'May');
		monthMap.put(6,'Jun');
		monthMap.put(7,'Jul');
		monthMap.put(8,'Aug');
		monthMap.put(9,'Sept');
		monthMap.put(10,'Oct');
		monthMap.put(11,'Nov');
		monthMap.put(12,'Dec');
		
		Integer finalMonth = (m.monthsBetween(date.today())+12);
		Integer lmonth;
		Integer lYear;
		for(Integer i=0; i<finalMonth; i++)
		{
			lYear= m.addmonths(i).year();
			lmonth = m.month()+i;
			if(lmonth >12)
			{
				lmonth = lmonth-12;
			}
			months.add(String.valueOf(monthMap.get(lmonth)+' '+(String.valueOf(lYear).right(2))));
			outflowTotal.add(0);
			inflowTotal.add(0);
			netOperatingFXCashFlow.add(0);
			netFXPositionObligation.add(0);
			netFXPositionProtection.add(0);
			hedgeTotalObligation.add(0);
			hedgeTotalProtection.add(0);
		}
		
	}
	

	public MAP<String,MAP<Integer,double>> getOutgoing()
	{
		
		outgoingMap = new MAP<String,MAP<Integer,double>>();
		Integer tempd;
		Integer fMonth;
		
		//populate with actual values
		LIST<Input__c> inList = new LIST<Input__c>();
		
		if(cur != 'Total')
		{
			inList = [SELECT 	Id, Name, Amount__c, Type__c, Transaction_Date__c, CurrencyIsoCode, Paid__c,
									User__c, Sort_Order__c, Input_Type__c
							FROM 	Input__c
							WHERE	Paid__c=false 
									AND Transaction_Date__c != null 
									AND Transaction_Date__c >: oldestDate.addDays(-1) 
									AND CurrencyIsoCode =: cur
									AND OwnerId =: UserInfo.getUserId()
									AND (Input_Type__c =: 'Invoice' OR Input_Type__c =: 'Purchase Order' OR Input_Type__c =: 'Forecast Order' OR Input_Type__c =: 'Outflow Other')
									ORDER BY Sort_Order__c ASC];
		}else
		{
			inList = [SELECT 	Id, Name, Amount__c, Type__c, Transaction_Date__c, CurrencyIsoCode, Paid__c,
									User__c, Sort_Order__c, Input_Type__c
							FROM 	Input__c
							WHERE	Paid__c=false 
									AND Transaction_Date__c != null 
									AND Transaction_Date__c >: oldestDate.addDays(-1) 
									AND OwnerId =: UserInfo.getUserId()
									AND (Input_Type__c =: 'Invoice' OR Input_Type__c =: 'Purchase Order' OR Input_Type__c =: 'Forecast Order' OR Input_Type__c =: 'Outflow Other')
									ORDER BY Sort_Order__c ASC];
		}
		
		Integer finalMonth2 = (oldestDate.monthsBetween(date.today())+12);
		for(Integer i=0; i<finalMonth2; i++)
		{
			outflowTotal[i] = 0;
			inflowTotal[i] = 0;
			hedgeTotalObligation[i] = 0;
			hedgeTotalProtection[i] = 0;
			netOperatingFXCashFlow[i] = 0;
			netFXPositionObligation[i] = 0;
			netFXPositionProtection[i] = 0;
		}
		
		for(Input__c inp:inList)
		{
			tempd = oldestDate.monthsBetween(inp.Transaction_Date__c);//Find where to place this in the map
			fMonth = ((oldestDate.monthsBetween(date.today()))+12);//find how far the map actually goes
			double dub = 0;
			//Add this record to the existing Map, Else Create the line in the map
			if(outgoingMap.containsKey(inp.Type__c))
			{
				if(outgoingMap.get(inp.Type__c).containsKey(tempd))
				{
					dub = outgoingMap.get(inp.Type__c).get(tempd)+ convert(double.valueof(inp.Amount__c));
					outgoingMap.get(inp.Type__c).put(tempd,dub);
					
				}
			}
			else
			{
				outgoingMap.put(inp.Type__c,new MAP<Integer,double>());
				//This is where we use our month extremities to define the columns in the map/table
				for(Integer i=0; i<fMonth; i++)
				{
					outgoingMap.get(inp.Type__c).put(i,0);
				}
				if(outgoingMap.get(inp.Type__c).containsKey(tempd))
				{
					dub = outgoingMap.get(inp.Type__c).get(tempd)+ convert(double.valueof(inp.Amount__c));
					outgoingMap.get(inp.Type__c).put(tempd,dub);
				}
			}
			
			outflowTotal[tempd] += convert(double.valueof(inp.Amount__c));
			netOperatingFXCashFlow[tempd] -= dub;
			netFXPositionObligation[tempd] -= dub;
			netFXPositionProtection[tempd] -= dub;
		}
		return outgoingMap;
	}
	
	public MAP<String,MAP<Integer,double>> getIncoming()
	{
		incomingMap = new MAP<String,MAP<Integer,double>>();
		Integer tempd;
		Integer fMonth;
		LIST<Input__c> incList = new LIST<Input__c>();
		
		if(cur != 'Total')
		{
			incList = [SELECT 	Id, Name, Amount__c, Type__c, Transaction_Date__c, CurrencyIsoCode, Paid__c,
									User__c, Sort_Order__c, Input_Type__c
							FROM 	Input__c
							WHERE	Paid__c=false AND Transaction_Date__c != null 
									AND Transaction_Date__c >: oldestDate.addDays(-1) 
									AND OwnerId =: UserInfo.getUserId()
									AND CurrencyIsoCode =: cur
									AND (Input_Type__c =: 'Trading Receipts' OR Input_Type__c =: 'Inflow - Other')
									ORDER BY Sort_Order__c ASC];
		}else
		{
			incList = [SELECT 	Id, Name, Amount__c, Type__c, Transaction_Date__c, CurrencyIsoCode, Paid__c,
									User__c, Sort_Order__c, Input_Type__c
							FROM 	Input__c
							WHERE	Paid__c=false AND Transaction_Date__c != null 
									AND Transaction_Date__c >: oldestDate.addDays(-1) 
									AND OwnerId =: UserInfo.getUserId()
									AND (Input_Type__c =: 'Trading Receipts' OR Input_Type__c =: 'Inflow - Other')
									ORDER BY Sort_Order__c ASC];
		}
		
		
		//populate with actual values
		for(Input__c inp: incList)
		{
			tempd = oldestDate.monthsBetween(inp.Transaction_Date__c);//Find where to place this in the map
			fMonth = ((oldestDate.monthsBetween(date.today()))+12);//find how far the map actually goes
			double dub = 0;
			
			//Add this record to the existing Map, Else Create the line in the map
			if(incomingMap.containsKey(inp.Type__c))
			{
				if(incomingMap.get(inp.Type__c).containsKey(tempd))
				{
					dub = incomingMap.get(inp.Type__c).get(tempd)+ convert(double.valueof(inp.Amount__c));
					incomingMap.get(inp.Type__c).put(tempd,dub);
					inflowTotal[tempd] = dub;
					netOperatingFXCashFlow[tempd] += dub;
					netFXPositionObligation[tempd] += dub;
					netFXPositionProtection[tempd] += dub;
				}
			}
			else
			{
				incomingMap.put(inp.Type__c,new MAP<Integer,double>());
				
				//This is where we use our month extremities to define the columns in the map/table
				for(Integer i=0; i<fMonth; i++)
				{
					incomingMap.get(inp.Type__c).put(i,0);
				}
				if(incomingMap.get(inp.Type__c).containsKey(tempd))
				{
					dub = incomingMap.get(inp.Type__c).get(tempd)+ convert(double.valueof(inp.Amount__c));
					incomingMap.get(inp.Type__c).put(tempd,dub);
					inflowTotal[tempd] += dub;
					netOperatingFXCashFlow[tempd] += dub;
					netFXPositionObligation[tempd] += dub;
					netFXPositionProtection[tempd] += dub;
				}
			}
		}
		return incomingMap;
	}
	
	
	public MAP<String,MAP<Integer,double>> getHedging()
	{
		hedgeMap = new MAP<String,MAP<Integer,double>>();
		Integer tempd;
		Integer fMonth;
		system.debug('ggggggggggggggggggggggggggggggggggggg '+cur);
		LIST<Input__c> hedList = new LIST<Input__c>();
		
		if(cur != 'Total')
		{
			hedList = [SELECT 	Id, Name, Amount__c, Type__c, Transaction_Date__c, CurrencyIsoCode, Paid__c,
									User__c, Sort_Order__c, Input_Type__c
							FROM 	Input__c
							WHERE	Paid__c=false AND Transaction_Date__c != null 
									AND Transaction_Date__c >: oldestDate.addDays(-1) 
									AND OwnerId =: UserInfo.getUserId()
									AND CurrencyIsoCode =: cur
									AND (Input_Type__c LIKE: 'Forward%' OR Input_Type__c LIKE: 'Option%')
									ORDER BY Sort_Order__c ASC];
		}else
		{
			hedList = [SELECT 	Id, Name, Amount__c, Type__c, Transaction_Date__c, CurrencyIsoCode, Paid__c,
									User__c, Sort_Order__c, Input_Type__c
							FROM 	Input__c
							WHERE	Paid__c=false AND Transaction_Date__c != null 
									AND Transaction_Date__c >: oldestDate.addDays(-1) 
									AND OwnerId =: UserInfo.getUserId()
									AND (Input_Type__c LIKE: 'Forward%' OR Input_Type__c LIKE: 'Option%')
									ORDER BY Sort_Order__c ASC];
		}
		
		//populate with actual values
		for(Input__c inp:hedList)
		{
			tempd = oldestDate.monthsBetween(inp.Transaction_Date__c);//Find where to place this in the map
			fMonth = ((oldestDate.monthsBetween(date.today()))+12);//find how far the map actually goes
			double dub = 0;
			
			//Add this record to the existing Map, Else Create the line in the map
			if(hedgeMap.containsKey(inp.Type__c))
			{
				if(hedgeMap.get(inp.Type__c).containsKey(tempd))
				{
					dub = hedgeMap.get(inp.Type__c).get(tempd)+ convert(double.valueof(inp.Amount__c));
					hedgeMap.get(inp.Type__c).put(tempd,dub);
					
					if(inp.Input_Type__c == 'Forward Contract - Other'||inp.Input_Type__c == 'Forward Contract - WUBS' || inp.Type__c == 'Option Protection - WUBS'||inp.Type__c == 'Option Protection - Other')
					{
						hedgeTotalProtection[tempd] = (0+dub);
						netFXPositionProtection[tempd]  = (0+dub);
					}else if(inp.Input_Type__c == 'Forward Contract - Other'||inp.Input_Type__c == 'Forward Contract - WUBS' || inp.Type__c == 'Option Obligation - WUBS'||inp.Type__c == 'Option Obligation - Other')
					{
						hedgeTotalObligation[tempd]  = (0+dub);
						netFXPositionObligation[tempd]  = (0+dub);
					}
				}
			}
			else
			{
				hedgeMap.put(inp.Type__c,new MAP<Integer,double>());
				
				//This is where we use our month extremities to define the columns in the map/table
				for(Integer i=0; i<fMonth; i++)
				{
					hedgeMap.get(inp.Type__c).put(i,0);
				}
				if(hedgeMap.get(inp.Type__c).containsKey(tempd))
				{
					dub = hedgeMap.get(inp.Type__c).get(tempd)+ convert(double.valueof(inp.Amount__c));
					hedgeMap.get(inp.Type__c).put(tempd,dub);
					
					if(inp.Input_Type__c == 'Forward Contract - Other'||inp.Input_Type__c == 'Forward Contract - WUBS' || inp.Type__c == 'Option Protection - WUBS'||inp.Type__c == 'Option Protection - Other')
					{
						hedgeTotalProtection[tempd] = dub;
						netFXPositionProtection[tempd] += dub;
					}else if(inp.Input_Type__c == 'Forward Contract - Other'||inp.Input_Type__c == 'Forward Contract - WUBS' || inp.Type__c == 'Option Obligation - WUBS'||inp.Type__c == 'Option Obligation - Other')
					{
						hedgeTotalObligation[tempd] += dub;
						netFXPositionObligation[tempd] += dub;
					}
				}
			}
		}
		return hedgeMap;
	}
	
	
	//This function will populate the Settings records for this user with the latest currency values
	public void updateMarketRate()
	{
		
		mrMap = new MAP<String, Double>();
		for(Market_Rate__c mr:[SELECT 	Id, Currency_Code__c, Currency_Name__c, Currency_Value__c, Name
								FROM	Market_Rate__c])
		{
			if(!mrMap.containsKey(mr.Currency_Code__c))
			{
				mrMap.put(mr.Currency_Code__c, mr.Currency_Value__c);
				system.debug('7777777777777777777777 THE Currency Code Is: '+mr.Currency_Code__c);
			}
		}
	}
	
	private double convert(Double d)
	{
		system.debug('1111111111111111111 THE Currency Code Is: '+cur);
		if(mrMap.containsKey(cur))
		{
			d = d*mrMap.get(cur);
		}
		
		return d;
	}
}