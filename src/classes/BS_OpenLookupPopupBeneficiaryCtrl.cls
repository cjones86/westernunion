/**=====================================================================
 * Name: BS_OpenLookupPopupBeneficiaryCtrl
 * Description: Controller class for BS_OpenLookupPopupBeneficiary page
 * Created Date: Feb 01, 2016
 * Created By: Nikhil Sharma
 * Task      : T-469137
 =====================================================================*/
public without sharing class BS_OpenLookupPopupBeneficiaryCtrl {

    public List<Global_Pay_ID_Management__c> listRecords {get;set;}
    public List<String> listFieldNames {get; set;}
    public String selectedObject;
    public String fieldName;
    public String searchText{get;set;}
    public String fieldSetName;
    public String deliveryMethod{get;set;}
    public String beneName {get;set;}
    public String beneId {get;set;}
    Public String selectedUsrId {get;set;}
    public boolean lookBuyer{get;set;}
    public List<User> listBuyerRecords{get;set;}
    public List<NetWorkUserWrapper> netWrapper{get;set;}

    public BS_OpenLookupPopupBeneficiaryCtrl(){
        System.debug('********** 1) BS_OpenLookupPopupBeneficiaryCtrl **********');
        deliveryMethod = ApexPages.currentPage().getParameters().get('type');
        lookBuyer = false;
        if(ApexPages.currentPage().getParameters().get('lookBuyer')!=null) {
            lookBuyer = Boolean.valueOf(ApexPages.currentPage().getParameters().get('lookBuyer'));
        }
        listRecords = new List<Global_Pay_ID_Management__c>();
        if(!lookBuyer) {
            runSearch();
            System.debug('********** 2a) runSearch() **********');
            }
        else {
            runBuyerSearch();
            System.debug('********** 2b) runBuyerSearch() **********');
            }
    }


    public void setBeneficiary(){

     Id usrId =Apexpages.currentPage().getParameters().get('userRecord');
     System.debug('********** 3) setBeneficiary() **********');
     System.debug('usrId:::: '+ usrId);

     Id accId;
     for(User us: [Select Id, AccountId From user Where Id=:usrId]) {
         accId = us.AccountId;
         System.debug('********** 4) setBeneficiary() - accID = ' + accId + ' **********');
         }


     for(Global_Pay_ID_Management__c gpm: [Select Id,User_Beneficiary__r.Name, Global_Pay_ID__c From Global_Pay_ID_Management__c
                                              Where Account_Beneficiary__c=:accId
                                              And Source_Invoice_Buyer_Account__c=:Utility.currentAccount]){

            beneName = gpm.User_Beneficiary__r.Name;
            System.debug('********** 5a) setBeneficiary() - beneName = ' + beneName + ' **********');
            beneId = gpm.Global_Pay_ID__c;
            System.debug('********** 5b) setBeneficiary() - beneId = ' + beneId + ' **********');

      }

    }

    /********************************************************************************************************************
    * Method to run the search with parameters passed via Page Parameters
    *********************************************************************************************************************/
    public PageReference runSearch() {
		netWrapper = new List<NetWorkUserWrapper>();
        // String fieldsToFetch = getFieldsForSelectedObject();
        String whereClause = '';
        //   String currentAccount = Utility.currentAccount;
        User us = Utility.loggedinUser;

        List<User> listUsers = new List<User>();

        System.debug('********** 6a) runSearch() - deliveryMethod = ' + deliveryMethod + ' **********');

        Set<String> accIds = new Set<String>();
        Set<Id> usrIds = new Set<Id>();
        Set<Id> cntIds = new Set<Id>();
        map<Id,datetime> accNetWorkId = new map<Id,datetime>();
        List<String> activeStatus = new List<String>{'3 - Active (Accepted)'}; //'1 - Pending (Sent)'
        User currenUser = Utility.loggedinUser;
        for(Network__c net: [Select Id, Inviter_User__c,Account_Inviter__c,Account_Invitee__c, Invitee_Contact__c, Status__c,CreatedDate from Network__c
                                Where (Account_Inviter__c = :currenUser.Contact.AccountId
                                OR Account_Invitee__c = :currenUser.Contact.AccountId)
                                AND Status__c IN :activeStatus]){
                 usrIds.add(net.Inviter_User__c);
                 cntIds.add(net.Invitee_Contact__c);
                 if(net.Account_Inviter__c!=Utility.currentAccount)
                 	accNetWorkId.put(net.Account_Inviter__c,net.CreatedDate);
                 if(net.Account_Invitee__c!=Utility.currentAccount)
                 	accNetWorkId.put(net.Account_Invitee__c,net.CreatedDate);
            System.debug('********** 6b) Inside for - net = ' + net + ' **********');
        }

        If(deliveryMethod == 'StandardPayment'){
            System.debug('********** 6c) Inside Standard Payment **********');
            for(user usr : [Select Id, Name,  Contact.AccountId,
                                ContactId, Contact.Account.CMP_Enabled__c  From User
                                Where CMP_Enabled__c = true
                                And Contact.Account.CMP_Enabled__c=true
                                And Contact.Account.CMP_Create_Transactions__c=true
                                And CMP_Create_Transactions__c = true
                                And (Id IN :usrIds OR ContactId IN: cntIds)
                                ]){

                 if(usr.ContactId!=null && usr.Contact.AccountId != null){
                     String accId = (String) usr.Contact.AccountId;
                     System.debug('********** 6d) runSearch() - accId = ' + accId + ' **********');
                     accIds.add(accId.substring(0,15));
                 }
             }
             String currentAccId = (String)Utility.currentAccount;
	         if(currentAccId!=null) {
		        for(Global_Pay_ID_Management__c gpm: [Select Id,User_Beneficiary__c,User_Beneficiary__r.Name, User_Beneficiary__r.Street, User_Beneficiary__r.City, User_Beneficiary__r.State, User_Beneficiary__r.Country, User_Beneficiary__r.PostalCode, Global_Pay_ID__c From Global_Pay_ID_Management__c
		                                              Where Account_Beneficiary__c IN :accIds
		                                              And Source_Invoice_Buyer_Account__c=:currentAccId.substring(0,15)]){

		            listRecords.add(gpm);

		      	}
	         }
        } else if(accNetWorkId.size()>0){
            System.debug('********** 6e) Outside Standard Payment (Holding-to-Holding) **********');
            for(Account acc : [SELECT Id,Name,CCT_Client_ID__c,BillingStreet,BillingCity,BillingState,BillingCountry,BillingPostalCode FROM Account
            				  WHERE CMP_Enabled__c=true AND CMP_Create_Transactions__c=true AND CMP_Holding_Enabled__c = true AND
            				  CMP_H2H_Transaction_Enabled__c = true AND ID IN : accNetWorkId.keySet() AND CCT_Client_ID__c!=NULL]) {
            	netWrapper.add(new NetWorkUserWrapper(acc,accNetWorkId.get(acc.Id)));
            }
            /*for(User usr : [Select Id, Name, AccountId,ContactId,Contact.AccountId From User
                                Where CMP_Enabled__c = true
                                And Contact.Account.CMP_Enabled__c=true
                                And Contact.Account.CMP_Create_Transactions__c=true
                                And CMP_Create_Transactions__c = true
                                And Contact.Account.CMP_Holding_Enabled__c = true
                                And Contact.Account.CMP_H2H_Transaction_Enabled__c = true
                                And (Id IN :usrIds OR ContactId IN: cntIds)]){
                if(usr.ContactId != null && usr.Contact.AccountId != null){
                     String accId = (String) usr.Contact.AccountId;
                     System.debug('********** MP) runSearch() - us = ' + us + ' **********');
                     System.debug('********** 6f) runSearch() - accId = ' + accId + ' **********');
                     accIds.add(accId.substring(0,15));
                }
            }*/
        }

       // System.assert(false, 'accIds::: '+accIds);
        /*Set<Id> userIds = new Set<Id>();
        String currentAccId = (String)Utility.currentAccount;
        System.debug('********** 6ga) runSearch() - currentAccId = ' + currentAccId + ' **********');
        System.debug('********** 6gb) runSearch() - accIds = ' + accIds + ' **********');
        if(currentAccId!=null) {
            /* Marc Powell 3/22/16 - Commenting out to test different logic below
            for(Global_Pay_ID_Management__c gpm: [Select Id,User_Beneficiary__c,User_Beneficiary__r.Name, User_Beneficiary__r.Street, User_Beneficiary__r.City, User_Beneficiary__r.State, User_Beneficiary__r.Country, User_Beneficiary__r.PostalCode, Global_Pay_ID__c
                                                  From Global_Pay_ID_Management__c
                                                  Where Account_Beneficiary__c IN :accIds
                                                  And Source_Invoice_Buyer_Account__c=:currentAccId.substring(0,15)]){

                listRecords.add(gpm);
                System.debug('********** 6h1) runSearch() - gpm = ' + gpm + ' **********');
            }
            */
            /*for(Global_Pay_ID_Management__c gpm: [Select Id,User_Beneficiary__c,User_Beneficiary__r.Name, User_Beneficiary__r.Street, User_Beneficiary__r.City, User_Beneficiary__r.State, User_Beneficiary__r.Country, User_Beneficiary__r.PostalCode, Global_Pay_ID__c
                                                  From Global_Pay_ID_Management__c
                                                  Where Account_Beneficiary__c IN :accIds]){

                listRecords.add(gpm);
                System.debug('********** 6h1) runSearch() - gpm = ' + gpm + ' **********');
            }
        }*/
        return null;
    }

    public void runBuyerSearch() {
        User currentUser =Utility.loggedInUser;
        listBuyerRecords = new List<User>();
        System.debug('********** 7a) runBuyerSearch() **********');
        List<String> activeStatus = new List<String>{'3 - Active (Accepted)'};
        System.debug('********** 7b) runBuyerSearch() - activeStatus = ' + activeStatus + ' **********');
        /*
        for(Network__c net: [Select Id, Inviter_User__r.Id,Inviter_User__r.Name,Inviter_User__r.Contact.AccountId from Network__c
                                Where Invitee_Contact__c = :currenUser.ContactId
                                AND Status__c IN :activeStatus]){
                 listBuyerRecords.add(net.Inviter_User__r);
        }
        */

        Set<Id> contactIds = new Set<Id>();
        for(Network__c net : [Select Id, Inviter_User__c, Invitee_Contact__c, Status__c, Status_DateTime__c, Type__c,
                                    Invitee_Contact__r.Name, Invitee_Contact__r.Account.Name,Inviter_User__r.Contact.AccountId,
                                    Inviter_User__r.ContactId, LastModifiedBy.Contact.AccountId,Invitee_Contact__r.AccountId,Inviter_User__r.Name
                                    From Network__c
                                    Where ((Account_Inviter__c = :currentUser.Contact.AccountId And Account_Invitee__c != null)
                                        OR (Account_Invitee__c = :currentUser.Contact.AccountId And Account_Inviter__c != null))
                              			And Status__c IN :activeStatus And Invitee_Contact__c!=null]){
                  if(net.Inviter_User__c == currentUser.Id)
                      contactIds.add(net.Invitee_Contact__c);
                  else
                      contactIds.add(net.Inviter_User__r.ContactId);
        }
        System.debug('********** 7c) runBuyerSearch() - contactIds = ' + contactIds + ' **********');

        for(User usr: [Select Id, Name, Street, City, State, Country, PostalCode, Contact.AccountId, ContactId, Contact.Account.Name From User Where ContactId IN :contactIds]){
            listBuyerRecords.add(usr);
            System.debug('********** 7c) runBuyerSearch() - usr = ' + usr + ' **********');
        }
    }

    public without sharing class NetWorkUserWrapper {
    	public String accountName{get;set;}
    	public String address{get;set;}
    	public datetime CreatedDate{get;set;}
    	public String CCTclientId{get;set;}

    	public NetWorkUserWrapper(Account acc,datetime networkdate) {
    		accountName = acc.Name;
    		address = acc.BillingStreet+ (acc.BillingCity!=null?', '+acc.BillingCity:'')+(acc.BillingState!=null?', '+acc.BillingState:'')+(acc.BillingCountry!=null?', '+acc.BillingCountry:'')+(acc.BillingPostalCode!=null?', '+acc.BillingPostalCode:'');
    		CreatedDate = networkdate;
    		CCTclientId = acc.CCT_Client_ID__c;
    	}
    }
}