public with sharing class EDGE_Modal_User_Change_PasswordCtrl{
    
    static apexLogHandler.apexLog logChangePassword = new apexLogHandler.apexLog('Modal_User_Change_Pw','changePw');
    
    public String password {get; set;}
    public String rePassword {get; set;}
    public String errorMessage{get;set;}
    public String successMessage{get;set;}
    public EDGE_Modal_User_Change_PasswordCtrl(){
    }
    
    public PageReference changePassword(){
        logChangePassword.logMessage('>>inside changePassword method ...>> ');
        boolean isvalidate = false;
        errorMessage='';
        successMessage='';
        if(String.isEmpty(password)){
            isvalidate = true;
            errorMessage =  (String.isEmpty(errorMessage)? '' : '<br/>') + System.Label.CM_Validation_User_PleaseEnterValueForPassword;
            // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CM_Validation_User_PleaseEnterValueForPassword));
        }
        if(String.isEmpty(rePassword)){
            isvalidate = true;
            errorMessage =  (String.isEmpty(errorMessage)? '' : '<br/>') + System.Label.CM_Validation_User_PleaseEnterValueForConfirmPassword;
            // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CM_Validation_User_PleaseEnterValueForConfirmPassword));
        }
        if(password.length() < 8){
            isvalidate = true;
            errorMessage =  (String.isEmpty(errorMessage)? '' : '<br/>') + System.Label.CM_Alert_User_PasswordInvalid;
            // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CM_Alert_User_PasswordInvalid));
        }
        if(isvalidate){
            logChangePassword.saveLogs();
            return null;
        }
        else{
            if(password.equals(rePassword)){
                try{             
                    System.setPassword(UserInfo.getUserId(), password); 
                    successMessage =  System.Label.CM_Modal_User_Password_Updated;
                    // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, Label.CM_Modal_User_Password_Updated));
                    // also set the dateTime field, when password is changed.
                    User u = [SELECT Last_Password_Changed__c FROM User WHERE Id = :UserInfo.getUserId()];
                    u.Last_Password_Changed__c = system.now();
                    update u;
                    logChangePassword.saveLogs();
                    return null;
                }
                catch(Exception ex){
                    //ApexPages.addMessages(ex);
                    errorMessage =  (String.isEmpty(errorMessage)? '' : '<br/>') + ex.getMessage().removeStartIgnoreCase('UNKNOWN_EXCEPTION: ');
                    // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage().removeStartIgnoreCase('UNKNOWN_EXCEPTION: ')));
                    logChangePassword.logDebug('Inside catch block');
                    logChangePassword.saveLogs();
                    return null;
                }   
            }
            else{
                errorMessage =  (String.isEmpty(errorMessage)? '' : '<br/>') + System.Label.CM_Alert_User_PasswordsDoNotMatch;
                // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.CM_Alert_User_PasswordsDoNotMatch));
                logChangePassword.saveLogs();
                return null;
            }    
        }   
    }
}