/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_InvoiceDetailController
 * Description: Controller class for Invoice Edit functionality
 * Created Date: 16 Feb' 2016
 * Created By: Rohit Sharma (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/

 public without sharing class EDGE_InvoiceDetailController {
     
    static apexLogHandler.apexLog logSave = new apexLogHandler.apexLog('InvoiceDetailCtrl','save');
    static apexLogHandler.apexLog logBeneinNetwork = new apexLogHandler.apexLog('InvoiceDetailCtrl','BeneinNetwork');
    static apexLogHandler.apexLog logEditInvoice = new apexLogHandler.apexLog('InvoiceDetailCtrl','editInvoice');

    public Invoice__c invoiceRecord{get;set;}
    public boolean edit{get;set;}
    public boolean canEdit{get;set;}
    public CMP_Administration__c cmpAdmin{get;set;}
    public Boolean displayPopup {get; set;}
    public boolean intigrationON{get;set;}
    public Boolean displayInvite {get; set;}
    public boolean isUserH2H{get;set;}
    public Boolean isDisplayBenePopUp{get;set;}
    public String latestContentId{get;set;}
    public String beneName{get;set;}
    public String beneIdH2H{get;set;}
    public boolean initiatedbySeller{get;set;}
    public String errorMessage{get;set;}
    public String beneNumber{get;set;}
    public Id accId {get;set;}
    public Id deleteAlertId{get;set;}
    public PageReference returnurl;
    public Decimal settlementAmt{get;set;}
    public boolean isSaveAndPayClicked{get;set;}
    public String hbCurrencyISO{get;set;}
    public List<SelectOption> H2Hcurrencies {get;set;}
    public boolean isSaveAndAddBeneClicked{get;set;}
    public boolean isInvSaved{get;set;}
    public list<SupplierWrapper> listSuppliers{get;set;}
    public Map<String,String> mapSuppIdAndAddress{get;set;}
    public string selectedSuppId{get;set;}
    public string selectedDelType{get;set;}
    public Map<String,boolean> mapSuppIdAndIsH2HEnabled{get;set;}
    public string selectedSuppKeyValue{get;set;}
    public string userHomeCurrency{
    	get{
    		return UserInfo.getDefaultCurrency();
    	}
    	set;}
    public Set<String> h2hCurrencySet{get;set;}
    public Set<String> stdCurrencySet{get;set;}
    public String invoiceIdForEmailTemplate{get;set;}
     

    public void closeBenePopup() {
        isDisplayBenePopUp = false;
    }
    public void showBenePopUp() {
        isDisplayBenePopUp = true;
    }
    public String encodedInvoiceId{get;set;}
 

    public List<SelectOption> getdelTypes(){
        List<SelectOption> optns = new List<Selectoption>();
        Schema.DescribeFieldResult fieldResult = invoice__c.Delivery_Method__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
                    if(f.getLabel() !=  'N/A')
            optns.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
         return optns;
        //optns.add(new selectOption( 'Holding-to-Holding',Label.Edge_DeliveryMethod_StandardPayment));
        //optns.add(new selectOption( 'Standard Payment',Label.Edge_DeliveryMethod_HoldingToHolding));
        //return optns;
    }

    public List<SelectOption> getopenPresentationOptions(){
        List<SelectOption> optns = new List<Selectoption>();
        mapSuppIdAndAddress = new Map<String,String>();
        mapSuppIdAndIsH2HEnabled = new Map<String,boolean>();
        fetchSupplierRecords();
        // before getting here you must populate your queryResult list with required fields
        for(SupplierWrapper obj : listSuppliers){
            
            System.debug('Hello'+obj.name +'helllo'+obj.address);
           if(obj.name != null && String.isNotBlank(obj.address)){
              String opt = obj.name;//  + ', '+ obj.address;
              String optId = obj.Id + '@'+ obj.canPayH2H + '@' + obj.isNotNetworked;
              if(obj.Id == selectedSuppId)
              	 selectedSuppKeyValue = optId;
              optns.add(new selectOption(optId , opt));
              mapSuppIdAndAddress.put(optId,obj.address);
              mapSuppIdAndIsH2HEnabled.put(optId,obj.canPayH2H);
           }
        }
        return optns;
    }


    // method to fetch supplier records based on searched text and current user's account
    public void fetchSupplierRecords(){
        listSuppliers = new list<SupplierWrapper>();
        for(Supplier__c sup : [SELECT Id, Name,Address_line_1__c,Address_Line_2__c,Buyer__c,City__c,Country__c,Post_Code__c,State_Province__c,
                                      Supplier__c,Network_Status__c,Supplier_Name__c,Supplier__r.CMP_H2H_Transaction_Enabled__c,
                                      CreatedDate,Created_Invoice_Date__c
                                FROM Supplier__c WHERE Buyer__c = :Utility.CurrentAccount]){
                 listSuppliers.add(new SupplierWrapper(sup));
        }
        listSuppliers.sort();
    }

    public class SupplierWrapper implements Comparable{
        public Supplier__c supp{get;set;}
        public String id{get;set;}
        public String address{get;set;}
        public String name{get;set;}
        public Boolean canPayH2H{get;set;}
        public boolean isIntegrationEnabled = Utility_Security.isIntegrationEnabled;
        public DateTime dtTime{get;set;}
        public boolean isNotNetworked{get;set;}
        public SupplierWrapper(Supplier__c sup){
            this.supp = sup;
            this.dtTime = sup.CreatedDate;
            if(sup.Created_Invoice_Date__c != null){
                this.dtTime = sup.Created_Invoice_Date__c;
            }
            if(sup.Network_Status__c.containsIgnoreCase('Not Currently Networked'))
                isNotNetworked = true;
            if(isIntegrationEnabled && sup.Supplier__c != null && sup.Network_Status__c.containsIgnoreCase('Accepted') && sup.Supplier__r.CMP_H2H_Transaction_Enabled__c)
                this.canPayH2H = true;
            else
                this.canPayH2H = false;
            this.id=sup.id;
            this.name = sup.Supplier_Name__c;
            this.address = getFullAddress(sup.Address_line_1__c,sup.Address_Line_2__c,sup.City__c,sup.State_Province__c,sup.Country__c,sup.Post_Code__c);
        }

        public Integer compareTo(Object compareTo) {
            SupplierWrapper supWrpr = (SupplierWrapper)compareTo;
            if(dtTime > supWrpr.dtTime)    return -1;
            if(dtTime < supWrpr.dtTime)    return 1;
            return 0;
        }

        public string getFullAddress(String addline1,String addline2,String city,String state,String country, String postalCode){
            String address = '';
            if(String.isNotBlank(addline1))
             address = addline1 + ', ';
            if(String.isNotBlank(addline2))
             address += addline2 + ', ';
            if(String.isNotBlank(city))
             address += city + ', ';
            if(String.isNotBlank(state))
             address += state + ', ';
            if(String.isNotBlank(country))
             address += country + ', ';
            if(String.isNotBlank(postalCode))
             address += postalCode;
            return address;
        }
    }
	//Action-Init method for page
    public EDGE_InvoiceDetailController() {
        errorMessage = '';
        isSaveAndPayClicked = false;
        isInvSaved = false;
        
        newAlertThreshold = new Alert_Threshold__c();
        System.debug('### get(Id)' + ApexPages.currentPage().getParameters().get('id'));
        //String invoiceId = EncryptionManager.doDecrypt(EncodingUtil.urlDecode(ApexPages.currentPage().getParameters().get('id'), 'UTF-8'));
        String invoiceId=null;
        if(ApexPages.currentPage().getParameters().get('id') !=null){
        	invoiceId = EncryptionManager.doDecrypt(ApexPages.currentPage().getParameters().get('id'));
        }
        if(!String.isBlank(invoiceId)){
        	invoiceDetail(invoiceId);
        }
    }
     //Action-Init method for Component @ page/Emailtemplate
     public string invoiceDetailForEmailTemplate{
         get{
             /*Start : code for T-536733 : Email Notifications */
            System.debug('### invoiceIdForEmailTemplate' + invoiceIdForEmailTemplate);
            /*End : code for T-536733 */
             if(!String.isBlank(invoiceIdForEmailTemplate)){ 
                invoiceDetail(invoiceIdForEmailTemplate);
             }
             return '';
         }
     }

     
     private void invoiceDetail(String invoiceId){
        if(String.isBlank(invoiceId)){
        	return;
        }
        for(Invoice__c tmpInv : getInvoice(invoiceId) ){
            this.invoiceRecord = tmpInv;
            
            beneNumber = invoiceRecord.Input_Beneficiary__r.Buyer_Supplier_Number__c;
		    }
		    
		    encodedInvoiceId = Utility.doEncryption(invoiceRecord.Id);
        
        if(ApexPages.currentPage().getParameters().get('edit') != null &&
                ApexPages.currentPage().getParameters().get('edit') != ''){
            editInvoice();
        }

        system.debug(' >>> url >>>'+ApexPages.currentPage().getParameters().get('retUrl'));
        if(ApexPages.currentPage().getParameters().get('retUrl') != null && ApexPages.currentPage().getParameters().get('retUrl') != ''){
            System.debug('hello'+ApexPages.currentPage().getParameters().get('retUrl').indexOfIgnoreCase('EDGE_Input_Management'));

            if(ApexPages.currentPage().getParameters().get('retUrl').indexOfIgnoreCase('EDGE_InvoicesAndPayments')!=-1)
                returnurl = new PageReference('/EDGE_InvoicesAndPayments');
            else if(ApexPages.currentPage().getParameters().get('retUrl').indexOfIgnoreCase('EDGE_Input_Management')!=-1)
                returnurl = new PageReference('/EDGE_Input_Management');
            else if(ApexPages.currentPage().getParameters().get('retUrl').indexOfIgnoreCase('EDGE_Activity_Dashboard')!=-1){
                returnurl = new PageReference('/EDGE_Activity_Dashboard');
                system.debug(' >>> return url >>>'+returnurl);
            }
        }

        if(invoiceRecord.Delivery_Method__c == 'Holding-to-Holding')
            hbCurrencyISO = invoiceRecord.Custom_Currency__c;

        selectedSuppId  = invoiceRecord.Supplier__c;
      	selectedDelType = invoiceRecord.Delivery_Method__c;

        accId = utility.currentAccount;
        EdgeGpJsonUtils jsonUtil = new EdgeGpJsonUtils(false);
        H2Hcurrencies = jsonUtil.getHoldingCurrencies();
        
        h2hCurrencySet = new Set<String>();
        for(SelectOption opt : H2Hcurrencies){
        	if(opt.getvalue() != ''){
        	h2hCurrencySet.add('\''+opt.getvalue()+'\'');
        	//h2hCurrencySet.add('\''+opt.getvalue()+'\'');
        	}
        }
        stdCurrencySet = new Set<String>();
        Schema.DescribeFieldResult fieldResult = Invoice__c.custom_currency__c.getDescribe();    
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
        for(Schema.picklistEntry f:ple)    
        {    
           stdCurrencySet.add('\''+f.getValue()+'\'');                    
        }    
 
        system.debug('>>>>currencySet'+h2hCurrencySet);
        if(invoiceRecord.Initiated_By__c == 'Seller')initiatedbySeller = true;
        else initiatedbySeller = false;

        if(Utility_Security.canMakeHBColdPayments){
            isUserH2H = true;
        }
        //NS: 18th Feb'16 - T-475237 (Starts here)
        canEditInvoice();

        cmpAdmin = CMP_Administration__c.getInstance();

        intigrationON = true;
        Global_Pay_Integration_Administration__c gPIA = Global_Pay_Integration_Administration__c.getInstance();
        if(gPIA!=null) {
            intigrationON = gPIA.Integration_Enabled__c;
        }
        settlementAmt = Utility.getSettlementCurrencyAmount(invoiceRecord.Amount__c, invoiceRecord.Custom_Currency__c, UserInfo.getDefaultCurrency());
     } 
    
     
    public void canEditInvoice(){
        canEdit = false;
        if(invoiceRecord!=null && invoiceRecord.RecordType.Name!='GP Invoice - Complete' &&  invoiceRecord.RecordType.Name!='Cold Payment - Complete' &&
           (invoiceRecord.Account__c==Utility.currentAccount || invoiceRecord.Buyer__r.Contact.AccountId == Utility.currentAccount)
                && (invoiceRecord.Status__c == 'Draft' || invoiceRecord.Status__c == 'Acknowledged' || invoiceRecord.Status__c == 'Pending Approval (Open)' ||
                invoiceRecord.Status__c == 'Approved' || invoiceRecord.Status__c == 'Paid' || invoiceRecord.Status__c == 'Void' || invoiceRecord.Status__c == 'Payment Approval Rejected')){
            canEdit = true;
        }else {
            edit = false;
        }
    }

    public boolean getIsRenderedPay () {
        if(invoiceRecord.Buyer__r.Contact.AccountId==Utility.currentAccount &&
          (invoiceRecord.Status__c=='Draft' || invoiceRecord.Status__c=='Acknowledged' ||
          invoiceRecord.Status__c=='Partial Payment Submitted')) {
            if(invoiceRecord.Beneficiary_GP_ID__c!=null && invoiceRecord.Amount__c != null &&
               invoiceRecord.Due_Date__c != null && invoiceRecord.Delivery_Method__c != null &&
               invoiceRecord.Delivery_Method__c != '' && invoiceRecord.Supplier__c != null) {
                if((invoiceRecord.Delivery_Method__c=='Standard Payment' && Utility_Security.canPayInvoices_SP) ||
                  (invoiceRecord.Delivery_Method__c=='Holding-to-Holding' && Utility_Security.canPayInvoices_H2H)) {
                    return true;
                }
            }
        }
        return false;
    }

    // Get fields from fieldset
    public Map<String,Schema.SObjectField> getFields() {
        SObjectType invoiceType = Schema.getGlobalDescribe().get('Invoice__c');
        return invoiceType.getDescribe().fields.getMap();
    }

    // GetInvoice with all the fields in the fieldset
    private List<Invoice__c> getInvoice(String id) {
        String query = 'SELECT ';
        for(String field : this.getFields().KeySet()) {
            query += field + ', ';
        }
        query += 'RecordType.name,'+
                 'Input_Beneficiary__r.Name, Input_Beneficiary__r.OwnerId,'+
                 'Input_Beneficiary__r.Buyer_Supplier_Number__c,Supplier__r.Supplier_Name__c,'+
                 'Buyer__r.ContactId,Buyer__r.Id,Buyer__r.Contact.AccountId,Batch_Id__r.Name,'+
                 'CreatedBy.Name,LastModifiedBy.Name,Owner.Name,Account__r.Name,'+
                 '(SELECT Order_ID__c,CurrencyIsoCode,Payment_Amount__c,Settlement_Method__c,CreatedDate FROM Payments__r)'+
                 ' FROM Invoice__c Where Id =: id LIMIT 1';
        return Database.query(query);
    }

    public PageReference editInvoice() {
        logEditInvoice.logMessage('>>inside editInvoice method ...>> ');
        isUserH2H = false;
        if(Utility_Security.canMakeHBColdPayments){
            isUserH2H = true;
        }
        for(FeedItem fPost : [SELECT RelatedRecordId FROM FeedItem
                                    WHERE ParentId=:invoiceRecord.Id AND Type='ContentPost' ORDER BY CreatedDate desc]) {
            latestContentId = fPost.RelatedRecordId;
            break;
        }
        System.Debug('###'+latestContentId);
        logEditInvoice.logDebug('###'+latestContentId);
        edit = true;
        PageReference pg = new PageReference(CMP_Administration__c.getInstance().CM_Community_BaseURL__c+'/EDGE_InvoiceDetail2?Id='+encodedInvoiceId+'&edit=true');
        //logEditInvoice.saveLogs();
        return pg;
    }

    public void closePayPopup() {
        displayPopup = false;
    }
    public void showPayPopUp() {
        displayPopup = true;
    }

    public void showInvite() {
        displayInvite = true;
     }
     public void closeInvite() {
        displayInvite = false;
     }

     public boolean hasError(){
        Boolean hasError = false;
        errorMessage = '';
        if(invoiceRecord.Invoice_Number__c!= null && !Utility.matchPattern(invoiceRecord.Invoice_Number__c)){
        	errorMessage = errorMessage+  (String.isEmpty(errorMessage)? '' : '<br/>') + System.Label.CM_EDGE_SWIFTCharactersOnly;
        	hasError = true;
        }
	    if(invoiceRecord.Reference_Number__c!= null && !Utility.matchPattern(invoiceRecord.Reference_Number__c)){
        	errorMessage = errorMessage+  (String.isEmpty(errorMessage)? '' : '<br/>') + System.Label.CM_EDGE_SWIFTCharactersOnly;
        	hasError = true;
        }
        if(invoiceRecord.Status__c == null || invoiceRecord.Status__c == ''){
            errorMessage = errorMessage+  (String.isEmpty(errorMessage)? '' : '<br/>') + System.Label.CM_Validation_Input_FieldStatusShouldNotBeBlank;
            hasError = true;
        }
        if(invoiceRecord.Due_Date__c == null && invoiceRecord.RecordType.name.indexOfIgnoreCase('Cold')==-1) {
            errorMessage = errorMessage+  (String.isEmpty(errorMessage)? '' : '<br/>') + System.Label.CM_Validation_Input_FieldPaymentDateShouldNotBeBlank;
            hasError = true;
        }
        /*if(invoiceRecord.Amount__c == null ) {
            errorMessage = errorMessage+   (String.isEmpty(errorMessage)? '' : '<br/>') + System.Label.CM_Validation_Input_FieldAmountShouldNotBeBlank;
            hasError = true;
        }*/
        if(invoiceRecord.RecordType.Name==cmpAdmin.CMP_Default_Invoice_Record_Type__c && !Utility.isValidSfdcId(invoiceRecord.Input_Beneficiary__c)) {
            errorMessage = errorMessage+  (String.isEmpty(errorMessage)? '' : '<br/>') + System.Label.CM_Validation_Input_FieldSupplierShouldNotBeBlank;
            hasError = true;
        }
        /*
        if(invoiceRecord.Custom_Currency__c == '' || invoiceRecord.Custom_Currency__c == 'Select' || invoiceRecord.Custom_Currency__c == null) {
            errorMessage = errorMessage+  (String.isEmpty(errorMessage)? '' : '<br/>') + System.Label.CM_Validation_Input_FieldSelectedCurrencyIsNotValid;
            hasError = true;
        }
        */
        return hasError;
     }

    public PageReference saveAndPay(){
        /*if(!getIsRenderedPay()){
            errorMessage = System.Label.CM_Edge_H2H_Payment_Transaction_Error_Message;
            return null;
        }*/
         isSaveAndPayClicked = true;
         save();
         if(isInvSaved){
	       	PageReference pgRef = new PageReference('/EDGE_Pay?invoiceId='+encodedInvoiceId+'&retUrl='+CMP_Administration__c.getInstance().CM_Community_BaseURL__c+'/EDGE_InvoiceDetail2?Id='+encodedInvoiceId);
	       	pgRef.setRedirect(true);
	       	return pgRef;
	     }
         return null;
     }

      public PageReference saveAndAddNew(){
        save();
        if(isInvSaved){
        	PageReference pgref = new PageReference ('/EDGE_Beneficiary?retURL=/EDGE_InvoiceDetail2?edit=true&InvoiceId='+encodedInvoiceId+ (returnUrl!=null?'&retUrl2='+returnUrl.getUrl():''));
	        pgref.setRedirect(true);
	        return pgref;
        }
        return null;
     }

     public PageReference saveAndAddBene(){
       isSaveAndAddBeneClicked = true;
       save();
       if(isInvSaved){
       	  PageReference pgRef = new PageReference('/EDGE_SelectBeneficiary?invoiceId='+encodedInvoiceId+'&retUrl='+CMP_Administration__c.getInstance().CM_Community_BaseURL__c+'/EDGE_InvoiceDetail2?Id='+encodedInvoiceId);
       	  pgRef.setRedirect(true);
       	  return pgRef;
       }
       return null;
     }

    public PageReference save() {
        logSave.logMessage('>>inside save method ...>> ');
    	isInvSaved = false;
        try{
            if(!hasError()) {
            	if(String.isNotBlank(selectedSuppId)){
            		invoiceRecord.Supplier__c = selectedSuppId;
            		for(Supplier__c sup: [Select Supplier_Name__c,Supplier__c,Supplier__r.CCT_Client_Id__c From Supplier__c Where Id=:selectedSuppId]){
	                    invoiceRecord.Beneficiary_GP_Name__c = sup.Supplier_Name__c;
	                    if(sup.Supplier__c != null)
	                        invoiceRecord.Seller_Account__c = sup.Supplier__c;
	                    if(selectedDelType == 'Holding-to-Holding'){
	                        invoiceRecord.Beneficiary_GP_ID__c = sup.Supplier__r.CCT_Client_Id__c;
	                    }
	                }
	                invoiceRecord.Delivery_Method__c = selectedDelType;
	                
	                if(invoiceRecord.Delivery_Method__c == 'Holding-to-Holding'){
	                    invoiceRecord.Custom_Currency__c = hbCurrencyISO;
	                }
            	}

               // updateInvoiceSellerAccount(invoiceRecord);
               invoicerecord.Balance__c = invoicerecord.amount__c;
                update invoiceRecord;
                logSave.logDebug('invoiceRecord ' + invoiceRecord);
                edit = false;
                isInvSaved = true;
                canEditInvoice();
                for(Invoice__c tmpInv : getInvoice(invoiceRecord.Id) ){
                    invoiceRecord = tmpInv;
                }
                if(returnurl!=null){
		          	returnurl.setRedirect(true);
		          	logSave.saveLogs();
		          	return returnurl;
		         }
           }
           logSave.logDebug('hasError() : ' + hasError());
           logSave.saveLogs();
           return null;
        } catch(DMLException ex) {
            logSave.logDebug('Catch Error : ' + ex.getdmlMessage(0));
            errorMessage = errorMessage+  (String.isEmpty(errorMessage)? '' : '<br/>') + ex.getdmlMessage(0);
            logSave.saveLogs();
            return null;
        } catch(Exception ex){
            logSave.logDebug('Catch Error : ' + ex.getMessage());
            errorMessage = errorMessage+   (String.isEmpty(errorMessage)? '' : '<br/>') + System.Label.EDGE_ERROR_INOVICE_EDIT+' : '+ex.getMessage();
            logSave.saveLogs();
            return null;
        }
    }

    public PageReference cancel() {
        edit = false;
        errorMessage = '';
        for(Invoice__c tmpInv : getInvoice(invoiceRecord.Id) ){
            invoiceRecord = tmpInv;
        }
        if(returnurl!=null) {
            returnurl.setRedirect(true);
            return returnurl;
        }
        return null;
    }

    /*public boolean getSendInviteStatus() {
        if(invoiceRecord.Status__c=='Full Payment Submitted' && invoiceRecord.Beneficiary_GP_ID__c!=null && invoiceRecord.Invite_to_my_network__c) {
            Invites__c relatedInvite = null;
            Network__c relatesNetwork = null;
            Global_Pay_ID_Management__c gPIdM = null;
            for(Global_Pay_ID_Management__c gPId : [SELECT Global_Pay_ID__c,Creation_Type__c,User_Beneficiary__c,User_Beneficiary__r.ContactId,User_Beneficiary__r.Contact.AccountId FROM Global_Pay_ID_Management__c WHERE Global_Pay_ID__c=:invoiceRecord.Beneficiary_GP_ID__c]) {
                gPIdM = gPId;
                break;
            }
            if(gPIdM!=null) {
                for(Invites__c invites : [SELECT Id FROM Invites__c WHERE Invitee_Contact__c=:gPIdM.User_Beneficiary__r.ContactId AND Inviter_User__c=:invoiceRecord.Buyer__r.Id]) {
                    relatedInvite = invites;
                    break;
                }
                for(Network__c networks : [SELECT Id,Status__c FROM Network__c WHERE (Account_Invitee__c=:gPIdM.User_Beneficiary__r.Contact.AccountId AND Account_Inviter__c=:invoiceRecord.Buyer__r.Contact.AccountId) OR
                                          (Account_Inviter__c=:gPIdM.User_Beneficiary__r.Contact.AccountId AND Account_Invitee__c=:invoiceRecord.Buyer__r.Contact.AccountId)]) {
                    relatesNetwork = networks;
                    break;
                }
               if(relatedInvite==null && relatesNetwork==null) {
                    Invites__c invite = new Invites__c(Invitation_Sent_Count__c=1,Benne_Email__c=invoiceRecord.Beneficiary_GP_Email_Address__c,Inviter_User__c=invoiceRecord.Buyer__r.Id,
                                                       Invitee_Contact__c=gPIdM.User_Beneficiary__r.ContactId,Status__c = 'Sent',Invoice__c=invoiceRecord.Id,
                                                       Invitation_Token__c=GPIntegrationUtility.generateInvitesTOKEN('Invitation_Token__c'),
                                                       Last_Sent_By_User__c=invoiceRecord.OwnerId,Status_DateTime__c = system.now());
                    insert invite;
                    relatedInvite = invite;
                    Network__c newNetwork = new Network__c(Invitee_Contact__c=gPIdM.User_Beneficiary__r.ContactId,Account_Invitee__c=gPIdM.User_Beneficiary__r.Contact.AccountId,
                                                           Inviter_User__c=invoiceRecord.Buyer__r.Id,Account_Inviter__c=invoiceRecord.Buyer__r.Contact.AccountId,
                                                           Status__c='1 - Pending (Sent)',Status_DateTime__c = system.now());
                    insert newNetwork;
                    relatesNetwork = newNetwork;
                } else if(relatesNetwork!=null && relatesNetwork.Status__c!='3 ? Active (Accepted)') {
                   relatesNetwork.Status__c='0 - Resend';
                   update relatesNetwork;
                }
            } else if(!String.isEmpty(invoiceRecord.Beneficiary_GP_Email_Address__c)) {
                for(Invites__c invites : [SELECT Id FROM Invites__c WHERE Invoice__c=:invoiceRecord.Id]) {
                    relatedInvite = invites;
                    break;
                }
                if(relatedInvite==null) {
                    Invites__c invite = new Invites__c(Invitation_Sent_Count__c=1,Benne_Email__c=invoiceRecord.Beneficiary_GP_Email_Address__c,Inviter_User__c=invoiceRecord.Buyer__r.Id,
                                                       Status__c = 'Sent',Invoice__c=invoiceRecord.Id,Invitation_Token__c=GPIntegrationUtility.generateInvitesTOKEN('Invitation_Token__c'),
                                                       Last_Sent_By_User__c=invoiceRecord.OwnerId,Status_DateTime__c = system.now());
                    insert invite;
                    relatedInvite = invite;
                }
            }
            return relatedInvite==null && relatesNetwork==null;
        }
        return false;
     }*/

     public boolean getShowMakePayable() {
        if((invoiceRecord.RecordType.Name==cmpAdmin.CMP_Default_Invoice_Record_Type__c ||
            invoiceRecord.RecordType.Name=='Invoice - Complete') &&
            Utility_Security.canCreateInvoice) {
            return true;
        }
        return false;
     }

     public PageReference makeGPInvoice() {
        invoiceRecord.RecordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get(cmpAdmin.CMP_GP_Default_Invoice_Record_Type__c).getRecordTypeId();
        invoiceRecord.RecordType.name = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get(cmpAdmin.CMP_GP_Default_Invoice_Record_Type__c).getName();
        invoiceRecord.Status__c = 'Draft';
        if(Utility_Security.canPayInvoices_H2H){
            invoiceRecord.Delivery_Method__c = 'Holding-to-Holding';
        } else {
            invoiceRecord.Delivery_Method__c = 'Standard Payment';
        }
        PageReference pr = editInvoice();
        return pr;
     }
     
     
     //Raghu Rankawat : start : T-536732
     
     public Alert_Threshold__c newAlertThreshold{get;set;}
     
     public List<Alert_Threshold__c> getAlertThresholdList(){
     	List<Alert_Threshold__c> alertThresholdList = new List<Alert_Threshold__c>();
     	for(Alert_Threshold__c at : [SELECT Id , Active__c , Days__c , Invoice__c 
     										FROM Alert_Threshold__c 
     										WHERE Invoice__c = :invoiceRecord.Id]){
     		alertThresholdList.add(at); 									
     	}
     	alertThresholdList.sort();
     	return alertThresholdList;
     }
     
     
     public PageReference addAlert(){
     	try{
     		System.debug('### ' + Integer.valueof(newAlertThreshold.Days__c));
     		if(Integer.valueof(newAlertThreshold.Days__c) >= 0){
     			newAlertThreshold.Invoice__c = invoiceRecord.Id;
     			newAlertThreshold.Active__c = true;
     			insert newAlertThreshold;
     		}
     		else{
     			CustomException c = new CustomException('Invalid Input'); //label
            	ApexPages.addMessages(c);
            	return null;
     		}
     	}catch(Exception e){
     		System.debug('### addAlert Error ' + e.getMessage());
     	}
     	PageReference pg = new PageReference(CMP_Administration__c.getInstance().CM_Community_BaseURL__c+'/EDGE_InvoiceDetail2?Id='+encodedInvoiceId+'&showReceivables=false');
        return pg;
     }
     //To create custom exception and show custom error message
	 public class CustomException extends Exception{} 
	 
	 
	 public PageReference deleteAlert(){
	 	
	 	if(deleteAlertId != null){
	 		Alert_Threshold__c delAlert = [SELECT Id FROM Alert_Threshold__c WHERE Id =:deleteAlertId];
	 		System.debug('### deleteAlert ' + delAlert);
	 		
	 		if(delAlert != null)
	 			delete delAlert;
	 	}
	 	
	 	PageReference pg = new PageReference(CMP_Administration__c.getInstance().CM_Community_BaseURL__c+'/EDGE_InvoiceDetail2?Id='+encodedInvoiceId+'&showReceivables=false');
        return pg;
	 }
     //Raghu Rankawat : end : T-536732
     

     // T-485988 : Knong view Payment
     /*
     private void updateInvoiceSellerAccount(Invoice__c invoice) {
        if(invoice.Delivery_Method__c != 'Holding-to-Holding' && !String.isBlank(invoice.Beneficiary_GP_ID__c)) {
            for(Global_Pay_ID_Management__c GPM: [SELECT Account_Beneficiary__c, Source_Email_Match__c,
                                                    User_Beneficiary__c From Global_Pay_ID_Management__c
                                                    WHERE Global_Pay_ID__c =:invoice.Beneficiary_GP_ID__c]){
                invoice.Seller_Account__c = GPM.Account_Beneficiary__c;
                break;
            }
        }
     }
	 */


     //@RemoteAction
     /*public static boolean BeneinNetwork (String BeneID, String userAccountId){

       logBeneinNetwork.logMessage('>>inside BeneinNetwork method ...>> ');
       //Find email matches for Bene email address
       Id BeneAccountID;
       Boolean BeneinNetwork = false;
       System.Debug('###'+BeneID);
       logBeneinNetwork.logDebug('###'+BeneID);
       System.Debug('###'+userAccountID);
       
         System.debug('bene manag>> ' + [select Account_Beneficiary__c, Global_Pay_ID__c, Id, Source_Email_Match__c, Source_Invoice_Buyer_Account__c, Source_Invoice_ID__c, User_Beneficiary__c from Global_Pay_ID_Management__c]);
         
       logBeneinNetwork.logDebug('###'+userAccountID);
       for(Global_Pay_ID_Management__c GPM: [select Account_Beneficiary__c, Global_Pay_ID__c, Id, Source_Email_Match__c, Source_Invoice_Buyer_Account__c, Source_Invoice_ID__c, User_Beneficiary__c from Global_Pay_ID_Management__c Where Global_Pay_ID__c = :BeneID and Account_Beneficiary__c != null and Source_Invoice_Buyer_Account__c = :userAccountID]){
         BeneAccountID = GPM.Account_Beneficiary__c;
         break;
       }
       if(BeneAccountID != null){
         //Find network connections for current User and Bene account
         for(Network__c net:[Select Id,Account_Invitee__c,Account_Inviter__c From Network__c Where (Account_Invitee__c = :userAccountID OR Account_Inviter__c = :userAccountID) AND (Account_Invitee__c = :BeneAccountID OR Account_Inviter__c = :BeneAccountID) AND Status__c = '3 - Active (Accepted)']){
           BeneinNetwork = true;
           break;
         }
       }
       logBeneinNetwork.saveLogs();
       return BeneinNetwork;
     }*/

     /*public List<SelectOption> getStatusPicklist() {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Invoice__c.Status__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        List<WU_Edge_Selectable_Invoice_Status__c> activeStatuses = [SELECT Next_Status__c FROM WU_Edge_Selectable_Invoice_Status__c
                                                                   WHERE Current_Status__c=:invoiceRecord.Status__c AND IsAvailable__c=true];
        for( Schema.PicklistEntry f : ple) {
            if(f.getValue()==invoiceRecord.Status__c){
                options.add(new SelectOption(f.getValue(), f.getLabel()));
            } else if(activeStatuses.size()>0) {
                for(WU_Edge_Selectable_Invoice_Status__c activeStatus : activeStatuses) {
                    if(f.getValue()==activeStatus.Next_Status__c){
                        options.add(new SelectOption(f.getValue(), f.getLabel()));
                    }
                }
            }
        }
        return options;
     }*/
}