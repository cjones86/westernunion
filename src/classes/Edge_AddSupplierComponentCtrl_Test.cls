/**=====================================================================
 * Name: Edge_AddSupplierComponentCtrl_Test
 * Description: Test class for Edge_AddSupplierComponentCtrl class
 * Created Date: May 17, 2016
 * Created By: Nikhil Sharma
 =====================================================================*/
 
@isTest
private class Edge_AddSupplierComponentCtrl_Test {
	
	
    private static User commUser;
    private static User commUser1;
    private static Account beneAccount;
    private static Account beneAccount1;
    private static Contact newContact;

	private static testMethod void test() {
        
        createTestData();
        System.runAs(commUser){
            Edge_AddSupplierComponentCtrl ctrl1 = new Edge_AddSupplierComponentCtrl();
            ctrl1.cancel();
            ctrl1.save();
            ctrl1.getLabelString('CM_EDGE_Activity_DashBoard_Title');
            List<Supplier__c> listSuppliers1 = [Select Id, Name From Supplier__c Where Buyer__c = :Utility.CurrentAccount];
            System.assertEquals(1,listSuppliers1.size());
        }
	}
	
	public static testMethod void testConstructor(){
	    createTestData();
		
	    Invoice__c invoice = test_Utility.createInvoice('GP Invoice - Active', beneAccount.Id, commUser.Id,'Pending Approval (Open)', false);
        invoice.Due_Date__c = date.today();
        invoice.Custom_Currency__c = 'INR';
        insert invoice;
        
        Supplier__c supp = test_Utility.createSupplier(beneAccount.Id,beneAccount1.Id,true);
        
        Network__c newNetwork = test_utility.createNetwork(newContact.Id, commUser.Id, beneAccount.Id, beneAccount1.Id,false);
        newNetwork.Status__c='3 - Active (Accepted)';
        insert newNetwork;
        
        System.runAs(commUser){
		        ApexPages.currentPage().getParameters().put('retUrl','{!$Site.Prefix}/apex/EDGE_Capture_Invoice');
            ApexPages.currentPage().getParameters().put('retUrl2','{!$Site.Prefix}/apex/EDGE_MakePaymentStep1');
            //ApexPages.currentPage().getParameters().put('InvoiceId',invoice.Id);
		        ApexPages.currentPage().getParameters().put('Id',supp.Id);
		        
		        Edge_AddSupplierComponentCtrl ctrl = new Edge_AddSupplierComponentCtrl();
		        ctrl.showPopup = true;
		        ctrl.addEdit = false;
		        ctrl.showAccountDetails = false;
		        ctrl.isSaved = false;
		        ctrl.manageBankDetail();
		        ctrl.saveAndNew();
		        
        }
	}
	
	 public static testMethod void testSupplier(){
        createTestData();
    
        Invoice__c invoice = test_Utility.createInvoice('GP Invoice - Active', beneAccount.Id, commUser.Id,'Pending Approval (Open)', false);
        invoice.Due_Date__c = date.today();
        invoice.Custom_Currency__c = 'INR';
        insert invoice;
        
        Supplier__c supp = test_Utility.createSupplier(beneAccount.Id,beneAccount1.Id,false);
        supp.Supplier_Name__c = 'test supp';
        supp.Invite_to_My_network__c = true;
        supp.Country__c = 'USA';
        insert supp;
        
        Network__c newNetwork = test_utility.createNetwork(newContact.Id, commUser.Id, beneAccount.Id, beneAccount1.Id,false);
        newNetwork.Status__c='3 - Active (Accepted)';
        insert newNetwork;
        
        System.runAs(commUser){
            ApexPages.currentPage().getParameters().put('retUrl','{!$Site.Prefix}/apex/EDGE_Capture_Invoice');
            ApexPages.currentPage().getParameters().put('retUrl2','{!$Site.Prefix}/apex/EDGE_MakePaymentStep1');
            ApexPages.currentPage().getParameters().put('InvoiceId',invoice.Id);
            ApexPages.currentPage().getParameters().put('Id',supp.Id);
            
            Edge_AddSupplierComponentCtrl ctrl = new Edge_AddSupplierComponentCtrl();
            ctrl.manageBankDetail();
            ctrl.saveAndNew();
            ctrl.renderAddressFields();
            
        }
  }
  
     public static testMethod void testRetUrl(){
        createTestData();
    
        Invoice__c invoice = test_Utility.createInvoice('GP Invoice - Active', beneAccount.Id, commUser.Id,'Pending Approval (Open)', false);
        invoice.Due_Date__c = date.today();
        invoice.Custom_Currency__c = 'INR';
        insert invoice;
        
        Supplier__c supp = test_Utility.createSupplier(beneAccount.Id,beneAccount1.Id,true);
        
        Network__c newNetwork = test_utility.createNetwork(newContact.Id, commUser.Id, beneAccount.Id, beneAccount1.Id,false);
        newNetwork.Status__c='3 - Active (Accepted)';
        insert newNetwork;
        
        System.runAs(commUser){
            ApexPages.currentPage().getParameters().put('retUrl','{!$Site.Prefix}/apex/EDGE_MakePaymentStep1');
            ApexPages.currentPage().getParameters().put('retUrl2','{!$Site.Prefix}/apex/EDGE_MakePaymentStep1');
            ApexPages.currentPage().getParameters().put('InvoiceId',invoice.Id);
            ApexPages.currentPage().getParameters().put('Id',supp.Id);
            
            Edge_AddSupplierComponentCtrl ctrl = new Edge_AddSupplierComponentCtrl();
            ctrl.manageBankDetail();
            ctrl.saveAndNew();
            
        }
  }
	
	    static void createTestData() {
        test_Utility.createCMPAdministration();
        test_Utility.createWUEdgeSharingAdmin();
        CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.CCT_Client_ID__c = '3232112';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount.BillingStreet = 'testStreet';
        beneAccount.BillingCity = 'city';
        beneAccount.BillingState = 'state';
        beneAccount.BillingCountry = 'USA';
        beneAccount.BillingPostalCode = '231231';
        
        beneAccount1 = test_Utility.createAccount(false);
        beneAccount1.RecordTypeId=accountRecordType;
        beneAccount1.ExternalId__c='11111145';
        beneAccount.CCT_Client_ID__c = '32321122131';
        beneAccount1.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount1.CMP_Enabled__c = true;
        List<Account> listAccount = new List<Account>{beneAccount, beneAccount1};
        insert listAccount;
        
        newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        Contact newContact1 = test_Utility.createContact(contactRecordType,false,beneAccount1.Id);
        List<Contact> listContacts = new List<Contact>{newContact,newContact1};
        insert listContacts;
        
        commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
        commUser.timezonesidkey='America/Indiana/Indianapolis';
        commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        
        commUser1 = test_Utility.createCommUser(newContact1.Id, false);
        commUser1.ProfileId = profileId;
        commUser1.emailencodingkey='UTF-8';
        commUser1.localesidkey='en_US';
        commUser1.timezonesidkey='America/Indiana/Indianapolis';
        commUser1.CMP_Enabled__c=true;
        commUser1.UserName = newContact1.Email+'.cmp1';
        
        list<User> listUsers = new List<User>{commUser,commUser1};
        insert listUsers;
      
      test_Utility.createHoldingBalance(beneAccount.Id, true);
      test_Utility.createForwardContracts(beneAccount.Id, true);
    }

}