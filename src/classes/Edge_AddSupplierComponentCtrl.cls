/**=====================================================================
 * Name: Edge_AddSupplierComponentCtrl
 * Description: Controller class for Edge_AddSupplierComponent component
 * Created Date: May 10, 2016
 * Created By: Nikhil Sharma
 * Task      : T-501133
 =====================================================================*/

public without sharing class Edge_AddSupplierComponentCtrl {
	
	static apexLogHandler.apexLog logSave = new apexLogHandler.apexLog('AddSupplierComponentCtrl','save');
	
    public Supplier__c supp{get;set;}
    public PageReference returnurl{get;set;}
    public string retUrl;
    public id suppId {get;set;}
    public boolean isSaved{get;set;}
    public boolean editMode{get;set;}
    public string invoiceId{get;set;}
    public boolean isShowSaveAndNew{get;set;}
    public boolean isShowManageBankDetail{get;set;}
    public boolean isNetworked{get;set;}
    public String errorMessage{get;set;}
    public Boolean showPopup{get;set;}
    public Boolean addEdit{get;set;}
    public Boolean showAccountDetails{get;set;}
    public string returnUrl2;
    public Map<Id,Id> mapInviteeContactIdAndAccId{get;set;}
    public List<SelectOption> sortedCountries {get;set;}
    public String renderCity {get;set;}
    public String renderPostCode {get;set;}
    public String renderState {get;set;}
    public String renderStreet {get;set;}
    public String cityLabel {get;set;}
    public String stateLabel {get;set;}
    public String postCodeLabel {get;set;}
    public String streetLabel {get;set;}

    public Edge_AddSupplierComponentCtrl(){
        supp = new Supplier__c();
        editMode=false;
        errorMessage='';
        returnUrl2 = null;
        isNetworked = false;
        renderCity = 'Hidden';
        renderPostCode = 'Hidden';
        renderState = 'Hidden';
        renderStreet = 'Hidden';
        sortedCountries = Utility.getSortedcountries();
        if(ApexPages.currentPage().getParameters().get('retUrl') != null && ApexPages.currentPage().getParameters().get('retUrl') != ''){
                    System.debug('Hello1');
            returnurl = new PageReference(ApexPages.currentPage().getParameters().get('retUrl'));
            retUrl = ApexPages.currentPage().getParameters().get('retUrl');
        }
        if(ApexPages.currentPage().getParameters().get('retUrl2') != null && ApexPages.currentPage().getParameters().get('retUrl2') != ''){
                        System.debug('Hello2');

            returnUrl2 = ApexPages.currentPage().getParameters().get('retUrl2');
        }
        if(ApexPages.currentPage().getParameters().get('InvoiceId') != null && ApexPages.currentPage().getParameters().get('InvoiceId') != ''){
                        System.debug('Hello3');

            invoiceId = EncryptionManager.doDecrypt(ApexPages.currentPage().getParameters().get('InvoiceId'));
        }
        if(ApexPages.currentPage().getParameters().get('id')!=null && ApexPages.currentPage().getParameters().get('id') != ''){
            System.debug('Hello4');
            suppId=EncryptionManager.doDecrypt(ApexPages.currentPage().getParameters().get('id'));
            for(Supplier__c sup:[SELECT  id, Supplier_Name__c, country__c ,Address_line_1__c,email_address__c,Supplier__c,
                                            Address_line_2__c,City__c ,State_Province__c, Post_Code__c, Country_ISO_Code__c,
                                            Additional_Email_1__c, Additional_Email_2__c // Raghu Rankawat : I-235486 :Added Additional Email Addresses 
                                            FROM supplier__c Where Id =: suppId]){
                supp = sup;
                editMode=true;
                break;
            }

        List<Network__c> netList = [Select Id, Name, Account_Invitee__c,Account_Inviter__c,Status__c
                                                      From Network__c
                                                      Where (Account_Invitee__c = :Utility.currentAccount And Account_Inviter__c = :supp.supplier__c) OR
                                                      (Account_Inviter__c = :Utility.currentAccount And Account_Invitee__c = :supp.supplier__c)];
           if(netList.size() > 0){
            isNetworked = true;
           }


        }
        if(editMode == false) supp.Invite_to_my_network__c = true;

        isShowSaveAndNew = false;
        isShowManageBankDetail = false;
        if(invoiceId == null && suppId == null)
            isShowSaveAndNew = true;
        if(invoiceId != null)
            isShowManageBankDetail = true;
    }
    
    public PageReference renderAddressFields(){
    	renderCity = 'Hidden';
        renderPostCode = 'Hidden';
        renderState = 'Hidden';
        renderStreet = 'Hidden';
        
        System.debug('country code >>>> '+ supp.Country__c);
        if(String.isNotBlank(supp.Country__c)){
        	for(CountrLogic__mdt cl : [Select City__c,PostCode__c,State_Province__c,Street__c,
        									  CityLabel__c,PostCodeLabel__c,State_ProvinceLabel__c,StreetLabel__c
        							   From CountrLogic__mdt Where Country_ISO_code__c=:supp.Country__c limit 1]){
        		System.debug('>>> cl >> '+ cl);
        		
        		renderCity = String.isBlank(cl.City__c)?'Hidden':cl.City__c;
        		renderPostCode = String.isBlank(cl.PostCode__c)?'Hidden':cl.PostCode__c;
        		renderState = String.isBlank(cl.State_Province__c)?'Hidden':cl.State_Province__c;
        		renderStreet = String.isBlank(cl.Street__c)?'Hidden':cl.Street__c;
        		
        		cityLabel = getLabelString(cl.CityLabel__c);
        		stateLabel = getLabelString(cl.State_ProvinceLabel__c);
    			postCodeLabel = getLabelString(cl.PostCodeLabel__c);
    			streetLabel = getLabelString(cl.StreetLabel__c);
        		
        		System.debug('>>> renderCity >> '+ renderCity);
        		break;
        	}
        }
        return null;
    }
    
    // This method return the String value for the Label id
     public String getLabelString(String labelName){
        Component.Apex.OutputText output = new Component.Apex.OutputText();
        output.expressions.value = '{!$Label.' + labelName + '}';        
        return String.valueOf(output.value);
    }

    public PageReference manageBankDetail(){
        save();
        if(invoiceId!=null && supp.Id!=null){
            System.debug('Hello5'+invoiceId);
            String encodedInvoiceId = Utility.doEncryption(invoiceId);
            String encodedSuppId = Utility.doEncryption(supp.Id);
            PageReference pageRef = new PageReference('/EDGE_BeneficiaryPaymentDetails?InvoiceId='+encodedInvoiceId + '&suppId='+encodedSuppId+ (String.isNotBlank(retUrl)?'&retUrl='+retUrl:'')+(String.isNotBlank(returnUrl2)?'&retUrl2='+returnUrl2:''));
            pageRef.setRedirect(true);
            return pageRef;
        }
        /*
        else if(ApexPages.currentPage().getParameters().get('Id')!=null && supp.Id!=null)
        {
                        PageReference pageRef = new PageReference('/EDGE_BeneficiaryPaymentDetails?Id='+invoiceId + '&suppId='+supp.Id+ (String.isNotBlank(retUrl)?'&retUrl='+retUrl:'')+(String.isNotBlank(returnUrl2)?'&retUrl2='+returnUrl2:''));
                        pageRef.setRedirect(true);
                        return pageRef;

        }
        */
        else if(supp.Id!=null){
        	  String encodedSuppId = Utility.doEncryption(supp.Id);
            PageReference pageRef = new PageReference('/EDGE_BeneficiaryPaymentDetails?suppId='+encodedSuppId + (String.isNotBlank(retUrl)?'&retUrl='+retUrl:'')+(String.isNotBlank(returnUrl2)?'&retUrl2='+returnUrl2:''));
            pageRef.setRedirect(true);
            return pageRef;
        }
        return null;
    }

    public PageReference saveAndNew(){
        save();
        if(supp.Id!=null){
            PageReference pageRef = new PageReference('/EDGE_Beneficiary'+ (String.isNotBlank(retUrl)?'?retUrl='+retUrl:''));
            pageRef.setRedirect(true);
            return pageRef;
        }
        return null;
    }

    public PageReference save(){
    	logSave.logMessage('>>inside save method ...>> ');
    	errorMessage = '';
        if(!editMode){
        supp.Buyer__c = Utility.CurrentAccount;
        }
        try{
            if(isBeneFieldsValidated()){
                upsert supp;
                logSave.logDebug('upsert supplier ' + supp);
                if(invoiceId != null){
                    Invoice__c inv = new Invoice__c(Id=invoiceId);
                    inv.Supplier__c = supp.Id;
                    if(retUrl.containsIgnoreCase('EDGE_Capture_Invoice')){
                        inv.Beneficiary_GP_Name__c = supp.Supplier_Name__c;
                        inv.Invite_to_My_network__c = supp.Invite_to_My_network__c;
                        update inv;
                        logSave.logDebug('update invoice ' + inv);
                        PageReference pgref = new PageReference('/EDGE_Capture_Invoice?id='+Utility.doEncryption(inv.Id)+(String.isNotBlank(returnUrl2)?'&retUrl='+returnUrl2:''));
                        pgref.setRedirect(true);
                        logSave.saveLogs();
                        return pgref;
                    }
                    else{
                        inv.Beneficiary_GP_Name__c = supp.Supplier_Name__c;
                        inv.Invite_to_My_network__c = supp.Invite_to_My_network__c;
                        update inv;
                        logSave.logDebug('update invoice ' + inv);
                        String encodedInvoiceId = Utility.doEncryption(inv.Id);
                        PageReference pgref = new PageReference('/EDGE_InvoiceDetail2?edit=true&id='+encodedInvoiceId+(String.isNotBlank(returnUrl2)?'&retUrl='+returnUrl2:''));
                        pgref.setRedirect(true);
                        logSave.saveLogs();
                        return pgref;
                    }
                }
                else if(returnUrl != null){
                    //if(supp.Invite_to_My_network__c == true)searchUser();
                    logSave.saveLogs();
                    return returnUrl;
                }
                logSave.saveLogs();
                return new PageReference('/EDGE_Beneficiary');
            }
            logSave.saveLogs();
            return null;

        }
        Catch(exception ex){
            if(ex.getMessage().containsIgnoreCase('DUPLICATES_DETECTED')){
                        errorMessage = label.cM_EDGE_DUPLICATE_SUPPLIER;

          }
          logSave.logDebug('Exception ' + ex.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage()));
            logSave.saveLogs();
            return null;
        }
    }

    private boolean isBeneFieldsValidated(){
    	if(String.isNotBlank(supp.Supplier_Name__c) && !Utility.matchPattern(supp.Supplier_Name__c) ||
    	   String.isNotBlank(supp.Address_line_1__c) && !Utility.matchPattern(supp.Address_line_1__c) ||
    	   String.isNotBlank(supp.City__c) && !Utility.matchPattern(supp.City__c) ||
    	   String.isNotBlank(supp.State_Province__c) && !Utility.matchPattern(supp.State_Province__c) ||
    	   String.isNotBlank(supp.Post_Code__c) && !Utility.matchPattern(supp.Post_Code__c) ||
    	   String.isNotBlank(supp.Country__c) && !Utility.matchPattern(supp.Country__c)){

	           errorMessage += (String.isNotBlank(errorMessage)?'<br/>':'')+System.Label.CM_EDGE_SWIFTCharactersOnly;
	           return false;
        }
        return true;
    }

    public PageReference cancel(){
        PageReference pgref = new PageReference('/EDGE_MakePaymentStep1');
        pgref.setRedirect(true);
        return pgref;
    }

    /*public void searchUser() {
      boolean isUserExists = false;
      boolean isUserAlreadyExists = false;

      mapInviteeContactIdAndAccId = new Map<Id,Id>();
      Set<Id> existingcontact = new Set<Id>();
      Set<Id> existingAccounts = new Set<Id>();
      Set<Id> existingInviterAccounts = new Set<Id>();



      for(Network__c network : [SELECT Invitee_Contact__c,Account_Invitee__c,Account_Inviter__c FROM Network__c
                                      WHERE Account_Inviter__c=:Utility.currentAccount
                                      OR Account_Invitee__c=:Utility.currentAccount]) {
          existingcontact.add(network.Invitee_Contact__c);
          existingAccounts.add(network.Account_Invitee__c);
          existingAccounts.add(network.Account_Inviter__c);
      }

      String searchUsers =  existingcontact.size()>0 ? 'AND ContactId NOT IN:existingcontact':'';
      for(User user : Database.query('SELECT Id,ContactId,Contact.AccountId,CMP_Enabled__c FROM User WHERE Email=:beneEmail AND IsActive=true And IsPortalEnabled = True And ContactId!=null And Contact.AccountId!=null Limit 1')) { // AND CMP_Enabled__c=true
         if(!existingAccounts.contains(user.Contact.AccountId)){
            mapInviteeContactIdAndAccId.put(user.ContactId, user.Contact.AccountId);
          }

      }
      if(mapInviteeContactIdAndAccId.size()>0) {
          inviteUserTONetwork();
          isUserExists = true;
      }
      if(mapInviteeContactIdAndAccId.size()==0) {
          inviteNewUserTOCMP();
      }

  }

  public void inviteUserTONetwork() {
      List<Network__c> newNetworks = new List<Network__c>();
      for(Id cntId: mapInviteeContactIdAndAccId.keySet()) {
          newNetworks.add(new Network__c(Invitee_Contact__c=cntId,
                                         Inviter_User__c=UserInfo.getUserId(),
                                         Status__c='1 - Pending (Sent)',Status_DateTime__c = system.now(),
                                         Account_Invitee__c=mapInviteeContactIdAndAccId.get(cntId),
                                         Account_Inviter__c=Utility.currentAccount));
      }
      insert newNetworks;
      // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,Label.CM_Network_InvitationSentNotification));
      //successMessage = Label.CM_Network_InvitationSentNotification;
      //errorMessage='';
  }

  public void inviteNewUserTOCMP() {
      Invites__c invite = new Invites__c(Invitation_Sent_Count__c=1,Benne_Email__c=supp.email_address__c,Inviter_User__c=UserInfo.getUserId(),
                                         Status__c = 'Sent',Invitation_Token__c=GPIntegrationUtility.generateInvitesTOKEN('Invitation_Token__c'),
                                         Last_Sent_By_User__c=UserInfo.getUserId(),Status_DateTime__c = system.now());
      try{
          insert invite;
          // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,Label.CM_Network_InvitationSentNotification));
          //successMessage = Label.CM_Network_InvitationSentNotification;
          //errorMessage = '';
      }
      Catch(Exception ex){
          if(ex.getMessage().containsIgnoreCase('DUPLICATE_VALUE')){
              // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, Label.CM_Network_InvitationAlreadySentNotification));
              errorMessage = Label.CM_Network_InvitationAlreadySentNotification;
              //successMessage='';
          }
          else if(ex.getMessage().containsIgnoreCase('INVALID_EMAIL_ADDRESS')){
              errorMessage = Label.CM_Edge_Invalid_Email_Address;
          }
          else{
              // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getMessage()));
              errorMessage = ex.getMessage();
              //successMessage='';
          }
      }
  }*/
}