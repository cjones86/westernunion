@isTest
private class CashFlowComponentTest {
	
	static testMethod void testCashFlowComponent (){	
		TestDataCreation.createTestData();

		PageReference pageRef = new PageReference('portalSettings');
		Test.setCurrentPage(pageRef);

		// Add parameters to page URL
		ApexPages.currentPage().getParameters().put('switchId', TestDataCreation.ps1.Id);
		ApexPages.currentPage().getParameters().put('delId', TestDataCreation.ps2.Id);

		// **************************
		// *** Run code
		Test.startTest();
		
		CashFlowComponent cdc = new CashFlowComponent();
		cdc.cur = 'Total';
		cdc.getMonths();
		cdc.getSpacer();
		cdc.firstMonthToDisplay(TestDataCreation.con.Id, null);
		cdc.monthSearch(system.today());
		cdc.getUserCurrency();
		cdc.addToInvoice(TestDataCreation.input1);
		cdc.addToPurchaseOrder(TestDataCreation.input2);
		cdc.addToForecastOrder(TestDataCreation.input3);
		cdc.addToOutflowOther(TestDataCreation.input4);
		cdc.addToTradingReceipts(TestDataCreation.input5);
		cdc.addToInflowOther(TestDataCreation.input6);
		cdc.addToForwardWUBS(TestDataCreation.input7);
		cdc.addToForwardOther(TestDataCreation.input8);
		cdc.addToOptionsWUBSProtection(TestDataCreation.input9);
		cdc.addToOptionsWUBSObligation(TestDataCreation.input10);
		cdc.addToOptionsOtherProtection(TestDataCreation.input11);
		cdc.addToOptionsOtherObligation(TestDataCreation.input12);
		cdc.firstMonthToDisplay(TestDataCreation.con.Id, TestDataCreation.acc.Id);
			
		Test.stopTest();
	}
}