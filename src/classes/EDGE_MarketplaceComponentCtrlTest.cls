/*=====================================================================
 * Appirio, Inc
 * Name: EDGE_MarketplaceComponentCtrlTest
 * Description: Controller Test class for EDGE_MarketplaceComponentCtrl
 * Created By: Priyanka Kumar (Appirio)
 =====================================================================*/
@isTest
private class EDGE_MarketplaceComponentCtrlTest{

    private static User commUser;
    private static User commUser1;
    private static Account beneAccount;
    private static Account beneAccount1;
    
    
    static testMethod void testMarketComponent() {
        createTestData();
        Account accRecord = [Select Id From Account where ExternalId__c='11111145'];// account record for commUser1 
        // See commUser1 profile as commUser
        System.runAs(commUser){
            test.startTest();
                EDGE_MarketplaceComponentCtrl cont = new EDGE_MarketplaceComponentCtrl();
                cont.networkStatus = Label.Edge_Connected;
                cont.companyName = 'testname';
                cont.goodsBuy = 'Test';
                cont.goodsSell = 'Test';
                cont.source = 'AFG';
                cont.supply = 'ATA';
                cont.industry = 'Wholesale Trade';
                cont.accountId = beneAccount.Id; 
                cont.inviteUserTONetwork();
                cont.getIndustryValues();
                cont.fetchNetworkStatusFilterRecords();
                cont.networkStatus = Label.Edge_Not_Connected;
                cont.fetchNetworkStatusFilterRecords();
                cont.networkStatus = Label.Edge_All;
                cont.fetchNetworkStatusFilterRecords();
                cont.fetchRecords();
                cont.searchProfile();
                cont.refresh();
                System.assertEquals(1,cont.profileList.size());
            test.stopTest();
        }
    }
    static testMethod void testMarketComponentSearch() {
        createTestData();
        Account accRecord = [Select Id From Account where ExternalId__c='11111145'];// account record for commUser1 
        // See commUser1 profile as commUser
        System.runAs(commUser){
            test.startTest();
                EDGE_MarketplaceComponentCtrl cont = new EDGE_MarketplaceComponentCtrl();
                cont.searchTxt = 'London'; 
                cont.searchProfile();
                System.assertEquals(1,cont.profileList.size());
            test.stopTest();
        }
    }
    
    static testMethod void testNetwork() {
        createTestData();
        
        Account accRecord = [Select Id From Account where ExternalId__c='11111145'];// account record for commUser1 
        // See commUser1 profile as commUser
        System.runAs(commUser){
            test.startTest();
                EDGE_MarketplaceComponentCtrl cont = new EDGE_MarketplaceComponentCtrl();
                cont.inviteUserTONetwork();
                System.assertEquals(1,cont.profileList.size());
            test.stopTest();
        }
    }
    
    
        //set up two community users.
    private static void createTestData(){
        test_Utility.createCMPAdministration();
        CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.CCT_Client_ID__c = '3232112';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
       // beneAccount.BillingStreet = 'testStreet';
        /*
        beneAccount.BillingCity = 'city';
        beneAccount.BillingState = 'state';
        beneAccount.BillingCountry = 'USA';
        beneAccount.BillingPostalCode = '231231';
         */
        
        beneAccount1 = test_Utility.createAccount(false);
        beneAccount1.RecordTypeId=accountRecordType;
        beneAccount1.ExternalId__c='11111145';
        beneAccount.CCT_Client_ID__c = '32321122131';
        beneAccount1.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount1.CMP_Enabled__c = true;
        beneAccount1.BillingState = 'London';
        List<Account> listAccount = new List<Account>{beneAccount, beneAccount1};
        insert listAccount;
        
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        Contact newContact1 = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        List<Contact> listContacts = new List<Contact>{newContact,newContact1};
        insert listContacts;
        
        commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
        commUser.timezonesidkey='America/Indiana/Indianapolis';
        commUser.CMP_Enabled__c=true;
        //commUser.IsPortalEnabled=true;
        commUser.UserName = newContact.Email+'.cmp';
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        
        commUser1 = test_Utility.createCommUser(newContact1.Id, false);
        commUser1.ProfileId = profileId;
        commUser1.emailencodingkey='UTF-8';
        commUser1.localesidkey='en_US';
        commUser1.timezonesidkey='America/Indiana/Indianapolis';
        commUser1.CMP_Enabled__c=true;
        commUser1.UserName = newContact1.Email+'.cmp1';
        
        list<User> listUsers = new List<User>{commUser,commUser1};
        insert listUsers;
        
        //set up market profile records for both accounts
        Marketplace_profile__c mp1 = test_Utility.createMarketProfile(false, beneAccount.Id);
        mp1.Goods_Services_Buy_Visibility__c = true;
        mp1.What_goods_services_do_they_Buy__c = 'Test';
        mp1.Goods_Services_Sell_Visibility__c = true;
        mp1.What_goods_services_do_they_Sell__c = 'Test';
        
        mp1.Source_From_Visibility__c = true;
        mp1.Where_do_they_source_from__c = 'AFG';
        mp1.What_Supplied_Visibility__c = true;
        mp1.Where_do_they_supply_to__c = 'ATA';
        mp1.Which_Industry_Visibility__c = true;
        mp1.What_Industry_do_they_belong_to__c = 'Wholesale Trade';

        
        Marketplace_profile__c mp2 = test_Utility.createMarketProfile(false, beneAccount1.Id);
        mp2.Goods_Services_Buy_Visibility__c = true;
        mp2.What_goods_services_do_they_Buy__c = 'Test';
        mp2.Goods_Services_Sell_Visibility__c = true;
        mp2.What_goods_services_do_they_Sell__c = 'Test';
        mp2.Source_From_Visibility__c = true;
        mp2.Where_do_they_source_from__c = 'AFG';
        mp2.What_Supplied_Visibility__c = true;
        mp2.Where_do_they_supply_to__c = 'ATA';
        mp2.Which_Industry_Visibility__c = true;
        mp2.What_Industry_do_they_belong_to__c = 'Wholesale Trade';






        
        List<Marketplace_profile__c> listMarketProfiles = new List<Marketplace_profile__c>{mp1,mp2};
        insert listMarketProfiles;
        
        Network__c newNetwork = test_utility.createNetwork(newContact.Id, commUser.Id, beneAccount.Id, beneAccount1.Id,false);
        newNetwork.Status__c='3 - Active (Accepted)';
        insert newNetwork;
        
    }
    

}