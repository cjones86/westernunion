/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_Batch_InvoicesCtrl
 * Description: Controller class for EDGE_Batch_Invoices page
 * Created Date: 01st Aug' 2016
 * Created By: Nikhil Sharma (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
public without sharing class EDGE_Batch_InvoicesCtrl {
	public List<InvoiceUIWrapper> buyerInvoices{get;set;}
    public String userDefaultCurrency{get;set;}
    
	public String localeDate{
        get{
            Utility utility = new Utility();
            return utility.localeDate;
        }
    }

	public String localeDateRegx{
        get{
            String dtStr = localeDate.replace('yyyy', '(\\d{2,4})');
            dtStr = dtStr.replace('mm', '(\\d{1,2})\\');
            dtStr = dtStr.replace('dd', '(\\d{1,2})\\');
            return '/'+dtStr+'( (\\d{1,2}):(\\d{1,2}))? ?(am|pm|AM|PM|Am|Pm)?/';
        }
    }

	public EDGE_Batch_InvoicesCtrl() {
	    userDefaultCurrency = UserInfo.getDefaultCurrency();
// 		currentAccount = Utility.currentAccount;
// 		createTransactionPermForSP = Utility_Security.canPayInvoices_SP;
// 		createTransactionPermForH2H = Utility_Security.canPayInvoices_H2H;
		buyerInvoices = getPayableInvoice();
	}

	public List<InvoiceUIWrapper> getPayableInvoice() {
		String userId = UserInfo.getUserId();
		String currentAccId = Utility.currentAccount;
		List<InvoiceUIWrapper> latestInvoices = new List<InvoiceUIWrapper>();

		for(Invoice__c Invoice: database.query('SELECT Name,Account__c,Balance__c,Account__r.Name,Reference_Number__c,RecordTypeId,RecordType.Name,CreatedBy.Contact.Account.Name, Custom_Currency__c,Delivery_Method__c,Type__c, TypeTrans__c,Due_Date__c,Amount__c,Status__c,Beneficiary_Name__c,Beneficiary_GP_Name__c,CreatedBy.Name,Id,Buyer__r.Contact.AccountId,Beneficiary_GP_ID__c,Invoice_Number__c,Supplier__c FROM Invoice__c WHERE Account__c=:currentAccId ORDER BY Due_Date__c DESC')){
			if(Invoice.RecordTypeId != null && !('Void'.equalsIgnoreCase(Invoice.Status__c) || 'Full Payment Submitted'.equalsIgnoreCase(Invoice.Status__c)))
			    latestInvoices.add(new InvoiceUIWrapper(Invoice));
		}
		return latestInvoices;
	}

}