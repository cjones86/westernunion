/**=====================================================================
 * Name: BS_CustomLookupCtrl
 * Description: Controller class for BS_CustomLookup component
 * Created Date: Feb 01, 2016
 * Created By: Nikhil Sharma
 * Task      : T-469137
 =====================================================================*/
public without sharing class BS_CustomLookupCtrl {
  public String fieldName {get;set;}
    public String objName {get;set;}
    public String selectedId {get;set;}
    public String fieldPopulatingId {get;set;}
    public String fieldPopulatingBeneId {get;set;}
    public String fieldPopulateBuyerId{get;set;}
    public String fieldPopulateAccountId{get;set;}
    public boolean lookBuyer{get;set;}
    public String deliveryType {get;set;}
    public boolean canCreateTransaction{get;set;}
    public boolean H2HSearch {get;set;}

    public String lookupTitle {
      get{
        if(lookupTitle == '' || lookupTitle == null){
          lookupTitle = Label.CM_Community_Button_SearchBuyerSupplier;
        }
        return lookupTitle;
      }
      set;
    }

    public String deleteTitle {
    get{
        if(deleteTitle == '' || deleteTitle == null){
          deleteTitle = Label.CM_Community_Button_DeleteBuyerSupplier;
        }
        return deleteTitle;
      }
      set;
  }

    public BS_CustomLookupCtrl (){
      canCreateTransaction = Utility_Security.canCreateNetworkTransaction;
      fieldName = '';
      objName = '';
    }
}