/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_DeleteTempFeedsScheduler_Test
 * Description: Test class for EDGE_DeleteTempFeedsScheduler Scheduler class
 * Created Date: 06 Apr 2016
 * Created By: Nikhil Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
@isTest
private class EDGE_DeleteTempFeedsScheduler_Test {

	private static testMethod void test() {
	    test_Utility.createCMPAdministration();
		test_Utility.createCMPAlert();
		test_Utility.createWubsIntAdministration();
		test_Utility.createGPH2HCurrencies();
        test_Utility.createWUEdgeSharingAdmin();
		CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.CCT_Client_ID__c = '3232112';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        insert beneAccount;
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact;
        
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
		commUser.timezonesidkey='America/Indiana/Indianapolis';
		commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        insert commUser;  
        
        Invoice__c submitInvoice = new Invoice__c(RecordTypeId=Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('GP Invoice - Active').getRecordTypeId());
        submitInvoice.Status__c = 'Invoice Submitted';
        submitInvoice.Initiated_By__c='Seller';
        submitInvoice.Account__c = beneAccount.Id;
        submitInvoice.Buyer__c = commUser.Id;
        submitInvoice.Beneficiary_GP_ID__c = '12345';
        insert submitInvoice;
        
        String cmpAId = [Select Id From CMP_Administration__c].Id;
        CMP_Administration__c cmpA1 = new CMP_Administration__c(Id=cmpAId);
        cmpA1.EDGE_Default_Feed_Invoice__c = (String)submitInvoice.Id;
        update cmpA1;
        
        Attachment uploadedAttachment = test_Utility.addAttachmentToParent(submitInvoice.Id, 'test','test2');
        FeedItem post = new FeedItem();
        post.Title = 'test';
        post.ParentId = submitInvoice.Id;
        post.Body = uploadedAttachment.Name + '-'+submitInvoice.Name;
        post.Type = 'ContentPost';
        post.ContentData = Blob.valueOf('test data'); 
        post.ContentFileName = 'test';
        post.Visibility = 'AllUsers';
        
        FeedItem post2 = new FeedItem();
        post2.Title = 'test2';
        post2.ParentId = submitInvoice.Id;
        post2.Body = uploadedAttachment.Name + '-'+submitInvoice.Name;
        post2.Type = 'ContentPost';
        post2.ContentData = Blob.valueOf('test data'); 
        post2.ContentFileName = 'test';
        post2.Visibility = 'AllUsers';
        
        List<FeedItem> listPosts = new List<FeedItem>{post,post2};
        insert listPosts;
        Test.StartTest();
		EDGE_DeleteTempFeedsScheduler sh1 = new EDGE_DeleteTempFeedsScheduler();
		String sch = '0 0 23 * * ?';
		try{
		    system.schedule('Test delete temp feed', sch, sh1); 
		}
		Catch(exception ex){}
		Test.stopTest(); 
	}

}