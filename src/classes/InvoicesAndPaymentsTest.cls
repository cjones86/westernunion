/**
 * Appirio, Inc
 * Name             : InvoicesAndPaymentsTest
 * Created Date     : 1 May 2015 
 * Created By       : Piyush Bansal (Appirio)
 * Description      : Test class for InvoicesAndPayments
 */
@isTest
private class InvoicesAndPaymentsTest {
	static testMethod void testInvoicesAndPayments(){
		test_Utility.createWUEdgeSharingAdmin();
        //starting  test execution	        
        Test.startTest();
        
        //create Test data below
        User testUser = test_Utility.createTestUser(true);
        System.runAs(testUser){
            
			Account acc = [select id,name from Account limit 1];
            Contact con = [select id,name from Contact limit 1];
            Beneficiary_Contact__c ben = new Beneficiary_Contact__c(name ='Test Ben');
            
            ApexPages.currentPage().getParameters().put('fin','test');
            ApexPages.currentPage().getParameters().put('beneficaryId',ben.Id);
            
            InvoicesAndPayments obj = new InvoicesAndPayments();
            
            Invoice__c inv = new Invoice__c(Status__c='Void',Due_Date__c = Date.today(),Amount__c=100,Input_Beneficiary__c=ben.Id);
            insert inv;
            obj.getCurrencyList();
            obj.toggleSort();
            obj.selectedCurrency = 'USD';
            obj.populateAmountField();
            obj.invoiceList = new List<inputWpr>();
            obj.approvePayment();
            obj.AddInputBeneficiary();
            obj.saveInvoice();
			obj.invoice = inv;
            obj.saveInvoice();
            obj.cancel();
            obj.getForwardContractCurrency();
            obj.invoice = null;
            obj.saveInvoice();
            
            List<inputWpr> tmp = obj.invoicePgCtl_genericList;
            Decimal dec = obj.aggregateTotalSettlement;
            dec = obj.hedgedTotalSettlement;
            dec = obj.unHedgedTotalSettlement;
            Datetime dt = obj.getlastUpdated();
            
            String currentUserLocale = obj.currentUserLocale;
    		obj.aggregateTotal = 100;
    		String aggregateTotalFormat = obj.aggregateTotalFormat;
    		obj.hedgedTotal = 200;
    		String hedgedTotalFormat = obj.hedgedTotalFormat;
    		String defaultValueFormat = obj.defaultValueFormat;
    		obj.unHedgedTotal = 500;
    		String unHedgedTotalFormat = obj.unHedgedTotalFormat;
    		Decimal unHedgedTotalSettlement = obj.unHedgedTotalSettlement;
    		String unHedgedTotalSettlementFormat = obj.unHedgedTotalSettlementFormat;
    		Decimal aggregateTotalSettlement = obj.aggregateTotalSettlement;
    		String aggregateTotalSettlementFormat = obj.aggregateTotalSettlementFormat;
    		Decimal hedgedTotalSettlement = obj.hedgedTotalSettlement;
            
            String str = obj.inputLink;
        }
         //stop test execution	        
        Test.stopTest();
    }
}