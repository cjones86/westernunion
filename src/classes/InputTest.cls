@isTest
private class InputTest {

	static testMethod void testInputTest1 (){	
		TestDataCreation.createTestData();

		PageReference pageRef = new PageReference('Input');
		Test.setCurrentPage(pageRef);

		// Add parameters to page URL
		ApexPages.currentPage().getParameters().put('cId', TestDataCreation.con.Id);
		
		// **************************
		// *** Run code
		Test.startTest();

        ApexPages.StandardController sc = new ApexPages.standardController(TestDataCreation.input1);
        Input input = new Input(sc);
        input.Record = TestDataCreation.input1;
        input.NewRecord();
        input.findUser();
        input.Record.Input_Type__c = 'Trading Receipts';
        
        input.Save();
        input.lineId = TestDataCreation.input1.id;
        input.getitems();
        input.getbeneficiaries();
        input.quickSave();
        input.Cancel();
        input.PopulateLists();
        input.linePaid();
        input.Record = TestDataCreation.input1;
        input.editLine();
        input.deleteLine();
        input.Record = TestDataCreation.input1;
        input.checkApp();
        input.newBeneficiary();
        input.ib.Name = 'Test';
        
        input.saveBeneficiary();

		Test.stopTest();
	}
	
	static testMethod void testInputTest2 (){	
		TestDataCreation.createTestData();

		PageReference pageRef = new PageReference('Input');
		Test.setCurrentPage(pageRef);

		// Add parameters to page URL
		ApexPages.currentPage().getParameters().put('cId', TestDataCreation.con.Id);
		
		// **************************
		// *** Run code
		Test.startTest();

        ApexPages.StandardController sc = new ApexPages.standardController(TestDataCreation.input2);
        Input input = new Input(sc);
        input.Record = TestDataCreation.input2;
        input.NewRecord();
        input.Record.Input_Type__c = 'Option - Other';
        
        input.Save();
        input.lineId = TestDataCreation.input2.id;
        input.getitems();
        input.getbeneficiaries();
        input.quickSave();
        input.Cancel();
        input.PopulateLists();
        input.linePaid();
        input.Record = TestDataCreation.input2;
        input.editLine();
        input.deleteLine();
        input.Record = TestDataCreation.input2;
        input.checkApp();
        input.newBeneficiary();
        input.ib.Name = 'Test';
        
        input.saveBeneficiary();

		Test.stopTest();
	}

	static testMethod void testInputTest3 (){	
		TestDataCreation.createTestData();

		PageReference pageRef = new PageReference('Input');
		Test.setCurrentPage(pageRef);

		// Add parameters to page URL
		ApexPages.currentPage().getParameters().put('cId', TestDataCreation.con.Id);
		
		// **************************
		// *** Run code
		Test.startTest();

        ApexPages.StandardController sc = new ApexPages.standardController(TestDataCreation.input3);
        Input input = new Input(sc);
        input.Record = TestDataCreation.input3;
        input.NewRecord();
        input.Record.Input_Type__c = 'Window Forward Contract - Other';
        
        input.Save();
        input.lineId = TestDataCreation.input3.id;
        input.getitems();
        input.getbeneficiaries();
        input.quickSave();
        input.Cancel();
        input.PopulateLists();
        input.linePaid();
        input.Record = TestDataCreation.input3;
        input.editLine();
        input.deleteLine();
        input.Record = TestDataCreation.input3;
        input.checkApp();
        input.newBeneficiary();
        input.ib.Name = 'Test';
        
        input.saveBeneficiary();

		Test.stopTest();
	}

	static testMethod void testInputTest4 (){	
		TestDataCreation.createTestData();

		PageReference pageRef = new PageReference('Input');
		Test.setCurrentPage(pageRef);

		// Add parameters to page URL
		ApexPages.currentPage().getParameters().put('cId', TestDataCreation.con.Id);
		
		// **************************
		// *** Run code
		Test.startTest();

        ApexPages.StandardController sc = new ApexPages.standardController(TestDataCreation.input4);
        Input input = new Input(sc);
        input.Record = TestDataCreation.input4;
        input.NewRecord();
        input.Record.Input_Type__c = 'Fixed Forward Contract - Other';
        
        input.Save();
        input.lineId = TestDataCreation.input4.id;
        input.getitems();
        input.getbeneficiaries();
        input.quickSave();
        input.Cancel();
        input.PopulateLists();
        input.linePaid();
        input.Record = TestDataCreation.input4;
        input.editLine();
        input.deleteLine();
        input.Record = TestDataCreation.input4;
        input.checkApp();
        input.newBeneficiary();
        input.ib.Name = 'Test';
        
        input.saveBeneficiary();

		Test.stopTest();
	}
}