/**=====================================================================
 * Appirio, Inc
 * Name: CMP_MyActionsCtrl_Test
 * Description: Controller Test class for CMP_MyActionsCtrl
 * Created Date: 01 Apr 2016
 * Created By: Nikhil Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
@isTest
private class CMP_MyActionsCtrl_Test {

	private static testMethod void test() {
        test_Utility.createCMPAdministration();
		test_Utility.createCMPAlert();
		test_Utility.createWubsIntAdministration();
		CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.CCT_Client_ID__c = '3232112';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        insert beneAccount;
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact;
        
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
		commUser.timezonesidkey='America/Indiana/Indianapolis';
		commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        insert commUser;    
        
        System.RunAs(commUser){
        	Test.startTest();
        		CMP_MyActionsCtrl ctrl = new CMP_MyActionsCtrl();
        		Boolean canMakeColdPayments = ctrl.canMakeColdPayments;
        		Boolean canSubmitInvoice = ctrl.canSubmitInvoice;
        		String regInt1 = CMP_MyActionsCtrl.RegisterInterest('SubmitInvoice',beneAccount.Id);
        		String regInt2 = CMP_MyActionsCtrl.RegisterInterest('MakePayment',beneAccount.Id);
        		String regInt3 = CMP_MyActionsCtrl.RegisterInterest('ManageHolding',beneAccount.Id);
        	Test.stopTest();
        }
	}

}