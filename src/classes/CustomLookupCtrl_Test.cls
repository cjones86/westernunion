/**=======================================================================================
 * Name: CustomLookupCtrl_Test
 * Description: Test Class of CustomLookupCtrl Class
 * Created Date: Apr 29, 2015
 * Created By: Rohit B. (Appirio)
 * 
 * Date Modified                Modified By                 	Description of the update
 * [Date]						[Developer Name]				[Short Description]
 ==========================================================================================*/

@isTest
private class CustomLookupCtrl_Test {
	
	@isTest static void test_method_one() {
		Input_Beneficiary__c ib = new Input_Beneficiary__c(Name = 'Test 1');
        insert ib;

		CustomLookupCtrl clc = new CustomLookupCtrl();
		PageReference pr1 = clc.getSelectedRecord();
		clc.fieldName = 'Name';
		clc.objName = 'Input_Beneficiary__c';
		clc.selectedId = ib.Id;
		clc.currentRecord = Null;
		clc.selectedRecordName = '';
		clc.fieldPopulatingId = '';
		clc.fieldSetNameAPI = '';
        String lookupTitle = clc.lookupTitle;
        String deleteTitle = clc.deleteTitle;
		PageReference pr2 = clc.getSelectedRecord();

		System.assertEquals(clc.selectedRecordName, ib.Name);
	}	
}