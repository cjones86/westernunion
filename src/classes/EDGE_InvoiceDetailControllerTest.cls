/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_InvoiceDetailControllerTest
 * Description: Controller Test class for Invoices 
 * Created Date: 22 Feb' 2016
 * Created By: Rohit Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update                                               
 =====================================================================*/
@isTest
private class EDGE_InvoiceDetailControllerTest {
	static Account account;
    static Supplier__c supp ;
    static Global_Pay_ID_Management__c gPIM;
	
	@isTest 		
	static void invoiceDetailsTest() {
		User commUser = createData();
        test_Utility.createWUEdgeSharingAdmin();
		Invoice__c invoice = test_Utility.createInvoice(false);
        invoice.RecordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('GP Invoice - Active').getRecordTypeId();
        invoice.Custom_Currency__c = 'USD';
        invoice.Invoice_Number__c = 'INVTest-123';
        invoice.Status__c = 'Draft';
        invoice.Initiated_By__c='Buyer';
        invoice.Due_Date__c = Date.today().addDays(30);
        invoice.Amount__c = 2000;
        invoice.Account__c = account.Id;
        invoice.Buyer__c = commUser.Id;
        invoice.Supplier__c = supp.id;
        insert invoice;
        gPIM.Source_Invoice_ID__c=invoice.Id;
        update gPIM;
        gPIM = [SELECT Creation_Type__c , Account_Beneficiary__c, Global_Pay_ID__c, Id, Source_Email_Match__c, 
                Source_Invoice_Buyer_Account__c, Source_Invoice_ID__c, User_Beneficiary__c 
                FROM Global_Pay_ID_Management__c];
        System.debug('@@@' + gPIM);
        FeedItem feedPost = new FeedItem();
        feedPost.ParentId = invoice.Id; //eg. Opportunity id, custom object id..
        feedPost.ContentData = Blob.valueof('Test data for pdf file');
        feedPost.Title = 'test.txt';
        feedPost.Body = feedPost.Title;
        feedPost.ContentFileName = feedPost.Title;
        feedPost.Visibility = 'AllUsers';
        insert feedPost;
        ApexPages.currentPage().getParameters().put('retUrl','/EDGE_InvoicesAndPayments');
		System.RunAs(commUser){
        	Test.startTest();
        	ApexPages.currentPage().getParameters().put('id',invoice.Id);
        	ApexPages.currentPage().getParameters().put('edit',invoice.Id);
        	EDGE_InvoiceDetailController edgeInvoice = new EDGE_InvoiceDetailController();
        	edgeInvoice.getdelTypes();
        	edgeInvoice.getopenPresentationOptions();
        	edgeInvoice.saveAndPay();
        	edgeInvoice.saveAndAddNew();
        	//edgeInvoice.updateInvoiceSellerAccount(invoice);
        	System.assert(true, edgeInvoice.userHomeCurrency + edgeInvoice.beneName + edgeInvoice.beneIdH2H + edgeInvoice.hbCurrencyISO);
        	edgeInvoice.saveAndAddBene();
        	edgeInvoice.showPayPopUp();
        	edgeInvoice.closePayPopup();
        	edgeInvoice.closeBenePopup();
        	edgeInvoice.showBenePopup();
        	edgeInvoice.showInvite();
        	edgeInvoice.closeInvite();
        	//edgeInvoice.getSendInviteStatus();
        	edgeInvoice.invoiceRecord.Status__c='';
        	edgeInvoice.invoiceRecord.Due_Date__c=null;
        	edgeInvoice.invoiceRecord.Custom_Currency__c='';
        	edgeInvoice.save();
        	edgeInvoice.invoiceRecord.Status__c='Draft';
        	edgeInvoice.invoiceRecord.Due_Date__c=Date.today().addDays(30);
        	edgeInvoice.invoiceRecord.Custom_Currency__c= 'USD';
        	edgeInvoice.save();
        	edgeInvoice.cancel();
        	edgeInvoice.invoiceRecord.Status__c='Full Payment Submitted';
        	edgeInvoice.invoiceRecord.Beneficiary_GP_ID__c = '12345';
        	edgeInvoice.getIsRenderedPay();
        	edgeInvoice.getShowMakePayable();
        	edgeInvoice.makeGPInvoice();
 
            //EDGE_InvoiceDetailController.BeneinNetwork('12345', account.Id);
        	List<Invoice__c> invoices = [Select id, balance__c from invoice__C];
        	System.assertEquals(2000, invoices[0].balance__c);
        	Test.stopTest();
		}
	}
	
	private static User createData() {
		test_Utility.createCMPAdministration();
		test_Utility.createCMPAlert();
		CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        insert beneAccount;
        account = beneAccount;
        supp = test_Utility.createSupplier(beneAccount.Id,beneAccount.id,false);
        supp.Supplier_Name__c = 'Test';
        supp.Address_line_1__c = 'Test';
        supp.Address_Line_2__c = 'Test';
        supp.City__c = 'Test';
        supp.State_Province__c = 'Test';
        //supp.Country__c = 'ANDORRA';
        supp.Post_Code__c = 'Test';
        insert supp;
        
        
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact;
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
		commUser.timezonesidkey='America/Indiana/Indianapolis';
		commUser.CMP_Enabled__c=true;
        commUser.ContactId=newContact.ID;
        commUser.UserName = newContact.Email+'.cmp';
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        commUser.CommunityNickname = commUser.alias;
        insert commUser;     
        if (commUser.Id != null) { 
           gPIM = new Global_Pay_ID_Management__c(Global_Pay_ID__c='12345',
                                                  User_Beneficiary__c=commUser.Id,                          
                                                  Creation_Type__c='Automatic Email Match');
           insert gPIM; 
        }
        return commUser;
	}
}