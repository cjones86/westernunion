public without sharing class EDGE_RegisterInterestController {

    static apexLogHandler.apexLog logRegisterInterest = new apexLogHandler.apexLog('RegisterInterestCtrl','RegisterInterest');
    
	public string FeatName {get;set;}
	public User currentUser {get;set;}
	public boolean selfReg {get;set;}

	public EDGE_RegisterInterestController() {
		currentUser = Utility.loggedInUser;

		if(currentUser.Contact.Account.RecordType.DeveloperName == 'EDGE_Self_Registered'){
			selfReg = true;
		}
		else{
			selfReg = false;
		}
		//userAccount = getAccount(currentUser.Contact.AccountId);
	}

	@RemoteAction
	public static String RegisterInterest(String Interest, String AccountId){
	    logRegisterInterest.logMessage('>>inside RegisterInterest method ...>> ');
		System.Debug('FeatName'+Interest);
		logRegisterInterest.logDebug('FeatName'+Interest);
		System.Debug('AccId'+AccountId);
		logRegisterInterest.logDebug('AccId'+AccountId);
		Account userAccount = [Select id, EDGE_Make_Payment_Interest__c,EDGE_Make_Payment_Interest_Date__c,EDGE_Manage_Holding_Interest__c,EDGE_Manage_Holding_Interest_Date__c,EDGE_Submit_Invoice_Interest__c,EDGE_Submit_Invoice_Interest_Date__c From Account Where Id = :AccountId Limit 1];
		if(Interest == 'SubmitInvoice' && !userAccount.EDGE_Submit_Invoice_Interest__c){
			userAccount.EDGE_Submit_Invoice_Interest__c = true;
			userAccount.EDGE_Submit_Invoice_Interest_Date__c = System.Now();
			update useraccount;
		}
		else if(Interest == 'MakePayment' && !userAccount.EDGE_Make_Payment_Interest__c){
			userAccount.EDGE_Make_Payment_Interest__c = true;
			userAccount.EDGE_Make_Payment_Interest_Date__c = System.Now();
			update useraccount;
		}
		else if(Interest == 'ManageHolding' && !userAccount.EDGE_Manage_Holding_Interest__c){
			userAccount.EDGE_Manage_Holding_Interest__c = true;
			userAccount.EDGE_Manage_Holding_Interest_Date__c = System.Now();
			update useraccount;
		}
		logRegisterInterest.saveLogs();
		return '';
	}
}