/**=====================================================================
 * Name: EDGE_NetworkTriggerHandler
 * Description: Handler Class to update the Supplier network records
 * Created Date: May 11, 2016
 * Created By: Priyanka Kumar
 * Task      : T-501534
 =====================================================================*/
public without sharing class EDGE_NetworkTriggerHandler {

	static apexLogHandler.apexLog logPopulateNetwork = new apexLogHandler.apexLog('NetworkTriggerHandler','populateNetwork');
	
	List<Supplier__c> suppliersToBeUpdated = new List<Supplier__c>();

    public void onAfterInsert(list<Network__c> newList) {
    	populateNetworkRecord(newList);
    	createSupplierRecords(newList, null);
    }

    public void onAfterUpdate(list<Network__c> newList,Map<Id,Network__c> oldMap) {
    	createSupplierRecords(newList, oldMap);
    }

    public void createSupplierRecords(list<Network__c> newList, Map<Id,Network__c> oldMap){

        Set<String> setNetworkedAccIds = new Set<String>();
        Set<String> inviteeSet = new Set<String>();
        Set<String> inviterSet = new Set<String>();
        Map<String,String> mapSuppKeyAndNetworkId = new Map<String,String>();
        Set<String> setValideCountryCodes = new Set<String>();
        Schema.DescribeFieldResult F = Contact.Country__c.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        for(integer k =0; k< P.size(); k++){
        	if(P[k].isActive()){
        		setValideCountryCodes.add(P[k].getValue());
        	}
        }
        for(Network__c net: newList){
            //System.assert(false, '3 - Active (Accepted)'.equalsIgnoreCase(net.Status__c) + ' >> ' + trigger.isUpdate  + ' >> ' + !'3 - Active (Accepted)'.equalsIgnoreCase(oldMap.get(net.Id).Status__c));
            if('3 - Active (Accepted)'.equalsIgnoreCase(net.Status__c) &&
                (trigger.isInsert || (trigger.isUpdate &&  !'3 - Active (Accepted)'.equalsIgnoreCase(oldMap.get(net.Id).Status__c)))){
                    if(net.Account_Invitee__c!=null && net.Account_Inviter__c!=null){
                         setNetworkedAccIds.add(net.Account_Invitee__c + '~' + net.Account_Inviter__c);
                         setNetworkedAccIds.add(net.Account_Inviter__c + '~' + net.Account_Invitee__c);
                         mapSuppKeyAndNetworkId.put(net.Account_Invitee__c,net.Id);
                         inviteeSet.add(net.Account_Invitee__c);
                         inviterSet.add(net.Account_Inviter__c);
                    }
                }
        }
				System.Debug('###'+setNetworkedAccIds);
        Set<String> setExistingSuppliers = new Set<String>();

        for(Supplier__c supp: [Select Id,Buyer__c,Supplier__c From Supplier__c Where (Buyer__c IN: inviteeSet And Supplier__c IN: inviterSet)
                                        OR (Buyer__c IN: inviterSet And Supplier__c IN: inviteeSet)]){
            setExistingSuppliers.add(supp.Buyer__c+'~'+supp.Supplier__c);
        }
				System.Debug('###'+setExistingSuppliers);

        Map<String,String> mapBuyerSupplierId = new Map<String,String>();
        for(String ids: setNetworkedAccIds){
					System.Debug('###'+setNetworkedAccIds);
            if(!setExistingSuppliers.contains(ids)){
                String buyerId = ids.split('~')[0];
                String suppId = ids.split('~')[1];
                mapBuyerSupplierId.put(buyerId,suppId);
                //mapBuyerSupplierId.put(ids,mapNetworkedAccIds.get(ids));
            }
        }
				System.Debug('###'+mapBuyerSupplierId);
				System.Debug('###'+mapBuyerSupplierId.values());
        Map<String,Account> mapAccIdAndAccount = new Map<String,Account>();
        for(Account acc: [Select Id,Name,BillingStreet,BillingCity,BillingState,BillingPostalCode,BillingCountry,Billing_Address_ISO_Country_Code__c
                                	From Account
                                Where Id IN: mapBuyerSupplierId.values()]){
            mapAccIdAndAccount.put(acc.Id,acc);
        }
		
		Map<String,String> mapSupAccIdAndEmail = new Map<String,String>();
		for(User us: [Select Contact.AccountId, Email From User Where IsActive=true And IsPortalEnabled=true And ContactId!=null And Contact.AccountId IN: mapAccIdAndAccount.keySet()]){
			mapSupAccIdAndEmail.put(us.Contact.AccountId,us.Email);
		}
        System.Debug('###'+mapBuyerSupplierId);
				System.Debug('###'+mapAccIdAndAccount);
        List<Supplier__c> listSuppliersToInsert = new List<Supplier__c>();
        for(String buyerId : mapBuyerSupplierId.keySet()){
                                                    //    System.assert(false, 'Hello');

            Supplier__c sup = new Supplier__c();
            sup.Buyer__c = buyerId;
            sup.Supplier__c = mapBuyerSupplierId.get(buyerId);

						System.Debug('###'+mapBuyerSupplierId.get(buyerId));
						System.Debug('###'+sup.Supplier__c);

            Account supAcc = mapAccIdAndAccount.get(sup.Supplier__c);
						System.Debug('###'+supAcc);
            sup.Supplier_Name__c = supAcc.Name;
            sup.Address_line_1__c = supAcc.BillingStreet;
            sup.City__c = supAcc.BillingCity;
            sup.State_Province__c = supAcc.BillingState;
            sup.Post_Code__c = supAcc.BillingPostalCode;
            if(setValideCountryCodes.contains(supAcc.BillingCountry))
            	sup.Country__c = supAcc.BillingCountry;
            else
            	sup.Country__c = String.isNotBlank(supAcc.Billing_Address_ISO_Country_Code__c)?supAcc.Billing_Address_ISO_Country_Code__c:'';
            if(mapSupAccIdAndEmail.containsKey(sup.Supplier__c)){
            	sup.Email_Address__c = mapSupAccIdAndEmail.get(sup.Supplier__c);
            }
            if(mapSuppKeyAndNetworkId.containsKey(buyerId))
                sup.Network__c = mapSuppKeyAndNetworkId.get(buyerId);
            else if(mapSuppKeyAndNetworkId.containsKey(sup.Supplier__c))
                sup.Network__c = mapSuppKeyAndNetworkId.get(sup.Supplier__c);
            listSuppliersToInsert.add(sup);
        }
                                                 //   System.assert(false, 'Hell1');


        if(listSuppliersToInsert.size() > 0)
            insert listSuppliersToInsert;
    }

    public void populateNetworkRecord(list<Network__c> newList) {

	logPopulateNetwork.logMessage('>>inside populateNetworkRecord method ...>> ');
	
    Set<Id> inviteeSet = new Set<Id>();
    Set<Id> inviterSet = new Set<Id>();

    for(Network__c record : newList){
    	inviteeSet.add(record.Account_Invitee__c);
    	inviterSet.add(record.Account_Inviter__c);
    }


    	for(Supplier__c supRec : [SELECT Id,Supplier__c,Network__c,Buyer__c FROM Supplier__c
    	                          WHERE Supplier__c != null
    	                          AND (Supplier__c IN :inviteeSet OR Supplier__c IN :inviterSet)]){
	    	for(Network__c networkRec : newList){
	    		if((networkRec.Account_Invitee__c != null || networkRec.Account_Inviter__c != null)){
	    			 if((networkRec.Account_Invitee__c == supRec.Supplier__c && networkRec.Account_Inviter__c == supRec.Buyer__c) ||
	    			    (networkRec.Account_Invitee__c == supRec.Buyer__c && networkRec.Account_Inviter__c == supRec.Supplier__c)){
	    			 	    //network record found
	    			 	    supRec.Network__c = networkRec.Id;
	    			 	    suppliersToBeUpdated.add(supRec);
	    			 	    break;
	    			 }
	    	  }
    	  }
      }

      if(suppliersToBeUpdated.size() > 0){
      	update suppliersToBeUpdated;
      	logPopulateNetwork.logDebug('suppliersToBeUpdated ' + suppliersToBeUpdated);
      }
      logPopulateNetwork.saveLogs();


    }

}