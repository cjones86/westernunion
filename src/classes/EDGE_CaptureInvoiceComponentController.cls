/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_CaptureInvoiceComponentController
 * Description: Controller to capture Invoice from CMP user
 * Created By: Nikhil Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update                                           
 =====================================================================*/
public without sharing class EDGE_CaptureInvoiceComponentController{
    
    public Invoice__c invoice{get;set;}  
    public Attachment uploadedAttachment{get;set;}
    public List<FeedItem> posts{get;set;}
    public Boolean isDisplayBenePopUp{get;set;}
    public boolean canCreateTransaction{get;set;}
    public Id recordTypeId{get;set;}
    public Id accId {get;set;}
    public integer deleteIndex{get;set;}
    public List<SelectOption> currencyOptions{get;set;}
    public String allowedFileType{get;set;}
    public InsightsAndInputTabCtrl InsightsAndInputCtl{get;set;}
    public boolean isUserH2H{get;set;}
    //public PageReference returnurl{get;set;}
    
    public EDGE_CaptureInvoiceComponentController() {
        canCreateTransaction = Utility_Security.canCreateNetworkTransaction;
        uploadedAttachment = new Attachment();
        //returnurl = Page.EDGE_MyDashboard;
        /*
        if(ApexPages.currentPage().getParameters().containsKey('retUrl') &&
           ApexPages.currentPage().getParameters().get('retUrl') != null && 
           ApexPages.currentPage().getParameters().get('retUrl') != ''){
            returnurl = new PageReference(ApexPages.currentPage().getParameters().get('retUrl'));
        }
        */
        posts = new List<FeedItem>();
        CMP_Administration__c cmpAdmin = CMP_Administration__c.getInstance();       
        String recordTypeName = cmpAdmin.CMP_GP_Default_Invoice_Record_Type__c;
        if(!canCreateTransaction) {
            recordTypeName = cmpAdmin.CMP_Default_Invoice_Record_Type__c;
        }
        allowedFileType = cmpAdmin.CMP_Allowable_File_Types__c;
        recordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        invoice = new Invoice__c(RecordTypeId=recordTypeId);    
        accId = utility.currentAccount;
        if(canCreateTransaction){
            if(Utility_Security.canPayInvoices_SP){
                isUserH2H = false;
                invoice.Delivery_Method__c = 'Standard Payment';
            }
            if(Utility_Security.canPayInvoices_H2H){
                isUserH2H = true;
                invoice.Delivery_Method__c = 'Holding-to-Holding';    
            }
        }
        invoice.Custom_Currency__c = Utility.loggedInUser.DefaultCurrencyIsoCode;
        updateCurrencyList();
    }
    
    public void updateCurrencyList() {
        currencyOptions = new List<SelectOption>();
        for(CMP_Currency_ISO_Mapping__c gpCurrency : [SELECT Currency_ISO_Code__c,Name,Id FROM CMP_Currency_ISO_Mapping__c ORDER BY Name]) {
            currencyOptions.add(new SelectOption(gpCurrency.Currency_ISO_Code__c,gpCurrency.Name));
        }
    }
    
    public void closeBenePopup() {
        isDisplayBenePopUp = false;    
     }
     public void showBenePopUp() {        
        isDisplayBenePopUp = true;
     }
     
     private void saveInvoice() {
        isDisplayBenePopUp = false;  
        boolean hasError = false;
        invoice.Status__c = 'Draft';
        invoice.Initiated_By__c = 'Buyer';
        /*if(invoice.Name == null || invoice.Name == '') {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.CM_Validation_Input_InvoiceNumberShouldNotBeBlank));
            hasError = true;    
        }*/
        if(invoice.Due_Date__c == null) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.CM_Validation_Input_FieldPaymentDateShouldNotBeBlank));
            hasError = true;
        }
        if(invoice.Amount__c == null ) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.CM_Validation_Input_FieldAmountShouldNotBeBlank));
            hasError = true;
        }
        if(invoice.Custom_Currency__c == '' || invoice.Custom_Currency__c == 'Select' || invoice.Custom_Currency__c == null) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.CM_Validation_Input_FieldSelectedCurrencyIsNotValid));
            hasError = true;
        }   
        if(!hasError) {
            invoice.Account__c = Utility.currentAccount;
            invoice.Buyer__c = UserInfo.getUserId();
            insert invoice;
            if(!posts.isEmpty()) {
                for(FeedItem att : posts) {
                    att.ParentId = invoice.Id;  
                }
                insert posts;
            }
        }
     }
     
     public PageReference save() {
        Savepoint sp = Database.setSavepoint();
        try {
            saveInvoice();
            if(invoice.Id!=null)
                return (new PageReference( Site.getPathPrefix()+'/apex/EDGE_ManageCashInput?fin=true'));
        } catch (Exception ex) {
            Database.rollback( sp );
            if(!ex.getMessage().contains('Duplicate Invoice Number and Beneficiary Found') && !ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.CM_Alert_UnableToSaveInvoice + ex.getMessage()));
            }
        }
        
        return null;
     }
     
     public PageReference saveNew() {
        Savepoint sp = Database.setSavepoint();
        try {
            saveInvoice();
            invoice = new Invoice__c(RecordTypeId=recordTypeId);
            posts.clear();
        } catch (Exception ex) {
            Database.rollback( sp );
            if(!ex.getMessage().contains('Duplicate Invoice Number and Beneficiary Found') && !ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.CM_Alert_UnableToSaveInvoice + ex.getMessage()));
            }
        }
        return null;
     }

     public PageReference cancel2() {
        return (new PageReference(Site.getPathPrefix()+'/apex/EDGE_ManageCashInput?fin=true'));
     }
     
     public PageReference attachmentUpdate() {
        try {
            if(uploadedAttachment.Name!=null && !String.isBlank(uploadedAttachment.Name)) {
                String filetype = uploadedAttachment.Name.subString(uploadedAttachment.Name.lastIndexOfIgnoreCase('.')+1);
                if((allowedFileType==null || String.isBlank(allowedFileType)) || allowedFileType.containsIgnoreCase(filetype)) {
                    FeedItem fI = new FeedItem();
                    fI.Title = uploadedAttachment.Name;
                    fI.Body = uploadedAttachment.Name;
                    fI.Type ='ContentPost';
                    fI.ContentData = uploadedAttachment.body;
                    fI.ContentFileName = uploadedAttachment.Name;
                    fI.Visibility = 'AllUsers';
                    posts.add(fI);
                    uploadedAttachment = new Attachment();
                }else {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.CM_Alert_File_InvalidFileFormat));
                }
            }
        }catch(Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error in Attachment' + ex.getMessage()));
        }
        return null;
     }
     
     public PageReference delAttachment() {
        if(posts!=null && posts.size()>0 && deleteIndex<posts.size()) {
        
        posts.remove(deleteIndex);
        }
        return null;
     }
}