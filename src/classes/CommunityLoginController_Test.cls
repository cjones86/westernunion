/**=======================================================================================
 * Name: CommunityLoginController_Test
 * Description: Test Class of CommunityLoginController Class
 * Created Date: Apr 29, 2015
 * Created By: Rohit B. (Appirio)
 * 
 * Date Modified                Modified By                 	Description of the update
 * [Date]						[Developer Name]				[Short Description]
 ==========================================================================================*/
 
@isTest
private class CommunityLoginController_Test {
	
	@isTest 
	static void test_method_one() {
	    
		CMP_Administration__c cmpAdministration = new CMP_Administration__c(Base_Invite_URL__c='https://bsnetwork-wubs.cs25.force.com/managecash',
																			CMP_Default_Account_ParentId__c=UserInfo.getUserId(),
																			CMP_Default_Account_RecordType__c='GBPGlobal',
																			CMP_Default_Contact_RecordType__c='GBPGlobal',
																			Default_User_Self_Registration_Profile__c='Cash Management Customers - CCT',
																			CMP_GP_Default_Invoice_Record_Type__c='GP Invoice - Active',
																			CMP_Default_Invoice_Record_Type__c='Invoice - Active',
																			CMP_Allowable_File_Types__c='pdf,docx,doc',
																			EDGE_TFA_Enabled_Profiles__c='Cash Management Customers - Non CCT,Cash Management Customers - CCT');
		
		insert cmpAdministration;
		
		CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.CCT_Client_ID__c = '3232112';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount.CMP_Create_Transactions__c = true;
        beneAccount.CMP_Enabled__c = true;
        beneAccount.CMP_Holding_Enabled__c = true;
        beneAccount.CMP_H2H_Transaction_Enabled__c = true;
        beneAccount.BillingStreet = 'testStreet';
        beneAccount.BillingCity = 'city';
        beneAccount.BillingState = 'state';
        beneAccount.BillingCountry = 'USA';
        beneAccount.BillingPostalCode = '231231';
        beneAccount.Billing_Address_ISO_Country_Code__c = 'USA';
        insert beneAccount;
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact;
        
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
		commUser.timezonesidkey='America/Indiana/Indianapolis';
		commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        commUser.CMP_Network_Invitation_Enabled__c = true;
        commUser.CMP_Create_Transactions__c = true;
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        insert commUser;    
        
        //set password for commUser
        system.setPassword(commUser.Id, 'abcd1234');
        
        String guestProfileId = ([SELECT Id, Name FROM Profile WHERE Name='Cash Management Profile' Limit 1]).Id;

        User guestUser = new User();
        guestUser.alias = 'testuser';
        guestUser.Email = 'testuser@test123.com';
        guestUser.EmailEncodingKey = 'ISO-8859-1';
        guestUser.LanguageLocaleKey = 'en_US';
        guestUser.LastName = 'Test User567';
        guestUser.LocaleSidKey = 'en_AU';
        guestUser.Account_Administrator__c = false;
        guestUser.ProfileId = guestProfileId;
        guestUser.TimeZoneSidKey = 'Australia/Sydney';
        guestUser.UserName = 'testuser@travelex.com.au';
        guestUser.DefaultCurrencyIsoCode = 'INR';
        insert guestUser;
        
        System.runAs(guestUser){
            CommunityLoginController clc = new CommunityLoginController();
    		PageReference pr1 = clc.doLogin();
    		clc.changeLanguage();
    		clc.Init();
    		clc.username = commUser.UserName;
    		clc.password = 'abcd1234';
    		pr1 = clc.doLogin();
    		clc.custAuth();
    		clc.resendToken();
    		clc.inputToken = '2131231231';
    		clc.doVerify();
    		clc.inputToken = '12345678';
    		// atempt wrong password for multiple times to cover some scenarios.
    		clc.doVerify();
    		clc.doVerify();
    		clc.doVerify();
    		clc.doVerify();
    		clc.doVerify();
    		clc.custAuth();
        }
	}	
}