/**
 * Appirio, Inc
 * Name             : ManageCashInputsCtlTest
 * Created Date     : 30 April 2015 
 * Created By       : Nikhil Sharma (Appirio)
 * Description      : Test class for ManageCashInputsCtl
 */
@isTest
private class ManageCashInputsCtlTest {
	static Account acc;
    static testMethod void myUnitTest() {
    	Test.startTest();
    	acc = test_Utility.createAccount(true);
    	Contact con = New Contact(AccountId = acc.Id, LastName = 'Test', FirstName = 'Test');
    	insert con;
    	User tUser = test_Utility.createCommUser(con.Id, true);
        System.runAs(tUser){
	        createTestData();
	        // ApexPages.currentPage().getParameters().put('aId', acc.Id);
	        // apexpages.currentPage().getParameters().put('cId','test12341234test');
	        
	        
	        ManageCashInputsCtl ctrl = new ManageCashInputsCtl();
	        
	        ctrl.incomingPgCtl  = new CustomPaginationController(1, new List<inputWpr>(), new List<List<inputWpr>>() );
            ctrl.hedgingPgCtl        = new CustomPaginationController(1, new List<inputWpr>(), new List<List<inputWpr>>());
            ctrl.outgoingPgCtlCTM    = new CustomPaginationController(1, new List<inputWpr>(), new List<List<inputWpr>>());
            ctrl.holdingPgCtl        = new CustomPaginationController(1, new List<inputWpr>(), new List<List<inputWpr>>());
			ctrl.PopulateLists();
			ctrl.Cancel();
	        ctrl.quickSave();
	        ctrl.record.Input_Type__c = 'Window Forward Contract - Other';
	        ctrl.toggleIncomingSort();
	        ctrl.togglehedgingSort();
	        ctrl.toggleoutgoingSort();
	        String str = ctrl.getTranslatedText();
	        ctrl.newBeneficiary();
	        ctrl.getbuyAndSellCurrency();
	        ctrl.getaccountTableCurrency();
	        ctrl.getaccountTradedCurrency();
	        ctrl.cancelInputs();
	       // ctrl.saveBeneficiary() ;

	        LIST<Input__c> inputSOQL = ctrl.inputSOQL; 
	        String lastUpdatedString = ctrl.lastUpdatedString; 
	        string lineId = ctrl.lineId; 
	        String soql2 = ctrl.soql2; 
	        
	        List<inputWpr> outgoingPgCtlCTM_genericList = ctrl.outgoingPgCtlCTM_genericList; 
	        List<inputWpr> holdingPgCtl_genericList = ctrl.holdingPgCtl_genericList; 
	        List<inputWpr> hedgingPgCtl_genericList= ctrl.hedgingPgCtl_genericList; 
	        List<inputWpr> incomingPgCtl_genericList= ctrl.incomingPgCtl_genericList; 
	        String userCurrency = ctrl.userCurrency; 
	       	ctrl.outgoingPgCtlCTM.nextPage();
	       	ctrl.outgoingPgCtlCTM.lastPage();
	       	ctrl.outgoingPgCtlCTM.previousPage();
	       	ctrl.outgoingPgCtlCTM.firstPage();
	       	        
	        String checkBoxOutgoingHtml = ctrl.checkBoxOutgoingHtml;
	        String checkBoxIncomingHtml = ctrl.checkBoxIncomingHtml;
	        String checkBoxHedgingHtml = ctrl.checkBoxHedgingHtml;
	        String checkBoxOutgoingPaidHtml = ctrl.checkBoxOutgoingPaidHtml; 


        }
        Test.stopTest();
    }
    
    
    static testMethod void myUnitTest2() {
    	Test.startTest();
    	acc = test_Utility.createAccount(true);
    	Contact con = New Contact(AccountId = acc.Id, LastName = 'Test', FirstName = 'Test');
    	insert con;
    	User tUser = test_Utility.createCommUser(con.Id, true);
        System.runAs(tUser){
	        createTestData();
	        ApexPages.currentPage().getParameters().put('aId', acc.Id);
	        apexpages.currentPage().getParameters().put('cId','test12341234test');	        
	        
	        ManageCashInputsCtl ctrl = new ManageCashInputsCtl();
	        ctrl.record.Input_Type__c ='Option - WUBS';
	        ctrl.selectedCurrency = 'USD';
	        ctrl.NewRecord();
	        ctrl.record.Input_Type__c ='Forecast Inflow';
	        ctrl.NewRecord();
	        ctrl.record.Input_Type__c ='Window Forward Contract - Other';
	        ctrl.NewRecord();
	        ctrl.record.Input_Type__c ='Fixed Forward Contract - Other';
	        ctrl.NewRecord();
	        ctrl.toggleIncomingSort();
	        ctrl.togglehedgingSort();
	        ctrl.toggleoutgoingSort();
	        ctrl.PopulateLists();
	        String contact = ctrl.findContact();
	        ctrl.Cancel();
	        ctrl.quickSave();
	       
	        ctrl.cancelBeneficiary();
	        ctrl.deleteInputs();
	        ctrl.getitems();
	        ctrl.getbeneficiaries();
	        ctrl.toggleholdingSort();
	        ctrl.newBeneficiary();
	        ctrl.ib.Name = 'test';
	        ctrl.saveBeneficiary();

	       	        
	        String checkBoxOutgoingHtml = ctrl.checkBoxOutgoingHtml;
	        String checkBoxIncomingHtml = ctrl.checkBoxIncomingHtml;
	        String checkBoxHedgingHtml = ctrl.checkBoxHedgingHtml;
	        String checkBoxOutgoingPaidHtml = ctrl.checkBoxOutgoingPaidHtml; 
        }
        Test.stopTest();
    }
    
    
    
    private static void createTestData() {
        test_Utility.createWUEdgeSharingAdmin();
    	Market_Rate__c mktRate = new Market_Rate__c(Currency_Code__c = 'USD', Currency_Name__c='test', Currency_Value__c=5.0);
    	insert mktRate;
    	Input_Beneficiary__c inputBeneficiary = new Input_Beneficiary__c(Name = 'WUBS');
    	insert inputBeneficiary;    	
    	
    	Invoice__c invoice = new Invoice__c(account__c = acc.Id);
    	insert invoice;
    	List<Input__c> inputList = new List<Input__c>();
    	inputList.add(test_Utility.createInputWithType(false, 'Purchase Order', acc.Id));
    	inputList.add(test_Utility.createInputWithType(false, 'Forecast Outflow', acc.Id));
    	inputList.add(test_Utility.createInputWithType(false, 'Forcast Inflow', acc.Id));
    	inputList.add(test_Utility.createInputWithType(false, 'Window Forward Contract - Other', acc.Id));
    	inputList.get(3).Buy_Currency__c = 'INR';
    	inputList.get(3).Sell_Currency__c = 'INR';
    	inputList.get(3).Window_End_Date__c = Date.Today().addDays(6);
    	inputList.get(3).Window_Start_Date__c = Date.Today();
    	insert inputList;
    	
    //	mktRate.Currency
    }
}