/**=====================================================================
 * Name: EDGE_InvoiceManagement
 * Description: Related to EDGE_Invoice_Management Page
 * Created Date: Mar 09, 2016
 * Created By: Rohit Sharma (JDC)
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
public without sharing class EDGE_InvoiceManagement {

    public String getLocaleDate() {
        Utility utility = new Utility();
        return utility.localeDate;
    }

    public String localeDate{
          get{
              Utility utility = new Utility();
              return utility.localeDate;
          }
      }
      public String localeDateRegx{
          get{
              String dtStr = localeDate.replace('yyyy', '(\\d{2,4})');
          dtStr = dtStr.replace('mm', '(\\d{1,2})\\');
          dtStr = dtStr.replace('dd', '(\\d{1,2})\\');
              return '/'+dtStr+'( (\\d{1,2}):(\\d{1,2}))? ?(am|pm|AM|PM|Am|Pm)?/';
          }
      }



    public List<InvoiceUIWrapper> buyerInvoices{get;set;}
    public List<InvoiceUIWrapper> beneficiaryInvoices{get;set;}
    public List<SelectOption> statusList{get;set;}
    public String status{get;set;}
    public List<SelectOption> currencyList{get;set;}
    public String currencyString{get;set;}
    public Boolean createTransactionPermForSP{get;set;}
    public Boolean createTransactionPermForH2H{get;set;}
    public string currentAccount{get;set;}
    public String userDefaultCurrency{get;set;}
    public String userCurrency{get;set;}
    private List<String> gPId;
    public Decimal aggregateTotal{get;set;}
    public Decimal hedgedTotal{get;set;}
    public Decimal unHedgedTotal {get;set;}
    public Decimal totalInHomeCurrency {get;set;}
    public Decimal conversionRate{get;set;}
    public List<InvoiceUIWrapper> invoices{get;set;}
    public List<String> selectedStatusSet{get;set;}
    public String currencyS{get;set;}
    public String requestType{get;set;}
    public String xlsType{get;set;}
    public integer loadCurrency{get;set;}


    public EDGE_InvoiceManagement() {
    status = '';
        currencyString = '';
        //statusList = getInvoiceStatus();
        currencyList =  getInvoiceCurrency();
        currentAccount = Utility.currentAccount;
        userDefaultCurrency = UserInfo.getDefaultCurrency();
        createTransactionPermForSP = Utility_Security.canPayInvoices_SP;
        createTransactionPermForH2H = Utility_Security.canPayInvoices_H2H;
        gPId = new List<String>();
        userCurrency = Utility.userCurrency;
        for(Global_Pay_ID_Management__c gpRecord : [SELECT Global_Pay_ID__c FROM Global_Pay_ID_Management__c WHERE User_Beneficiary__c=:UserInfo.getUserId()]) {
            gPId.add(gpRecord.Global_Pay_ID__c);
        }
        if(!String.isEmpty(ApexPages.currentPage().getParameters().get('xlsType'))) {
            xlsType = ApexPages.currentPage().getParameters().get('xlsType');
            requestType = xlsType;
            status = !String.isEmpty(ApexPages.currentPage().getParameters().get('status')) ? ApexPages.currentPage().getParameters().get('status') :'';
            currencyString = !String.isEmpty(ApexPages.currentPage().getParameters().get('currencyString')) ? ApexPages.currentPage().getParameters().get('currencyString') : '';
            searchInvoice();
        } else {
            conversionRate = 0.0;
            aggregateTotal = 0.0;
            hedgedTotal = 0.0;
            unHedgedTotal = 0.0;
            totalInHomeCurrency = 0.0;
            invoices = new List<InvoiceUIWrapper>();
        }
        System.Debug('###'+currencyString);
    }

    public Pagereference refreshInvoices() {
        //loadCurrency=1;
        toggleSort();
        return null;
    }

    public Pagereference resetInvoices() {
        status = '';
        currencyString = '';
        //loadCurrency=1;
        toggleSort();
        return null;
    }

    public List<SelectOption> payCurrencyOptions{
        get{

            String strQuery = 'Select count(Id), Custom_Currency__c FROM Invoice__c' ;
            String accountString = 'Account__c=\''+currentAccount+'\'';
            if(requestType=='Receivables') {
            accountString = '(Seller_Account__c=\''+currentAccount+'\' ' + 'OR (Supplier__c != null AND Supplier__r.Supplier__c=\''+currentAccount+'\'))';
            }
            strQuery +=' WHERE '+accountString+'  group by  Custom_Currency__c';

            if(payCurrencyOptions==null){
                payCurrencyOptions = new List<SelectOption>();
                set<String> currencySet = new set<String>();
                payCurrencyOptions.add(new SelectOption('',Label.CM_EDGE_Select));
                for(AggregateResult Invoice: database.query(strQuery)){
                    String currencyCode = (String) Invoice.get('Custom_Currency__c');
                    if(!String.isBlank(currencyCode) && !currencySet.Contains(currencyCode)) {
                        currencySet.add(currencyCode);
                        payCurrencyOptions.add(new SelectOption(currencyCode,currencyCode));
                    }
                }
            }
            return payCurrencyOptions;
        }
        set;
    }

    public String currentUserLocale{
        get{
            String currentUserLocaleVal = UserInfo.getLocale();
            if(currentUserLocaleVal.contains('_')){
                currentUserLocaleVal = currentUserLocaleVal.substring(0, currentUserLocaleVal.indexOf('_'));
            }
            return currentUserLocaleVal;
        }
        set;
    }

    public String aggregateTotalFormat{
        get{
            String aggregateTotalVal = aggregateTotal.format();
            if(!aggregateTotalVal.contains(Utility.getDecimalChar)){
                aggregateTotalVal = aggregateTotal.format() + Utility.getDecimalChar+'00';
            }
            return aggregateTotalVal;
        }
        set;
    }

    public String hedgedTotalFormat{
        get{
            String hedgedTotalVal = hedgedTotal.format();
            if(!hedgedTotalVal.contains(Utility.getDecimalChar)){
                hedgedTotalVal = hedgedTotal.format() + Utility.getDecimalChar+'00';
            }
            return hedgedTotalVal;
        }
        set;
    }

    public String defaultValueFormat{
        get{
            Integer aTemp = 0;
            String efaultValue = aTemp.format();
            if(!efaultValue.contains(Utility.getDecimalChar)){
                efaultValue = aTemp.format() + Utility.getDecimalChar+'00';
            }
            return efaultValue;
        }
        set;
    }

    public String unHedgedTotalFormat{
        get{
            String unHedgedTotalVal = unHedgedTotal.format();
            if(!unHedgedTotalVal.contains(Utility.getDecimalChar)){
                unHedgedTotalVal = unHedgedTotal.format() + Utility.getDecimalChar+'00';
            }
            return unHedgedTotalVal;
        }
        set;
    }

    public String unHedgedTotalSettlementFormat{
        get{
            String unHedgedTotalSettlementVal = unHedgedTotalSettlement.format();
            if(!unHedgedTotalSettlementVal.contains(Utility.getDecimalChar)){
                unHedgedTotalSettlementVal = unHedgedTotalSettlement.format() + Utility.getDecimalChar+'00';
            }
            return unHedgedTotalSettlementVal;
        }
        set;
    }

    public Decimal aggregateTotalSettlement{
        get{
            return Utility.getSettlementCurrencyAmount(aggregateTotal, currencyString, UserInfo.getDefaultCurrency());
        }
    }
    public String aggregateTotalSettlementFormat{
        get{
            String aggregateTotalSettlementVal = aggregateTotalSettlement.format();
            if(!aggregateTotalSettlementVal.contains(Utility.getDecimalChar)){
                aggregateTotalSettlementVal = aggregateTotalSettlement.format() + Utility.getDecimalChar+'00';
            }
            return aggregateTotalSettlementVal;
        }
        set;
    }
    public Decimal hedgedTotalSettlement{
        get{
            return Utility.getSettlementCurrencyAmount(hedgedTotal, currencyString, Utility.userCurrency);
        }
    }
    public Decimal unHedgedTotalSettlement {
        get{
            return Utility.getSettlementCurrencyAmount(unHedgedTotal, currencyString, Utility.userCurrency);
        }
    }

    public List<String> getInvoiceStatus() {
         //List<SelectOption> options = new List<SelectOption>();
         //Map<String,String> optionsMap = new Map<String,String>();
         List<String> options = new List<String>();

         //optionsMap.put('',Label.CM_EDGE_Select);
         Schema.DescribeFieldResult fieldResult = Invoice__c.Status__c.getDescribe();
         List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
         if(Utility_Security.canCreateTransaction){
             for( Schema.PicklistEntry f : ple) {
                 if(f.getValue()!='Pending Approval (Open)' && f.getValue()!='Approved' && f.getvalue() != 'Paid'){
                  //options.add(new SelectOption(f.getValue(), f.getLabel()));
                  //optionsMap.put(f.getValue(), f.getLabel());
                  options.add(f.getLabel());
                 }
             }
         }
         else{
             for( Schema.PicklistEntry f : ple) {
                 if(f.getValue()=='Pending Approval (Open)' || f.getValue()=='Approved' || f.getvalue() == 'Paid' || f.getvalue() == 'Void'){
                  //options.add(new SelectOption(f.getValue(), f.getLabel()));
                  //optionsMap.put(f.getValue(), f.getLabel());
                  options.add(f.getLabel());
                 }
             }
         }
         return options;
    }

    public List<SelectOption> getInvoiceCurrency() {
         List<SelectOption> options = new List<SelectOption>();
         options.add(new SelectOption('',Label.CM_EDGE_Select));
         Schema.DescribeFieldResult fieldResult = Invoice__c.Custom_Currency__c.getDescribe();
         List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
         for( Schema.PicklistEntry f : ple) {
              options.add(new SelectOption(f.getLabel(), f.getValue()==currencyString ? 'Currenct('+currencyString+')':f.getValue()));
         }
         return options;
    }

    public void toggleSortAction() {
        loadCurrency = 0;
        toggleSort();
    }

    public void toggleSort() {
        // simply toggle the direction
        System.debug('1>> '+ loadCurrency);
        searchInvoice();
        populateAmountField();
    }


    public void searchInvoice(){
        System.debug('>> chkpoint 2>> ');
        String strQuery = 'Select Name,Account__c,RecordType.Name,Custom_Currency__c,Delivery_Method__c,Type__c,Due_Date__c,Amount__c,Beneficiary_GP_ID__c,Beneficiary_Name__c,Beneficiary_GP_Name__c,Supplier__c,'+
                          'Status__c,Buyer__c,Buyer__r.Name,Initiated_By__c,TypeTrans__c,Buyer__r.Contact.AccountId,CreatedBy.Contact.Account.Name,CreatedBy.Name,Id, Invoice_Number__c, Account__r.Name, Reference_Number__c FROM Invoice__c' ;
        selectedStatusSet = new List<String>();
        String statusString = '';
        if (!String.isEmpty(status)) {
            //statusString = 'Status__c =\''+status+'\' AND ';
             if(status.contains(',')){
                List<String> statusStringList = status.split(',');
                statusString = 'Status__c IN :statusStringList AND ';
            }else{
                statusString = 'Status__c =\''+status+'\' AND ';
            }
        }
        String selectCurrency = '';
        if(!String.isEmpty(currencyString)){
            selectCurrency = ' AND Custom_Currency__c =\''+currencyString+'\' ';
        }
        String accountString = 'Account__c=\''+currentAccount+'\'';
        if(requestType=='Receivables') {
            accountString = '(Seller_Account__c=\''+currentAccount+'\' ' + 'OR (Supplier__c != null AND Supplier__r.Supplier__c=\''+currentAccount+'\'))';
        }
        strQuery +=' WHERE '+statusString+accountString+selectCurrency+'  ORDER BY Due_Date__c DESC';
        System.debug('>> chkpoint 3>> '+ strQuery);
        system.debug('requestType : '+strQuery);
        invoices = new List<InvoiceUIWrapper>();
        InvoiceUIWrapper invWrapper  ;

        for(Invoice__c Invoice: database.query(strQuery)){
            if(Invoice.RecordTypeId != null)
            {
                if(requestType=='Receivables'){
                    invWrapper = new InvoiceUIWrapper(Invoice);
                     if(Invoice.TypeTrans__c.equalsIgnoreCase(Label.CM_SectionHeading_InvoiceDetail_Payables)){
                         invWrapper.getCategoryType = Label.CM_EDGE_Receivables_Others;
                     }
                }
                else
                {
                    invWrapper = new InvoiceUIWrapper(Invoice);
                    
                }
                invoices.add(invWrapper);
                
            }
        }

        system.debug('requestType : '+invoices.size());
    }

    public Pagereference populateAmountField() {
        aggregateTotal = 0.0;
        hedgedTotal = 0.0;
        unHedgedTotal = 0.0;
        totalInHomeCurrency = 0.0;
        Decimal hedgeAccount = 0.0;
        Decimal marketRate = 0.0;
        Decimal holdingBalance = 0.0;
        conversionRate = 0.0;
        currencyS = currencyString;
        if(currencyS == null ||  currencyS == '') {
            currencyS = userDefaultCurrency;
            //return null;
        }
        //conversionRate = [SELECT conversionrate FROM currencytype WHERE isocode =: selectedCurrency LIMIT 1].conversionRate;
        // Get Market rates for the selected currency
        for(Market_Rate__c rate : [SELECT Id, Currency_Code__c, Currency_Value__c
                                   FROM Market_Rate__c
                                   WHERE Currency_Code__c =:currencyS]) {
            conversionRate = rate.Currency_Value__c;
        }
        for(AggregateResult aggregateResult : [SELECT sum(FX_Balance__c) settlementAmount,
                                               avg(Base_USD__c) marketRate
                                               FROM Forward_Contracts__c
                                               WHERE Account_ID__c =: currentAccount
                                               AND Custom_Currency__c =: currencyS]) {

            if(aggregateResult.get('settlementAmount') != null)
            hedgeAccount = (Decimal)    aggregateResult.get('settlementAmount');
            if(aggregateResult.get('marketRate') != null)
            marketRate = (Decimal)    aggregateResult.get('marketRate');

        }
        //conversionRate = marketRate;
        for(AggregateResult aggregateResult : [SELECT sum(Amount__c) holdingBalance
                                               FROM Holding_Balance__c
                                               WHERE Account__c =: currentAccount
                                               AND Custom_Currency__c =: currencyS]) {

           if(aggregateResult.get('holdingBalance') != null)
           holdingBalance = (Decimal)   aggregateResult.get('holdingBalance');
         }
         hedgedTotal = hedgeAccount + holdingBalance;
         for(InvoiceUIWrapper invoiceRec: invoices){
            if(invoiceRec.Invoice.Custom_Currency__c == currencyS && invoiceRec.Invoice.Amount__c != null) {
                    aggregateTotal += invoiceRec.Invoice.Amount__c;
                }
         }
         if(conversionRate==0 || conversionRate<0){
            //Conversion market not found for selected currency.
            totalInHomeCurrency = aggregateTotal;
         }else{
            totalInHomeCurrency = aggregateTotal / conversionRate;
         }
         unHedgedTotal = aggregateTotal - hedgedTotal;
         return null;
    }
}