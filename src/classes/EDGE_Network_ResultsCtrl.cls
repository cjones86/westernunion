//Modified By : Priyanka Kumar   24th May 2016  T-504975
Public without sharing class EDGE_Network_ResultsCtrl{

    static apexLogHandler.apexLog logSaveInvItr = new apexLogHandler.apexLog('Network_ResultsCtrl','saveInvItr');
    static apexLogHandler.apexLog logInviteNewUserTOCMP = new apexLogHandler.apexLog('Network_ResultsCtrl','inviteNewUser');
    static apexLogHandler.apexLog logResendInvitation = new apexLogHandler.apexLog('Network_ResultsCtrl','resendInvitation');
    static apexLogHandler.apexLog logInviteUserTONetwork = new apexLogHandler.apexLog('Network_ResultsCtrl','inviteUserTONet');

    Public List<networkWrapper> listNetworkConnections{get;set;}
    Public Map<Id,String> mapNetIdAndStatus;
    Public String netStatus{get;set;}
    public string netConId {get;set;}
    public Map<Id,String> mapConIdAndCompanyName;
    public Map<String,Id> mapCompIdAndCompanyName;
    // public Boolean isUserPrimary{get;set;}
    Public User currentUser;
    Public String successMessage{get;set;}
    Public String errorMessage{get;set;}
    // Public List<SelectOption> statusList{get;set;}
    // public String status{get;set;}
    // public String h2hStatus{get;set;}
    // Public List<SelectOption> h2hStatusList{get;set;}
    Public String company{get;set;}
    Public boolean isFilterApplied;
    Public List<inviteWrapper> listInvites{get;set;}
    public String beneEmail{get;set;}
    public boolean isUserExists{get;set;}
    public boolean isUserAlreadyExists{get;set;}
   // public Set<Id> userContactId;
    public Map<Id,Id> mapInviteeContactIdAndAccId;
    public Id inviteToUpdate{get;set;}
    public String  PersonalMessage{get;set;}
    // public String isCntPrimary{get;set;}
    public Map<Id,User> mapContactIdAndUser;
           public Map<String,String> mapToStoreResults {get;set;}
           public Map<String,String> mapToStoreResultsAnd{get;set;}
           List<errorWrapper> errorsOrSucessMessage{get;set;}
           public boolean errorSize{get;set;}


    public EDGE_Network_ResultsCtrl(){
        errorsOrSucessMessage = new List<errorWrapper>();
        mapToStoreResults =    new Map<String,String>();
        mapToStoreResultsAnd = new Map<String,String>();
        errorSize = false;
        netConId  ='';
        successMessage = '';
        errorMessage = '';
        fetchRequestDetails();
    }

    public string getFullAddress(String street, String city,String state,String country, String postalCode){
        String address = '';
        if(String.isNotBlank(street))
         address = street + ', ';
        if(String.isNotBlank(city))
         address += city + ', ';
        if(String.isNotBlank(state))
         address += state + ', ';
        if(String.isNotBlank(country))
         address += country + ', ';
        if(String.isNotBlank(postalCode))
         address += postalCode;
        return address;
    }
    public static map<String, String> networkStatusTranslation{
		get{
			if(networkStatusTranslation==null){
				Schema.DescribeFieldResult F = Network__c.Status__c.getDescribe();
				List<Schema.PicklistEntry> P = F.getPicklistValues();
				networkStatusTranslation = new Map<String, String>();

				for(Schema.PicklistEntry e : P)
				{
				    networkStatusTranslation.put(e.value, e.label);
				}
			}
			return networkStatusTranslation;
    	}
    	set;
    }

    public static map<String, String> invitesStatusTranslation{
		get{
			if(invitesStatusTranslation==null){
				Schema.DescribeFieldResult F = Invites__c.Status__c.getDescribe();
				List<Schema.PicklistEntry> P = F.getPicklistValues();
				invitesStatusTranslation = new Map<String, String>();

				for(Schema.PicklistEntry e : P)
				{
				    invitesStatusTranslation.put(e.value, e.label);
				}
			}
			return invitesStatusTranslation;
    	}
    	set;
    }
    // public List<SelectOption> getNetworkStatus() {
    //      List<SelectOption> allStatus = new List<SelectOption>();
    //      allStatus.add(new SelectOption('','Status'));
    //      Schema.DescribeFieldResult fieldResult = Network__c.Status__c.getDescribe();
    //      List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
    //      for( Schema.PicklistEntry f : ple) {
    //           allStatus.add(new SelectOption(f.getLabel(), f.getValue()==status ? 'Status('+status+')':f.getValue()));
    //      }
    //      return allStatus;
    // }

    public void fetchRequestDetails(){
        Boolean isCommUser = (Utility.isCCTuser || Utility.isNonCCTuser);
        listNetworkConnections = new List<networkWrapper>();
        listInvites = new List<inviteWrapper>();
        mapContactIdAndUser = new Map<Id,User>();
      //  mapNetIdAndStatus = new Map<Id,String>();
        List<Network__c> listNetworks = new List<Network__c>();
        Set<Id> inviteeContactIds = new Set<Id>();
        Set<Id> contactIds = new Set<Id>();
        mapConIdAndCompanyName = new Map<Id,String>();
        mapCompIdAndCompanyName = new Map<String,Id>();
        currentUser = Utility.loggedInUser;
        // if(currentUser.Contact.Primary_Contact__c)
        //         isUserPrimary = true;
        if(isCommUser){

            for(Invites__c inv : [Select Id, Status__c,Benne_Email__c From Invites__c
                                    Where Inviter_User__r.AccountId =:currentUser.Contact.AccountId And Status__c IN ('Sent','Expired')]){
                listInvites.add(new inviteWrapper(inv));

              // Commented following line to avoid invite records from network table, since there is another table for invite records.
              //listNetworkConnections.add(new networkWrapper(inv));

            }

            //System.debug()

            set<Id> accIds = new set<Id>();
            for(Network__c net : [Select Id, Inviter_User__c, Invitee_Contact__c, Status__c, Status_DateTime__c, Type__c,
                                    Invitee_Contact__r.Name, Invitee_Contact__r.Account.Name,Inviter_User__r.Contact.AccountId,
                                    Inviter_User__r.ContactId, LastModifiedBy.Contact.AccountId,Invitee_Contact__r.AccountId,Inviter_User__r.Name,
                                    Account_Invitee__c, Account_Inviter__c
                                    From Network__c
                                    Where ((Account_Inviter__c = :currentUser.Contact.AccountId And Account_Invitee__c != null)
                                        OR (Account_Invitee__c = :currentUser.Contact.AccountId And Account_Inviter__c != null))
                              			And Invitee_Contact__c!=null and Hide_from_Network_Connections__c = false ] ){
                  //  System.debug('>>1 ' + net.Inviter_User__r.Contact.AccountId + ' ::1 ' + currentUser.Contact.AccountId);
                  //  System.debug('>>2 ' + net.Invitee_Contact__r.AccountId + ' ::2 ' + currentUser.Contact.AccountId);


                    listNetworks.add(net);
                    if(net.Inviter_User__c == currentUser.Id)
                    {
                      contactIds.add(net.Invitee_Contact__c);
                      System.debug('if'+net.Invitee_Contact__c);

                    }
                  else
                  {
                                            contactIds.add(net.Inviter_User__r.ContactId);
                                            System.debug('else'+net.Inviter_User__r.ContactId);

                  }

            }

            System.debug('listNetworks::: ' + listNetworks);

            System.debug('contactIds::: ' + contactIds);

            Map<Id,Boolean> mapContactIdAndH2HStatus = new Map<Id,Boolean>();
            Map<Id,String> mapConIdAndAddress = new Map<Id,String>();

            for(User usr : [Select Id,Name, ContactId, EDGE_Payment_Approvals__c,CMP_Create_Transactions__c, CMP_Enabled__c,CMP_Network_Invitation_Enabled__c,
                                    Contact.Account.CMP_Holding_Enabled__c, Contact.Account.CMP_H2H_Transaction_Enabled__c,
                                    Contact.Account.BillingStreet ,Contact.Account.BillingCity, Contact.Account.BillingState,
                                    Contact.Account.BillingCountry, Contact.Account.BillingPostalCode,EDGE_Submit_Invoice_enabled__c,
                                    /*Contact.MailingStreet, Contact.MailingCity,Contact.MailingState,Contact.MailingCountry, Contact.MailingPostalCode,*/
                                    Contact.Account.CMP_Enabled__c, Contact.Account.CMP_Create_Transactions__c,Contact.Account.Name,Contact.AccountId
                            From User Where ContactId = : contactIds]){
                        Utility_Security.us = usr;
                        mapContactIdAndUser.put(usr.ContactId,usr);
                        mapContactIdAndH2HStatus.put(usr.ContactId,Utility_Security.canH2HTransaction);
                        mapConIdAndCompanyName.put(usr.ContactId,usr.Contact.Account.Name);
                        //Start : T-504975 :Priyanka
                        if(!mapCompIdAndCompanyName.containskey(usr.Contact.Account.Name))
                          mapCompIdAndCompanyName.put(usr.Contact.Account.Name,usr.Contact.Account.Id);
                        //End : T-504975 :Priyanka
                        String address = getFullAddress(usr.Contact.Account.BillingStreet, usr.Contact.Account.BillingCity,usr.Contact.Account.BillingState,usr.Contact.Account.BillingCountry, usr.Contact.Account.BillingPostalCode);
                        mapConIdAndAddress.put(usr.ContactId,address);
            }

            for(Network__c net : listNetworks){
                if(net.Account_Inviter__c == currentUser.Contact.AccountId && mapContactIdAndH2HStatus.containsKey(net.Invitee_Contact__c)){
                    boolean isH2HCapable = mapContactIdAndH2HStatus.get(net.Invitee_Contact__c);
                    String accName = mapConIdAndCompanyName.get(net.Invitee_Contact__c);
                    String address = mapConIdAndAddress.get(net.Invitee_Contact__c);
                    String compId = mapCompIdAndCompanyName.get(accName);
                    listNetworkConnections.add( new networkWrapper(net,isH2HCapable,compId,accName,currentUser,address, mapContactIdAndUser.get(net.Inviter_User__r.ContactId)));
                }
                else {
                    boolean isH2HCapable = mapContactIdAndH2HStatus.get(net.Inviter_User__r.ContactId);
                    String accName = mapConIdAndCompanyName.get(net.Inviter_User__r.ContactId);
                    String address = mapConIdAndAddress.get(net.Inviter_User__r.ContactId);
                    String compId = mapCompIdAndCompanyName.get(accName);
                    listNetworkConnections.add( new networkWrapper(net,isH2HCapable,compId,accName,currentUser,address,null));
                }
            }
        }
    }


    public class inviteWrapper{
        public Invites__c invite{get;set;}
		public String invStatusTrans{
			get{
				if(invitesStatusTranslation.containsKey(invite.Status__c)){
					return invitesStatusTranslation.get(invite.Status__c);
				}else{
					return invite.Status__c;
				}
			}
		}
		public inviteWrapper(Invites__c inv){
            this.invite = inv;
        }
    }

    public class networkWrapper{
        public String isH2HEnabled {get;set;}
        public Network__c net {get;set;}
        public String companyName {get;set;}
        public String address {get;set;}
        public String netStatus {get;set;}
        public boolean canReactivate{get;set;}
        public boolean canDeactivate{get;set;}
        public boolean canAccept{get;set;}
        public boolean canReject{get;set;}
        public boolean canResendInvite{get;set;}
        public boolean canInviteeSubmitInvoice{get;set;}
        public boolean canInviterSubmitInvoice{get;set;}
        public boolean canInviteToCMP{get;set;}
        public String inviteeUserName{get;set;}
        public String inviteeUserId{get;set;}
        public String companyId{get;set;} //T-504975 :Priyanka
        public String encodedNetworkId{get;set;}

        public networkWrapper(Invites__c invite){
            this.companyName = 'N/A';
            this.address = 'N/A';
            this.netStatus = invite.Status__c;
            this.canInviteToCMP = true;
            this.isH2HEnabled = Label.CM_Field_Value_No;
        }
		public String netStatusTrans{
			get{
				if(networkStatusTranslation.containsKey(netStatus)){
				return networkStatusTranslation.get(netStatus);
			}else{
					return netStatus;
			}
			}
		}

        public networkWrapper(Network__c netwk, Boolean isH2HCapable, String compId, String compName, User currentUser, String address, User inviteeUser){
            this.net = netwk;
            this.netStatus = net.Status__c;
            if(net.Status__c == '4 - Deactivated' && net.LastModifiedBy.Contact.AccountId == currentUser.Contact.AccountId)
                this.canReactivate = true;

            if(net.Status__c=='3 - Active (Accepted)')
                this.canDeactivate = true;

            if(net.Status__c=='1 - Pending (Sent)' && currentUser.Contact.AccountId == net.Invitee_Contact__r.AccountId)
                this.canAccept = true;

            if(net.Status__c=='1 - Pending (Sent)' && currentUser.Contact.AccountId == net.Inviter_User__r.Contact.AccountId)
                this.canResendInvite = true;

            Utility_Security.us = Utility.loggedInUser;
            if(net.Status__c=='3 - Active (Accepted)' && currentUser.Contact.AccountId == net.Invitee_Contact__r.AccountId && inviteeUser==null && Utility_Security.canSubmitInvoices) // loggedInUser is invitee
                this.canInviteeSubmitInvoice = true;

            if(net.Status__c=='3 - Active (Accepted)' && currentUser.Contact.AccountId == net.Inviter_User__r.Contact.AccountId && Utility_Security.canSubmitInvoices){ // loggedInUser is inviter
                this.canInviterSubmitInvoice = true;
                if(inviteeUser != null)
                {
                this.inviteeUserName = inviteeUser.Name;
                this.inviteeUserId = inviteeUser.Id;
                }
            }

            if(net.Status__c=='1 - Pending (Sent)' && currentUser.Contact.AccountId == net.Invitee_Contact__r.AccountId ){ // loggedInUser is invitee
                this.canReject = true;
            }

            this.address = address;
            this.companyName = compName;
            encodedNetworkId = Utility.doEncryption(compId);
            if(encodedNetworkId.contains('+')){
                encodedNetworkId = encodedNetworkId.replace('+','%2B');
            }
                this.companyId = encodedNetworkId;

            if(isH2HCapable)
                this.isH2HEnabled = Label.CM_Field_Value_Yes;
            else
                this.isH2HEnabled = Label.CM_Field_Value_No;
        }
    }

    // public void showFilteredResults(){
    //     isFilterApplied = true;
    //     fetchRequestDetails();
    //     List<networkWrapper> listFilteredNetworkConnections = new  List<networkWrapper>();
    //   // System.assert(false,'>> '+ company + '  :: ' + status + ' >> ' + h2hStatus);
    //     for(networkWrapper wrpr: listNetworkConnections){
    //         if(String.isNotBlank(company) && !wrpr.companyName.containsIgnoreCase(company))
    //             continue;
    //         if(status != null && wrpr.net.Status__c != status)
    //             continue;
    //         if(h2hStatus != 'None' && h2hStatus != wrpr.isH2HEnabled)
    //             continue;
    //         listFilteredNetworkConnections.add(wrpr);
    //     }
    //     listNetworkConnections.clear();
    //     listNetworkConnections.addAll(listFilteredNetworkConnections);
    // }

    public void saveInvItr(){
        mapToStoreResultsAnd.clear();
    	try{
    		boolean canUpdate = true;
        logSaveInvItr.logMessage('>>inside saveInvItr method ...>> ');
        System.debug('netConId:: '+ netConId + ' >>> ' + netStatus);
        logSaveInvItr.logDebug('netConId:: '+ netConId + ' >>> ' + netStatus);
        if(netConId != null){
        	/* Priyanka - Start : T-538985 : Prevent activate network connection */
        	for(Supplier__c sup : [Select Id,Beneficiary_Status__c,Network__r.Status__c FROM Supplier__c WHERE Network__c = :netConId]){
        		if(sup.Beneficiary_Status__c == 'In-Active' && sup.Network__r.Status__c == '4 - Deactivated' && netStatus == '3 - Active (Accepted)'){
        			//cannot reactivate
        			System.debug('canupdate ');
        			canUpdate = false;
        			errorMessage = Label.CM_EDGE_Prevent_Activate_Network_Connection;
        			break;
        		}
        	}
        	/* Priyanka - End : T-538985 : Prevent activate network connection */
        	if(canUpdate){
            Network__c network = new Network__c(Id=netConId,Status__c=netStatus);
            System.debug('in inviter save::: ');
            logSaveInvItr.logDebug('in inviter save::: ');
            errorMessage = '';
            update network;
            logSaveInvItr.logDebug('' + network);
            // System.debug('>>> ' + isCntPrimary);
            // if(isCntPrimary=='true'){
            //     Contact cnt = new Contact(Id=currentUser.ContactId);
            //     cnt.Primary_Contact__c = true;
            //     update cnt;
            // }

        }
        fetchRequestDetails();
        PageReference currentPage = ApexPages.CurrentPage();
        currentPage.setRedirect(true);
        logSaveInvItr.saveLogs();
        }
    	}catch(Exception ex){
    		errorMessage = ex.getMessage();
    	}
    }


    public PageReference searchUser() {
        errorSize = false;
        mapToStoreResults.clear();
        mapToStoreResultsAnd.clear();

        isUserExists = false;
        isUserAlreadyExists = false;
        mapInviteeContactIdAndAccId = new Map<Id,Id>();
        Set<Id> existingcontact = new Set<Id>();
        Set<Id> existingAccounts = new Set<Id>();
        Set<Id> existingInviterAccounts = new Set<Id>();
        Decimal limitOfNewUser ;
        for( NewUserLimit__c limitsOFUser : NewUserLimit__c.getAll().values()){
            limitOfNewUser = limitsOFUser.LimitOfUser__c;
        }

        if(String.isBlank(beneEmail)){
            mapToStoreResultsAnd.put(Label.CM_EDGE_Network_EnterEmail,beneEmail);
            return null;
        }
        List<String> benneEmails =  new List<String>();
        benneEmails = beneEmail.split(',');
        System.debug('hELLO'+benneEmails);

        for(Network__c network : [SELECT Invitee_Contact__c,Account_Invitee__c,Account_Inviter__c FROM Network__c
                                        WHERE Account_Inviter__c=:Utility.currentAccount
                                        OR Account_Invitee__c=:Utility.currentAccount]) {
            existingcontact.add(network.Invitee_Contact__c);
            existingAccounts.add(network.Account_Invitee__c);
            existingAccounts.add(network.Account_Inviter__c);
        }



        String searchUsers =  existingcontact.size()>0 ? 'AND ContactId NOT IN:existingcontact':'';


        for(User user : Database.query('SELECT Id,ContactId,Contact.AccountId,CMP_Enabled__c,Email FROM User WHERE Email IN : benneEmails AND IsActive=true And IsPortalEnabled = True And ContactId!=null And Contact.AccountId!=null Limit 1')) { // AND CMP_Enabled__c=true
                  if(existingAccounts.contains(user.Contact.AccountId)){ // To check search email user's account already exists in invitee accounts of current user's network or not.
                    isUserAlreadyExists = true;
                    mapToStoreResults.put(user.Email ,Label.CM_PageMessage_UserAlreadyExists);
                  }
                  else{
                  mapInviteeContactIdAndAccId.put(user.ContactId, user.Contact.AccountId);
                  mapToStoreResults.put(user.Email ,Label.CM_Network_InvitationSentNotification);
                  }
        }
        List<String> newUserEmails = new List<String>();
        boolean limitExceede = false;

        for(String allEmails :  benneEmails){
            if(!mapToStoreResults.containsKey(allEmails)){
                if(newUserEmails.size() < limitOfNewUser){
                    newUserEmails.add(allEmails);
                    System.debug('Hello'+newUserEmails.size());
                }
            else{
                if(!mapToStoreResultsAnd.containsKey(Label.New_User_limit_exceeded_so_Failed)){
                    mapToStoreResultsAnd.put(Label.New_User_limit_exceeded_so_Failed,allEmails );
                    limitExceede = true;
                }
                else{
                    mapToStoreResultsAnd.put(Label.New_User_limit_exceeded_so_Failed, mapToStoreResultsAnd.get(Label.New_User_limit_exceeded_so_Failed)+' , '+ allEmails);
                }
            }
        }}
        // if(limitExceede){
        //     return null;
        // }
        if(mapInviteeContactIdAndAccId.size()>0) {
            inviteUserTONetwork();
            isUserExists = true;
        }
        if(newUserEmails.size() > 0) {
            inviteNewUserTOCMP(newUserEmails);
        }
        for(String mapValues  : mapToStoreResults.KeySet() ){
            if(!mapToStoreResultsAnd.containsKey(mapToStoreResults.get(mapValues))){
                 mapToStoreResultsAnd.put(mapToStoreResults.get(mapValues) ,mapValues);
            }
            else
            {
                mapToStoreResultsAnd.put(mapToStoreResults.get(mapValues) , mapToStoreResultsAnd.get(mapToStoreResults.get(mapValues)) +' , ' + mapValues );
            }
        }
        //code for sending confirmation emails
   //     Utility.sendConfirmationEmailForInvite(mapToStoreResultsAnd);
        fetchRequestDetails();
        return null;
    }

    public void inviteNewUserTOCMP(List<String> emailsForNewUser) {
        List<id> duplicateEmailID =  New List<Id>();
        logInviteNewUserTOCMP.logMessage('>>inside inviteNewUserTOCMP method ...>> ');
        List<Invites__c> invites = new List<Invites__c>();
        for(String email  : emailsForNewUser ){
        Invites__c invite = new Invites__c(Invitation_Sent_Count__c=1,Benne_Email__c=email,Inviter_User__c=UserInfo.getUserId(),
                                           Status__c = 'Sent',Invitation_Token__c=GPIntegrationUtility.generateInvitesTOKEN('Invitation_Token__c'),
                                           Last_Sent_By_User__c=UserInfo.getUserId(),Status_DateTime__c = system.now(),Personal_Message__c = PersonalMessage);
            invites.add(invite);
        }

        try{
            Database.SaveResult[] lsr =  Database.insert(invites,false);
            logInviteNewUserTOCMP.logDebug('' + invites);
            Integer i = 0;
            for(Database.SaveResult sv : lsr){
                if(sv.isSuccess()){
                    mapToStoreResults.put(invites[i].Benne_Email__c , Label.CM_Network_InvitationSentNotification);
                }
                else
                {
                    if(sv.getErrors()[0].getMessage().containsIgnoreCase('duplicate value found')){
                            mapToStoreResults.put(invites[i].Benne_Email__c , Label.CM_Network_InvitationAlreadySentNotification);
                    }
                    else{
                            mapToStoreResults.put(invites[i].Benne_Email__c , Label.CM_Edge_Invalid_Email_Address);
                    }

                }
                i++;
            }
       }
        Catch(Exception ex){
           // System.assert(false, 'Not in catch');
            logInviteNewUserTOCMP.logDebug('Exception ' + ex.getMessage());
            logInviteNewUserTOCMP.saveLogs();
            if(ex.getMessage().containsIgnoreCase('DUPLICATE_VALUE')){
                // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, Label.CM_Network_InvitationAlreadySentNotification));
                errorMessage = Label.CM_Network_InvitationAlreadySentNotification;
                successMessage='';
            }
            else if(ex.getMessage().containsIgnoreCase('INVALID_EMAIL_ADDRESS')){
                errorMessage = Label.CM_Edge_Invalid_Email_Address;
            }
            else{
                // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getMessage()));
                errorMessage = ex.getMessage();
                successMessage='';
            }
        }
        logInviteNewUserTOCMP.saveLogs();
    }

    public void resendInvitation(){
        mapToStoreResultsAnd.clear();
        logResendInvitation.logMessage('>>inside resendInvitation method ...>> ');
        Invites__c invite =  new Invites__c(Id=inviteToUpdate, Status__c='Resend');
        try{
            update invite;
            logResendInvitation.logDebug('' + invite);
        }
        Catch(Exception ex){
            logResendInvitation.logDebug('Exception ' + ex.getMessage());
            logResendInvitation.saveLogs();
            //  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getMessage()));
             errorMessage = ex.getMessage();
             successMessage='';
        }
        logResendInvitation.saveLogs();
    }

    public void inviteUserTONetwork() {
        logInviteUserTONetwork.logMessage('>>inside inviteUserTONetwork method ...>> ');
        List<Network__c> newNetworks = new List<Network__c>();
        for(Id cntId: mapInviteeContactIdAndAccId.keySet()) {
            newNetworks.add(new Network__c(Invitee_Contact__c=cntId,
                                           Personal_Message__c = PersonalMessage,
                                           Inviter_User__c=UserInfo.getUserId(),
                                           Status__c='1 - Pending (Sent)',Status_DateTime__c = system.now(),
                                           Account_Invitee__c=mapInviteeContactIdAndAccId.get(cntId),
                                           Account_Inviter__c=Utility.currentAccount));
        }
        system.debug('>>>>network record'+newNetworks);
        logInviteUserTONetwork.logDebug('>>>>network record'+newNetworks);
        insert newNetworks;
        logInviteUserTONetwork.logDebug('' + newNetworks);
        logInviteUserTONetwork.saveLogs();
    }
    public class errorWrapper{
        public String Message{get;set;}
        public boolean errorOrSucees{get;set;}
        public errorWrapper(String Message,boolean errorOrSucees ){
            this.Message = Message;
            this.errorOrSucees = errorOrSucees;
        }
    }
}