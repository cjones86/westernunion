/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_CompanyProfileController
 * Description: Controller class for EDGE_CompanyProfile page(T-501144)
 * Created Date: 10 May 2016
 * Created By: Priyanka Kumar (Appirio)
 * Modified By : Priyanka Kumar  23 June 2016  I-223556
 * Modified By : Priyanka Kumar  27 June 2016  T-514906
 =====================================================================*/

public class EDGE_CompanyProfileController {

	static apexLogHandler.apexLog logSave = new apexLogHandler.apexLog('CompanyProfileController','save');
	static apexLogHandler.apexLog logGetProfile = new apexLogHandler.apexLog('CompanyProfileController','getProfile');
	
    public Account acc {get;set;}
    public Marketplace_profile__c profile {get;set;}
    public Boolean isSaved{get;set;}
    public Boolean isEditVisible{get;set;}
    public DateTime paymentDate{get;set;}
    private String companyId;
    public Boolean noSelectionLabel{get;set;}
    private static final String SHOW_ALL_DETAILS = System.Label.CM_EDGE_Yes_Show_all_details;
    private static final String NO=System.Label.CM_Edge_NO;
    public String source{get;set;}
    public String sourceCommaString{get;set;}
    public String supply{get;set;}
    public String supplyCommaString{get;set;}
    public String sourceSelectedValues{get;set;}
    public String supplySelectedValues{get;set;}
    public Boolean isWUVerified{get;set;}
    public Boolean noConnect{get;set;}
    public String errorMessage{get;set;}
    public boolean isShowSuccessMsg{get;set;}
    public boolean updateNetworkRecord;
    Network__c relatedNetwork;
    public string selectedSicCode{get;set;}
    public String SelectedIndustryName{get;set;}
    
	public List<SelectOption> IndustryCodeWithName{get;set;}
	public transient  Map<String , String> mapExtendedValue{get;set;}
	public  Map<String, String> mapKeyCodeValueName{get;set;}
	public string lestTry{get;set;}
 	
    
    public Map<String,String> getSourceValues(){
		    Map<String,String> options = new Map<String,String>();
		          
		     Schema.DescribeFieldResult fieldResult =
		     Marketplace_profile__c.Where_do_they_source_from__c.getDescribe();
		     List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		          
		     for( Schema.PicklistEntry f : ple)
		     {
		        options.put(f.getLabel(), f.getValue());
		     }       
		     return options;
  }
  
    public Map<String,String> getSupplyValues(){
		     Map<String,String> options = new Map<String,String>();
		          
		     Schema.DescribeFieldResult fieldResult =
		     Marketplace_profile__c.Where_do_they_supply_to__c.getDescribe();
		     List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		          
		     for( Schema.PicklistEntry f : ple)
		     {
		        options.put(f.getLabel(), f.getValue());
		     }       
		     return options;
    }
  
  
    

    public EDGE_CompanyProfileController(){
        lestTry = 'Kya';
        
        isEditVisible = false;
        noConnect = false;
        sourceSelectedValues = '';
        supplySelectedValues = '';
        isShowSuccessMsg = false;
        
        IndustryCodeWithName = new List<SelectOption>();
        mapExtendedValue = new Map<String , String>();
        mapKeyCodeValueName = new Map<String , String>();
        
        
        Schema.DescribeFieldResult fieldResult = Marketplace_profile__c.Industry_Sic_Code__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
                    if(f.getLabel() !=  'N/A')
            IndustryCodeWithName.add(new SelectOption(f.getValue(), f.getLabel()));
            mapExtendedValue.put(f.getLabel(),f.getValue() );
            mapKeyCodeValueName.put(f.getValue(),f.getLabel() );
        }     
        
        
        
        String compId;
        if(apexpages.currentpage().getparameters().get('id') != null)
         compId = EncryptionManager.doDecrypt(apexpages.currentpage().getparameters().get('id'));
        

        isSaved = true;
        //fieldRender=false;
        if(compId != null && compId != '')
        {
            companyId = compId;
        }else{
            companyId = Utility.currentAccount;
        }

        if(companyId == Utility.currentAccount){
            isEditVisible = true;
        }else{
        	// profile is not user's own account,check if active network exists or not.
        	 for(Network__c network : [Select Id, Name, Account_Invitee__c,Account_Inviter__c,Status__c From Network__c
           Where ((Account_Invitee__c = :Utility.currentAccount And Account_Inviter__c = :companyId) OR
           (Account_Inviter__c = :Utility.currentAccount And Account_Invitee__c = :companyId))]){
           	relatedNetwork = network;
           	break;
           }
           
           if(relatedNetwork == null){
           	//no network exists between current users account and profile account which is opened now
           	  noConnect = true;
           }else if(relatedNetwork != null && relatedNetwork.Status__c != '3 - Active (Accepted)') {
           	  //no active connection exists
              noConnect = true;
           }
        	
        }

    }
    
   public PageReference inviteUserTONetwork() {
        Id cntId;
        for(User us: [Select Id, ContactId From User Where IsActive = True And IsPortalEnabled = True
                                And ContactId!=null And Contact.AccountId!=null And Contact.AccountId=:companyId Limit 1]){
            cntId = us.ContactId;
        }
        for(Network__c network : [Select Id, Name, Account_Invitee__c,Account_Inviter__c,Status__c From Network__c
                                           Where ((Account_Invitee__c = :Utility.currentAccount And Account_Inviter__c = :companyId) OR
                                           (Account_Inviter__c = :Utility.currentAccount And Account_Invitee__c = :companyId))]){
           	relatedNetwork = network;
           	break;
        }
        if(companyId != null && cntId != null && relatedNetwork == null ){
            Network__c newNetwork = new Network__c(Inviter_User__c=UserInfo.getUserId(), 
                                              Invitee_Contact__c = cntId,
                                              Status__c='1 - Pending (Sent)',Status_DateTime__c = system.now(),
                                              Account_Invitee__c=companyId,
                                              Account_Inviter__c=Utility.currentAccount);
            try{
                insert newNetwork;
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Invite has been sent.'));
                //errorMessage = 'Invite has been sent.';
                isShowSuccessMsg = true;
                return null;
            }Catch(exception ex){
                
            }
        }else if(relatedNetwork != null && relatedNetwork.Status__c != '3 - Active (Accepted)'){
        	//update the existing network record.
        	relatedNetwork.Status__c = '0 - Resend';
          update relatedNetwork;
          isShowSuccessMsg = true;
        	
        }
        return null;
    }
    

    public pageReference edit(){
        isSaved = false;
        return null;
    }

    public pageReference save(){
    	logSave.logMessage('>>inside save method ...>> ');
        isSaved = true;
        try{
            if(profile.Selected_Visibility__c==SHOW_ALL_DETAILS){
               profile.Goods_Services_Buy_Visibility__c=True;
               profile.Goods_Services_Sell_Visibility__c=True;
               profile.Source_From_Visibility__c=True;
               profile.What_Supplied_Visibility__c=True;
               profile.Which_Industry_Visibility__c=True;
               profile.Accreditation_visibilty__c = True;
            }
            
            system.debug('source>>>'+source + 'profile>>>'+profile.Where_do_they_source_from__c);
            logSave.logDebug('source>>>'+source + 'profile>>>'+profile.Where_do_they_source_from__c);
            if(source != ''){
	            source = source.replace(',',';');
	            profile.Where_do_they_source_from__c = source;
            }
            if(supply != ''){
	            supply = supply.replace(',',';');
	            profile.Where_do_they_supply_to__c = supply;
            }
            profile.Industry_Sic_Code__c  = selectedSicCode;
            update profile;
            logSave.logDebug('profile ' + profile);
            getProfile();
        }Catch(Exception ex){
        	logSave.logDebug('Exception ' + ex.getMessage());
            ApexPages.addMessages(ex);
        }
        logSave.saveLogs();
        return null;
    }

    public pageReference cancel(){
        isSaved = true;
        String compId = apexpages.currentpage().getparameters().get('id');
        if(compId != null && compId != '')
        {
            companyId = compId;
        }else{
            companyId = Utility.currentAccount;
        }
        
        getProfile();
        return null;
    }

    public void getProfile(){
        lestTry = 'Hello';
       /// SelectedIndustryName = 'None';
    	logGetProfile.logMessage('>>inside getProfile method ...>> ');
        if(companyId != null){
            List<Marketplace_profile__c> profileList = [Select Account__c,What_goods_services_do_they_Buy__c,What_goods_services_do_they_Sell__c,
                        What_Industry_do_they_belong_to__c,Where_do_they_source_from__c,Where_do_they_supply_to__c, Selected_Visibility__c,
                        Goods_Services_Buy_Visibility__c , Goods_Services_Sell_Visibility__c , Source_From_Visibility__c , What_Supplied_Visibility__c,
                        Which_Industry_Visibility__c,Accreditation_Certification__c,Accreditation_visibilty__c,Account__r.Id,Account__r.Name,Account__r.Legal_Company_Name__c,Account__r.Employee_Email__c,
                        Account__r.BillingPostalCode,Account__r.BillingState,Account__r.BillingCity,Account__r.BillingCountry,Account__r.BillingStreet,
                        Account__r.Account_Status_GBP__c,Industry_Sic_Code__c
                        FROM Marketplace_profile__c WHERE Account__c = :companyId ORDER BY CreatedDate LIMIT 1];
            if(profileList.size() > 0){
              profile = profileList[0];
              selectedSicCode = profileList[0].Industry_Sic_Code__c;
              SelectedIndustryName = mapKeyCodeValueName.get(profileList[0].Industry_Sic_Code__c);
              
            if(String.isNotBlank(profile.account__r.Account_Status_GBP__c) && 
                (profile.account__r.Account_Status_GBP__c.equalsIgnoreCase('Active') || profile.account__r.Account_Status_GBP__c.equalsIgnoreCase('Yet to Trade')))
                  isWUVerified = true;
            else
                  isWUVerified = false;
              
              
              if(profile.Where_do_they_source_from__c != null){
                  sourceSelectedValues = profile.Where_do_they_source_from__c.replace(';',',');
                  String[] sourceArray = profile.Where_do_they_source_from__c.split(';');
                  system.debug('sourceArray>>>'+sourceArray);
                  logGetProfile.logDebug('sourceArray>>>'+sourceArray);
                  Map<String,String> mapSourceValues = getSourceValues();
                  sourceCommaString = '';
                  system.debug('mapSourceValues>>>'+mapSourceValues);
                  logGetProfile.logDebug('mapSourceValues>>>'+mapSourceValues);
                  for(String sourceKey : mapSourceValues.KeySet()){
	                  for(String src : sourceArray){
	                  	if(src ==  mapSourceValues.get(sourceKey)){
	                  		if(sourceCommaString == '' && sourceKey != null)
	                  		   sourceCommaString = sourceKey;
	                  		else
	                  		  sourceCommaString += ','+ sourceKey; 
	                  	}
	                  }
                  }
                  
              }else{
                  sourceCommaString = '';
                  sourceSelectedValues = '';
              }
                  
              if(profile.Where_do_they_supply_to__c != null){
                  supplySelectedValues = profile.Where_do_they_supply_to__c.replace(';',',');
                  String[] supplyArray = profile.Where_do_they_supply_to__c.split(';');
                  Map<String,String> mapSupplyValues = getSupplyValues();
                  supplyCommaString = '';
                  for(String supplyKey : mapSupplyValues.KeySet()){
                    for(String sup : supplyArray){
                      if(sup ==  mapSupplyValues.get(supplyKey)){
                        if(supplyCommaString == '' && supplyKey != null)
                           supplyCommaString = supplyKey;
                        else
                          supplyCommaString += ','+ supplyKey; 
                      }
                    }
                  }
                  
              }else{
                  supplyCommaString = '';     
                  supplySelectedValues = '';
              }
                  
                  
              if(profile.Selected_Visibility__c==NO)
              {
                  noSelectionLabel=true;
              }
            }else{
              //create a new profile
              Marketplace_profile__c prof = new Marketplace_profile__c(Account__c = companyId);
              insert prof;
              logGetProfile.logDebug('prof ' + prof);
              profile = prof;
            }
           
            Set<id> setOfInvoice= new Set<id>();
            for(Invoice__c inv: [Select Id From Invoice__c Where Account__c = :companyId])
                setOfInvoice.add(inv.id);
            
            for(Payment__c payement:  [Select id, Payment_Date_Time__c from Payment__c where Invoice_ID__c in:setOfInvoice order By Payment_Date_Time__c  limit 1])
                paymentDate=payement.Payment_Date_Time__c;
        }
        logGetProfile.saveLogs();
    }
}