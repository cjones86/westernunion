@isTest
public class AttachmentTriggerManager_Test {
    static testMethod void myUnitTest() {
        createTestData();
    }
    
    private static void createTestData() {
    	ID naInvestigationCaseRecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Operations' AND SobjectType = 'Case'].Id;
        Account newAccount = new Account(Name = 'Test Account', OwnerId = UserInfo.getUserId());
        insert newAccount;
        Contact newContact = new Contact(AccountId = newAccount.Id, LastName = 'Test Contact');
        insert newContact;
        User testUser = new User();
	     //testUser = new User();
		testUser.alias = 'testuser';
		testUser.Email = 'testuser@test123.com';
		testUser.EmailEncodingKey = 'ISO-8859-1';
		testUser.LanguageLocaleKey = 'en_US';
		testUser.LastName = 'Test User567';
		testUser.LocaleSidKey = 'en_AU';
		testUser.Account_Administrator__c = false;
        testUser.CMP_Document_Notification__c = true;
		testUser.ProfileId = [SELECT Id FROM Profile WHERE Name LIKE 'Cash Management Customers - CCT' LIMIT 1].Id;
		testUser.TimeZoneSidKey = 'Australia/Sydney';
		testUser.UserName = 'testuser@travelex.com.au';
		testUser.ContactId = newContact.Id;
		testUser.DefaultCurrencyIsoCode = 'INR';
		insert testUser;
        Account_Document__c accD = new Account_Document__c();
        accD.Account__c = newAccount.Id;
        insert accD;
        Case cs = new Case(ContactId = newContact.Id, RecordTypeId = naInvestigationCaseRecordTypeId);
        cs.CMP_Public__c = true;
        insert cs;
        Case_Document__c newCaseDoc = new Case_Document__c(Case__c = cs.Id, Name = 'Test.txt',ID__c = accD.Id);
        insert newCaseDoc;
        Attachment attach = new Attachment();
        attach.Name = 'Test.txt';
        attach.Body = Blob.valueOf('ashishs shiweeovch');
        attach.ParentId = accD.Id;
        insert attach;
        
        /*
        System.runAs(testUser) {
            Attachment attach1 = new Attachment();
            attach1.Name = 'Test.txt';
            attach1.Body = Blob.valueOf('ashishs shiweeovch');
            attach1.ParentId = accD.Id;
            insert attach1;
        }
		*/
    }

}