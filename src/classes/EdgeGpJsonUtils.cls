//Created By   : Ranjeet Singh
//Created date : March/11/2016
//Description  : T-482225, Retreive H2H Get Holding Currencies from WUBS 2.0

//This class parse different json responses from the GP
public without sharing class EdgeGpJsonUtils {
	public String errorTxt{get;set;}
	public boolean isApiOn{
		get;
		set;
	}
	Boolean isAddErrorToPage{get;set;}

	public EdgeGpJsonUtils(){
		isAddErrorToPage = true;
		errorTxt = '';
	}
	//If error needs to be added to apex-page messages.
	public EdgeGpJsonUtils(boolean isAddErrorToPg){
		isAddErrorToPage = isAddErrorToPg;
		errorTxt = '';
	}
	//Get currency picklist for JSON sample : '[{"code":"121","description": "des"}]'
    public List<SelectOption> getCurrency(String jsonStr){
        List<SelectOption> selectLst = new List<SelectOption>();
        try{
            if(!String.isEmpty(jsonStr)){
                List<Object>values = (List<Object>)(JSON.deserializeUntyped(jsonStr));
                for(Object val : values){
                    Map<String, Object> mapVal = (Map<String, Object>)val;
                    selectLst.add(new SelectOption(mapVal.get('code')+'', mapVal.get('code')+''));
                }
            }
        }catch(Exception Ex){
        	errorTxt = Ex+ '';
        	if(isAddErrorToPage){
            	ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Ex+',\r\n Stack Trace:'+ Ex.getStackTraceString()));
        	}
        }
        return selectLst;
    }


    public List<SelectOption> getHoldingCurrencies(){
    	System.debug('????????'+Utility.cctClientID);
    	return getHoldingCurrencies(Utility.cctClientID);
    }
    public List<SelectOption> getHoldingCurrencies(String customerId){
    	                    System.debug('abhishek2'+customerId);
    	isApiOn = false;
        List<SelectOption> selectLst = new List<SelectOption>();
				selectLst.add(new SelectOption('','--'+Label.CM_EDGE_Picklist_Value_None+'--'));
        if(Utility_Security.canH2HTransaction){
	        EdgeGpTransctionAPI apiCall = new EdgeGpTransctionAPI();
	        //	EdgeGpTransctionAPI.isMockupResponse = true;
	        if(apiCall.getHoldingCurrencies(CustomerId)){
				EdgeGpJsonUtils jsonUtil = new EdgeGpJsonUtils();
				selectLst.addall(jsonUtil.getCurrency(apiCall.calloutResponseResult));
				if(selectLst==null || selectLst.size()==0){
					System.debug('hellodebug');
					 selectLst.addall(H2Hcurrencies);
					 errorTxt = 'config';
				}else{
					isApiOn = true;
					errorTxt = jsonUtil.errorTxt;
				}
	        }else{
	        	   	 					System.debug('hellodebug1');

				 selectLst.addall(H2Hcurrencies);
				 errorTxt = 'config';
	        }
        }
    	return selectLst;
	}

    //T-476811: NS(03/11)  Get Holding Balances for Json response
    public List<HoldingBalancesResponseWrpr> getHoldingBalances(String jsonStr){
        List<HoldingBalancesResponseWrpr> listHBs = new List<HoldingBalancesResponseWrpr>();
        System.debug('jsonStr::::: '+ jsonStr);
        try{
            if(!String.isEmpty(jsonStr)){
                List<Object>values = (List<Object>)(JSON.deserializeUntyped(jsonStr));
                for(Object val : values){
                    Map<String, Object> mapVal = (Map<String, Object>)val;
                    String customerId = (String) (mapVal.containsKey('customerId')?mapVal.get('customerId'):'');
                    String curr = (String) (mapVal.containsKey('currency')?mapVal.get('currency'):'');
                    decimal avlBalance = (decimal) (mapVal.containsKey('bookBalance')?mapVal.get('bookBalance'):0.0);
                    decimal bookBalance = (decimal) (mapVal.containsKey('availableBalance')?mapVal.get('availableBalance'):0.0);
                    HoldingBalancesResponseWrpr hb = new HoldingBalancesResponseWrpr(customerId,curr,avlBalance,bookBalance);
                    //if(listHBs.size() < 3)
                        listHBs.add(hb);
                   // else
                   //     break;
                }
            }
            return listHBs;
        }catch(Exception Ex){
        	errorTxt = Ex+ '';
        	if(isAddErrorToPage){
            	ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Ex+',\r\n Stack Trace:'+ Ex.getStackTraceString()));
        	}
        }
        return null;
    }


    //Get H2Hcurrencies from custom setting.
    public List<SelectOption> H2Hcurrencies{
    	get{
			if(H2Hcurrencies==null ||H2Hcurrencies.size()==0){
				Map<String,Global_Pay_H2H_Currencies__c> mapH2Hcurrencies = Global_Pay_H2H_Currencies__c.getall();
				System.debug('abhishek3'+mapH2Hcurrencies);
				List<SelectOption> H2Hcurr = new list<SelectOption>();
				if(Utility_Security.canH2HTransaction){
					//H2Hcurr.add(new selectoption('','--None--'));
					for(Global_Pay_H2H_Currencies__c H2H: mapH2Hcurrencies.values()){
						if(H2H.isactive__c){
							H2Hcurr.add(new SelectOption(H2H.name,H2H.Name));
						}
					}
					H2Hcurr.sort();
					H2Hcurrencies = H2Hcurr;
			    }
			}
		    return H2Hcurrencies;
    	}
    	set;
    }

    public class HoldingBalancesResponseWrpr{
        public String customerId{get;set;}
        public String currency1{get;set;}
        public decimal availableBalance{get;set;}
        public decimal bookBalance{get;set;}

        public HoldingBalancesResponseWrpr(String cId, String curr, decimal ab, decimal bb){
            this.customerId = cId;
            this.currency1 = curr;
            this.availableBalance = ab;
            this.bookBalance = bb;
        }
    }

}