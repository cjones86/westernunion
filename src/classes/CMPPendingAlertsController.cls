/**=====================================================================
 * Appirio, Inc
 * Name: CMPPendingAlertsController
 * Description: Controller to get all pending alerts of current user
 * Created Date: 25 Jan 2016
 * Created By: Rohit Sharma (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
public without sharing class CMPPendingAlertsController {
	public List<CMPAlerts> cmpAlerts{get;set;}
	private CMP_Alert_Management__c cmpAlertsManagement;
	private List<String> invoiceStatus = new List<String>();
	private List<String> invoicerejectStatus = new List<String>();
	private List<String> inviteStatus = new List<String>();
	private List<String> networkOpenStatus = new List<String>();
	private List<String> networkCompleteStatus = new List<String>();
	private List<String> invoicePendingApprovalStatus = new List<String>();
	private String currentUserId{get;set;}
	private String currentAccountId{get;set;}
	private User u = new User();

	public CMPPendingAlertsController() {
		cmpAlerts = new List<CMPAlerts>();
		cmpAlertsManagement = CMP_Alert_Management__c.getInstance();
		invoiceStatus = cmpAlertsManagement.Invoices_Raised_Invoice_Statuses__c.split(',');
		invoicerejectStatus = cmpAlertsManagement.Invoices_Rejected_Invoice_Statuses__c.split(',');
		inviteStatus = cmpAlertsManagement.CMP_Invites_Open_Status_Values__c.split(',');
		networkOpenStatus = cmpAlertsManagement.Network_Invites_Open_Status_Values__c.split(',');
		networkCompleteStatus = cmpAlertsManagement.Network_Invites_Complete_Status_Values__c.split(',');
		invoicePendingApprovalStatus = cmpAlertsManagement.Invoices_Pending_Approval_Statuses__c.split(',');
		u = Utility.loggedInUser; // ([SELECT Id,ContactId,AccountId,EDGE_Payment_Approvals__c FROM User WHERE Id=:UserInfo.getUserId() Limit 1]);
		currentUserId = u.ContactId;
		currentAccountId = u.Contact.AccountId;
		// check if user has permission to see payment approvals.
		if(u.EDGE_Payment_Approvals__c){
		    fillPayments();
		}
		System.debug('@@@utility' + Utility_Security.canCreateTransaction);
		if(Utility_Security.canCreateTransaction){
			fillPendingBatchInvoices();
			fillInvoices();
			fillInvoiceRejected();
		}
		
		//fillInvites();
		fillNetworks();
		cmpAlerts.sort();
	}

	private void fillPayments() {
	    List<Payment__c> listpmt = new List<Payment__c> ([Select Id,Payment_Status__c,Name From Payment__c
	                            Where Invoice_ID__r.Account__c=:currentAccountId
	                            And Payment_Status__c IN: invoicePendingApprovalStatus
	                            Order by LastModifiedDate desc]);
			if(listpmt.size()>0){
				CMPAlerts cmpAlert = new CMPAlerts();
				//cmpAlert.recordId = pmt.Id;
				cmpAlert.recordType = 'HIGH';
				cmpAlert.displayMsg = listpmt.size() + ' ' +Label.EDGE_AlertMSG_PendingApproval;
				cmpAlert.objectType = 'payment';
				cmpAlerts.add(cmpAlert);
			}
	}

	private void fillInvoices() {
		for(Invoice__c invoice : [SELECT Id,Name,Buyer__c,Buyer__r.ContactId,
									Buyer__r.Name,Owner.Name, Status__c, Invoice_Number__c FROM Invoice__c
									WHERE Initiated_By__c='Seller'/* AND Buyer__c=:UserInfo.getUserId()*/ AND
									Status__c IN :invoiceStatus AND 
									(Account__c=:Utility.currentAccount OR Supplier__r.Buyer__c=:Utility.currentAccount)
									order by LastModifiedDate desc]) {
			CMPAlerts cmpAlert = new CMPAlerts();
			cmpAlert.recordId = invoice.Id;
			cmpAlert.recordType = 'HIGH';
			cmpAlert.displayMsg = 	invoice.Owner.Name + ' ' + Label.EDGE_AlertMSG_InvoiceSubmitted;
			if(invoice.Invoice_Number__c != null) cmpAlert.displayMsg = cmpAlert.displayMsg + ' ' + invoice.Invoice_Number__c;
			cmpAlert.objectType = 'invoice';
			cmpAlerts.add(cmpAlert);
		}
		
		
	}
	
	 Set<String> setStatus = new Set<String>{'Pending Approval (Open)','Payment Approval Pending'};
	 
	 private void fillPendingBatchInvoices() {
    AggregateResult[] results = [SELECT Count(Id) batchCount,Batch_Id__c
								                  FROM Invoice__c
								                  WHERE Status__c IN :setStatus AND Batch_Id__c!= null
								                  GROUP BY Batch_Id__c];
								                  
		Map<Id,Batch__c> mapBatch = new Map<Id,Batch__c>([Select Id,Name from Batch__c]);						                  
								                  
								                  
      for (AggregateResult ar : results) {
      	//String inv_num = (String) ar.get('Invoice_Number__c');
      	Integer batchCount = (Integer) ar.get('batchCount');
        //String inv_Id = (String) ar.get('Id'); 
        String batch_Id = (String) ar.get('Batch_Id__c');      
           
                  
      CMPAlerts cmpAlert = new CMPAlerts();
      cmpAlert.recordId = batch_Id;
      cmpAlert.recordType = 'HIGH';
      cmpAlert.displayMsg = batchCount+' invoice pending with Batch Name: '+ mapBatch.get(batch_Id).Name ;
      //if(invoice.Invoice_Number__c != null) cmpAlert.displayMsg = cmpAlert.displayMsg + ' ' + invoice.Invoice_Number__c;
      cmpAlert.objectType = 'batch';
      cmpAlerts.add(cmpAlert);
      }
  }
	

	private void fillInvoiceRejected() {
		for(Invoice__c invoice : [SELECT Id,Name,Buyer__c,Buyer__r.ContactId,Invoice_Number__c,
									Buyer__r.Name,Owner.Name, LastModifiedDate, LastModifiedBy.Name, Status__c FROM Invoice__c
									WHERE Initiated_By__c='Seller' /* AND Buyer__c=:UserInfo.getUserId()*/ AND
									Status__c = :invoicerejectStatus AND CreatedBy.Contact.AccountId=:currentAccountId
									order by LastModifiedDate desc]) {
			CMPAlerts cmpAlert = new CMPAlerts();
			cmpAlert.recordId = invoice.Id;
			cmpAlert.recordType = 'HIGH';
			cmpAlert.displayMsg =  invoice.LastModifiedBy.Name + ' ' + Label.EDGE_AlertMSG_InvoiceRejected;
			if(invoice.Invoice_Number__c != null) cmpAlert.displayMsg = cmpAlert.displayMsg + ' ' + invoice.Invoice_Number__c;
			cmpAlert.objectType = 'invoice';
			cmpAlerts.add(cmpAlert);
		}
	}

	/*private void fillInvites() {
		Date startDate = Date.today().addDays(-Integer.valueOf(cmpAlertsManagement.CMP_Invites_Overdue_Invite_Age_Days__c));
		for(Invites__c invite : [SELECT Id,Inviter_User__c,Invitee_Contact__r.Name, LastModifiedDate
								 FROM Invites__c WHERE
								 Last_Invitation_Sent_Date_Time__c >=:startDate AND
								 Status__c IN :inviteStatus AND Inviter_User__c=:UserInfo.getUserId()
								 ORDER BY Last_Invitation_Sent_Date_Time__c DESC]) {
			CMPAlerts cmpAlert = new CMPAlerts();
			cmpAlert.recordId = invite.Id;
			cmpAlert.recordType = 'LOW';
			cmpAlert.displayMsg = 	invite.Invitee_Contact__r.Name + ' ' + Label.EDGE_AlertMSG_InviteRejected;
			cmpAlert.objectType = 'invite';
			cmpAlert.eventLastMonth = invite.LastModifiedDate.format('MMM yyyy');
			cmpAlerts.add(cmpAlert);
		}
	}*/

	private void fillNetworks() {
		Date startDate = Date.today().addDays(-Integer.valueOf(cmpAlertsManagement.Network_Invites_Completed_Age_Days__c));
		//Inviter
		/*for(Network__c network : [SELECT Id,Inviter_User__c,Invitee_Contact__r.Name
								 FROM Network__c WHERE
								 Status_DateTime__c >=:startDate AND
								 Status__c IN:networkOpenStatus AND Inviter_User__c=:UserInfo.getUserId()]) {
			CMPAlerts cmpAlert = new CMPAlerts();
			cmpAlert.recordId = network.Id;
			cmpAlert.recordType = 'LOW';
			cmpAlert.displayMsg = 	'Your Network Invitation to '+network.Invitee_Contact__r.Name+' is still pending.';
			cmpAlert.objectType = 'network - inviter';
			cmpAlerts.add(cmpAlert);
		}*/
		//Invitee
		for(Network__c network : [SELECT Id,Inviter_User__r.Name,Invitee_Contact__r.LastName, LastModifiedDate, Account_Invitee__c, Account_Inviter__c, Inviter_Account__c
								 FROM Network__c WHERE Status__c = :networkOpenStatus AND Account_Invitee__c=:currentAccountId]) {
			CMPAlerts cmpAlert = new CMPAlerts();
			cmpAlert.recordId = network.Id;
			cmpAlert.recordType = 'MED';
			cmpAlert.displayMsg = 	network.Inviter_User__r.Name + ', ' + network.Inviter_Account__c + ' ' + Label.EDGE_AlertMSG_NetworkPending;
			cmpAlert.objectType = 'network - invitee';
			cmpAlert.eventLastMonth = network.LastModifiedDate.format('MMM yyyy');
			cmpAlerts.add(cmpAlert);
		}
	}

	public class CMPAlerts implements Comparable {
		public Id recordId{get;set;}
		public String recordType{get;set;}
		public String displayMsg{get;set;}
		public String objectType{get;set;}
		public string eventLastMonth{get;set;}
		public String InvoiceStatus{get;set;}

		public String relatedRecUrl{
			get{
			    String encodedInvoiceId;
				if(recordId != null)
                    encodedInvoiceId = Utility.doEncryption(recordId);
				if(objectType == 'invoice')return '/EDGE_Invoice_Submit_Detail?Id='+encodedInvoiceId+'&retUrl=/edge_activity_dashboard';
				if(objectType == 'network - invitee')return '/EDGE_Network';
				if(objectType == 'invite')return '/EDGE_Network';
				if(objectType == 'payment')return '/Edge_Payment_Approvals';
				if(objectType == 'batch')return '/Edge_MultiLine_Order_Review?OrderId='+recordId+'&retUrl=/edge_activity_dashboard';
				return '';
			}
		}

	    public Integer compareTo(Object compareTo) {
	    	CMPAlerts alert = (CMPAlerts)compareTo;
	    	if(recordType=='HIGH' && (alert.recordType=='MED' || alert.recordType=='LOW')) {
	        	return -1;
	        }else if(recordType=='MED') {
	        	if(alert.recordType=='HIGH') {
	        		return 1;
	        	}else if(alert.recordType=='LOW') {
	        		return -1;
	        	}
	        }else if(recordType=='LOW' && (alert.recordType=='MED' || alert.recordType=='HIGH')) {
	        	return -1;
	        }
	        return 0;
	    }
	}
}