public class EncryptionManager {
   
   	public static  Blob key = Blob.valueof([   Select e.Private_Key_String__c
                                                               From Encryption_Key__mdt e 
                                                               where label = 'Private Key'].get(0).Private_Key_String__c);
                                                               
    public static String doEncrypt(String strToEncrypt) {
        strToEncrypt += UserInfo.getUserId();
        Blob data = Blob.valueOf(strToEncrypt);
        Blob encrypted = Crypto.encryptWithManagedIV('AES128', key, data);
        system.debug('<<<Encrypted Value : ' + EncodingUtil.base64Encode(encrypted));
        return EncodingUtil.base64Encode(encrypted); 
        
    }
    
    public static String doDecrypt(String encryptedTxt) {
        if(encryptedTxt.contains(encryptedTxt)){
            encryptedTxt = encryptedTxt.replace(' ' , '+');
        }
        Blob encryptedData = EncodingUtil.base64Decode(encryptedTxt);
        Blob decrypted = Crypto.decryptWithManagedIV('AES128', key , encryptedData);
        String decryptedString = decrypted.toString();
        decryptedString = decryptedString.substring(0 , decryptedString.indexOf(UserInfo.getUserId()));
        System.debug('<<<decryptedString'+ decryptedString);
        return  decryptedString;
    } 
    
}