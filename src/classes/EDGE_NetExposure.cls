/**=====================================================================
 * Name: EDGE_NetExposure
 * Description: Controller class to calculate Invoice/Input net Exposure
 * Created Date: Mar 10, 2016
 * Created By: Rohit Sharma (JDC)
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
global without sharing class EDGE_NetExposure {

    public String userLanguage{get;set;}
    public String chartLanguage{get;set;}
    public NetCashFlowCtrl NetCashFlow{get;set;}
    public String accountName {get; set;}
    public Id currentAccount {get;set;}
    public String currentContact{get;set;}
    public boolean isGPUser {
        get{
            return Utility_Security.canCreateInvoice;
        }set;
    }
    
    public  DateTime lastupdate{
        get{
            return Utility.lastUpdateMarketDatetime;
        }
    }
    
    public EDGE_NetExposure() {
        chartLanguage = UserInfo.getLanguage();
        userLanguage = UserInfo.getLanguage();
        for(Account acc : [SELECT Id, Name FROM Account Where Id =: Utility.currentAccount]){
    		accountName = acc.Name;
    	}
        if(userLanguage.contains('_')){
            userLanguage = userLanguage.substring(0, userLanguage.indexOf('_'));
        }
        if(userLanguage == 'en'){
            userLanguage = '';
        }else{
            userLanguage = '_'+ userLanguage;
        }
        NetCashFlow = new  NetCashFlowCtrl();
        if(system.currentPageReference().getParameters().get('currency') != null) {
            NetCashFlowCtrl.selectedCurrency = System.currentPageReference().getParameters().get('currency');
        }
        currentAccount = utility.currentAccount;
        currentContact = utility.loggedInUser.ContactId;
    }
    
    @RemoteAction
    global static List<NetCashFlowCtrl.wrapperInvoice> loadOppsWithAcc(String selectedCurrency1, Integer selectedMonth1, Id accId ) {
        system.debug('************loadOpps : >> '+Utility.currentAccount);
        return NetCashFlowCtrl.loadOpps(selectedCurrency1, selectedMonth1 , accId); 
    }

    @RemoteAction
    global static List<NetCashFlowCtrl.wrapperInvoice> loadOpps(String selectedCurrency1, Integer selectedMonth1 ) {
        system.debug('************loadOpps : >> '+Utility.currentAccount);
        return NetCashFlowCtrl.loadOpps(selectedCurrency1, selectedMonth1,'' );
    }
}