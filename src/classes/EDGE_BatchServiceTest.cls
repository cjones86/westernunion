/**================================================================      
* Appirio, Inc
* Name: EDGE_BatchServiceTest Test Class
* Description: Test class for EDGE_BatchService
* Created Date: 26-Oct-2016
* Created By: Raghu Rankawat (Appirio)
*
* Date Modified      Modified By      Description of the update
*
==================================================================*/
@isTest
private class EDGE_BatchServiceTest {

    @isTest
    private static void batchServiceMethod(){
        Test.startTest();
        Batch__c batch = EDGE_BatchService.getActiveBatchForCurrentContact();
        PageReference pgRef = EDGE_BatchService.submitBatchToGP2(batch);
        System.assertEquals(pgRef.getURL().contains('/EDGE_MultiLineOrderPayGP2'),true);
        
        // List<InvoiceUIWrapper_MultiLine> invWrapList = new List<InvoiceUIWrapper_MultiLine>();
        // Invoice__c inv = test_Utility.createInvoice(true);
        // InvoiceUIWrapper_MultiLine invWrap = new InvoiceUIWrapper_MultiLine(inv);
        // invWrapList.add(invWrap);
        // EDGE_BatchService.addInvoicesToBatch(batch.Id,invWrapList);
        
        Test.stopTest();
    }
}