/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_ChatterPostControllerTest
 * Description: Test class for EDGE_ChatterPostController to create/display chatter feeds
 * Created By: Rohit Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update                                           
 =====================================================================*/
@isTest
private class EDGE_ChatterPostControllerTest {

    static testMethod void chatterTest() {
        test_Utility.createCMPAdministration();
		test_Utility.createCMPAlert();
		test_Utility.currencyISOMapping();
        test_Utility.createWUEdgeSharingAdmin();
		CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        insert beneAccount;
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact;
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
		commUser.timezonesidkey='America/Indiana/Indianapolis';
		commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        commUser.CommunityNickname = commUser.alias;
        insert commUser;
        Invoice__c submitInvoice = new Invoice__c(RecordTypeId=Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('GP Invoice - Active').getRecordTypeId());
	    submitInvoice.Status__c = 'Invoice Submitted';
	    submitInvoice.Initiated_By__c='Seller';
	    submitInvoice.Account__c = beneAccount.Id;
	    submitInvoice.Buyer__c = commUser.Id;
	    submitInvoice.Beneficiary_GP_ID__c = '12345';
	    insert submitInvoice;
        System.RunAs(commUser){
        	Test.startTest();
	        EDGE_ChatterPostController chatterFeeds = new EDGE_ChatterPostController();
	        chatterFeeds.parentId = submitInvoice.Id;
	        chatterFeeds.content = 'Test feed ';
	        chatterFeeds.createNewChatterFeed();
	        List<FeedItem> feedsChatter = chatterFeeds.getChatterFeeds();
	        system.assertEquals(feedsChatter.size(),1);
	        chatterFeeds.addFeedComment();
	        chatterFeeds.deleteFeedComment();
	        chatterFeeds.deleteFeed();
	        Test.stopTest();
        }
    }
}