/**=====================================================================
 * Name: EDGE_MultiLineOrderController
 * Description: Related to EDGE_Invoice_Management Page
 * Created Date: Mar 09, 2016
 * Created By: Rohit Sharma (JDC)
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
public without sharing class EDGE_MultiLineOrderController {
	
	  static apexLogHandler.apexLog log = new apexLogHandler.apexLog('MultiLineOrderController','reviewToPay');
	  static apexLogHandler.apexLog logInvoice = new apexLogHandler.apexLog('MultiLineOrderController','search');
	  
	  

   
	public String localeDate{
	      get{
	          Utility utility = new Utility();
	          return utility.localeDate;
	      }
	}
	public String localeDateRegx{
	      get{
	          String dtStr = localeDate.replace('yyyy', '(\\d{2,4})');
	      dtStr = dtStr.replace('mm', '(\\d{1,2})\\');
	      dtStr = dtStr.replace('dd', '(\\d{1,2})\\');
	          return '/'+dtStr+'( (\\d{1,2}):(\\d{1,2}))? ?(am|pm|AM|PM|Am|Pm)?/';
	      }
	  }




    public List<InvoiceUIWrapper_MultiLine> buyerInvoices{get;set;}
    public List<InvoiceUIWrapper_MultiLine> beneficiaryInvoices{get;set;}
    public List<SelectOption> statusList{get;set;}
    public String status{get;set;}
    public List<SelectOption> currencyList{get;set;}
    public String currencyString{get;set;}
    public Boolean createTransactionPermForSP{get;set;}
    public Boolean createTransactionPermForH2H{get;set;}
    public string currentAccount{get;set;}
    public String userDefaultCurrency{get;set;}
    public String userCurrency{get;set;}
    private List<String> gPId;
    public Decimal aggregateTotal{get;set;}
    public Decimal hedgedTotal{get;set;}
    public Decimal unHedgedTotal {get;set;}
    public Decimal totalInHomeCurrency {get;set;}
    public Decimal conversionRate{get;set;}
    public List<InvoiceUIWrapper_MultiLine> invoices{get;set;}
    public List<String> selectedStatusSet{get;set;}
    public String currencyS{get;set;}
    public String requestType{get;set;}
    public String xlsType{get;set;}
    public integer loadCurrency{get;set;}
    public boolean canProcessBatch {get;set;}
    public boolean canProceed{get;set;}
    public string confirmationMsg {get;set;}
    public input__c input{get;set;}
    public List<SelectOption> payableOptions{get;set;} 
    public String optionvalue{get;set;}
    public String SelectedDate = '';
    public Date stDate{get;set;}
    public Date endDate{get;set;}



    public EDGE_MultiLineOrderController() {
        input = new input__c();
        step2 = false;
        stDate = null;
        endDate = null;
        canProcessBatch = false;
        canProceed = false;
        status = '';
        currencyString = '';
        confirmationMsg = '';
        optionvalue = Label.CM_EDGE_ONLY_PAYABLE_INVOICE;
        //statusList = getInvoiceStatus();
        currencyList =  getInvoiceCurrency();
        currentAccount = Utility.currentAccount;
        userDefaultCurrency = UserInfo.getDefaultCurrency();
        createTransactionPermForSP = Utility_Security.canPayInvoices_SP;
       // createTransactionPermForH2H = Utility_Security.canPayInvoices_H2H;
        gPId = new List<String>();
        userCurrency = Utility.userCurrency;
        for(Global_Pay_ID_Management__c gpRecord : [SELECT Global_Pay_ID__c FROM Global_Pay_ID_Management__c WHERE User_Beneficiary__c=:UserInfo.getUserId()]) {
            gPId.add(gpRecord.Global_Pay_ID__c);
        }
        
        
        //if(ApexPages.currentPage() != null && ApexPages.currentPage().getParameters().containsKey('xlsType')) {
        if(!String.isEmpty(ApexPages.currentPage().getParameters().get('xlsType'))) {
            xlsType = ApexPages.currentPage().getParameters().get('xlsType');
            requestType = xlsType;
            status = !String.isEmpty(ApexPages.currentPage().getParameters().get('status')) ? ApexPages.currentPage().getParameters().get('status') :'';
            currencyString = !String.isEmpty(ApexPages.currentPage().getParameters().get('currencyString')) ? ApexPages.currentPage().getParameters().get('currencyString') : '';
          searchInvoice();
        //}
        } else {
            conversionRate = 0.0;
            aggregateTotal = 0.0;
            hedgedTotal = 0.0;
            unHedgedTotal = 0.0;
            totalInHomeCurrency = 0.0;
            invoices = new List<InvoiceUIWrapper_MultiLine>();
        }   
        payableOptions = getPayableOption();
         //searchInvoice();

        //searchInvoice();
        System.Debug('###'+currencyString);
        
    }        

    public Pagereference refreshInvoices() {
        //loadCurrency=1;
        toggleSort();
        return null;
    }

    public Pagereference resetInvoices() {
        status = '';
        currencyString = '';
        //loadCurrency=1;
        toggleSort();
        return null;
    }

    public List<SelectOption> payCurrencyOptions{
        get{
            
            String strQuery = 'Select count(Id), Custom_Currency__c FROM Invoice__c' ;
            String accountString = 'Account__c=\''+currentAccount+'\'';
            if(requestType=='Receivables') {
            accountString = '(Seller_Account__c=\''+currentAccount+'\' ' + 'OR (Supplier__c != null AND Supplier__r.Supplier__c=\''+currentAccount+'\'))';
            }
            strQuery +=' WHERE '+accountString+'  group by  Custom_Currency__c';
                    
            if(payCurrencyOptions==null){
                payCurrencyOptions = new List<SelectOption>();
                set<String> currencySet = new set<String>();
                payCurrencyOptions.add(new SelectOption('',Label.CM_EDGE_Select));
                for(AggregateResult Invoice: database.query(strQuery)){
                    String currencyCode = (String) Invoice.get('Custom_Currency__c');
                    if(!String.isBlank(currencyCode) && !currencySet.Contains(currencyCode)) {
                        currencySet.add(currencyCode);
                        payCurrencyOptions.add(new SelectOption(currencyCode,currencyCode));
                    }
                }
            }
            return payCurrencyOptions;
        }
        set;
    }

    public String currentUserLocale{
        get{
            String currentUserLocaleVal = UserInfo.getLocale();
            if(currentUserLocaleVal.contains('_')){
                currentUserLocaleVal = currentUserLocaleVal.substring(0, currentUserLocaleVal.indexOf('_'));
            }
            return currentUserLocaleVal;
        }
        set;
    }

    public String aggregateTotalFormat{
        get{
            String aggregateTotalVal = aggregateTotal.format();
            if(!aggregateTotalVal.contains(Utility.getDecimalChar)){
                aggregateTotalVal = aggregateTotal.format() + Utility.getDecimalChar+'00';
            }
            return aggregateTotalVal;
        }
        set;
    }

    public String hedgedTotalFormat{
        get{
            String hedgedTotalVal = hedgedTotal.format();
            if(!hedgedTotalVal.contains(Utility.getDecimalChar)){
                hedgedTotalVal = hedgedTotal.format() + Utility.getDecimalChar+'00';
            }
            return hedgedTotalVal;
        }
        set;
    }

    public String defaultValueFormat{
        get{
            Integer aTemp = 0;
            String efaultValue = aTemp.format();
            if(!efaultValue.contains(Utility.getDecimalChar)){
                efaultValue = aTemp.format() + Utility.getDecimalChar+'00';
            }
            return efaultValue;
        }
        set;
    }

    public String unHedgedTotalFormat{
        get{
            String unHedgedTotalVal = unHedgedTotal.format();
            if(!unHedgedTotalVal.contains(Utility.getDecimalChar)){
                unHedgedTotalVal = unHedgedTotal.format() + Utility.getDecimalChar+'00';
            }
            return unHedgedTotalVal;
        }
        set;
    }

    public String unHedgedTotalSettlementFormat{
        get{
            String unHedgedTotalSettlementVal = unHedgedTotalSettlement.format();
            if(!unHedgedTotalSettlementVal.contains(Utility.getDecimalChar)){
                unHedgedTotalSettlementVal = unHedgedTotalSettlement.format() + Utility.getDecimalChar+'00';
            }
            return unHedgedTotalSettlementVal;
        }
        set;
    }

    public Decimal aggregateTotalSettlement{
        get{
            return Utility.getSettlementCurrencyAmount(aggregateTotal, currencyString, UserInfo.getDefaultCurrency());
        }
    }
    public String aggregateTotalSettlementFormat{
        get{
            String aggregateTotalSettlementVal = aggregateTotalSettlement.format();
            if(!aggregateTotalSettlementVal.contains(Utility.getDecimalChar)){
                aggregateTotalSettlementVal = aggregateTotalSettlement.format() + Utility.getDecimalChar+'00';
            }
            return aggregateTotalSettlementVal;
        }
        set;
    }
    public Decimal hedgedTotalSettlement{
        get{
            return Utility.getSettlementCurrencyAmount(hedgedTotal, currencyString, Utility.userCurrency);
        }
    }
    public Decimal unHedgedTotalSettlement {
        get{
            return Utility.getSettlementCurrencyAmount(unHedgedTotal, currencyString, Utility.userCurrency);
        }
    }

    public List<String> getInvoiceStatus() {
         //List<SelectOption> options = new List<SelectOption>();
         //Map<String,String> optionsMap = new Map<String,String>();
         List<String> options = new List<String>();
         
         //optionsMap.put('',Label.CM_EDGE_Select);
         Schema.DescribeFieldResult fieldResult = Invoice__c.Status__c.getDescribe();
         List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
         if(Utility_Security.canCreateTransaction){
             for( Schema.PicklistEntry f : ple) {
                 if(f.getValue()!='Pending Approval (Open)' && f.getValue()!='Approved' && f.getvalue() != 'Paid'){
                  //options.add(new SelectOption(f.getValue(), f.getLabel()));
                  //optionsMap.put(f.getValue(), f.getLabel());
                  options.add(f.getLabel());
                 }
             }
         }
         else{
             for( Schema.PicklistEntry f : ple) {
                 if(f.getValue()=='Pending Approval (Open)' || f.getValue()=='Approved' || f.getvalue() == 'Paid' || f.getvalue() == 'Void'){
                  //options.add(new SelectOption(f.getValue(), f.getLabel()));
                  //optionsMap.put(f.getValue(), f.getLabel());
                  options.add(f.getLabel());
                 }
             }
         }
         return options;
    }

    public List<SelectOption> getInvoiceCurrency() {
         List<SelectOption> options = new List<SelectOption>();
         options.add(new SelectOption('',Label.CM_EDGE_Select));
         Schema.DescribeFieldResult fieldResult = Invoice__c.Custom_Currency__c.getDescribe();
         List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
         for( Schema.PicklistEntry f : ple) {
              options.add(new SelectOption(f.getLabel(), f.getValue()==currencyString ? 'Currenct('+currencyString+')':f.getValue()));
         }
         return options;
    }
    
    public void toggleSortAction() {
        loadCurrency = 0;
        toggleSort();
    }

    public void toggleSort() {
        // simply toggle the direction
        System.debug('1>> '+ loadCurrency);
        searchInvoice();
        populateAmountField();
    }
    
    public Boolean step2{get;set;}
    public String lastSubmittedUser{get;set;}
    public DateTime lastSubmittedDateTime{get;set;}
    
    //Review & Pay
    public boolean showWarning{get;set;}
    public boolean checkIfInvoiceModifiedRecently(Set<Id> invoiceIds){
   		//check if selected invoices has been modified
    	Decimal invoiceLastModifiedThrs = CMP_Administration__c.getInstance().InvoiceLastModificationThreshold_In_Min__c;    	
    	for(Invoice__c inv: [Select Id, LastModifiedDate From Invoice__c Where Id IN: invoiceIds]){
    		long no_of_Min = (System.now().getTime() - (inv.LastModifiedDate).getTime())/60000;
    		if(no_of_Min < invoiceLastModifiedThrs){
    			showWarning = true;
    			confirmationMsg = System.Label.CM_PageMesssage_InvoiceStatusModified;
    			return true;
    		}
    	}
    	return false;   	
   }
   
   public PageReference reviewToPay() {
   	      Batch__c currentBatch = EDGE_BatchService.getActiveBatchForCurrentContact();
   	      String msg = EDGE_BatchService.addInvoicesToBatch(currentBatch.Id,invoices,canProcessBatch);
   	      system.debug('confirmation msg>>>'+msg + 'can process batch >>>'+canProcessBatch);
   	      if(msg != null && canProcessBatch == false){
   	      	showWarning = true;
   	      	confirmationMsg = msg;
   	      	//canProcessBatch = false;
   	      	//canProceed = true;
   	      	return null;
   	      }
            
	        PageReference pg = new PageReference('/EDGE_MultiLine_Order_Review?orderId='+currentBatch.Id);
	        String retURL = CMP_Administration__c.getInstance().CM_Community_BaseURL__c+'/EDGE_MultiLineOrderStep1?step=1';
	        pg.getParameters().put('retUrl', retURL);
	        pg.setRedirect(true);
	        return pg;
   }
   
   
    /*public PageReference reviewToPay() {
        if(!step2) {
        	
        	//check if selected invoices has been processed
        	if(!canProceed){
	    		Set<Id> selInvoiceIds = new Set<Id>();
	    		for(InvoiceUIWrapper_MultiLine inv: invoices) {
	    			if(inv.Selected){
	    				selInvoiceIds.add(inv.Invoice.Id);
	    			}
	    		}
	    		if(selInvoiceIds.size()>0 && checkIfInvoiceModifiedRecently(selInvoiceIds)){
	    			//searchInvoice();
	    			return null;
	    		}canProceed = true;
        	}
        	
        	System.debug('>>canProceed > '+ canProceed);
        	System.debug('>>step2 > '+ step2);
        	System.debug('>> canProcessBatch > '+ canProcessBatch );
        	System.debug('>>> '+ canProceed);
        	
        	if(!canProcessBatch){
        		System.debug('In process batch::: ');
        		//check if batch already exists for invoice
        		Decimal softLockReleaseTime = CMP_Administration__c.getInstance().Soft_Lock_Release_Threshold_in_minutes__c;
	        	for(InvoiceUIWrapper_MultiLine inv: invoices) {
	        		if(inv.selected && inv.Invoice.Batch_Id__c != null && inv.Invoice.GP2_Last_Order_Submitted_Date_Time__c != null &&
	        		 inv.Invoice.GP2_Last_Submitted_By__c != null){
	        			//this is already associated with a batch	        			
	        			//check if it is within 20 mins
		                long no_of_Min = (System.now().getTime() - (inv.Invoice.GP2_Last_Order_Submitted_Date_Time__c).getTime())/60000;
		                
			        	//if different user is trying again to add the batch
		                if(inv.Invoice.GP2_Last_Submitted_By__c != userInfo.getUserId()){
		                    if(no_of_Min < softLockReleaseTime){
		                      //show warning message
		                      showWarning = true;
		                      lastSubmittedUser = inv.Invoice.GP2_Last_Submitted_By__r.Name;
		                      lastSubmittedDateTime = inv.Invoice.GP2_Last_Order_Submitted_Date_Time__c;
		                      confirmationMsg = lastSubmittedUser +' has already started the process of submitting this Invoice/Batch for payment at ' + lastSubmittedDateTime + ' Do you wish to proceed with payment?';
	        				  return null;
		                    }
			        	}        			
	        		}
	            }
        	}
        	
        	/*
            step2 = true;
            ApexPages.currentPage().getParameters().put('step','2');
            
            List<InvoiceUIWrapper_MultiLine> tempInvs = new List<InvoiceUIWrapper_MultiLine>();
            for(InvoiceUIWrapper_MultiLine inv: invoices) {
                if(inv.selected){
                	inv.selected = false;
                	tempInvs.add(inv);
                }
            }
            invoices.clear();
            invoices.addAll(tempInvs);
            
            
            
            Batch__c currentBatch = EDGE_BatchService.getActiveBatchForCurrentContact();
            
        	List<Invoice__c> listInvoiceToUpdateWithBatchId = new List<Invoice__c>();
        	if(currentBatch.Id != null){
        		for(InvoiceUIWrapper_MultiLine inv: invoices) {
        			if(inv.Selected){
			        		Invoice__c invoice = new Invoice__c(Id = inv.Invoice.Id);
			        		invoice.Batch_Id__c = currentBatch.Id;
			        		//invoice.GP2_Last_Order_Submitted_Date_Time__c = System.Now(); /* handle this in EDGE_BatchService
			        		//invoice.GP2_Last_Submitted_By__c = UserInfo.getUserId();     
			        		listInvoiceToUpdateWithBatchId.add(invoice);
        			} 
	            }
	        	if(listInvoiceToUpdateWithBatchId.size() > 0){
	        		try{
	        			update listInvoiceToUpdateWithBatchId;
	        		}Catch(exception ex){
	        			//System.debug('>>> error in update batch invoices >>> '+ ex.getMessage());
	        			log.logDebug('>>> error in update batch invoices >>> '+ ex.getMessage()); 
	        		}
	        	}
            }
            
            PageReference pg = new PageReference('/EDGE_MultiLine_Order_Review?orderId='+currentBatch.Id);
            String retURL = CMP_Administration__c.getInstance().CM_Community_BaseURL__c+'/EDGE_MultiLineOrderStep1?step=1';
            pg.getParameters().put('retUrl', retURL);
            pg.setRedirect(true);
            return pg;
            
        } else {
			
        	String currentContactId;
        	for(User us : [Select Id, ContactId From User Where Id=:UserInfo.getUserId() And ContactId!=null]){
        		currentContactId = us.ContactId;
        	}
        	
        	Batch__c bh = new Batch__c(Batch_Submitted__c=System.Now(),Contact__c=currentContactId,Status__c='Inactive',
        	GP2_Last_Submitted_By__c = UserInfo.getUserId(),GP2_Last_Order_Submitted_Date_Time__c = System.Now());
        	try{
        		insert bh;
        	}Catch(exception ex){
        		System.debug('>>> error in creating batch >>> '+ ex.getMessage());
        		log.logDebug('>>> error in creating batch >>> '+ ex.getMessage()); 
        	}
        	List<Invoice__c> listInvoiceToUpdateWithBatchId = new List<Invoice__c>();
        	if(bh.Id != null){
        		for(InvoiceUIWrapper_MultiLine inv: invoices) {
	        		Invoice__c invoice = new Invoice__c(Id = inv.Invoice.Id);
	        		invoice.Batch_Id__c = bh.Id;
	        		invoice.GP2_Last_Order_Submitted_Date_Time__c = System.Now();
	        		invoice.GP2_Last_Submitted_By__c = UserInfo.getUserId();
	        		listInvoiceToUpdateWithBatchId.add(invoice); 
	            }
	        	if(listInvoiceToUpdateWithBatchId.size() > 0){
	        		try{
	        			update listInvoiceToUpdateWithBatchId;
	        		}Catch(exception ex){
	        			//System.debug('>>> error in update batch invoices >>> '+ ex.getMessage());
	        			log.logDebug('>>> error in update batch invoices >>> '+ ex.getMessage()); 
	        		}
	        	}
	        	log.saveLogs();
	        	
	        	*****************************************************************************
				    Submit batch order to payment API (refer EDGE_BatchService.submitBatchToGP2() method)
				******************************************************************************  
	            PageReference pg = new PageReference('/EDGE_MultiLineOrderPayGP2?batchId='+bh.Id);
	            String retURL = CMP_Administration__c.getInstance().CM_Community_BaseURL__c+'/EDGE_MultiLineOrderStep1?step=1';
	            pg.getParameters().put('retUrl', retURL);
	            pg.setRedirect(true);
	            return pg;
        	}
          
        }     
        return null;
    }*/
    
   public PageReference removeSelectedInvoices() {
        List<InvoiceUIWrapper_MultiLine> tempInvs = new List<InvoiceUIWrapper_MultiLine>();
        List<Invoice__c> invoiceListToUpdate = new List<Invoice__c>();
        
        for(InvoiceUIWrapper_MultiLine inv: invoices) {
            if(inv.selected == false){ 
            	tempInvs.add(inv);
            	Invoice__c invoice = new Invoice__c(Id = inv.Invoice.Id , Batch_Id__c = null);
	        	invoiceListToUpdate.add(invoice);
            }
        }
        try{
        	update invoiceListToUpdate;
        }catch(Exception e){
        	System.debug('Exception removeSelectedInvoices ' + e.getMessage());
        }
        
        invoices.clear();
        invoices.addAll(tempInvs);
        return null;
    } 
    
    public PageReference backToStep1() {
        step2 = false;
    	canProcessBatch = false;
    	showWarning = false;
    	canProceed = false;
        ApexPages.currentPage().getParameters().put('step','1');
        searchInvoice();
        populateAmountField();
        return null;
    }
    
    public void searchInvoice(){        
        
        if(!step2) {
            //System.debug('>> chkpoint 2>> ');
            //logInvoice.logMessage('>> chkpoint 2>> ');
            String strQuery = 'Select Name,Account__c,Batch_Id__c,Supplier__c,Supplier__r.Supplier__c,Supplier__r.Buyer__c,RecordType.Name,Custom_Currency__c,Delivery_Method__c,Type__c,Due_Date__c,Amount__c,Beneficiary_GP_ID__c,Beneficiary_Name__c,Beneficiary_GP_Name__c,'+
                              'Status__c,Buyer__c,Buyer__r.Name,TypeTrans__c,Buyer__r.Contact.AccountId,CreatedBy.Contact.Account.Name,CreatedBy.Name,Id, Invoice_Number__c, Account__r.Name, Reference_Number__c,GP2_Last_Order_Submitted_Date_Time__c,GP2_Last_Submitted_By__r.Name,GP2_Last_Submitted_By__c FROM Invoice__c' ;
            selectedStatusSet = new List<String>();
            String statusString = '';
            if (!String.isEmpty(status)) {
                //statusString = 'Status__c =\''+status+'\' AND ';
                 if(status.contains(',')){
                    List<String> statusStringList = status.split(',');
                    statusString = 'AND Status__c IN :statusStringList';
                }else{
                    statusString = 'AND Status__c =\''+status+'\'';
                }
            }
            
            String selectCurrency = '';
            
            if(!String.isEmpty(currencyString)){
                selectCurrency = ' AND Custom_Currency__c =\''+currencyString+'\' ';
            }
            
            
           // String accountString = 'Account__c=\''+currentAccount+'\'';
            /*
            if(requestType=='Receivables') {
                accountString = '(Seller_Account__c=\''+currentAccount+'\' ' + 'OR (Supplier__c != null AND Supplier__r.Supplier__c=\''+currentAccount+'\'))';
            }*/
         //  String   delMethodFilter = '';
			//String delMethodFilter = ' And Delivery_Method__c = \'Standard Payment\' ';
            //strQuery += ' WHERE '+statusString+accountString+selectCurrency+delMethodFilter+'  ORDER BY Due_Date__c DESC';
            String filterQuery = statusString+selectCurrency+SelectedDate ;
            strQuery += ' WHERE Name != NULL '+filterQuery+'  ORDER BY Due_Date__c DESC';
        
          // System.assert(false,strQuery );
            System.debug('>> chkpoint 3>> '+ strQuery);
            logInvoice.logDebug('requestType : '+strQuery);
            
            Batch__c currentBatch = EDGE_BatchService.getActiveBatchForCurrentContact();
            
            invoices = new List<InvoiceUIWrapper_MultiLine>();
            for(Invoice__c Invoice: database.query(strQuery)){
                if(Invoice.RecordTypeId != null){
                  InvoiceUIWrapper_MultiLine inv = new InvoiceUIWrapper_MultiLine(Invoice);
                	
                  if(Invoice.Batch_Id__c == currentBatch.Id){
                  	 inv.selected = true;	
                  }
                  
            	  if(optionvalue.equalsIgnoreCase(Label.CM_EDGE_ONLY_PAYABLE_INVOICE)){
        	      	if(inv.isPayNowAccess)
                		invoices.add(inv);	
                		inv.isPayable = Label.CM_Field_Value_Yes;
                  }
                  else{
				  	if(inv.isPayNowAccess){
                   	      inv.isPayable = Label.CM_Field_Value_Yes;
                    }
                    else{
                    	  inv.isPayable =  Label.CM_Field_Value_No;
                    }
                    invoices.add(inv);
                  }
                
                }
                    
            }
            
            system.debug('requestType : '+invoices.size());
            //logInvoice.logDebug('requestType : '+invoices.size());
           // logInvoice.saveLogs();
        }        
    }

    public Pagereference populateAmountField() {
        aggregateTotal = 0.0;
        hedgedTotal = 0.0;
        unHedgedTotal = 0.0;
        totalInHomeCurrency = 0.0;
        Decimal hedgeAccount = 0.0;
        Decimal marketRate = 0.0;
        Decimal holdingBalance = 0.0;
        conversionRate = 0.0;
        currencyS = currencyString;
        if(currencyS == null ||  currencyS == '') {
            currencyS = userDefaultCurrency;
            //return null;
        }
        //conversionRate = [SELECT conversionrate FROM currencytype WHERE isocode =: selectedCurrency LIMIT 1].conversionRate;
        // Get Market rates for the selected currency
        for(Market_Rate__c rate : [SELECT Id, Currency_Code__c, Currency_Value__c
                                   FROM Market_Rate__c
                                   WHERE Currency_Code__c =:currencyS]) {
            conversionRate = rate.Currency_Value__c;
        }
        for(AggregateResult aggregateResult : [SELECT sum(FX_Balance__c) settlementAmount,
                                               avg(Base_USD__c) marketRate
                                               FROM Forward_Contracts__c
                                               WHERE Account_ID__c =: currentAccount
                                               AND Custom_Currency__c =: currencyS]) {

            if(aggregateResult.get('settlementAmount') != null)
            hedgeAccount = (Decimal)    aggregateResult.get('settlementAmount');
            if(aggregateResult.get('marketRate') != null)
            marketRate = (Decimal)    aggregateResult.get('marketRate');

        }
        //conversionRate = marketRate;
        for(AggregateResult aggregateResult : [SELECT sum(Amount__c) holdingBalance
                                               FROM Holding_Balance__c
                                               WHERE Account__c =: currentAccount
                                               AND Custom_Currency__c =: currencyS]) {

           if(aggregateResult.get('holdingBalance') != null)
           holdingBalance = (Decimal)   aggregateResult.get('holdingBalance');
         }
         hedgedTotal = hedgeAccount + holdingBalance;
         for(InvoiceUIWrapper_MultiLine invoiceRec: invoices){
            if(invoiceRec.Invoice.Custom_Currency__c == currencyS && invoiceRec.Invoice.Amount__c != null) {
                    aggregateTotal += invoiceRec.Invoice.Amount__c;
                }
         }
         if(conversionRate==0 || conversionRate<0){
            //Conversion market not found for selected currency.
            totalInHomeCurrency = aggregateTotal;
         }else{
            totalInHomeCurrency = aggregateTotal / conversionRate;
         }
         unHedgedTotal = aggregateTotal - hedgedTotal;
         return null;
    }
     private List<SelectOption> getPayableOption(){
        List<SelectOption> PayableOption = new List<SelectOption>();
        PayableOption.add(new SelectOption(lABEL.CM_EDGE_ONLY_PAYABLE_INVOICE , lABEL.CM_EDGE_ONLY_PAYABLE_INVOICE));
        PayableOption.add(new SelectOption(lABEL.CM_EDGE_ALL_INVOICES , lABEL.CM_EDGE_ALL_INVOICES));
        return PayableOption;
        
    }
    
     public  void generateInvoiceFilterQuery() {
         
		//String objTypeField = objectType=='Invoice'? 'Due_Date__c':'Transaction_Date__c';
		//System.assert(false,'stdate:: '+ stDate + '  >> ' + endDate);
		system.debug('>>> generateInvoiceFilterQuery >>> '+ 'stdate:: '+ stDate + '  >> ' + endDate);
		log.logDebug('>>> generateInvoiceFilterQuery >>> '+ 'stdate:: '+ stDate + '  >> ' + endDate); 
        	String startDateL = stDate+'' ;//'yyyy-MM-dd\'T\'hh:mm:ss\'Z\''
            startDateL = startDateL.replace('00:00:00', '');
            SelectedDate = 'AND'+' Due_Date__c'+'>='+startDateL;
       // if(input.Window_End_Date__c != system.today()){
        	String endDateL = endDate+'' ;//'yyyy-MM-dd\'T\'hh:mm:ss\'Z\''
            endDateL = endDateL.replace('00:00:00', '');
            SelectedDate += 'AND'+' Due_Date__c'+'<='+endDateL;
        //    }
         //System.assert(false, 'Hello'+SelectedDate);
        system.debug('>>> generateInvoiceFilterQuery >>> '+ 'SelectedDate:: '+ SelectedDate + '  >> ' + endDateL);
            	searchInvoice();
            
	}

}