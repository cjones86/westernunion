/**
 * An apex page controller that exposes the site login functionality
 */
global with sharing class CommunitiesLoginController {

    global CommunitiesLoginController () {}
    
     global PageReference forwardToAuthPage() {
        system.debug('**********Test init');
        String startUrl = System.currentPageReference().getParameters().get('startUrl');
        String displayType = System.currentPageReference().getParameters().get('display');
        return Network.forwardToAuthPage(startUrl, displayType);
    }
    
    // Code we will invoke on page load.
    global PageReference forwardToCustomAuthPage() {
    	system.debug('**********Test reach');
    	//System.assertEquals(true, false);
    	if(Site.getPrefix().contains('managecash')){
	    	PageReference pgRef = Page.CommunityLogin;
	    	pgRef.setRedirect(true);
	        return pgRef;
    	}else{
    		return forwardToAuthPage();
    	} 
    }
}