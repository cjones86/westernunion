/**
 * Appirio, Inc
 * Name             : ManageCashInvoiceCtlTest
 * Created Date     : 23 March 2015 
 * Created By       : Shailendra Singh (Appirio)
 * Description      : Test class for ManageCashInvoiceCtl
 */
@isTest
private class ManageCashInvoiceCtlTest {
	static Account acc;
	static Input_Beneficiary__c ib;
	static Contact newContact;
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        createTestData();
        Beneficiary_Contact__c ben = new Beneficiary_Contact__c(name ='Test Ben');
        insert ben;
        ApexPages.currentPage().getParameters().put('aId', acc.Id);
        ApexPages.currentPage().getParameters().put('beneficaryId',ben.Id);
        ApexPages.currentPage().getParameters().put('fin','test');
        ManageCashInvoiceCtl ctrl = new ManageCashInvoiceCtl();
        List<SelectOption> options = ctrl.getForwardContractCurrency();
    //    insert ctrl.invoice;
     //   ctrl.cancel();
        pagereference pr = ctrl.saveInvoice();
        ctrl.invoice.Status__c = 'test';
        ctrl.invoice.Due_Date__c = Date.Today();
        ctrl.invoice.Amount__c = 1.0;
        ctrl.invoice.Input_Beneficiary__c = ib.Id;
        ctrl.invoice.Custom_Currency__c = 'AED';
        ctrl.hasError = false;
        ctrl.closeBenePopup();
        ctrl.showBenePopUp();
        Id accId = ctrl.AccId;
        String inputLink = ctrl.inputLink;
        insert ctrl.invoice;
        pr = ctrl.saveInvoice();
        ctrl.cancel2();
    }
    
    private static void CreateTestData() {
        test_Utility.createCMPAdministration();
		test_Utility.createCMPAlert();
		test_Utility.createWubsIntAdministration();
		test_Utility.createGPH2HCurrencies();
		test_Utility.createGPIntegrationAdministration();
		test_Utility.currencyISOMapping();
        test_Utility.createWUEdgeSharingAdmin();
		CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.CCT_Client_ID__c = '3232112';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount.CMP_Create_Transactions__c = true;
        beneAccount.CMP_Enabled__c = true;
        beneAccount.CMP_Holding_Enabled__c = true;
        beneAccount.CMP_H2H_Transaction_Enabled__c = true;
        beneAccount.BillingStreet = 'testStreet';
        beneAccount.BillingCity = 'city';
        beneAccount.BillingState = 'state';
        beneAccount.BillingCountry = 'USA';
        beneAccount.BillingPostalCode = '231231';
        beneAccount.Billing_Address_ISO_Country_Code__c = 'USA';
        
        acc = new Account(name = 'acc');
    	
    	List<Account> listAcc = new List<Account>{beneAccount,acc};
    	insert listAcc;
    	
        newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact;
    	
    	InsightsAndInputTabCtrl insight = new InsightsAndInputTabCtrl();
    	//Transaction_Type__c tt : [SELECT id, name, Option_Index__c From Transaction_Type__c order by Option_Index_
    	List<Transaction_Type__c> listTransactions = new List<Transaction_Type__c>();
    	Transaction_Type__c tt1 = new Transaction_Type__c(name='tt1',Option_Index__c=1);
    	listTransactions.add(tt1);
    	Transaction_Type__c tt2 = new Transaction_Type__c(name='tt2',Option_Index__c=2);
    	listTransactions.add(tt2);
    	insert listTransactions;
    	ib = new Input_Beneficiary__c(name='test');
    	insert ib;    	
    }
}