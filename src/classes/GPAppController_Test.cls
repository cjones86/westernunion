/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_InvoiceManagement_Test
 * Description: Controller Test class for GPAppController
 * Created Date: 05 Apr 2016
 * Created By: Nikhil Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
@isTest
private class GPAppController_Test {
	
	private static User commUser;

  private static testMethod void test() {
  	
        createTestData();
        Invoice__c inv = new Invoice__c(Beneficiary_GP_ID__c='12345',Beneficiary_GP_Email_Address__c='testuser@test123.com');
        insert inv;
        Invoice__c inv1 = new Invoice__c(Beneficiary_GP_Email_Address__c='testuser@test123.com');
        insert inv1;
        
        ApexPages.currentPage().getParameters().put('Id',inv.Id);
        Global_Pay_ID_Management__c gPIM = new Global_Pay_ID_Management__c(Global_Pay_ID__c='12345',
                                                                               User_Beneficiary__c=commUser.Id,
                                                                               Source_Email_Match__c='testuser@test123.com',
                                                                               Creation_Type__c='Automatic Email Match');
        insert gPIM;
        System.RunAs(commUser){
            
            Test.StartTest();
            //ApexPages.StandardController controller = new ApexPages.StandardController(inv);
            GPAppController obj = new GPAppController();
            obj.saveInvoice();
            obj.payInvoice();
            ApexPages.currentPage().getParameters().put('currUrl','/EDGE_InvoiceDetail2');
            obj.payInvoice();
            obj.updateInvoice();
            obj.isCMPEnabled = false;
            obj.demoCallBackUrl();
            obj.Save();
            Test.StopTest();
        }
  }
  
    private static testMethod void test1() {
    
        createTestData();
        Invoice__c inv = new Invoice__c(Beneficiary_GP_ID__c='12345',Beneficiary_GP_Email_Address__c='testuser@test123.com');
        insert inv;
        Invoice__c inv1 = new Invoice__c(Beneficiary_GP_Email_Address__c='testuser@test123.com');
        insert inv1;
        
        ApexPages.currentPage().getParameters().put('Id',inv.Id);
        ApexPages.currentPage().getParameters().put('app','payBeneficiary');
        Global_Pay_ID_Management__c gPIM = new Global_Pay_ID_Management__c(Global_Pay_ID__c='12345',
                                                                               User_Beneficiary__c=commUser.Id,
                                                                               Source_Email_Match__c='testuser@test123.com',
                                                                               Creation_Type__c='Automatic Email Match');
        insert gPIM;
        System.RunAs(commUser){
            
            Test.StartTest();
            //ApexPages.StandardController controller = new ApexPages.StandardController(inv);
            GPAppController obj = new GPAppController();
            obj.saveInvoice();
            obj.payInvoice();
            ApexPages.currentPage().getParameters().put('currUrl','/EDGE_InvoiceDetail2');
            obj.payInvoice();
            obj.updateInvoice();
            obj.isCMPEnabled = false;
            obj.demoCallBackUrl();
            obj.Save();
            Test.StopTest();
        }
  }
  
     private static testMethod void test2() {
    
        createTestData();
        Invoice__c inv = new Invoice__c(Beneficiary_GP_ID__c='12345',Beneficiary_GP_Email_Address__c='testuser@test123.com');
        insert inv;
        Invoice__c inv1 = new Invoice__c(Beneficiary_GP_Email_Address__c='testuser@test123.com');
        insert inv1;
        
        ApexPages.currentPage().getParameters().put('Id',inv.Id);
        ApexPages.currentPage().getParameters().put('app','SelectBene');
        Global_Pay_ID_Management__c gPIM = new Global_Pay_ID_Management__c(Global_Pay_ID__c='12345',
                                                                               User_Beneficiary__c=commUser.Id,
                                                                               Source_Email_Match__c='testuser@test123.com',
                                                                               Creation_Type__c='Automatic Email Match');
        insert gPIM;
        System.RunAs(commUser){
            
            Test.StartTest();
            //ApexPages.StandardController controller = new ApexPages.StandardController(inv);
            GPAppController obj = new GPAppController();
            Test.StopTest();
        }
  }
  
   private static testMethod void test3() {
    
        createTestData();
        Invoice__c inv = new Invoice__c(Beneficiary_GP_ID__c='12345',
        								Beneficiary_GP_Email_Address__c='testuser@test123.com',
        								Delivery_Method__c = 'Holding-to-Holding' );
        insert inv;
        Invoice__c inv1 = new Invoice__c(Beneficiary_GP_Email_Address__c='testuser@test123.com');
        insert inv1;
        
        Account acc = test_Utility.createAccount(true);
        
        Supplier__c supp = test_Utility.createSupplier(null, acc.Id, true);
        
        Batch__c bt = new Batch__c(CurrencyIsoCode = 'USD');
        insert bt;
        
        inv.Batch_Id__c = bt.Id;
        update inv;
        
        ApexPages.currentPage().getParameters().put('Id',inv.Id);
        ApexPages.currentPage().getParameters().put('app','manageBeneficiary');
        ApexPages.currentPage().getParameters().put('suppId',supp.Id);
        ApexPages.currentPage().getParameters().put('batchId',bt.Id);
        Global_Pay_ID_Management__c gPIM = new Global_Pay_ID_Management__c(Global_Pay_ID__c='12345',
                                                                               User_Beneficiary__c=commUser.Id,
                                                                               Source_Email_Match__c='testuser@test123.com',
                                                                               Creation_Type__c='Automatic Email Match');
        insert gPIM;
        System.RunAs(commUser){
            
            Test.StartTest();
            //ApexPages.StandardController controller = new ApexPages.StandardController(inv);
            GPAppController obj = new GPAppController();
            GPAppController.batchInvoices BI = new GPAppController.batchInvoices();
            BI.convertToJson(BI);
            Test.StopTest();
        }
  }
  
  static void createTestData(){
  	    test_Utility.createCMPAdministration();
	    test_Utility.createCMPAlert();
	    test_Utility.createWubsIntAdministration();
	    test_Utility.createGPH2HCurrencies();
	    test_Utility.createGPIntegrationAdministration();
	    test_Utility.currencyISOMapping();
	    test_Utility.createWUEdgeSharingAdmin();    
	    CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.CCT_Client_ID__c = '12345';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount.CMP_Create_Transactions__c = true;
        beneAccount.CMP_Enabled__c = true;
        beneAccount.CMP_Holding_Enabled__c = true;
        beneAccount.CMP_H2H_Transaction_Enabled__c = true;
        beneAccount.BillingStreet = 'testStreet';
        beneAccount.BillingCity = 'city';
        beneAccount.BillingState = 'state';
        beneAccount.BillingCountry = 'USA';
        beneAccount.BillingPostalCode = '231231';
        beneAccount.Billing_Address_ISO_Country_Code__c = 'USA';
        insert beneAccount;
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact;
        
        commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
        commUser.timezonesidkey='America/Indiana/Indianapolis';
        commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        commUser.CMP_Network_Invitation_Enabled__c = true;
        commUser.CMP_Create_Transactions__c = true;
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        insert commUser;
        
        

        
  }
}