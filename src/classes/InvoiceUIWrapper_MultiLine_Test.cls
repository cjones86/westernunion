/**=====================================================================
 * Appirio, Inc
 * Name: InvoiceUIWrapper_MultiLine_Test
 * Description: Test class for InvoiceUIWrapper_MultiLine
 * Created Date: 19th Aug 2016
 * Created By: Nikhil Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
 
@isTest
private class InvoiceUIWrapper_MultiLine_Test {

	private static testMethod void test() {
        test_Utility.createCMPAdministration();
		test_Utility.createCMPAlert();
		test_Utility.createWubsIntAdministration();
		test_Utility.createGPH2HCurrencies();
        test_Utility.createWUEdgeSharingAdmin();
		CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.CCT_Client_ID__c = '3232112';
        beneAccount.CMP_Enabled__c = true;
        beneAccount.CMP_Create_Transactions__c = true;
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount.BillingStreet = 'testStreet';
        beneAccount.BillingCity = 'city';
        beneAccount.BillingState = 'state';
        beneAccount.BillingCountry = 'USA';
        beneAccount.BillingPostalCode = '231231';
        
        Account beneAccount1 = test_Utility.createAccount(false);
        beneAccount1.RecordTypeId=accountRecordType;
        beneAccount1.ExternalId__c='11111145';
        beneAccount.CCT_Client_ID__c = '32321122131';
        beneAccount1.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount1.CMP_Enabled__c = true;
        
        List<Account> listAccount = new List<Account>{beneAccount, beneAccount1};
        insert listAccount;
        
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        Contact newContact1 = test_Utility.createContact(contactRecordType,false,beneAccount1.Id);
        
        List<Contact> listContacts = new List<Contact>{newContact,newContact1};
        insert listContacts;
        
        User commUser1 = test_Utility.createCommUser(newContact1.Id, false);
        commUser1.ProfileId = profileId;
        commUser1.emailencodingkey='UTF-8';
        commUser1.localesidkey='en_US';
		commUser1.timezonesidkey='America/Indiana/Indianapolis';
		commUser1.CMP_Enabled__c=true;
		commUser1.Email = 'testuser@test1233.com';
        commUser1.UserName = newContact1.Email+'.cmp1';
        
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
        commUser.CMP_Enabled__c = true;
        commUser.CMP_Create_Transactions__c = true;
        commUser.CMP_Network_Invitation_Enabled__c = true;
		commUser.timezonesidkey='America/Indiana/Indianapolis';
		commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        
        list<User> listUsers = new List<User>{commUser,commUser1};
        insert listUsers;   
        
        Supplier__c supp = test_Utility.createSupplier(beneAccount1.Id,beneAccount.Id, true);
        
        
        Invoice__c invoice = test_Utility.createInvoice('GP Invoice - Active', beneAccount.Id, commUser.Id,'Draft', false);
        invoice.Beneficiary_GP_Name__c = 'testGP';
        invoice.Supplier__c = supp.Id;
        invoice.Beneficiary_GP_ID__c = '123131131';
        invoice.Delivery_Method__c = 'Standard Payment';
        invoice.GP2_Last_Order_Submitted_Date_Time__c = System.now();
        insert invoice;
        Input__c inp = test_Utility.createInputWithType(true,'Purchase Order', beneAccount.Id);
        
        Test.startTest();
        System.runAs(commUser){
            InvoiceUIWrapper_MultiLine invWrp2 = new InvoiceUIWrapper_MultiLine(inp);
            invWrp2.itemType = 'Input';
            String categoryTrns1 = invWrp2.categoryTrns;
            // System.debug('>>>> invoice>>> '+ invoice.RecordTypeId);
            // try{
            //     InvoiceUIWrapper invWrp3 = new InvoiceUIWrapper(invoice);
            // }Catch(exception ex){}
            
            InvoiceUIWrapper_MultiLine invWrp1 = new InvoiceUIWrapper_MultiLine();
            Invoice__c invo = [Select Id, Name, Status__c,RecordType.Name,Custom_Currency__c,RecordTypeId,Invoice_Number__c,Delivery_Method__c,
                                        Initiated_By__c,Due_Date__c,Amount__c,Account__c,Buyer__c,Buyer__r.Contact.AccountId,Beneficiary_GP_ID__c,Supplier__r.Buyer__c,
                                        Beneficiary_GP_Name__c,Reference_Number__c,Type__c,Account__r.Name,TypeTrans__c,Supplier__c,Supplier__r.Supplier__c
                                From Invoice__c where Id = :invoice.Id];
            System.debug('invo:::: '+ invo.RecordType.Name);
            invWrp1.Invoice = invo;
            InvoiceUIWrapper_MultiLine invWrp3 = new InvoiceUIWrapper_MultiLine(invo);
            invWrp3.reffrenceBasedOnRecordType();
            String categoryTrns = invWrp3.categoryTrns;
            Boolean isViewSubmitAccess = invWrp3.isViewSubmitAccess;
            Boolean isViewAccess = invWrp3.isViewAccess;
            Boolean isEditAccess = invWrp3.isEditAccess;
            Boolean isPayNowAccess = invWrp3.isPayNowAccess;    
            invWrp3.selected = true;
            invWrp3.settlementAmt = 11;
            
        }

        
        Test.stopTest();
	}

}