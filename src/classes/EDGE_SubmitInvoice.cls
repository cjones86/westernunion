/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_SubmitInvoice
 * Description: Controller to submit Invoice to from bene to connected buyer's
 * Created Date: 10 Feb' 2016
 * Created By: Rohit Sharma (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
public without sharing class EDGE_SubmitInvoice {

    static apexLogHandler.apexLog logInsertInvoice = new apexLogHandler.apexLog('EDGE_SubmitInvoice','insertInvoice');
    static apexLogHandler.apexLog logUploadAttachmentFeed = new apexLogHandler.apexLog('EDGE_SubmitInvoice','uploadAttachment');
    
    public Invoice__c invoice{get;set;}
    //private FeedItem uploadedAttachment;
    transient public Blob fileData{get;set;}
    public boolean canCreateTransaction{get;set;}
    public Id recordTypeId{get;set;}
    public integer deleteIndex{get;set;}
    public String allowedFileType{get;set;}
    public String buyerName{get;set;}
    public String accountId{get;set;}
    public String buyerId{get;set;}
    public PageReference returnurl{get;set;}
    public boolean isSaveClicked{get;set;}
    public boolean isInvSaved{get;set;}
    public String parentInvoiceId{get;set;}
    public String invoiceOwnerId{get;set;}
    public String latestContentId{get;set;}
    public String errorMessage{get;set;}
    public String fileName{get;set;}
    public String fileId{get;set;}

    public EDGE_SubmitInvoice() {
        canCreateTransaction = Utility_Security.canCreateNetworkTransaction;
        //uploadedAttachment = new FeedItem();
        fileId = null;
        fileName = null;
        returnurl = Page.EDGE_MyDashboard;
        isSaveClicked = false;
        isInvSaved = false;
        errorMessage = '';
        if(ApexPages.currentPage().getParameters().get('buyerId') != null && ApexPages.currentPage().getParameters().get('buyerId') != ''){
            buyerId = ApexPages.currentPage().getParameters().get('buyerId');
        }

        if(ApexPages.currentPage().getParameters().get('buyerName') != null && ApexPages.currentPage().getParameters().get('buyerName') != ''){
            buyerName = ApexPages.currentPage().getParameters().get('buyerName');
        }

        if(ApexPages.currentPage().getParameters().get('accountId') != null && ApexPages.currentPage().getParameters().get('accountId') != ''){
            accountId = ApexPages.currentPage().getParameters().get('accountId');
        }


        if(ApexPages.currentPage().getParameters().get('retUrl') != null && ApexPages.currentPage().getParameters().get('retUrl') != ''){
            returnurl = new PageReference(ApexPages.currentPage().getParameters().get('retUrl'));
        }
        CMP_Administration__c cmpAdmin = CMP_Administration__c.getInstance();
        parentInvoiceId = cmpAdmin.EDGE_Default_Feed_Invoice__c;
        String recordTypeName = cmpAdmin.CMP_GP_Default_Invoice_Record_Type__c;
        /*if(!canCreateTransaction) {
            recordTypeName = cmpAdmin.CMP_Default_Invoice_Record_Type__c;
        }*/

        if(!String.isEmpty(parentInvoiceId)) {
            invoiceOwnerId = [SELECT OwnerId FROM Invoice__c WHERE Id=:parentInvoiceId].OwnerId;
        }
        allowedFileType = cmpAdmin.CMP_Allowable_File_Types__c;
        recordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        invoice = new Invoice__c(RecordTypeId=recordTypeId);
        for(Global_Pay_ID_Management__c gpm: [Select Id,User_Beneficiary__r.Name, Global_Pay_ID__c From Global_Pay_ID_Management__c
                                              Where User_Beneficiary__c=:UserInfo.getUserId()]){
            invoice.Beneficiary_GP_Name__c = gpm.User_Beneficiary__r.Name;
            invoice.Beneficiary_GP_ID__c = gpm.Global_Pay_ID__c;
        }
        latestContentId = null;
    }

     private boolean pageMessage() {
        System.debug('**********inside pageMessage');
        invoice.Status__c = 'Invoice Submitted';
        invoice.Initiated_By__c = 'Seller';
        invoice.RecordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('GP Invoice - Active').getRecordTypeId();
        boolean hasError = false;
        FeedItem uploadedAttachment = null;
        errorMessage = '';
        if(String.isEmpty(buyerId)) {
            errorMessage = System.Label.EDGE_Netwok_User;
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.EDGE_Netwok_User));
            hasError = true;
            System.debug('**********hasError = true');
        }else {
            System.debug('**********else');
            invoice.Account__c = accountId;
            invoice.Buyer__c = buyerId;
            invoice.Seller_Account__c = utility.currentAccount;
          for(Supplier__c sup:[Select id, supplier__c, Buyer__c From Supplier__c Where supplier__c = :utility.currentAccount and Buyer__c = :accountId order by Created_Invoice_Date__c desc]){
            invoice.supplier__c = sup.id;
            break;
          }
          
            for(User UserBuyer : [SELECT Id,Name FROM User WHERE Id=:buyerId]) {
                invoice.Buyer__r = UserBuyer;
                buyerId = UserBuyer.Id;
                buyerName = UserBuyer.Name;
            }
        }
        if(fileId==null) {
            errorMessage += '<br/>'+System.Label.EDGE_Upload_Attachment_Message;
            hasError = true;
        }else {
            for(FeedItem feed : [SELECT Id,ContentData,ContentFileName FROM FeedItem WHERE Id=:fileId]) {
                uploadedAttachment = feed;
            }
            if(uploadedAttachment==null) {
                errorMessage += '<br/>'+System.Label.EDGE_Upload_Attachment_Message;
                fileName = null;
                fileId=null;
                latestContentId=null;
                hasError = true;
            }
        }
        /*if(invoice.Invoice_Number__c == null || invoice.Invoice_Number__c == '') {
            errorMessage += '<br/>'+System.Label.CM_Validation_Input_InvoiceNumberShouldNotBeBlank;
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.CM_Validation_Input_InvoiceNumberShouldNotBeBlank));
            hasError = true;
        }*/
        System.debug('**********hasError: ' + hasError);
        return hasError;
     }

     private boolean insertInvoice() {
        logInsertInvoice.logMessage('>>inside insertInvoice method ...>> ');
        boolean hasError = pageMessage();
        logInsertInvoice.logDebug('hasError ' + hasError);
        if(!hasError) {
                System.debug('**********insertInvoice - hasError: ' + hasError);
                logInsertInvoice.logDebug('**********insertInvoice - hasError: ' + hasError);
                invoice.Seller_Account__c = utility.currentAccount;
                insert invoice;
                logInsertInvoice.logDebug('invoice record' + invoice);
               if(fileId!=null) {
                    FeedItem uploadedAttachment = [SELECT Id,ContentData,ContentFileName FROM FeedItem WHERE Id=:fileId];
                    if(uploadedAttachment!=null) {
                        FeedItem post = new FeedItem();
                        post.Title = uploadedAttachment.ContentFileName;
                        post.ParentId = invoice.Id; //eg. Opportunity id, custom object id..
                        post.Body = uploadedAttachment.ContentFileName + '-'+invoice.Name;
                        post.ContentData = uploadedAttachment.ContentData;
                        post.ContentFileName = uploadedAttachment.ContentFileName;
                        post.Visibility = 'AllUsers';
                        insert post;
                    }
                    fileId = null;
                }
            }
            logInsertInvoice.saveLogs();
            return hasError;
     }

     public PageReference redirectToHome(){
        if(isSaveClicked){
            String encodedInvoiceId = Utility.doEncryption(invoice.Id);
            returnurl = new PageReference (CMP_Administration__c.getInstance().CM_Community_BaseURL__c+'/EDGE_Invoice_Submit_Detail?Id='+encodedInvoiceId);
            return returnurl;
        }
        else{
            PageReference pgRef = new PageReference('/EDGE_SubmitInvoice');
            pgRef.setRedirect(true);
            return pgRef;
        }
     }

     public PageReference save() {
        Savepoint sp = Database.setSavepoint();
        try {
            if(!insertInvoice()){
                isSaveClicked = true;
                isInvSaved = true;
            }
        } catch (Exception ex) {
            errorMessage += '<br/>'+System.Label.CM_Alert_UnableToSaveInvoice + ex;
            Database.rollback( sp );
        }
        return null;
     }

     public PageReference saveNew() {
        Savepoint sp = Database.setSavepoint();
        try {
            if(!insertInvoice()) {
                isInvSaved = true;
                invoice = new Invoice__c(RecordTypeId=recordTypeId);
            }
        } catch (Exception ex) {
            errorMessage += '<br/>'+System.Label.CM_Alert_UnableToSaveInvoice + ex;
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.CM_Alert_UnableToSaveInvoice + ex));
            Database.rollback( sp );
        }
        return null;
     }

     public PageReference uploadAttachmentFeed() {
        logUploadAttachmentFeed.logMessage('>>inside uploadAttachmentFeed method ...>> ');
        try {
            logUploadAttachmentFeed.logDebug('');
            errorMessage = '';
            if(fileName!=null && !String.isBlank(fileName)) {
                String filetype = fileName.subString(fileName.lastIndexOfIgnoreCase('.')+1);
                if((allowedFileType==null || String.isBlank(allowedFileType)) || !allowedFileType.containsIgnoreCase(filetype)) {
                    errorMessage = System.Label.CM_Alert_File_InvalidFileFormat;
                    return null;
                }
            }
            FeedItem uploadedAttachment = new FeedItem();
            uploadedAttachment.ContentFileName = fileName;
            uploadedAttachment.Title = uploadedAttachment.ContentFileName;
            uploadedAttachment.ParentId = parentInvoiceId;//'a1d1b0000008jhw'; //eg. Opportunity id, custom object id..
            uploadedAttachment.Body = uploadedAttachment.ContentFileName + '-submitinvoice';
            uploadedAttachment.Visibility = 'AllUsers';
            uploadedAttachment.ContentData = fileData;
            insert uploadedAttachment;
            logUploadAttachmentFeed.logDebug('uploadedAttachment ' + uploadedAttachment);
            fileId = uploadedAttachment.Id;
            latestContentId = [SELECT RelatedRecordId FROM FeedItem WHERE Id=:fileId limit 1].RelatedRecordId;
            update (new ContentVersion(Id=latestContentId,OwnerId=invoiceOwnerId));
        }catch (Exception ex) {
            logUploadAttachmentFeed.logDebug('Exception' + ex.getMessage());
            errorMessage = ''+ex;
        }
        fileData = null;
        logUploadAttachmentFeed.saveLogs();
        return null;
     }

     public PageReference deleteAttachmentFeed() {
       fileId = null;
       fileName = null;
       latestContentId = null;
       return null;
     }

}