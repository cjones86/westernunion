/**=====================================================================
 * Appirio, Inc
 * Name: CMPPendingAlertscontrollerTest
 * Description: Controller Test class for CMPPendingAlertscontroller
 * Created Date: 05 Fab 2016
 * Created By: Rohit Sharma (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
@isTest
private  class CMPPendingAlertscontrollerTest {

    @isTest
    static void testDocumentLoad() {
        test_Utility.createCMPAdministration();
        test_Utility.createCMPAlert();
        test_Utility.createWUEdgeSharingAdmin();
        CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;

        String Runasuser = UserInfo.getUserId();
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount.CMP_Enabled__c=true;
        beneAccount.CMP_Create_Transactions__c = true;
        insert beneAccount;
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact;
        Account beneAccount1 = test_Utility.createAccount(false);
        beneAccount1.RecordTypeId=accountRecordType;
        beneAccount1.ExternalId__c='12345';
        beneAccount1.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount1.CMP_Enabled__c=true;
        insert beneAccount1;
        Contact newContact1 = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact1;
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.CMP_Create_Transactions__c = true;
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
        commUser.timezonesidkey='America/Indiana/Indianapolis';
        commUser.CMP_Enabled__c=true;
        commUser.CMP_Network_Invitation_Enabled__c=true;
        commUser.EDGE_Payment_Approvals__c = true;
        commUser.UserName = newContact.Email+'.cmp';
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        commUser.CommunityNickname = commUser.alias;
        insert commUser;
        if (commUser.Id != null) {
            Global_Pay_ID_Management__c gPIM = new Global_Pay_ID_Management__c(Global_Pay_ID__c='12345',
                                                                               User_Beneficiary__c=commUser.Id,
                                                                               Creation_Type__c='Automatic Email Match');
           insert gPIM;
        }
        CMP_Alert_Management__c alertManagement = CMP_Alert_Management__c.getOrgDefaults();
        alertManagement.Invoices_Rejected_Invoice_Statuses__c = 'Rejected';
        alertManagement.Invoices_Raised_Invoice_Statuses__c = 'Invoice Submitted';
        alertManagement.CMP_Invites_Open_Status_Values__c = 'Sent, In Progress';
        alertManagement.Invoices_Raised_Invoice_Record_Types__c = 'GP Invoice - Active';
        alertManagement.Network_Invites_Complete_Status_Values__c = '2 - Rejected (Declined), 3 - Active (Accepted), 4 - Deactivated';
        alertManagement.Network_Invites_Open_Status_Values__c = '1 - Pending (Sent)';
        alertManagement.Invoices_Pending_Approval_Statuses__c = 'Pending Approval';
        upsert alertmanagement;

        Invoice__c invoice = test_Utility.createInvoice(false);
        invoice.RecordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('GP Invoice - Active').getRecordTypeId();
        invoice.Custom_Currency__c = 'USD';
        invoice.Invoice_Number__c = 'INVTest-123';
        invoice.Status__c = 'Invoice Submitted';
        invoice.Initiated_By__c='Seller';
        invoice.Due_Date__c = Date.today().addDays(30);
        invoice.Amount__c = 2000;
        //invoice.Account__c = Utility.currentAccount;
        invoice.Account__c = beneAccount.Id;
        invoice.Buyer__c = commUser.Id;
        insert invoice;
        
        //Insert Payment record
        Payment__c pmt = new Payment__c();
        pmt.Invoice_ID__c = invoice.Id;
        pmt.Notification_User__c = commUser.Id;
        pmt.Payment_Status__c = 'Pending Approval';
        pmt.Order_ID__c = '3123123113';
        pmt.Payment_Amount__c = 23123;
        pmt.Payment_Date_Time__c = System.Now();
        insert pmt;
        
        
        /*Invites__c invite = new Invites__c(Invitation_Sent_Count__c=1,
                                           Benne_Email__c='rohit.sharma.satya@gmail.com',
                                           Inviter_User__c=commUser.Id,
                                           Invitee_Contact__c=newContact1.Id,
                                           Status__c = 'Sent',
                                           Invoice__c=invoice.Id,
                                           Invitation_Token__c=GPIntegrationUtility.generateInvitesTOKEN('Invitation_Token__c'),
                                           Last_Sent_By_User__c=invoice.OwnerId,
                                           Status_DateTime__c = system.now());
        insert invite;*/
        Network__c newNetwork = new Network__c(Inviter_User__c=Runasuser,
                                               Invitee_Contact__c=newContact.Id,
                                               Account_Invitee__c = beneAccount.Id,
                                               Account_Inviter__c = beneAccount1.Id,
                                               Status__c='1 - Pending (Sent)',
                                               Status_DateTime__c = system.now());
        insert newNetwork;
        /*Network__c newNetwork1 = new Network__c(Invitee_Contact__c=newContact.Id,
                                               Inviter_User__c=commUser.Id,
                                               Status__c='3 - Active (Accepted)',
                                               Status_DateTime__c = system.now().addDays(-8));
        insert newNetwork1;*/
        
        Utility_Security.canCreateTransaction = true;
        System.RunAs(commUser){
            Invoice__c invoicereject = test_Utility.createInvoice(false);
            invoicereject.RecordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('GP Invoice - Active').getRecordTypeId();
            invoicereject.Custom_Currency__c = 'USD';
            invoicereject.Invoice_Number__c = 'INVTest-123';
            invoicereject.Status__c = 'Rejected';
            invoicereject.Initiated_By__c='Seller';
            invoicereject.Due_Date__c = Date.today().addDays(30);
            invoicereject.Amount__c = 2000;
            invoicereject.Account__c = Utility.currentAccount;
            invoicereject.Buyer__c = Runasuser;
            insert invoicereject;
            
            User currentUser = [Select   Id, Profile.Name, ProfileId, ContactId, DefaultCurrencyIsoCode, Name, IsPrmSuperUser,LocaleSidKey,EDGE_Submit_Invoice_Enabled__c,
                  CMP_Create_Transactions__c, CMP_Enabled__c,CMP_Network_Invitation_Enabled__c, Contact.Primary_Contact__c,Contact.Account.CCT_Client_ID__c,EDGE_Payment_Approvals__c,
                  Contact.Account.CMP_Holding_Enabled__c, Contact.Account.CMP_H2H_Transaction_Enabled__c, Contact.Account.CMP_Enabled__c, Contact.Account.CMP_Create_Transactions__c,
                  CurrencyIsoCode, Contact.AccountId, Contact.Account.CurrencyIsoCode, LanguageLocaleKey, Contact.Account.Name, Contact.Account.OwnerId, Contact.Account.RecordType.DeveloperName FROM  User
                    WHERE  Id = :UserInfo.getUserId() LIMIT 1];
            Utility.loggedInUser = currentUser;
            Utility_Security.us = currentUser;

            Test.startTest();
            	test_Utility.createGPIntegrationAdministration();
                Global_Pay_Integration_Administration__c gPIA = Global_Pay_Integration_Administration__c.getInstance();
             	System.debug('@@@gPIA : ' + gPIA.Integration_Enabled__c);
                System.debug('@@@isIntegrationEnabled : ' + Utility_Security.isIntegrationEnabled);
                System.debug('@@@us : ' + Utility_Security.us);
            	Contact con = [SELECT AccountId FROM Contact WHERE Id = :Utility_Security.us.contactID];
            	Account acc = [SELECT CMP_Create_Transactions__c FROM Account WHERE Id = :con.AccountId];
            	System.debug('@@@us Contact :' + acc);
                
                CMPPendingAlertsController cmp = new CMPPendingAlertsController();
                //system.assertEquals(3,cmp.cmpAlerts.size());
            Test.stopTest();
        }

    }
}