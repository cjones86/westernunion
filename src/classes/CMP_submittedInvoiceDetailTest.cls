/**=====================================================================
 * Appirio, Inc
 * Name: CMP_submittedInvoiceDetailTest
 * Description: Test class logic for CMP_submittedInvoiceDetailController
 * Created Date: 12 Feb' 2016
 * Created By: Rohit Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update                                                  
 =====================================================================*/
@isTest
private class CMP_submittedInvoiceDetailTest {

    static testMethod void previewInvoiceTest() {
    	Account beneAccount = test_Utility.createAccount(false);
		User commUser = createTestData(beneAccount);
		Invoice__c submitInvoice = new Invoice__c(RecordTypeId=Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('GP Invoice - Active').getRecordTypeId());
	    submitInvoice.Status__c = 'Invoice Submitted';
	    submitInvoice.Initiated_By__c='Seller';
	    submitInvoice.Account__c = beneAccount.Id;
	    submitInvoice.Buyer__c = commUser.Id;
	    submitInvoice.Beneficiary_GP_ID__c = '12345';
	    insert submitInvoice;
		System.RunAs(commUser){
        	Test.startTest();
        	ApexPages.currentPage().getParameters().put('id',submitInvoice.Id);
        	CMP_SubmittedInvoiceDetailController cmpInv = new CMP_SubmittedInvoiceDetailController();
        	cmpInv.feedPost.ContentData = Blob.valueof('Test data for pdf file');
        	cmpInv.feedPost.Title = 'test.txt';
        	cmpInv.uploadAttachment();
        	cmpInv.viewContent();
        	cmpInv.showRejectPopup();
        	cmpInv.closeRejectPopup();
        	cmpInv.accept();
        	cmpInv.rejectReason = 'Test reject';
        	cmpInv.reject();
        	FeedItem fi = new FeedItem();
            fi.ParentId = cmpInv.invoiceRecord.Id;
            fi.Body = System.Label.CM_ReasonToReject+ ' '+cmpInv.rejectReason;
            fi.Visibility = 'AllUsers';
            insert fi;
        	cmpInv.feedId = fi.Id;
        	cmpInv.deleteFeed();
        	system.assertEquals(cmpInv.contentPost.size(),0);
        	cmpInv.redirectURL();
        	Test.stopTest();
		}
    }
    
    static User createTestData(Account beneAccount) {
    	test_Utility.createCMPAdministration();
		test_Utility.createCMPAlert();
		test_Utility.currencyISOMapping();
        test_Utility.createWUEdgeSharingAdmin();
		CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        insert beneAccount;
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact;
        Account beneAccount1 = test_Utility.createAccount(false);
        beneAccount1.RecordTypeId=accountRecordType;
        beneAccount1.ExternalId__c='12345';
        beneAccount1.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        insert beneAccount1;
        Contact newContact1 = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact1;
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
		commUser.timezonesidkey='America/Indiana/Indianapolis';
		commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        commUser.CommunityNickname = commUser.alias;
        insert commUser;
        return commUser;
    }
}