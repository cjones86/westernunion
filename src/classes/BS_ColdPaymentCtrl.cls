/**=====================================================================
 * Name: BS_ColdPaymentCtrl
 * Description: Controller class for BS_ColdPayment page
 * Created Date: Feb 01, 2016
 * Created By: Nikhil Sharma
 * Task      : T-469137
 =====================================================================*/

public without sharing class BS_ColdPaymentCtrl{
     Public Id invRecordTypeId;
     Public boolean isUserH2H{get;set;}
     Public boolean isSaved{get;set;}
     Public String deliveryMethod {get;set;}
     Public InvoiceWrapper invWrp {get;set;}
     public Boolean displayPopup {get; set;}
     public String popupURL{get;set;}
     public String selectedInvoice{get;set;}
     public boolean isTransactionAllowedToCMP{get;set;}
     public List<SelectOption> H2Hcurrencies {get;set;}
     public Boolean isDisplayBenePopUp{get;set;}
     public String isError{get;set;}
     public String beneName{get;set;}
     public Id accId {get;set;}
	 public String hbCurrencyISO{get;set;}
	 public String errorMessage{get;set;}

   public void closeBenePopup() {
       isDisplayBenePopUp = false;
    }
    public void showBenePopUp() {
       isDisplayBenePopUp = true;
    }

   public PageReference closePopup() {
      displayPopup = false;
        PageReference returnurl = new PageReference (CMP_Administration__c.getInstance().CM_Community_BaseURL__c+'/EDGE_InvoiceDetail2?Id='+invWrp.inv.Id);
        return returnurl;
  }
    public Boolean intigrationON{get;set;}
    public void showPay() {
        popupURL = CMP_Administration__c.getInstance().Base_Invite_URL__c+'/GPAppTest?app=payBeneficiary&id='+selectedInvoice;
        displayPopup = true;
    }
    public boolean isApiOn{
    	get;
    	set;
    }
    public BS_ColdPaymentCtrl(){
    	 hbCurrencyISO = '';
		 isApiOn = false;
         User us = Utility.loggedinUser;
         Utility_Security.us = us;
         errorMessage = '';
         isTransactionAllowedToCMP = Utility_Security.canMakeStandardColdPayments;
         accId = utility.currentAccount;
         isError = 'false';
         if(!isTransactionAllowedToCMP){
            ApexPages.Message errMsg= new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.BS_CreatingTransactionPermissionError);
            ApexPages.addmessage(errMsg);
         }
         else{
             isUserH2H = false;
             isSaved = false;
             Invoice__c inv = new Invoice__c();
             invRecordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('Cold Payment - Active').getRecordTypeId();
             inv.RecordTypeId = invRecordTypeId;
             deliveryMethod = 'StandardPayment';
             inv.Delivery_Method__c = 'Standard Payment';
             inv.Buyer__c = UserInfo.getUserId();
             inv.Invite_to_my_network__c = false;
             intigrationON = true;
             if(Utility_Security.canMakeHBColdPayments){
                 isUserH2H = true;
				 //Get currency for HH.
				 String customerId ='';
				 EdgeGpJsonUtils jsonUtil = new EdgeGpJsonUtils(false);
			 	 H2Hcurrencies = jsonUtil.getHoldingCurrencies();
				 isApiOn = jsonUtil.isApiOn;

                 inv.Delivery_Method__c = 'Holding-to-Holding';
                 deliveryMethod = 'HoldingToHolding';
                 invWrp = new InvoiceWrapper(inv);
             }
             else{
                 invWrp = new InvoiceWrapper(inv);
             }
         }
     }
	 public String cctClientID{
		 get{
		 	return Utility.cctClientID;
		 }
	 }
     Public void payInvoice(){
         try{
           System.Debug('###'+invwrp.inv);
            integer error = 0;
            isSaved = false;
            selectedInvoice = '';
            System.Debug('###'+invWrp);
            System.Debug('###'+invWrp.inv);
            User u = Utility.loggedInUser;
            invWrp.inv.Status__c = 'Draft';
            invWrp.inv.Account__c = u.contact.AccountId;
            if(invWrp.inv.Delivery_Method__c == 'Holding-to-Holding'){
                for(Account HBAccount : [Select id, Name, (Select Id, Name, Email From Contacts) From Account Where CCt_Client_Id__c = :invWrp.inv.Beneficiary_GP_ID__c]) {
                    invWrp.inv.Beneficiary_GP_Email_Address__c = HBAccount.Contacts[0].Email;
                    invWrp.inv.Seller_Account__c = HBAccount.Id;
                    break;
                }
                invWrp.inv.Beneficiary_GP_Name__c = beneName;
                invWrp.inv.Custom_Currency__c = hbCurrencyISO;
             }
             updateInvoiceSellerAccount(invWrp.inv);
			 if(invWrp.inv.Due_date__c==null){
             	invWrp.inv.Due_date__c = Datetime.now().date();
             }             
             insert invWrp.inv;
             isSaved = true;
             selectedInvoice = String.valueOf(invWrp.inv.Id);
             popupURL = CMP_Administration__c.getInstance().Base_Invite_URL__c+'/GPAppTest?app=payBeneficiary&id='+selectedInvoice;
             displayPopup = true;
         } catch(DMLException ex) {
             isError = 'true';
             errorMessage += '<br/>'+ ex.getdmlMessage(0);
            // ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getdmlMessage(0)));
         } catch (Exception ex) {
             isError = 'true';
             errorMessage += '<br/>'+ System.Label.CM_Alert_UnableToSaveInvoice + ex.getMessage();
            // ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.CM_Alert_UnableToSaveInvoice + ex.getMessage()));
         }
     }

     Public Class InvoiceWrapper{
         Public Invoice__c inv{get;set;}
         Public InvoiceWrapper(Invoice__c invoice){
             inv = invoice;
         }
     }

     // T-485988 : Knong view Payment
     private void updateInvoiceSellerAccount(Invoice__c invoice) {
        if(invoice.Delivery_Method__c != 'Holding-to-Holding' && !String.isBlank(invoice.Beneficiary_GP_ID__c)) {
            for(Global_Pay_ID_Management__c GPM: [SELECT Account_Beneficiary__c, Source_Email_Match__c, 
                                                    User_Beneficiary__c From Global_Pay_ID_Management__c 
                                                    WHERE Global_Pay_ID__c =:invoice.Beneficiary_GP_ID__c]){
                invoice.Seller_Account__c = GPM.Account_Beneficiary__c;
                break;
            }
        }
     }
     
     @RemoteAction
     public static boolean BeneinNetwork (String BeneID, String userAccountId){

       //Find email matches for Bene email address
       Id BeneAccountID;
       Boolean BeneinNetwork = false;
       System.Debug('###'+BeneID);
       System.Debug('###'+userAccountID);
       for(Global_Pay_ID_Management__c GPM: [select Account_Beneficiary__c, Global_Pay_ID__c, Id, Source_Email_Match__c, Source_Invoice_Buyer_Account__c, Source_Invoice_ID__c, User_Beneficiary__c from Global_Pay_ID_Management__c Where Global_Pay_ID__c = :BeneID and Account_Beneficiary__c != null and Source_Invoice_Buyer_Account__c = :userAccountID]){
         BeneAccountID = GPM.Account_Beneficiary__c;
         break;
       }
       if(BeneAccountID != null){
         //Find network connections for current User and Bene account
         for(Network__c net:[Select Id,Account_Invitee__c,Account_Inviter__c From Network__c Where (Account_Invitee__c = :userAccountID OR Account_Inviter__c = :userAccountID) AND (Account_Invitee__c = :BeneAccountID OR Account_Inviter__c = :BeneAccountID) AND Status__c = '3 - Active (Accepted)']){
           BeneinNetwork = true;
           break;
         }
       }

       return BeneinNetwork;
     }
 }