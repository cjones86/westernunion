public with sharing class EDGE_ChangePasswordController {
      public String username {get; set;}   
    public Boolean isSuccess{get;set;}
    public String selLanguage{get;set;}
    
    //public String username{get; set;}
      public String password {get; set;}
      public String selectedLanguage{get;set;}
      public List<SelectOption> languageOptions{get;set;}
      public pageReference pgRef{get;set;}
    public String errorMessage{get;set;}
    public boolean isShowValidtionScreen{get;set;}
    public boolean isAccountLocked{get;set;}
    public integer validateFailCounter{get;set;}
    private EDGE_Custom_Auth__c eca;
    public String successMessage{get;set;}
    Public String email{get;set;}
    Public String encodedEmail{get;set;}
    Public String inputToken{get;set;}
    Private String secToken;
    Private User currUser;
    
      public EDGE_ChangePasswordController() {
        validateFailCounter = 0;
        languageOptions = new List<SelectOption>();
        for(CashManagementLanguageConfiguration__c cmLang : [Select id, Name, Label__c, ISO_Code__c from CashManagementLanguageConfiguration__c where Active__c = true]){
          languageOptions.add(new SelectOption(cmLang.ISO_Code__c, cmLang.Name));
        }
        system.debug('Cookie :'+retriveCookie());
        selectedLanguage = retriveCookie();
    
        selectedLanguage =  selectedLanguage.trim().length() > 0 ? selectedLanguage : 'en_US';
        changeLanguage();
        successMessage = '';
      }

      public PageReference changeLanguage(){
        System.Debug('###'+selectedLanguage);
        Cookie selLanguage = new Cookie('selLang', selectedLanguage,null,-1,true);
        ApexPages.currentPage().setCookies(new Cookie[]{selLanguage});
        PageReference pg = Page.CommunityLogin;
        pg.setRedirect(true);
        return pg;
      }
      
     private string retriveCookie(){
    Cookie selLanguage = ApexPages.currentPage().getCookies().get('selLang');
    if(selLanguage != null)
      return selLanguage.getValue();
    return '';
   }

    
    public PageReference forgotPassword() {
        //boolean success = Site.forgotPassword(username);
        // PageReference pr = Page.ForgotPasswordConfirm;
        // pr.setRedirect(true);
        //system.assert(false,username);
        isSuccess = false;
        if(username == null || username == ''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CM_EDGE_ForgotPassword_FillUsernameMsg));
            return null;
        }
        
        String usrProfile;
                for(User us :[Select Id, Name, Email, Profile.Name From User Where UserName = :userName]){
                    email = us.Email;
                    getMaskedEmail(email);
                    currUser = us;
                    usrProfile = us.Profile.Name;
                    isSuccess = true;
                    break;
                }
                String allowedTFAProfiles = CMP_Administration__c.getInstance().EDGE_TFA_Enabled_Profiles__c;
        if(pgRef!=null){
            if(!allowedTFAProfiles.containsIgnoreCase(usrProfile)){             
                pgRef.setredirect(true);
                        return pgRef;
            }
            isShowValidtionScreen = true;
            custAuth();
        }
        
        if(isSuccess == false){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CM_EDGE_Forgot_Password_NoUserExistsMessage));
            return null;
        }

        return null;
    }
    
   Public void getMaskedEmail(String Email){
        encodedEmail = '';
        if(String.isNotBlank(Email)){
           // Let's say Email = 'nikhil.sharma@appirio.com';
            encodedEmail = Email.left(2); // encodedEmail = 'ni'
            String s1 = '*';
            List<String> lst = Email.split('@'); // lst = {'nikhil.sharma', 'appirio.com'}
            Integer n1 = lst[0].length()-2; // n1 = Length('nikhil.sharma') - 2
            encodedEmail += s1.repeat(n1)+'@'; // encodedEmail = 'ni' + '****... n1 times' + '@'
            String str1 = lst[1].substringBefore('.'); // Str1 = 'appirio' (lst[1] = 'appirio.com')
            encodedEmail += s1.repeat(str1.length()-2); // encodedEmail = 'ni' + '****..*' + '@' + '*****'
            encodedEmail += str1.right(2); // encodedEmail = 'ni' + '****..*' + '@' + '*****' + 'io'
            String str = lst[1].substringAfter('.'); // str = 'com'
            encodedEmail += '.'+str;
        }
    }
    
    public void custAuth(){
        // Check if there already exists custom auth record
        for(EDGE_Custom_Auth__c auth: [Select Id, Name, Email_Address__c, Login_Status__c, OwnerId, Recipient_User__c, Security_Token__c,
                                              Login_Attempts__c,Last_Attempt_Date_Time__c
                                            From EDGE_Custom_Auth__c Where Recipient_User__c = :currUser.Id
                                            And Login_Status__c != 'Expired' Limit 1]){
            eca = auth;
            validateFailCounter = (Integer)auth.Login_Attempts__c;
            if(auth.Login_Status__c == 'Locked'){
                isAccountLocked = true;
                errorMessage = Label.Edge_Login_TFA_Account_Locked_Message;
            }
            return;
        }
        
        // Create new Custom auth record if there doesn't exist any.
        eca = new EDGE_Custom_Auth__c();
        eca.Email_Address__c = email;
        eca.Login_Status__c = 'Active';
        eca.OwnerId = currUser.Id;
        eca.Recipient_User__c = currUser.Id;
        decimal tkn = getSecurityToken();
        String token = tkn.toPlainString();
        if(token.length() < 8){
            String s1 = '0';
            Integer n1 = 8 - token.length();
            token += s1.repeat(n1);
        }
        eca.Security_Token__c = token;
        secToken = eca.Security_Token__c;
        insert eca;
    }
    
    public Integer getSecurityToken(){
        Integer r = Math.round(Math.random()*100000000);
        return r;
    }
    
    public pageReference resendToken(){
        System.debug('eca::: ' + eca);
        try{
            eca.Login_Status__c = 'Resend';
            update eca;
        }catch(exception ex){
            System.debug('ex:: '+ ex.getMessage());
            custAuth(); // create new custom auth record
        }
        successMessage = Label.EDGE_Login_TFA_ResendCodeInstructions;
        errorMessage = '';
        return null;
    }
    
     public pageReference doVerify(){
        System.debug('inputToken>>> ' + inputToken);
        System.debug('secToken>>> ' + secToken);
        boolean isUserAlreadyLoggedIn = true; // if user is logged in from another browser then auth record will not exist
        if(String.isEmpty(inputToken)|| inputToken.trim().length() != 8){
            errorMessage = Label.Edge_Login_TFA_Enter_8_Digit_Token;
            successMessage = '';
            return null;
        }
         inputToken = inputToken.trim();
        
        for(EDGE_Custom_Auth__c ec : [Select Id, Name, Email_Address__c, Login_Status__c, OwnerId, Recipient_User__c, Security_Token__c,
                                              Login_Attempts__c,Last_Attempt_Date_Time__c
                                            From EDGE_Custom_Auth__c Where Recipient_User__c = :currUser.Id
                                            And Login_Status__c != 'Expired' Limit 1]){
                                                
            eca = ec;
            secToken = eca.Security_Token__c;
            validateFailCounter = (Integer)ec.Login_Attempts__c;
            isUserAlreadyLoggedIn = false;
        }
        
        if(isUserAlreadyLoggedIn){
            errorMessage = Label.Edge_Login_TFA_Invalid_Token_Message;
            successMessage = '';
            return null;
        }
        
        if(validateFailCounter >= 4){
            isAccountLocked = true;
            eca.Login_Attempts__c = 5;
            eca.Last_Attempt_Date_Time__c = System.now();
            eca.Login_Status__c = 'Locked';
            update eca;
            errorMessage = Label.Edge_Login_TFA_Account_Locked_Message;
            successMessage = '';
            return null;
        }
        if(inputToken.equalsIgnoreCase(secToken)){
            pgRef = Site.login(username,password,null);
            pgRef.setredirect(true);
            delete eca;            
            return pgRef;
        }
        else{
            validateFailCounter += 1;
            eca.Login_Attempts__c = validateFailCounter;
            eca.Last_Attempt_Date_Time__c = System.now();
            eca.Login_Status__c = 'Failed';
            update eca;
            errorMessage = Label.Edge_Login_TFA_Invalid_Token_Message;
            successMessage = '';
            return null;
        }
    }

}