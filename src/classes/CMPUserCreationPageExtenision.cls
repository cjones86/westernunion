public without sharing class CMPUserCreationPageExtenision {
    public Contact contactRecord {get; set;}
    public User commUser {get; set;}
    public List<SelectOption> profileList {get; set;}
    public String userLicense {get; set;}
    public Boolean isSave {get; set;}
    public String profileId {get; set;}
    public String profileName {get; set;}
    public boolean isCMPUser{
    	get{
    		if(!String.isEmpty(profileName)){
    			return profileName.containsIgnoreCase(Label.Community_Profile);
    		}
    		return false;
    	}
    }

    public CMPUserCreationPageExtenision(ApexPages.StandardController stdController){
        String contactId = ApexPages.currentPage().getParameters().get('contactId');
        String userId = ApexPages.currentPage().getParameters().get('Id');
        profileList = new List<SelectOption>();
        isSave = false;
        if(String.isEmpty(userId)){
            commUser = new User();
            if(!String.isEmpty(contactId)){
                // Updated : 07/02/2015 - T-412820 - Megha Agarwal - pre-populate value from contact
                contactRecord = [SELECT FirstName, LastName, Name, Email from Contact where Id =: contactId];
                commUser = new User(FirstName = contactRecord.FirstName, LastName = contactRecord.LastName, ContactId = contactRecord.Id, Email = contactRecord.Email , username = contactRecord.Email, isActive =  true);
                if(commUser.Email != null){
                  commUser.CommunityNickname = commUser.Email.contains('@') ? commUser.Email.subString(0, commUser.Email.indexOf('@')) : commUser.Email;
                }
                if(commUser.FirstName != null){
                    commUser.alias = commUser.FirstName.subString(0,1);
                }else{
                    commUser.alias = '';
                }
                if(commUser.lastName != null){
                    commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
                }
                commUser.alias = commUser.alias.tolowerCase();


            }else{
                commUser = new User();
            }
            for(CMPUserFieldDefaultValue__c defaultValue : CMPUserFieldDefaultValue__c.getAll().values()){
                commUser.put(defaultValue.Name, defaultValue.default_Value__c);
            }
            commUser.emailencodingkey='UTF-8';
            commUser.languagelocalekey='en_US';
            commUser.localesidkey='en_US';
            commUser.timezonesidkey='America/Indiana/Indianapolis';
        }else{
            commUser = [SELECT Id, FirstName, LastName,Email, Username, Alias, IsActive, ProfileId, ContactId,
                            Phone, Extension, CommunityNickname, Fax, Title, MobilePhone, CompanyName,Name,
                            EmailEncodingKey, Department, Street, LanguageLocaleKey, State, PostalCode, Country,
                            FederationIdentifier, TimeZoneSidKey, LocaleSidKey, City, CurrencyIsoCode, Profile.Name,EDGE_Payment_Approvals__c,
                            Profile.UserLicense.Name, EDGE_Submit_Invoice_Enabled__c, CMP_Document_Notification__c,DefaultCurrencyISOCode,
                            Collaboration_Center__c, CMP_Create_Transactions__c, CMP_Enabled__c, CMP_Network_Invitation_Enabled__c, User_Id__c, EDGE_Reporting__c
                            FROM User WHERE Id =:userId];
            profileName = commUser.Profile.Name;
            userLicense = commUser.Profile.UserLicense.Name;
        }
        String prof = '%'+Label.Community_Profile+'%';
        for(Profile pf : [Select Id, Name, UserLicense.Name from Profile WHERE Name LIKE :prof LIMIT 999]){
            profileList.add(new SelectOption(pf.Id, pf.Name));
        }
        if(profileList.size() > 0){
            profileId = profileList.get(0).getValue();
        }
    }


    public void custSave(){
        try{
        	commUser.ProfileId = profileId;
            if(String.isEmpty(commUser.Id)){
                insert commUser;
            }else{
                update commUser;
            }
            isSave = true;
            //return new PageReference('/apex/CMPUserDetailPage?'+commUser.Id);
        }
        catch(DMLException ex){

        	//System.assertEquals(ex.getMessage(), null);
            ApexPages.addMessages(ex);
            isSave = false;
            system.debug('---exception >>>>>------'+ex);
            //return null;
        }
    }

    public PageReference custCancel(){
        if(String.isEmpty(commUser.Id)){
            return new PageReference('/'+contactRecord.Id);
        }else{
            return new PageReference('/'+commUser.ContactId);
        }
    }

    public void setPassword(){
    	//PasswordManager.resetPassword(commUser.Id);
    	//System.resetPassword(commUser.Id, true);
    	if(!String.isEmpty(commUser.Id))
    	  System.debug('result:::: '+ (System.ResetPasswordResult) System.resetPassword(commUser.Id, true));
    }
}