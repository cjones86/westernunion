/**
 * Appirio, Inc
 * Name             : CashFlowComponentCloneTest
 * Created Date     : 26 March 2015 
 * Description      : Test class for CashFlowComponentClone
 */
@isTest
private class CashFlowComponentCloneTest {
	static User portalUser;
	  private static User commUser;
    private static User commUser1;
    private static Account beneAccount;
	
    static testMethod void myUnitTest() {
    	portalUser = test_Utility.createTestUser(true);
    	createTestData();
    	System.runAs(commUser) {
    		
		  Invoice__c invoice = test_Utility.createInvoice('GP Invoice - Active', beneAccount.Id, commUser.Id,'Pending Approval (Open)', false);
	      invoice.Due_Date__c = date.today();
	      invoice.Custom_Currency__c = 'INR';
	      insert invoice;
      
      
    		Test.startTest();
    		
        	CashFlowComponentClone ctrl = new CashFlowComponentClone();
        	CashFlowComponentClone.selMonth = 3;
        	ctrl.currentContact = commUser.ContactId;
        	ctrl.currentAccount = beneAccount.Id;
        	ctrl.cur = 'INR';
        	System.debug('ctrl.isNonCCTuser =>' + ctrl.isNonCCTuser);
        	List<String> mnthList = ctrl.getMonths();
        	system.assert(mnthList.size() > 0);
        	ctrl.getSpacer();
        	Date stDate = ctrl.firstMonthToDisplay(commUser.ContactId, commUser.Contact.AccountId);
        	system.assertEquals(stDate,date.Today().toStartOfMonth());
        	
        	ctrl.monthSearch(System.today());
        	CashFlowComponentClone.updateUser(true, commUser.Id);
        	//ctrl.addToInvoice(invoice);
        	
        	ctrl = new CashFlowComponentClone();
        	ctrl.cur = 'Total';
        	CashFlowComponentClone.selMonth = 3;
        	System.debug('ctrl.isNonCCTuser =>' + ctrl.isNonCCTuser);
        	ctrl.getMonths();
        	ctrl.getSpacer();
        	ctrl.firstMonthToDisplay(commUser.ContactId, commUser.Contact.AccountId);
        	ctrl.monthSearch(System.today());        
        	ctrl.getUserCurrency();	
        	
        	Test.stopTest();
        	
        	System.assert(ctrl.outflowTotal.size() != 0);
        	System.assertEquals(ctrl.netHedgeTotal.size(),8);
        	System.assertEquals(ctrl.netOperatingFXCashFlow.size(),8);
        	
    	}
    	User currentUser = [Select   Id, ContactId, DefaultCurrencyIsoCode, Name, Contact.AccountId, IsPrmSuperUser, CurrencyIsoCode FROM  User WHERE  Id =: UserInfo.getUserId() LIMIT 1];
    	CashFlowComponentClone ctrl = new CashFlowComponentClone();
    	CashFlowComponentClone.selMonth = 3;
    	ctrl.cur = 'INR';
    	ctrl.getMonths();
    	ctrl.getSpacer();
    	ctrl.firstMonthToDisplay(currentUser.ContactId, currentUser.Contact.AccountId);
    	ctrl.monthSearch(System.today());
        	    	
    	ctrl = new CashFlowComponentClone();
    	ctrl.cur = 'Total';
    	CashFlowComponentClone.selMonth = 3;
    	ctrl.getMonths();
    	ctrl.getSpacer();
    	ctrl.firstMonthToDisplay(currentUser.ContactId, currentUser.Contact.AccountId);
    	ctrl.monthSearch(System.today());      
    	
    	List<Input__c> inputList = new List<Input__c>();
    	Forward_Contracts__c frwd = new Forward_Contracts__c();
    	frwd.FX_Balance__c = 28334;
    	frwd.Maturity_Date__c = system.today();
    	frwd.Account_ID__c = beneAccount.Id;
    	insert frwd;
    	
    	inputList.add(test_Utility.createInput(false));    	
    	ctrl.addToTradingReceipts(inputList.get(0));
    	ctrl.addToInflowOther(inputList.get(0));
    	ctrl.addToPurchaseOrder(inputList.get(0));
    	ctrl.addToForecastOrder(inputList.get(0));
    	ctrl.addToOutflowOther(inputList.get(0));
    	ctrl.addToForwardWUBS(frwd);
    	ctrl.addToForwardOther(inputList.get(0));
    	
    	ctrl.addToOptionsWUBSProtection(inputList.get(0));
    	ctrl.addToOptionsWUBSObligation(inputList.get(0));
    	ctrl.addToOptionsOtherProtection(inputList.get(0));
    	ctrl.addToOptionsOtherObligation(inputList.get(0));
    }
    
    static void createTestData() {
    	portalUser = [Select   Id, ContactId, DefaultCurrencyIsoCode, Name, Contact.AccountId, IsPrmSuperUser, CurrencyIsoCode FROM  User WHERE  Id =: UserInfo.getUserId() LIMIT 1];
    	List<Input__c> inputList = new List<Input__c>();
    	inputList.add(test_Utility.createInput(false));
    	inputList.add(test_Utility.createInput(false));
    	inputList.add(test_Utility.createInput(false));
    	inputList.add(test_Utility.createInput(false));
    	inputList.add(test_Utility.createInput(false));
    	inputList.add(test_Utility.createInput(false));
    	inputList.add(test_Utility.createInput(false));
    	inputList.add(test_Utility.createInput(false));
    	inputList.add(test_Utility.createInput(false));
    	inputList.add(test_Utility.createInput(false));
    	
    	inputList.get(1).Input_Type__c = 'Forecast Outgoing';
    	inputList.get(2).Input_Type__c = 'Forecast Outflow';
    	inputList.get(3).Input_Type__c = 'Option - Other';
    	inputList.get(4).Input_Type__c = 'Forecast Inflow';
    	
    	insert inputList;
    	
    	  test_Utility.createCMPAdministration();
    	  test_Utility.createWUEdgeSharingAdmin();
        CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.CCT_Client_ID__c = '3232112';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount.BillingStreet = 'testStreet';
        beneAccount.BillingCity = 'city';
        beneAccount.BillingState = 'state';
        beneAccount.BillingCountry = 'USA';
        beneAccount.BillingPostalCode = '231231';
        
        Account beneAccount1 = test_Utility.createAccount(false);
        beneAccount1.RecordTypeId=accountRecordType;
        beneAccount1.ExternalId__c='11111145';
        beneAccount.CCT_Client_ID__c = '32321122131';
        beneAccount1.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount1.CMP_Enabled__c = true;
        List<Account> listAccount = new List<Account>{beneAccount, beneAccount1};
        insert listAccount;
        
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        Contact newContact1 = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        List<Contact> listContacts = new List<Contact>{newContact,newContact1};
        insert listContacts;
        
        commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
		    commUser.timezonesidkey='America/Indiana/Indianapolis';
		    commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        
        commUser1 = test_Utility.createCommUser(newContact1.Id, false);
        commUser1.ProfileId = profileId;
        commUser1.emailencodingkey='UTF-8';
        commUser1.localesidkey='en_US';
		    commUser1.timezonesidkey='America/Indiana/Indianapolis';
		    commUser1.CMP_Enabled__c=true;
        commUser1.UserName = newContact1.Email+'.cmp1';
        
        list<User> listUsers = new List<User>{commUser,commUser1};
        insert listUsers;
        
    	
    	

    	
    	
    	test_Utility.createHoldingBalance(beneAccount.Id, true);
    	test_Utility.createForwardContracts(beneAccount.Id, true);
    }
}