public without sharing class Edge_SearchSupplierModal {
    
    public String deliveryType{get;set;}
    public transient list<SupplierWrapper> listSuppliers{get;set;}
    
    public Edge_SearchSupplierModal(){
        // System.assert(false,' >>> ' + deliveryType);
         deliveryType = ApexPages.currentPage().getParameters().get('type');
         fetchSupplierRecords();
    }
    
    public void fetchSupplierRecords(){
        listSuppliers = new list<SupplierWrapper>();
        for(Supplier__c sup : [Select Id, Name,Address_line_1__c,Address_Line_2__c,Buyer__c,City__c,Country__c,Post_Code__c,State_Province__c,
                                    Supplier__c,Network_Status__c,Supplier_Name__c,Supplier__r.CMP_H2H_Transaction_Enabled__c,Supplier__r.CCT_Client_ID__c 
                                From Supplier__c Where Buyer__c = :Utility.CurrentAccount Limit 1000]){
                            listSuppliers.add(new SupplierWrapper(sup));
        }
    }
    
    public class SupplierWrapper {
        public Supplier__c supp{get;set;}
        public String address{get;set;}
        public String name{get;set;}
        public Boolean canPayH2H{get;set;}
        public boolean isIntegrationEnabled = Utility_Security.isIntegrationEnabled;
        public String cctId{get;set;}
        public String netStatus{get;set;}
        public String encodedSuppId{get;set;}
        public String id{get;set;}
        public SupplierWrapper(Supplier__c sup){        	
        	this.supp = sup;
            if(isIntegrationEnabled && sup.Supplier__c != null && sup.Network_Status__c != null && sup.Network_Status__c.containsIgnoreCase('Accepted') && sup.Supplier__r.CMP_H2H_Transaction_Enabled__c)
                this.canPayH2H = true;
            else 
                this.canPayH2H = false;
            this.name = sup.Supplier_Name__c; 
            this.netStatus = 'inActive';
            if(sup.Network_Status__c != null && sup.Network_Status__c.containsIgnoreCase('Accepted'))
            	this.netStatus = 'Active';
            this.address = getFullAddress(sup.Address_line_1__c,sup.Address_Line_2__c,sup.City__c,sup.State_Province__c,sup.Country__c,sup.Post_Code__c);
            this.cctId = sup.Supplier__r.CCT_Client_ID__c;
            encodedSuppId = EncryptionManager.doEncrypt(sup.id);
            if(encodedSuppId.contains('+')){
              this.id = encodedSuppId.replace('+','%2B');
            }else{
              this.id = encodedSuppId;
            }
        }
        
        public string getFullAddress(String addline1,String addline2,String city,String state,String country, String postalCode){
            String address = ''; 
            if(String.isNotBlank(addline1))
             address = addline1 + ', ';
            if(String.isNotBlank(addline2))
             address += addline2 + ', ';
            if(String.isNotBlank(city))
             address += city + ', ';
            if(String.isNotBlank(state))
             address += state + ', ';
            if(String.isNotBlank(country))
             address += country + ', ';
            if(String.isNotBlank(postalCode))
             address += postalCode;
            return address;
        }
    }
}