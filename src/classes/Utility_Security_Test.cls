@isTest
private class Utility_Security_Test {

	private static testMethod void test() {

        populateBoolean(); 
        
        test_Utility.createCMPAdministration();
        test_Utility.createCMPAlert();
        test_Utility.createWubsIntAdministration();
        test_Utility.createGPH2HCurrencies();
        test_Utility.createGPIntegrationAdministration();
        test_Utility.currencyISOMapping();
    
        CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.CCT_Client_ID__c = '3232112';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount.CMP_Create_Transactions__c = true;
        beneAccount.CMP_Enabled__c = true;
        beneAccount.CMP_Holding_Enabled__c = true;
        beneAccount.CMP_H2H_Transaction_Enabled__c = true;
        beneAccount.BillingStreet = 'testStreet';
        beneAccount.BillingCity = 'city';
        beneAccount.BillingState = 'state';
        beneAccount.BillingCountry = 'USA';
        beneAccount.BillingPostalCode = '231231';
        beneAccount.Billing_Address_ISO_Country_Code__c = 'USA';
        insert beneAccount;
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact;
        
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
        commUser.timezonesidkey='America/Indiana/Indianapolis';
        commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        commUser.CMP_Network_Invitation_Enabled__c = true;
        commUser.CMP_Create_Transactions__c = true;
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        insert commUser;    
        
        System.RunAs(commUser){
            populateBoolean();
        }
        

	}
	
	public static void populateBoolean(){
	    
    	    Boolean canViewAndCreateCashFlowData = Utility_Security.canViewAndCreateCashFlowData;
            Boolean canCreatePaymentsByWireDraftOrACH = Utility_Security.canCreatePaymentsByWireDraftOrACH;
            Boolean canSettlePaymentFromHolding = Utility_Security.canSettlePaymentFromHolding;
            Boolean canReceivePaymentsIntoHolding_H2H = Utility_Security.canReceivePaymentsIntoHolding_H2H;
            Boolean canManageHoldingBalances = Utility_Security.canManageHoldingBalances;
            Boolean canShowHoldingBalance = Utility_Security.canShowHoldingBalance;
            Boolean canManageBeneficiaryInCMP = Utility_Security.canManageBeneficiaryInCMP;
    
            Boolean canManageBeneficiaryInGP = Utility_Security.canManageBeneficiaryInGP;
            Boolean canCreateInvoice = Utility_Security.canCreateInvoice;
            Boolean canManageBeneficiary = Utility_Security.canManageBeneficiary;
            Boolean canPayInvoices_SP = Utility_Security.canPayInvoices_SP;
            Boolean canPayInvoices_H2H = Utility_Security.canPayInvoices_H2H;
    
            Boolean canMakeStandardColdPayments = Utility_Security.canMakeStandardColdPayments;
            Boolean canMakeHBColdPayments = Utility_Security.canMakeHBColdPayments;
            Boolean canCreateNetworkTransaction = Utility_Security.canCreateNetworkTransaction;
            Boolean canCreateH2HTransactions = Utility_Security.canCreateH2HTransactions;
           Boolean canSubmitInvoices = Utility_Security.canSubmitInvoices;
            // Boolean isNetworkEnabled = Utility_Security.isNetworkEnabled;
            WUBS_Integration_Administration__c wubsIntegrationAdmin = Utility_Security.wubsIntegrationAdmin;
           
            
	}

}