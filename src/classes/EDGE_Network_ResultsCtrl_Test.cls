/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_Network_ResultsCtrl_Test
 * Description: Controller Test class for EDGE_Network_ResultsCtrl
 * Created Date: 01 Apr 2016
 * Created By: Nikhil Sharma (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/

@isTest
private class EDGE_Network_ResultsCtrl_Test {

	private static testMethod void test1() {
        test_Utility.createCMPAdministration();
		//test_Utility.createCMPAlert();
		//test_Utility.createWubsIntAdministration();
		//test_Utility.createGPH2HCurrencies();
		//test_Utility.createGPIntegrationAdministration();
		CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;

        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.CCT_Client_ID__c = '3232112';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount.BillingStreet = 'testStreet';
        beneAccount.BillingCity = 'city';
        beneAccount.BillingState = 'state';
        beneAccount.BillingCountry = 'USA';
        beneAccount.BillingPostalCode = '231231';

        Account beneAccount1 = test_Utility.createAccount(false);
        beneAccount1.RecordTypeId=accountRecordType;
        beneAccount1.ExternalId__c='11111145';
        beneAccount.CCT_Client_ID__c = '32321122131';
        beneAccount1.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount1.CMP_Enabled__c = true;

        List<Account> listAccount = new List<Account>{beneAccount, beneAccount1};
        insert listAccount;

        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        Contact newContact1 = test_Utility.createContact(contactRecordType,false,beneAccount1.Id);

        List<Contact> listContacts = new List<Contact>{newContact,newContact1};
        insert listContacts;

        User commUser1 = test_Utility.createCommUser(newContact1.Id, false);
        commUser1.ProfileId = profileId;
        commUser1.emailencodingkey='UTF-8';
        commUser1.localesidkey='en_US';
		commUser1.timezonesidkey='America/Indiana/Indianapolis';
		commUser1.CMP_Enabled__c=true;
		commUser1.Email = 'testuser@test1233.com';
        commUser1.UserName = newContact1.Email+'.cmp1';

        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
		commUser.timezonesidkey='America/Indiana/Indianapolis';
		commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }

        list<User> listUsers = new List<User>{commUser,commUser1};
        insert listUsers;

        Invites__c inv1 = test_Utility.createInvite(newContact.Id,commUser.Id,'test231@testdf.com',false);
        Network__c net1 = test_Utility.createNetwork(newContact1.Id,commUser.Id,beneAccount1.Id,beneAccount.Id,false);
        Network__c net2 = test_Utility.createNetwork(newContact1.Id,commUser.Id,beneAccount1.Id,beneAccount.Id,false);net2.Status__c = '4 - Deactivated';
        Network__c net3 = test_Utility.createNetwork(newContact1.Id,commUser.Id,beneAccount1.Id,beneAccount.Id,false);net3.Status__c = '3 - Active (Accepted)';
        // Network__c net4 = test_Utility.createNetwork(newContact1.Id,commUser.Id,beneAccount1.Id,beneAccount.Id,false);net4.Status__c = '4 - Deactivated';
        // Network__c net5 = test_Utility.createNetwork(newContact1.Id,commUser.Id,beneAccount1.Id,beneAccount.Id,false);net5.Status__c = '4 - Deactivated';


        Invites__c inv2 = test_Utility.createInvite(newContact1.Id,commUser1.Id,'dffdsl@testdfds.com',false);
        Network__c net4 = test_Utility.createNetwork(newContact.Id,commUser1.Id,beneAccount.Id,beneAccount1.Id,false);

        List<Invites__c> listInvites = new List<Invites__c>{inv1,inv2};
        insert listInvites;

        List<Network__c> listNetworks = new List<Network__c>{net1,net2,net3,net4};
        insert listNetworks;

        Test.startTest();
        System.RunAs(commUser){
                map<String, String> networkStatusTra = EDGE_Network_ResultsCtrl.networkStatusTranslation;
                map<String, String> invitesStatusTranslation = EDGE_Network_ResultsCtrl.invitesStatusTranslation;
        		EDGE_Network_ResultsCtrl ctrl = new EDGE_Network_ResultsCtrl();
        		ctrl.netStatus = '3 - Active (Accepted)';
        		ctrl.netConId = net1.Id;
        		ctrl.saveInvItr();
        		ctrl.beneEmail = 'testuser@test1233.com';
        		PageReference pref = ctrl.searchUser();
        		ctrl.beneEmail = 'testuser123@test1233.com';
            List<String> beneEmails = new List<String>{'testuser123@test1233.com'};
        		ctrl.inviteNewUserTOCMP(beneEmails);
        		// To check duplicate value for invitation search
        		ctrl.inviteNewUserTOCMP(beneEmails);
        		ctrl.resendInvitation();
        		ctrl.inviteUserTONetwork();
                //String invStatusTrans = ctrl.listInvites.get(0).invStatusTrans;
        }

        /*System.RunAs(commUser1){
        		EDGE_Network_ResultsCtrl ctrl = new EDGE_Network_ResultsCtrl();
        		ctrl.netStatus = '3 - Active (Accepted)';
        		ctrl.netConId = net1.Id;
        		ctrl.saveInvItr();
        		ctrl.beneEmail = 'testuser@test1233.com';
        		PageReference pref = ctrl.searchUser();
        		ctrl.beneEmail = 'testuser123@test1233.com';
        		ctrl.inviteNewUserTOCMP();
        		// To check duplicate value for invitation search
        		ctrl.inviteNewUserTOCMP();
        		ctrl.resendInvitation();
        		ctrl.inviteUserTONetwork();
        }*/
        Test.stopTest();
	}

	/*private static testMethod void test2() {
		test_Utility.createCMPAdministration();
	    test_Utility.createCMPAlert();
	    test_Utility.createWubsIntAdministration();
	    test_Utility.createGPH2HCurrencies();
	    test_Utility.createGPIntegrationAdministration();
	    CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;

        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.CCT_Client_ID__c = '3232112';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount.BillingStreet = 'testStreet';
        beneAccount.BillingCity = 'city';
        beneAccount.BillingState = 'state';
        beneAccount.BillingCountry = 'USA';
        beneAccount.BillingPostalCode = '231231';
        beneAccount.CMP_Create_Transactions__c = true;
        beneAccount.CMP_Holding_Enabled__c = true;
        beneAccount.CMP_H2H_Transaction_Enabled__c = true;

        Account beneAccount1 = test_Utility.createAccount(false);
        beneAccount1.RecordTypeId=accountRecordType;
        beneAccount1.ExternalId__c='11111145';
        beneAccount.CCT_Client_ID__c = '32321122131';
        beneAccount1.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount1.BillingStreet = 'testStreet';
        beneAccount1.BillingCity = 'city';
        beneAccount1.BillingState = 'state';
        beneAccount1.BillingCountry = 'USA';
        beneAccount1.BillingPostalCode = '231231';
        beneAccount1.CMP_Enabled__c = true;
        beneAccount1.CMP_Create_Transactions__c = true;
        beneAccount1.CMP_Holding_Enabled__c = true;
        beneAccount1.CMP_H2H_Transaction_Enabled__c = true;


        List<Account> listAccount = new List<Account>{beneAccount, beneAccount1};
        insert listAccount;

        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        Contact newContact1 = test_Utility.createContact(contactRecordType,false,beneAccount1.Id);

        List<Contact> listContacts = new List<Contact>{newContact,newContact1};
        insert listContacts;

        User commUser1 = test_Utility.createCommUser(newContact1.Id, false);
        commUser1.ProfileId = profileId;
        commUser1.emailencodingkey='UTF-8';
        commUser1.localesidkey='en_US';
	    commUser1.timezonesidkey='America/Indiana/Indianapolis';
	    commUser1.CMP_Enabled__c=true;
        commUser1.UserName = newContact1.Email+'.cmp1';
        list<User> listUsers = new List<User>{commUser1};
        insert listUsers;

        Invites__c inv2 = test_Utility.createInvite(newContact1.Id,commUser1.Id,'dffdsl@testdfds.com',false);
        inv2.Inviter_User__c = commUser1.Id;
        Network__c net4 = test_Utility.createNetwork(newContact.Id,commUser1.Id,beneAccount.Id,beneAccount1.Id,false);

        List<Invites__c> listInvites = new List<Invites__c>{inv2};
        insert listInvites;

        List<Network__c> listNetworks = new List<Network__c>{net4};
        insert listNetworks;

        Test.startTest();
		System.RunAs(commUser1){

            EDGE_Network_ResultsCtrl ctrl = new EDGE_Network_ResultsCtrl();
            ctrl.netStatus = '3 - Active (Accepted)';
            ctrl.netConId = net4.Id;
            ctrl.saveInvItr();
            ctrl.beneEmail = 'testuser@test1233.com';
            PageReference pref = ctrl.searchUser();
            ctrl.beneEmail = 'testuser123@test1233.com';
            List<String> beneEmails = new List<String>{'testuser123@test1233.com'};
            ctrl.inviteNewUserTOCMP(beneEmails);
            // To check duplicate value for invitation search
            ctrl.inviteNewUserTOCMP(beneEmails);
            ctrl.resendInvitation();
            ctrl.inviteUserTONetwork();
		}

        Test.stopTest();
	}*/


}