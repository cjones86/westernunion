public class GPAppInModalController {

    public Boolean displayPopup {get; set;}
    public String iFrameUrl {get;set;}
    public String baseGPUrl{get;set;}   
    
    public GPAppInModalController() {
    	baseGPUrl =	Global_Pay_Integration_Administration__c.getInstance().GP_API_baseURL__c;
    }
    
    public void closePopup() {        
        displayPopup = false;    
    }  
       
    public void showLogin() {
        displayPopup = true;
        iFrameUrl = '/apex/GPAppTest?app=gp';
    }
    
    public void showPay() {        
        displayPopup = true;
        iFrameUrl = '/apex/GPAppTest?app=payBeneficiary';
    }

}