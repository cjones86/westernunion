/**=====================================================================
 * Appirio, Inc
 * Name: InviteUpdateController_Test
 * Description: Controller Test class for InviteUpdateController
 * Created Date: 04 Apr 2016
 * Created By: Nikhil Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
@isTest
private class InviteUpdateController_Test {

    private static testMethod void test() {
        test_Utility.createCMPAdministration();
        test_Utility.createCMPAlert();
        test_Utility.createWubsIntAdministration();
        test_Utility.createGPH2HCurrencies();
        test_Utility.createGPIntegrationAdministration();
        test_Utility.createCMPLanguageSettings();
        test_Utility.createWUEdgeSharingAdmin();
        CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.CCT_Client_ID__c = '3232112';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount.BillingStreet = 'testStreet';
        beneAccount.BillingCity = 'city';
        beneAccount.BillingState = 'state';
        beneAccount.BillingCountry = 'USA';
        beneAccount.BillingPostalCode = '231231';
        
        Account beneAccount1 = test_Utility.createAccount(false);
        beneAccount1.RecordTypeId=accountRecordType;
        beneAccount1.ExternalId__c='11111145';
        beneAccount.CCT_Client_ID__c = '32321122131';
        beneAccount1.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount1.CMP_Enabled__c = true;
        
        List<Account> listAccount = new List<Account>{beneAccount, beneAccount1};
        insert listAccount;
        
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        Contact newContact1 = test_Utility.createContact(contactRecordType,false,beneAccount1.Id);
        
        List<Contact> listContacts = new List<Contact>{newContact,newContact1};
        insert listContacts;
        
        User commUser1 = test_Utility.createCommUser(newContact1.Id, false);
        commUser1.ProfileId = profileId;
        commUser1.emailencodingkey='UTF-8';
        commUser1.localesidkey='en_US';
        commUser1.timezonesidkey='America/Indiana/Indianapolis';
        commUser1.CMP_Enabled__c=true;
        commUser1.UserName = newContact1.Email+'.cmp1';
        
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
        commUser.timezonesidkey='America/Indiana/Indianapolis';
        commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        
        list<User> listUsers = new List<User>{commUser,commUser1};
        insert listUsers;
        
        Invoice__c invoice1 = test_Utility.createInvoice('GP Invoice - Active', beneAccount.Id, commUser.Id,'Pending Approval (Open)', true);
        
        Invites__c inv1 = test_Utility.createInvite(newContact.Id,commUser.Id,'dfjlskd@sdkfjkdsl.com',false);
        inv1.Invoice__c= invoice1.Id;
        Network__c net1 = test_Utility.createNetwork(newContact1.Id,commUser.Id,beneAccount1.Id,beneAccount.Id,false);
        Network__c net2 = test_Utility.createNetwork(newContact1.Id,commUser.Id,beneAccount1.Id,beneAccount.Id,false);net2.Status__c = '4 - Deactivated';
        Network__c net3 = test_Utility.createNetwork(newContact1.Id,commUser.Id,beneAccount1.Id,beneAccount.Id,false);net3.Status__c = '3 - Active (Accepted)';
        
        
        Invites__c inv2 = test_Utility.createInvite(newContact1.Id,commUser1.Id,'dffdsl@testdfds.com',false);
        Network__c net4 = test_Utility.createNetwork(newContact.Id,commUser1.Id,beneAccount.Id,beneAccount1.Id,false);
        
        List<Invites__c> listInvites = new List<Invites__c>{inv1,inv2};
        insert listInvites;
        
        List<Network__c> listNetworks = new List<Network__c>{net1,net2,net3,net4};
        insert listNetworks;
        
        Test.startTest();
        System.RunAs(commUser){
                InviteUpdateController ctrl = new InviteUpdateController();
                 PageReference pageRef = Page.Edge_Registration;
                Test.setCurrentPage(pageRef);
                ApexPages.currentPage().getParameters().put('ID',inv1.Id);
                ApexPages.currentPage().getParameters().put('Token',GPIntegrationUtility.generateInvitesTOKEN('Invitation_Token__c'));
                ApexPages.currentPage().getParameters().put('Name',inv1.Name);
                InviteUpdateController ctrl1 = new InviteUpdateController();
                ctrl1.invite = inv1;
                ctrl1.listInvites = listInvites;
                List<SelectOption> langSettings = ctrl1.getLanguageSettings();
                ctrl1.newContact.FirstName = 'test';
                ctrl1.newContact.LastName = 'test';
                ctrl1.newContact.MailingStreet = 'testStreet';
                ctrl1.newContact.MailingCity = 'city';
                ctrl1.newContact.MailingState = 'state';
                ctrl1.newContact.MailingCountry = 'USA';
                ctrl1.newContact.MailingPostalCode = '231231';
                ctrl1.newContact.Email = 'dfjlskd@sdkfjkdsl.com';
                ctrl1.companyName = 'testCompany321312';
                ctrl1.isWubsCustomer = true;
                ctrl1.acceptUser();
                ctrl1.changeLanguage();
                ctrl1.cancel();
                
                                
                ctrl1.newContact.Email = 'test@test.com';
                ctrl1.acceptUser();
                ApexPages.Message[] pageMessages = ApexPages.getMessages();
                System.assert(pageMessages.size() > 0 );
                
                System.debug(ctrl1.getCaptchaLanguage);
                System.debug(ctrl1.SiteKey);

                

        }
        Test.stopTest();
    }
}