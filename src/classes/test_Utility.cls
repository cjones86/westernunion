/**=====================================================================
 * Name: test_Utility 
 * Description: utility class for for test data creation
 * Created Date: Mar 13, 2015
 * Created By: Aashita Sharma 
 =====================================================================*/

public with sharing class test_Utility {
            
   // method to create invoice record
    public static Invoice__c createInvoice(boolean isInsert){       
        Invoice__c invoice = new Invoice__c();
        invoice.CurrencyIsoCode = 'USD';
        if(isInsert)
            insert invoice;
        return invoice;
    }
    
    //method to create market profile record
    public static Marketplace_profile__c createMarketProfile(boolean isInsert, Id accId){
        Marketplace_profile__c profile = new Marketplace_profile__c();
        profile.Account__c = accId;
        profile.Name = 'test Market Profile';
        if(isInsert)
            insert profile;
        return profile;
    }
    
    public static Invoice__c createInvoice(String rtName, String accId, String commUserId, String status, boolean isInsert){
        Invoice__c invoice = new Invoice__c();
        invoice.RecordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get(rtName).getRecordTypeId();
        invoice.Custom_Currency__c = 'USD';
        invoice.Status__c = status;
        invoice.Initiated_By__c='Seller';
        invoice.Due_Date__c = Date.today().addDays(30);
        invoice.Amount__c = 2000;
        invoice.Account__c = accId;
        invoice.Buyer__c = commUserId;
        if(isInsert)
            insert invoice;
        return invoice;
    }
    
    
    public static Input__c createInputWithType(boolean isInsert, String inputType, String accId){
      Input__c input = new Input__c(Input_Type__c = inputType);
      input.Amount__c = 11;
      input.Parent_Account__c = accId;
      input.CurrencyIsoCode = 'INR';
      input.Custom_Currency__c = 'INR';
      input.Transaction_Date__c = date.today().addDays(2);
      
      if(isInsert) 
      insert input;
      return input;
    }
    
    public static Input__c createInput(boolean isInsert){
      Input__c input = new Input__c(Input_Type__c = 'Purchase Order');
      input.Amount__c = 11;
      input.CurrencyIsoCode = 'INR';
      input.Custom_Currency__c = 'INR';
      input.Transaction_Date__c = date.today().addDays(2);
      
      if(isInsert) 
      insert input;
      return input;
    }
	public static Attachment addAttachmentToParent(Id parentId, String attName, String testData) {  
	    Blob b = Blob.valueOf(testData);  
	      
	    Attachment attachment = new Attachment();  
	    attachment.ParentId = parentId;  
	    attachment.Name = attName;  
	    attachment.Body = b;  
	      
	    insert(attachment);  
	    return attachment;
	}  
    public static User createTestUser(boolean isInsert){
       Campaign camp = new Campaign(Name= 'Bronze Trigger Emails');
        insert camp; 
      
      Account acc = new Account();
      acc.Name = 'testname';
      insert acc;
      
      Contact contact = new Contact();
      contact.AccountId =  acc.Id;
      contact.LastName = 'testlastname';
      insert contact;
      
      User testUser = new User();
      //testUser = new User();
      testUser.alias = 'testuser';
      testUser.Email = 'testuser@test123.com';
      testUser.EmailEncodingKey = 'ISO-8859-1';
      testUser.LanguageLocaleKey = 'en_US';
      testUser.LastName = 'Test User567';
      testUser.LocaleSidKey = 'en_AU';
      testUser.Account_Administrator__c = false;
      testUser.ProfileId = [SELECT Id FROM Profile WHERE Name LIKE 'Partner community - FX management tool' LIMIT 1].Id;
      testUser.TimeZoneSidKey = 'Australia/Sydney';
      testUser.UserName = 'testuser@travelex.com.au';
      testUser.ContactId = contact.Id;
      testUser.DefaultCurrencyIsoCode = 'INR';
      if(isInsert)
      insert testUser;
      return testUser;
    }   
    
    
    public static Historical_Payments__c createHistoricalPayments(ID accID ,Boolean isInsert) {
    	Historical_Payments__c payment = new Historical_Payments__c(Account_ID__c = accID, Amount__c = 14, Date__c = '201411', CurrencyISOCode = 'INR');
    	if(isInsert) {
    		insert payment;
    	} 
    	return payment;
    }
    
    public static Holding_Balance__c createHoldingBalance(ID accID ,Boolean isInsert) {
    	Market_Rate__c mRate = new Market_Rate__c(Currency_Code__c = 'INR', Currency_Name__c = 'INR', Currency_Value__c = 60, Last_Updated__c = System.today());
    	insert mRate;
    	Holding_Balance__c holdingBal = new Holding_Balance__c(Account__c = accID, Amount__c = 10000, Custom_Currency__c = 'INR', External_id__c = (datetime.now()+''));
    	holdingBal.Market_Rate__c = mRate.id;
    	if(isInsert) {
    		insert holdingBal;
    	} 
    	return holdingBal;
    } 
    
    public static Account createAccount(Boolean isInsert) {
      Account acc = new Account();
      acc.Name = 'testname';
      if(isInsert) {
      	insert acc;
      } 
      return acc;
    }   
    
    public static Account createAccountWithAllDetail(Boolean isInsert , String accountRecordType, String ExternalId , String  CCT_Client ) {
        Account beneAccount = new Account();
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c=ExternalId;
        beneAccount.CCT_Client_ID__c = CCT_Client;
        beneAccount.CMP_Create_Transactions__c = true;
        beneAccount.CMP_Enabled__c = true;
        beneAccount.CMP_Holding_Enabled__c = true;
        beneAccount.CMP_H2H_Transaction_Enabled__c = true;
        beneAccount.BillingStreet = 'testStreet';
        beneAccount.BillingCity = 'city';
        beneAccount.BillingState = 'state';
        beneAccount.BillingCountry = 'USA';
        beneAccount.BillingPostalCode = '231231';
        beneAccount.Billing_Address_ISO_Country_Code__c = 'USA';
        return beneAccount;
    }
    
    public static Beneficiary__c createBeneficiary(String name, ID accID, Boolean isInsert) {
    	Bene_Enrollment_Campaign__c beneEnrollment = new Bene_Enrollment_Campaign__c(Account__c = accID, Campaign_Start_Date__c = System.today(), Campaign_End_Date__c = System.today().addDays(30), Campaign_Status__c = 'Planned');
    	insert beneEnrollment;
    	Beneficiary__c beneficiary = new Beneficiary__c(Name = name, Account_Name__c = accID, Enrollment_Campaign__c = beneEnrollment.id);
    	if(isInsert) {
    		insert beneficiary;
    	}
    	return beneficiary;
    }
    
    public static Forward_Contracts__c createForwardContracts(ID accID, Boolean isInsert) {
         Forward_Contracts__c forwardContracts = new  Forward_Contracts__c();
         forwardContracts.Account_ID__c = accID;

         forwardContracts.Settlement_Amount__c = 20; 
         forwardContracts.Maturity_Date__c =  Date.today().addDays(5);
         forwardContracts.Start_Date__c = Date.today();
         forwardContracts.Transaction_Number__c = 25;
         forwardContracts.Custom_Currency__c = 'USD';
         if(isInsert) {
         	insert forwardContracts;
         } 
         return forwardContracts;    
    }          
     
     
    public static User createCommUser(Id conId, Boolean isInsert){
    	User testUser = new User();
      	testUser.alias = 'testuder';
      	testUser.Email = 'testuser@test1233.com';
      	testUser.EmailEncodingKey = 'ISO-8859-1';
      	testUser.LanguageLocaleKey = 'en_US';
      	testUser.LastName = 'Test User5627';
      	testUser.LocaleSidKey = 'fr';
      	testUser.Account_Administrator__c = false;
      	testUser.ProfileId = [SELECT Id FROM Profile WHERE Name LIKE '%CCT%' LIMIT 1].Id;
      	testUser.TimeZoneSidKey = 'Australia/Sydney';
      	testUser.UserName = 'testuser@travelex.com.auc';
      	testUser.ContactId = conId;
      	testUser.DefaultCurrencyIsoCode = 'INR';
      	if(isInsert)
      		insert testUser;
      	return testUser;
	}
	
	
	public static void createCMPLanguageSettings(){
	    CashManagementLanguageConfiguration__c cmpl1 = new CashManagementLanguageConfiguration__c(Name='Czech',
	                                                                                              Active__c=true,
	                                                                                              ISO_Code__c='cs',
	                                                                                              Label__c='čeština',
	                                                                                              WUBS_EDGE_EULA__c='/testUrl');
	   CashManagementLanguageConfiguration__c cmpl2 = new CashManagementLanguageConfiguration__c(Name='English',
	                                                                                              Active__c=true,
	                                                                                              ISO_Code__c='en_US',
	                                                                                              Label__c='English',
	                                                                                              WUBS_EDGE_EULA__c='/testUrl');
	
	    List<CashManagementLanguageConfiguration__c> langSettings = new List<CashManagementLanguageConfiguration__c>{cmpl1,cmpl2};
	    insert langSettings;
	}
	
	public static void createWUEdgeSharingAdmin(){
	    WU_Edge_Sharing_Administration__c sharingAdmin = new WU_Edge_Sharing_Administration__c(Bypass_Sharing_User_IDs__c= null,
	                                                                                       Sharing_Enabled__c = false,
	                                                                                       Sharing_Enabled_Invites__c = false,
	                                                                                       Sharing_Enabled_Invoice__c = false,
	                                                                                       Sharing_Enabled_Invoice_Record_Types__c = 'Cold Payment - Active, Cold Payment - Complete, GP Invoice - Active, GP Invoice - Complete',
	                                                                                       Sharing_Enabled_Network__c = false,
	                                                                                       Sharing_Enabled_Profiles__c = 'Cash Management Customers - CCT, Cash Management Customers - Non CCT, Cash Management Customers - CCT (CCPlus), Cash Management Customers - Non CCT (CCPlus)',
	                                                                                       Sharing_Enabled_Supplier__c = false);
            insert sharingAdmin;
	}
	
	public static void createCMPAdministration() {
		CMP_Administration__c cmpAdministration = new CMP_Administration__c(Base_Invite_URL__c='https://bsnetwork-wubs.cs25.force.com/managecash',
																			CMP_Default_Account_ParentId__c=UserInfo.getUserId(),
																			CMP_Default_Account_RecordType__c='GBPGlobal',
																			CMP_Default_Contact_RecordType__c='GBPGlobal',
																			Default_User_Self_Registration_Profile__c='Cash Management Customers - CCT',
																			CMP_GP_Default_Invoice_Record_Type__c='GP Invoice - Active',
																			CMP_Default_Invoice_Record_Type__c='Invoice - Active',
																			CMP_Allowable_File_Types__c='pdf,docx,doc');
		insert cmpAdministration;
	}
    
    
  public static void createCMPAdministrationWithInvoice(String invoiceId) {
    CMP_Administration__c cmpAdministration = new CMP_Administration__c(Base_Invite_URL__c='https://bsnetwork-wubs.cs25.force.com/managecash',
                                      CMP_Default_Account_ParentId__c=UserInfo.getUserId(),
                                      CMP_Default_Account_RecordType__c='GBPGlobal',
                                      CMP_Default_Contact_RecordType__c='GBPGlobal',
                                      Default_User_Self_Registration_Profile__c='Cash Management Customers - CCT',
                                      CMP_GP_Default_Invoice_Record_Type__c='GP Invoice - Active',
                                      CMP_Default_Invoice_Record_Type__c='Invoice - Active',
                                      CMP_Allowable_File_Types__c='pdf,docx,doc',
                                      EDGE_Default_Feed_Invoice__c=invoiceId);
    insert cmpAdministration;
  }

  public static void createGPIntegrationAdministration(){
    Global_Pay_Integration_Administration__c gpIntAdm = new Global_Pay_Integration_Administration__c(
                                        GP_API_baseURL__c='https://geoed-qa.cctdev.com/geo/sso/resource/1000000120/1000000120/',
                                        GP_API_manageBeneficiary__c='https://geoed-qa.cctdev.com/geo/sso/resource/1000000120/manageBeneficiary',
                                        GP_API_manageHoldingBalance__c='https://geoed-qa.cctdev.com/geo/sso/resource/1000000120/holdingBalances',
                                        GP_API_payBeneficiary__c='https://geoed-qa.cctdev.com/geo/sso/resource/1000000120/payBeneficiary',
                                        GP_API_payIntoHolding__c='https://geoed-qa.cctdev.com/geo/sso/resource/1000000120/payIntoHolding',
                                        GP_API_selectBeneficiary__c='https://geoed-qa.cctdev.com/geo/sso/resource/1000000120/selectBeneficiary',
                                        Integration_Enabled__c=true);
    insert gpIntAdm;
  }

  public static void createWubsIntAdministration(){
    WUBS_Integration_Administration__c wubsIntAdmin = new WUBS_Integration_Administration__c(
                                                certificate_Key__c = 'le_cdf71f86_9760_4bf7_94c9_6f6868c5c2e1',
                                                Integration_Enabled__c = true,
                                                WUBS_API_baseURL__c = 'https://api.business.westernunion.com/WUBS.API/',
                                                WUBS_API_GetHoldingBalance__c = 'https://api.business.westernunion.com/WUBS.API/holdings/',
                                                WUBS_API_GetHoldingCurrencies__c = 'https://api.business.westernunion.com/WUBS.API/currencies/holding/'
                                                );
    insert wubsIntAdmin;
  }
	
  public static void createGPH2HCurrencies(){
    
    Global_Pay_H2H_Currencies__c gpCurr1 = new Global_Pay_H2H_Currencies__c(
                                                    Name='AED',
                                                    Currency_Name__c='Utd. Arab Emir. Dirh',
                                                    Flag_Icon__c='AED.ico',
                                                    Holding_Currency_Color__c='#828282',
                                                    isActive__c=false);
    Global_Pay_H2H_Currencies__c gpCurr2 = new Global_Pay_H2H_Currencies__c(
                                                    Name='AUD',
                                                    Currency_Name__c='Australian Dollar',
                                                    Flag_Icon__c='AUD.ico',
                                                    Holding_Currency_Color__c='#828282',
                                                    isActive__c=true);
    Global_Pay_H2H_Currencies__c gpCurr3 = new Global_Pay_H2H_Currencies__c(
                                                    Name='BHD',
                                                    Currency_Name__c='Bahraini Dinar',
                                                    Flag_Icon__c='BHD.ico',
                                                    Holding_Currency_Color__c='#828282',
                                                    isActive__c=true);
    Global_Pay_H2H_Currencies__c gpCurr4 = new Global_Pay_H2H_Currencies__c(
                                                    Name='CAD',
                                                    Currency_Name__c='Canadian Dollar',
                                                    Flag_Icon__c='CAD.ico',
                                                    Holding_Currency_Color__c='#828282',
                                                    isActive__c=true);
    Global_Pay_H2H_Currencies__c gpCurr5 = new Global_Pay_H2H_Currencies__c(
                                                    Name='CHF',
                                                    Currency_Name__c='Swiss Franc',
                                                    Flag_Icon__c='CHF.ico',
                                                    Holding_Currency_Color__c='#828282',
                                                    isActive__c=true);
    List<Global_Pay_H2H_Currencies__c> listGPCurrencies = new List<Global_Pay_H2H_Currencies__c>{gpCurr1,gpCurr2,gpCurr3,gpCurr4,gpCurr5};
    insert listGPCurrencies;
  }

	public static void createCMPAlert() {
		CMP_Alert_Management__c cmpAlert = new CMP_Alert_Management__c(CMP_Invites_Open_Status_Values__c='Sent, In Progress',
																			CMP_Invites_Overdue_Invite_Age_Days__c=14,
																			Invoices_Raised_Invoice_Record_Types__c='GP Invoice - Active',
																			Invoices_Raised_Invoice_Statuses__c='Pending Approval (Open)',
																			Network_Invites_Completed_Age_Days__c=7,
																			Network_Invites_Complete_Status_Values__c='Accepted, Expired',
																			Network_Invites_Open_Status_Values__c='Sent, In Progress,Pending',
																			Invoices_Pending_Approval_Statuses__c='Pending Approval',
                                                                            Invoices_Rejected_Invoice_Statuses__c='Rejected');
		insert cmpAlert;
	}
	
	public static Contact createContact(String recordtypeId,boolean isInsert,String accId) {
		Contact testContact = new Contact(RecordTypeId = recordtypeId,FirstName='test',
										  LastName='test',Email='test@test.com',AccountId=accId);
		if(isInsert)
			insert testContact;
		return testContact;
	}

  public static Invites__c createInvite(String contactId, String inviterUserId, String beneEmail, boolean isInsert){
    Invites__c invite = new Invites__c(Invitation_Sent_Count__c=1,
                                           Benne_Email__c=beneEmail,
                                           Inviter_User__c=UserInfo.getUserId(),
                                           Invitee_Contact__c=contactId,
                                           Status__c = 'Sent',
                                           Invitation_Token__c=GPIntegrationUtility.generateInvitesTOKEN('Invitation_Token__c'),
                                           Status_DateTime__c = system.now());
    if(isInsert)
      insert invite;
    return invite;
  }

  public static Network__c createNetwork(String contactId, String inviterUserId, String inviterAccountId, String inviteeAccountId, boolean isInsert){
     Network__c newNetwork = new Network__c(Inviter_User__c=UserInfo.getUserId(),
                                               Invitee_Contact__c=contactId,
                                               Account_Invitee__c = inviterAccountId,
                                               Account_Inviter__c = inviteeAccountId,
                                               Status__c='1 - Pending (Sent)',
                                               Status_DateTime__c = system.now());
     if(isInsert)
      insert newNetwork;
    return newNetwork;
  }
  
  public static Supplier__c createSupplier(String supplierId,String buyerId, boolean isInsert){
  	Supplier__c sup = new Supplier__c(Supplier__c = supplierId,Buyer__c = buyerId);
  	if(isInsert)
      insert sup;
    return sup;
  }
	
	public static void currencyISOMapping() {
		List<CMP_Currency_ISO_Mapping__c> cISOs = new List<CMP_Currency_ISO_Mapping__c>();
		cISOs.add(new CMP_Currency_ISO_Mapping__c(Name='United States dollar',Currency_ISO_Code__c='USA'));
		cISOs.add(new CMP_Currency_ISO_Mapping__c(Name='United Arab Emirates dirham',Currency_ISO_Code__c='AED'));
		insert cISOs;
	}
	public static void globalPayOpenInvoiceStatuses() {
		List<Global_Pay_Open_Invoice_Statuses__c> cISOs = new List<Global_Pay_Open_Invoice_Statuses__c>();
		cISOs.add(new Global_Pay_Open_Invoice_Statuses__c(Name='Accepted',isOpen__c=true));
		cISOs.add(new Global_Pay_Open_Invoice_Statuses__c(Name='Full Payment Submitted',isOpen__c=false));
		insert cISOs;
	}
	
}