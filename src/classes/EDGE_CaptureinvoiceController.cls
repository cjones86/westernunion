/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_CaptureinvoiceController
 * Description: Controller to capture Invoice from CMP user
 * Created Date: 02 Feb' 2016
 * Created By: Rohit Sharma (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
   08-Mar-2016                  Ranjeet Singh               Upgraded for Edge-UI
 =====================================================================*/
public without sharing class EDGE_CaptureinvoiceController {

    static apexLogHandler.apexLog logSaveInvoice = new apexLogHandler.apexLog('CaptureinvoiceCtrl','saveInvoice');
    static apexLogHandler.apexLog logUploadAttachmentFeed = new apexLogHandler.apexLog('CaptureinvoiceCtrl','uploadAttchmntFeed');

    public Invoice__c invoice{get;set;}
    public String fileName{get;set;}
    public String fileId{get;set;}
    transient public Blob fileData{get;set;}
    //public List<FeedItem> posts{get;set;}
    public Boolean isDisplayBenePopUp{get;set;}
    public boolean canCreateTransaction{get;set;}
    public Id recordTypeId{get;set;}
    public Id accId {get;set;}
    public integer deleteIndex{get;set;}
    public List<SelectOption> currencyOptions{get;set;}
    public String allowedFileType{get;set;}
    public InsightsAndInputTabCtrl InsightsAndInputCtl{get;set;}
    public boolean isUserH2H{get;set;}
    public PageReference returnurl{get;set;}
    public boolean isInvSaved{get;set;}
    public boolean hasError{get;set;}
    public boolean isSaveClicked{get;set;}
    public String latestContentId{get;set;}
    public String recordTypeName{get;set;}
    public CMP_Administration__c cmpAdmin{get;set;}
    public boolean beneInNetwork{get;set;}
    public string errorMessage{get;set;}
    public string parentInvoiceId{get;set;}
    public String invoiceOwnerId{get;set;}
    public String beneName{get;set;}
    public String beneIdH2H{get;set;}
    public String hbCurrencyISO{get;set;}
    public List<SelectOption> H2Hcurrencies {get;set;}
    public String OldInvoiceId{get;set;}
    public String beneNumber{get;set;}
    public boolean isSaveAndPayClicked{get;set;}
    public boolean isSaveAndNewClicked{get;set;}
    public boolean isAddNewClicked{get;set;}
    public String deliveryType{get;set;}
    public boolean isSaveAndAddBeneClicked{get;set;}
    public list<SupplierWrapper> listSuppliers{get;set;}
    public Map<String,String> mapSuppIdAndAddress{get;set;}
    public string selectedSuppId{get;set;}
    public string selectedDelType{get;set;}
    public Map<String,boolean> mapSuppIdAndIsH2HEnabled{get;set;}
    public string selectedSuppKeyValue{get;set;}
    public String isSuppInviteToNetwork{get;set;}
    public Set<String> h2hCurrencySet{get;set;}
    public Set<String> stdCurrencySet{get;set;}

    public List<SelectOption> getdelTypes(){
        List<SelectOption> optns = new List<Selectoption>();
        Schema.DescribeFieldResult fieldResult = invoice__c.Delivery_Method__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
                    if(f.getLabel() !=  'N/A')
            optns.add(new SelectOption(f.getValue(), f.getLabel()));
        }
         return optns;
    }

    public List<SelectOption> getopenPresentationOptions(){
        List<SelectOption> optns = new List<Selectoption>();
        mapSuppIdAndAddress = new Map<String,String>();
        mapSuppIdAndIsH2HEnabled = new Map<String,boolean>();
        fetchSupplierRecords();
        // before getting here you must populate your queryResult list with required fields
        for(SupplierWrapper obj : listSuppliers){
           if(obj.name != null && String.isNotBlank(obj.address)){
              String opt = obj.name;//  + ', '+ obj.address;
              String optId = obj.Id + '@'+ obj.canPayH2H + '@' + obj.isNotNetworked;
              if(obj.Id == invoice.Supplier__c)
                 selectedSuppKeyValue = optId;
              optns.add(new selectOption(optId , opt));
              mapSuppIdAndAddress.put(optId,obj.address);
              mapSuppIdAndIsH2HEnabled.put(optId,obj.canPayH2H);
           }
        }
        return optns;
    }


    // method to fetch supplier records based on searched text and current user's account
    public void fetchSupplierRecords(){
        listSuppliers = new list<SupplierWrapper>();
        for(Supplier__c sup : [SELECT Id, Name,Address_line_1__c,Address_Line_2__c,Buyer__c,City__c,Country__c,Post_Code__c,State_Province__c,
                                      Supplier__c,Network_Status__c,Supplier_Name__c,Supplier__r.CMP_H2H_Transaction_Enabled__c,
                                      CreatedDate,Created_Invoice_Date__c,Invite_to_my_Network__c
                                FROM Supplier__c WHERE Buyer__c = :Utility.CurrentAccount and Beneficiary_Status__c = 'Active']){
                 listSuppliers.add(new SupplierWrapper(sup));
        }
        listSuppliers.sort();
    }

    public class SupplierWrapper implements Comparable{
        public Supplier__c supp{get;set;}
        public String id{get;set;}
        public String address{get;set;}
        public String name{get;set;}
        public Boolean canPayH2H{get;set;}
        public boolean isIntegrationEnabled = Utility_Security.isIntegrationEnabled;
        public DateTime dtTime{get;set;}
        public boolean isNotNetworked{get;set;}
        public SupplierWrapper(Supplier__c sup){
            this.supp = sup;
            this.dtTime = sup.CreatedDate;
            if(sup.Created_Invoice_Date__c != null){
                this.dtTime = sup.Created_Invoice_Date__c;
            }
            if(sup.Network_Status__c.containsIgnoreCase('Not Currently Networked'))
                isNotNetworked = true;
            if(isIntegrationEnabled && sup.Supplier__c != null && sup.Network_Status__c.containsIgnoreCase('Accepted') && sup.Supplier__r.CMP_H2H_Transaction_Enabled__c)
                this.canPayH2H = true;
            else
                this.canPayH2H = false;
            this.id=sup.id;
            this.name = sup.Supplier_Name__c;
            this.address = getFullAddress(sup.Address_line_1__c,sup.Address_Line_2__c,sup.City__c,sup.State_Province__c,sup.Country__c,sup.Post_Code__c);
        }

        public Integer compareTo(Object compareTo) {
            SupplierWrapper supWrpr = (SupplierWrapper)compareTo;
            if(dtTime > supWrpr.dtTime)    return -1;
            if(dtTime < supWrpr.dtTime)    return 1;
            return 0;
        }

        public string getFullAddress(String addline1,String addline2,String city,String state,String country, String postalCode){
            String address = '';
            if(String.isNotBlank(addline1))
             address = addline1 + ', ';
            if(String.isNotBlank(addline2))
             address += addline2 + ', ';
            if(String.isNotBlank(city))
             address += city + ', ';
            if(String.isNotBlank(state))
             address += state + ', ';
            if(String.isNotBlank(country))
             address += country + ', ';
            if(String.isNotBlank(postalCode))
             address += postalCode;
            return address;
        }
    }

    public EDGE_CaptureinvoiceController() {
        isApiOn = false;
        beneInNetwork = false;
        hbCurrencyISO = '';
        isSuppInviteToNetwork = 'true';
        isSaveAndPayClicked = false;
        //isSaveAndAddBeneClicked = false;
        canCreateTransaction = Utility_Security.canCreateNetworkTransaction;
        if(ApexPages.currentPage().getParameters().get('retUrl') != null && ApexPages.currentPage().getParameters().get('retUrl') != ''){
            returnurl = new PageReference(ApexPages.currentPage().getParameters().get('retUrl'));
        }else if(ApexPages.currentPage().getHeaders().get('Referer') != null && ApexPages.currentPage().getHeaders().get('Referer') != ''){
            returnurl = new PageReference(ApexPages.currentPage().getHeaders().get('Referer'));
        } else {
            returnurl = Page.EDGE_Activity_Dashboard;
        }

        if(ApexPages.currentPage().getParameters().get('id') != null &&
                    ApexPages.currentPage().getParameters().get('id') != ''){
            for(Invoice__c tmpInv :
                   getInvoice(EncryptionManager.doDecrypt(ApexPages.currentPage().getParameters().get('id'))) ){
                this.invoice = tmpInv;
                selectedDelType = invoice.Delivery_Method__c;
                if(!String.isBlank(invoice.Delivery_Method__c) && invoice.Delivery_Method__c.equalsIgnoreCase('Holding-to-Holding')){
                    hbCurrencyISO = invoice.Custom_Currency__c;
                }
                if(tmpInv.Supplier__c != null){
                	if(!tmpInv.Supplier__r.Invite_to_my_Network__c)
                		isSuppInviteToNetwork = 'false';
                }
            }
            if(invoice != null)
                fetchUploadedAttachment();
        }

        isInvSaved = false;
        isSaveClicked = false;
        isSaveAndNewClicked = false;
        /**/
        //posts = new List<FeedItem>();
        cmpAdmin = CMP_Administration__c.getInstance();
        recordTypeName = cmpAdmin.CMP_GP_Default_Invoice_Record_Type__c;
        parentInvoiceId = cmpAdmin.EDGE_Default_Feed_Invoice__c;
        if(!canCreateTransaction) {
            recordTypeName = cmpAdmin.CMP_Default_Invoice_Record_Type__c;
        }
        if(!String.isEmpty(parentInvoiceId)) {
            List<Invoice__c> invoices =  [SELECT OwnerId FROM Invoice__c WHERE Id=:parentInvoiceId];
            if(invoices.size() > 0)
                invoiceOwnerId =invoices[0].OwnerId;
        }
        allowedFileType = cmpAdmin.CMP_Allowable_File_Types__c;
        accId = utility.currentAccount;
        if(canCreateTransaction){
            if(Utility_Security.canPayInvoices_H2H){
                 String customerId ='';
                 EdgeGpJsonUtils jsonUtil = new EdgeGpJsonUtils(false);
                 H2Hcurrencies = jsonUtil.getHoldingCurrencies();
                 isApiOn = jsonUtil.isApiOn;


        h2hCurrencySet = new Set<String>();
        for(SelectOption opt : H2Hcurrencies){
          if(opt.getvalue() != ''){
          h2hCurrencySet.add('\''+opt.getvalue()+'\'');
          //h2hCurrencySet.add('\''+opt.getvalue()+'\'');
          }
        }
        stdCurrencySet = new Set<String>();
        Schema.DescribeFieldResult fieldResult = Invoice__c.custom_currency__c.getDescribe();
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.picklistEntry f:ple)
        {
           stdCurrencySet.add('\''+f.getValue()+'\'');
        }


        system.debug('>>>>currencySet'+h2hCurrencySet);




            }
        }

        if(invoice == null){
            recordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
            invoice = new Invoice__c(RecordTypeId=recordTypeId);
            invoice.Invite_to_my_network__c = false;
            //if(invoice.custom_Currency__c == null) invoice.Custom_Currency__c = Utility.loggedInUser.DefaultCurrencyIsoCode;
        }
    }

    // Get fields from fieldset
    public Map<String,Schema.SObjectField> getFields() {
        SObjectType invoiceType = Schema.getGlobalDescribe().get('Invoice__c');
        return invoiceType.getDescribe().fields.getMap();
    }

    // GetInvoice with all the fields in the fieldset
    private List<Invoice__c> getInvoice(String id) {
        String query = 'SELECT ';
        for(String field : this.getFields().KeySet()) {
            query += field + ', ';
        }
        query += 'RecordType.name,'+
                 'Input_Beneficiary__r.Name, Input_Beneficiary__r.OwnerId,'+
                 'Input_Beneficiary__r.Buyer_Supplier_Number__c,'+
                 'Buyer__r.ContactId,Buyer__r.Id,Buyer__r.Contact.AccountId,'+
                 'CreatedBy.Name,LastModifiedBy.Name,Owner.Name,Account__r.Name,'+
                 'Supplier__r.Invite_to_my_Network__c,'+
                 '(SELECT Order_ID__c,CurrencyIsoCode,Payment_Amount__c,Settlement_Method__c,CreatedDate FROM Payments__r)'+
                 ' FROM Invoice__c Where Id =: id LIMIT 1';
        return Database.query(query);
    }

    public boolean isApiOn{
        get;
        set;
    }

     public PageReference deleteAttachmentFeed() {
       fileId = null;
       fileName = null;
       latestContentId = null;
       return null;
     }


    public void updateCurrencyList() {
        currencyOptions = new List<SelectOption>();
        for(CMP_Currency_ISO_Mapping__c gpCurrency : [SELECT Currency_ISO_Code__c,Name,Id FROM CMP_Currency_ISO_Mapping__c ORDER BY Name]) {
            currencyOptions.add(new SelectOption(gpCurrency.Currency_ISO_Code__c,gpCurrency.Name));
        }
        System.Debug('********** EDGE_CaptureinvoiceController - Inside method: updateCurrencyList');
    }

/*
    public void closeBenePopup() {
        if(!String.isEmpty(invoice.Beneficiary_GP_ID__c)) {
            Network__c relatesNetwork = null;
            Global_Pay_ID_Management__c gPIdM = null;
            for(Global_Pay_ID_Management__c gPId : [SELECT Global_Pay_ID__c,Creation_Type__c,User_Beneficiary__c,User_Beneficiary__r.Email,User_Beneficiary__r.ContactId,User_Beneficiary__r.Contact.AccountId FROM Global_Pay_ID_Management__c WHERE Global_Pay_ID__c=:invoice.Beneficiary_GP_ID__c]) {
                gPIdM = gPId;
                break;
            }
            if(gPIdM!=null) {
                for(Network__c networks : [SELECT Id FROM Network__c WHERE (Account_Invitee__c=:gPIdM.User_Beneficiary__r.Contact.AccountId AND Account_Inviter__c=:Utility.currentAccount) OR
                                          (Account_Inviter__c=:gPIdM.User_Beneficiary__r.Contact.AccountId AND Account_Invitee__c=:Utility.currentAccount)]) {
                    relatesNetwork = networks;
                    SyStem.Debug('********** EDGE_CaptureinvoiceController - Inside Method: closeBenePopup'+ gPIDM);
                    break;
                }
                if(String.isEmpty(invoice.Beneficiary_GP_Email_Address__c)) {
                    invoice.Beneficiary_GP_Email_Address__c = gPIdM.User_Beneficiary__r.Email;
                }
            }
            if(relatesNetwork!=null) {
               invoice.Invite_to_my_network__c = false;
               beneInNetwork = true;
            }
        }
        isDisplayBenePopUp = false;
     }
     public void showBenePopUp() {
        isDisplayBenePopUp = true;
     }
*/

     private void saveInvoice() {
        logSaveInvoice.logMessage('>>inside saveInvoice method ...>> ');
        hasError = false;
        invoice.Status__c = recordTypeName == cmpAdmin.CMP_GP_Default_Invoice_Record_Type__c ? 'Draft' :'Pending Approval (Open)';
        invoice.Initiated_By__c = 'Buyer';
        invoice.Balance__c = invoice.Amount__c;
       // System.assert(false,'>>>> '+ selectedDelType);
        logSaveInvoice.logDebug('' + selectedSuppId);
       if(String.isNotBlank(selectedSuppId)){
            invoice.Supplier__c = selectedSuppId;
            for(Supplier__c sup: [Select Supplier_Name__c,Supplier__c,Supplier__r.CCT_Client_Id__c,Email_Address__c From Supplier__c Where Id=:selectedSuppId]){

                if(String.isBlank(sup.Email_Address__c) && invoice.Invite_to_my_network__c){
                    errorMessage += (String.isNotBlank(errorMessage)?'<br/>':'')+System.Label.CM_EDGE_Bene_Email_Error;
                hasError = true;
                }

                invoice.Beneficiary_GP_Name__c = sup.Supplier_Name__c;
                if(sup.Supplier__c != null)
                    invoice.Seller_Account__c = sup.Supplier__c;
                if(selectedDelType == 'Holding-to-Holding'){
                    invoice.Beneficiary_GP_ID__c = sup.Supplier__r.CCT_Client_Id__c;
                }
            }
            invoice.Delivery_Method__c = selectedDelType;
            if(!String.isBlank(invoice.Delivery_Method__c) && invoice.Delivery_Method__c.equalsIgnoreCase('Holding-to-Holding')){
                invoice.Custom_Currency__c = hbCurrencyISO;
            }
       }


        if(invoice.Invoice_Number__c!= null && !Utility.matchPattern(invoice.Invoice_Number__c)){
            errorMessage += (String.isNotBlank(errorMessage)?'<br/>':'')+System.Label.CM_EDGE_SWIFTCharactersOnly;
            hasError = true;
        }
        if(invoice.Due_Date__c == null) {
            errorMessage += (String.isNotBlank(errorMessage)?'<br/>':'')+System.Label.CM_Validation_Input_FieldPaymentDateShouldNotBeBlank;
            hasError = true;
        }
        if(invoice.Amount__c == null ) {
            errorMessage += (String.isNotBlank(errorMessage)?'<br/>':'')+System.Label.CM_Validation_Input_FieldAmountShouldNotBeBlank;
            hasError = true;
        }
        if(String.isEmpty(invoice.Custom_Currency__c) || invoice.Custom_Currency__c.equalsIgnoreCase('Select')) {
            errorMessage += (String.isNotBlank(errorMessage)?'<br/>':'')+System.Label.CM_Validation_Input_FieldSelectedCurrencyIsNotValid;
            hasError = true;
        }
        /*
        if(recordTypeName == cmpAdmin.CMP_GP_Default_Invoice_Record_Type__c && invoice.Invite_to_my_network__c && String.isEmpty(invoice.Beneficiary_GP_Email_Address__c)) {
            errorMessage += (String.isNotBlank(errorMessage)?'<br/>':'')+System.Label.CM_Error_Message_Invoice_Email_Required;
            hasError = true;
        }
        */
        if(filename!=null && !String.isBlank(filename)) {
                String filetype = filename.subString(filename.lastIndexOfIgnoreCase('.')+1);
                if((allowedFileType==null || String.isBlank(allowedFileType)) || !allowedFileType.containsIgnoreCase(filetype)) {
                    errorMessage += (String.isNotBlank(errorMessage)?'<br/>':'')+ System.Label.CM_Alert_File_InvalidFileFormat;
                    hasError = true;
                }
        }
        logSaveInvoice.logDebug('hasError ' + hasError);
        if(!hasError) {
           // updateInvoiceSellerAccount(invoice);
            invoice.Account__c = Utility.currentAccount;
            invoice.Buyer__c = UserInfo.getUserId();
            invoice.Balance__c = invoice.Amount__c;
            upsert invoice;
            if(fileId!=null) {
                FeedItem uploadedAttachment=null;
                List<FeedItem> uploadedAttachments = [SELECT Id,ContentData,ContentFileName FROM FeedItem WHERE Id=:fileId];
                if(uploadedAttachments.size()>0){
                    uploadedAttachment = uploadedAttachments[0];
                }
                if(uploadedAttachment!=null && uploadedAttachment.Id!=null) {
                   FeedItem post = new FeedItem();
                    post.Title = uploadedAttachment.ContentFileName;
                    post.ParentId = invoice.Id;
                    post.Body = filename + '-'+invoice.Name;
                    post.ContentData = uploadedAttachment.ContentData;
                    post.ContentFileName = uploadedAttachment.ContentFileName;
                    post.Visibility = 'AllUsers';
                    insert post;
                }
            }
        }
        logSaveInvoice.saveLogs();
     }

     private void fetchUploadedAttachment(){
        for(FeedItem fi : [Select Id, ContentFileName, Title, Body,RelatedRecordId, ContentData From FeedItem Where ParentId=:invoice.Id]){
            fileData = fi.ContentData;
            latestContentId = fi.RelatedRecordId;
            fileName = fi.ContentFileName;
        }
     }

/*
     public PageReference redirectToHome(){
        if(isSaveClicked && !hasError && OldInvoiceId!=null) { // && !hasError && invoice.Id!=null
            //returnurl = new PageReference (CMP_Administration__c.getInstance().CM_Community_BaseURL__c+'/EDGE_InvoiceDetail2?Id='+OldInvoiceId);
            System.debug('returnUrl::: '+ returnurl);
            returnurl.setRedirect(true);
            return returnurl;
        }
        else{
            PageReference pgRef = new PageReference('/EDGE_Capture_Invoice');
            pgRef.setRedirect(true);
            return pgRef;
        }
     }
*/

     public PageReference saveAndPay(){
         isSaveAndPayClicked = true;
         save();
         String encodedInvoiceId = Utility.doEncryption(OldInvoiceId);
         if(!hasError && OldInvoiceId!=null){
             PageReference pgref = new PageReference ('/EDGE_Pay?invoiceId='+encodedInvoiceId+'&retURL='+CMP_Administration__c.getInstance().CM_Community_BaseURL__c+'/EDGE_InvoiceDetail2?Id='+encodedInvoiceId);
             pgref.setRedirect(true);
             return pgref;
         }
         return null;
     }

     public PageReference saveAndAddBene(){
        isSaveAndAddBeneClicked = true;
        save();
        String encodedInvoiceId = Utility.doEncryption(OldInvoiceId);
        if(!hasError && OldInvoiceId!=null){
            PageReference pgref = new PageReference ('/EDGE_SelectBeneficiary?invoiceId='+encodedInvoiceId+'&retURL=/EDGE_InvoiceDetail2?Id='+encodedInvoiceId);
            pgref.setRedirect(true);
            return pgref;
        }
        return null;
     }

     public PageReference saveAndAddNew(){
        isAddNewClicked = true;
        save();
        if(!hasError && invoice.Id!=null){
        	  String encodedInvoiceId = Utility.doEncryption(invoice.Id);
            PageReference pgref = new PageReference ('/EDGE_Beneficiary?retURL=/EDGE_Capture_Invoice&InvoiceId='+encodedInvoiceId+ (returnUrl!=null?'&retUrl2='+returnUrl.getUrl():''));
            pgref.setRedirect(true);
            return pgref;
        }
        return null;
     }

     public PageReference save() {
        errorMessage = '';
        SyStem.Debug('###'+invoice.Custom_Currency__c);
        Savepoint sp = Database.setSavepoint();
        Pagereference pg =null;
        hasError=false;
        isInvSaved = false;
        isSaveClicked = true;
        try {
            saveInvoice();
            if(!hasError && invoice.Id!=null){
                OldInvoiceId = invoice.Id;
                isInvSaved = true;
                return returnurl;
            }
            else
                return null;

            system.debug('***invoice:'+invoice);

            if(isSaveAndPayClicked == false && isSaveAndAddBeneClicked == false){
                invoice = new Invoice__c(RecordTypeId=recordTypeId);
                fileId = null;//We need this Id for redirect.
                fileName = null;
                latestContentId = null;
            }

        } catch(DMLException ex) {
            Database.rollback( sp );
            errorMessage += (String.isNotBlank(errorMessage)?'<br/>':'')+ex.getdmlMessage(0);
            hasError = true;
        } catch (Exception ex) {
            Database.rollback( sp );
            errorMessage += (String.isNotBlank(errorMessage)?'<br/>':'')+System.Label.CM_Alert_UnableToSaveInvoice + ex.getMessage();
            system.debug('***errorMessage:'+errorMessage);
            hasError=true;
        }
        for(ApexPages.Message message : ApexPages.getMessages()) {
            if( message.getSeverity() == ApexPages.Severity.ERROR && !errorMessage.containsIgnoreCase(message.getDetail())){
                errorMessage += (String.isNotBlank(errorMessage)?'<br/>':'')+ message.getDetail();
                system.debug('***1errorMessage:'+errorMessage);
                hasError=true;
            }
        }

        return null;
     }

     public PageReference saveNew() {
        hasError=false;
        isSaveAndNewClicked = true;
        isInvSaved = false;
        errorMessage = '';
        Pagereference pg =null;
        Savepoint sp = Database.setSavepoint();
        try {
            saveInvoice();
            if(!hasError && invoice.Id!=null){
                isInvSaved = true;
                return new PageReference('/EDGE_Capture_Invoice'+(returnUrl!=null?'?retUrl='+returnUrl.getUrl():''));
            }
            /*
            invoice = new Invoice__c(RecordTypeId=recordTypeId);
            fileId = null;
            fileName = null;
            latestContentId = null;
            */
            // }
        } catch(DMLException ex) {
            Database.rollback( sp );
            errorMessage = (String.isNotBlank(errorMessage)?'<br/>':'')+ex.getdmlMessage(0);
            hasError=true;
        } catch (Exception ex) {
            Database.rollback( sp );
            errorMessage += (String.isNotBlank(errorMessage)?'<br/>':'')+System.Label.CM_Alert_UnableToSaveInvoice + ex.getMessage();
            hasError=true;
        }
        return null;
     }

    public PageReference uploadAttachmentFeed() {
        logUploadAttachmentFeed.logMessage('>>inside uploadAttachmentFeed method ...>> ');
        try {
            errorMessage ='';
            if(fileName!=null && !String.isBlank(fileName)) {
                String filetype = fileName.subString(fileName.lastIndexOfIgnoreCase('.')+1);
                if((allowedFileType==null || String.isBlank(allowedFileType)) || !allowedFileType.containsIgnoreCase(filetype)) {
                    errorMessage = System.Label.CM_Alert_File_InvalidFileFormat;
                    logUploadAttachmentFeed.logDebug('Error message' + errorMessage );
                    return null;
                }
            }
            FeedItem uploadedAttachment = new FeedItem();
            uploadedAttachment.ContentFileName = fileName;
            uploadedAttachment.Title = uploadedAttachment.ContentFileName;
            uploadedAttachment.ParentId = parentInvoiceId;//'a1d1b0000008jhw'; //eg. Opportunity id, custom object id..
            uploadedAttachment.Body = uploadedAttachment.ContentFileName + '-submitinvoice';
            uploadedAttachment.Visibility = 'AllUsers';
            uploadedAttachment.ContentData = fileData;
            insert uploadedAttachment;
            logUploadAttachmentFeed.logDebug('' + uploadedAttachment);
            fileId = uploadedAttachment.Id;
            latestContentId = [SELECT RelatedRecordId FROM FeedItem WHERE Id=:fileId limit 1].RelatedRecordId;
            update (new ContentVersion(Id=latestContentId,OwnerId=invoiceOwnerId));
        }catch (Exception ex) {
            System.Debug('###'+ex);
            logUploadAttachmentFeed.logDebug('Exception ' + ex.getMessage());
            logUploadAttachmentFeed.saveLogs();
            errorMessage += (String.isNotBlank(errorMessage)?'<br/>':'')+ex;
            hasError=true;
        }
        fileData = null;
        logUploadAttachmentFeed.saveLogs();
        return null;
     }
/*
     // T-485988 : Knong view Payment
     private void updateInvoiceSellerAccount(Invoice__c invoice) {
        if(invoice.Delivery_Method__c != 'Holding-to-Holding' && !String.isBlank(invoice.Beneficiary_GP_ID__c)) {
            for(Global_Pay_ID_Management__c GPM: [SELECT Account_Beneficiary__c, Source_Email_Match__c,
                                                    User_Beneficiary__c From Global_Pay_ID_Management__c
                                                    WHERE Global_Pay_ID__c =:invoice.Beneficiary_GP_ID__c]){
                invoice.Seller_Account__c = GPM.Account_Beneficiary__c;
                break;
            }
        }
     }
     */
     /*
     public PageReference attachmentUpdate() {
        try {
            if(uploadedAttachment.ContentFileName!=null && !String.isBlank(uploadedAttachment.ContentFileName)) {
                String filetype = uploadedAttachment.ContentFileName.subString(uploadedAttachment.ContentFileName.lastIndexOf('.')+1);
                if((allowedFileType==null || String.isBlank(allowedFileType)) || allowedFileType.indexOf(filetype)!=-1) {
                    FeedItem fI = new FeedItem();
                    fI.Title = uploadedAttachment.ContentFileName;
                    fI.Body = uploadedAttachment.ContentFileName;
                    fI.Type ='ContentPost';
                    fI.ContentData = uploadedAttachment.ContentData;
                    fI.ContentFileName = uploadedAttachment.ContentFileName;
                    fI.Visibility = 'AllUsers';
                    posts.add(fI);
                    uploadedAttachment = new FeedItem();
                }else {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.CM_Alert_File_InvalidFileFormat));
                }
            }
        }catch(Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error in Attachment' + ex.getMessage()));
        }
        return null;
     }

     public PageReference delAttachment() {
        if(posts!=null && posts.size()>0 && deleteIndex<posts.size()) {

        posts.remove(deleteIndex);
        }
        return null;
     }*/
}