//Created By : Ranjeet Singh
// Description : Controler Class for c:EDGE_Site_Navigation
//Edit : Raghu Rankawat : 21-Oct-2016 : added getCurrentBatch() method (Task T-546889)
public with sharing class EDGE_Site_NavigationController {
	public Boolean canCreatePaymentsByWireDraftOrACH{
		get{
			return Utility_Security.canCreatePaymentsByWireDraftOrACH;
		}
	}
	public Boolean canShowHoldingBalance{
		get{
			return !Utility.isNonCCTuser;
		}
	}
	public Boolean CollabUser{
		get{
			return Utility.isCollaborationUser;
		}
	}

	public Boolean isPaymentApprovals{
		get{
			return Utility_Security.isPaymentApprovals;
		}
	}

	public Boolean isNetworkEnabled{
		get{
			return Utility_Security.isNetworkEnabled;
		}
	}

	public Boolean isReportingEnabled{
		get{
			return Utility_Security.isReporting;
		}
	}

	public User user{get;set;}
	public EDGE_Site_NavigationController(){
        user = [SELECT id, email, username, usertype, communitynickname, defaultCurrencyIsoCode,timezonesidkey, languagelocalekey, firstname, lastname, phone, title,
                street, city, country, postalcode, state, localesidkey, mobilephone, extension, fax, contact.email,SmallPhotoUrl
                FROM User
                WHERE id = :UserInfo.getUserId()];
        // guest users should never be able to access this page
        if (user.usertype == 'GUEST') {
            throw new NoAccessException();
        }
	}
	
	public PageReference getCurrentBatch(){

		Batch__c currentBatch = EDGE_BatchService.getActiveBatchForCurrentContact();
		PageReference pg = new PageReference('/EDGE_MultiLine_Order_Review?orderId='+currentBatch.Id);
        String retURL = CMP_Administration__c.getInstance().CM_Community_BaseURL__c+'/EDGE_MultiLineOrderStep1?step=1';
        pg.getParameters().put('retUrl', retURL);
        //pg.setRedirect(true);
        return pg;
	}
}