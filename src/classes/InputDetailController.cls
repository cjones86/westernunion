//
// (c) 2014 Appirio, Inc.
//
// InputDetailController
// Used as Page Controller to show the details of Input object record and add attachment to record
//
// 02 Apr, 2015    Ashish Goyal  			Original (Ref. T-375814)
// 23 Apr, 2015	   Pratibha Chhimpa			Modified(I-156475)
public without sharing class InputDetailController {

    static apexLogHandler.apexLog logDelAttachment = new apexLogHandler.apexLog('InputDetailController','delAttachment');
    static apexLogHandler.apexLog logCustomDelete = new apexLogHandler.apexLog('InputDetailController','customDelete');
    static apexLogHandler.apexLog logSave = new apexLogHandler.apexLog('InputDetailController','save');

	public Input__c inputRecord {get;set;}
	public Input__c inputOptionRecord {get;set;}
	transient public List<Attachment> listChildAttachments {get;set;}
	public String attachmentIdToDelete {get;set;}
	public Boolean isForcast {get; set;}
	public Boolean isOption {get; set;}
	public Boolean isFixed {get; set;}
	public Boolean isWindow {get; set;}
	public Boolean isHolding {get; set;}
	public String selectedTranscation{get;set;}
	public String fieldset{get;set;}
	public String returnURL {get; set;}
	 //added line for test edit permissions
    public boolean canEdit{get;set;}
	public boolean edit{get;set;}
    public String latestContentId{get;set;}

	  // Constructor
	 public InputDetailController(){
	 	isForcast = false;
		isOption = false;
		isFixed = false;
		isWindow = false;
		isHolding = false;
		inputOptionRecord = new Input__c();
		returnURL = ApexPages.currentPage().getParameters().get('retURL');
		if(ApexPages.currentPage().getParameters().get('id') != null && ApexPages.currentPage().getParameters().get('id') != ''){
      String inputId = EncryptionManager.doDecrypt(ApexPages.currentPage().getParameters().get('id'));
		 	for(Input__c tempInput : [SELECT id, Input_Type__c, Type__c, Option_Protection_Obligation__c from Input__c WHERE Id =: inputId]){
		 		inputRecord = tempInput;
		 		selectedTranscation = tempInput.Input_Type__c;
		 	}
		 	system.debug('**********:'+selectedTranscation);
		 	if(inputRecord.Input_Type__c == 'Forecast Outflow' || inputRecord.Input_Type__c == 'Purchase Order'
		 			|| inputRecord.Input_Type__c == 'Forecast Inflow'){
	    		isForcast = true;
	    		fieldset = 'InputDetailFS3';
	    	}
	    	else if(inputRecord.Input_Type__c == 'Option - Other' || inputRecord.Input_Type__c == 'Option - WUBS'){
	    		isOption = true;
	    		fieldset = 'InputDetailFS2';
	    		if(inputRecord.Option_Protection_Obligation__c!=null){
			 		for(Input__C tempInput : [SELECT id, Amount__c,Input_Type__c, Type__c, Option_Protection_Obligation__c from Input__c WHERE Id =: inputRecord.Option_Protection_Obligation__c]){
			 			if(inputRecord.Type__c.contains('Protection')){
			 				inputOptionRecord = tempInput;
			 			}else{
			 				inputOptionRecord = inputRecord;
			 				inputRecord = tempInput;
			 			}
			 		}
	    		}
	    	}
			else if(inputRecord.Input_Type__c == 'Fixed Forward Contract - Other'){
	    		isFixed = true;
	    		fieldset = 'Fixed_Forward_Contract_Other';
	    	}
	    	else if(inputRecord.Input_Type__c == 'Window Forward Contract - Other'){
	    		isWindow = true;
	    		fieldset = 'Window_Forward_Contract_Other';
	    	}
	    	else if(inputRecord.Input_Type__c == 'Foreign Currency Balance'){
	    		isHolding = true;
	    		fieldset = 'Holding_Balance_Other';
	    	}
	    	system.debug('@@@@@ ' + inputRecord.Input_Type__c);
	    	inputRecord =  getInput(inputRecord.id);
	    	if(inputOptionRecord.Id != null){
	    		inputOptionRecord = getInput(inputOptionRecord.id);
	    	}
	    	canEdit = false;
         	/*canEdit = [SELECT RecordId, HasEditAccess FROM UserRecordAccess
                            WHERE UserId = :userInfo.getuserId() AND RecordId = :inputRecord.id LIMIT 1].HasEditAccess ;
            if(!canEdit){*/
        	if(inputRecord.OwnerId == UserInfo.getUserId() || inputRecord.OwnerId == Utility.currentAccountOwner){
        		canEdit = true;
        	}
            //}
            listChildAttachments = findAttachments();

		 }
		 if(ApexPages.currentPage().getParameters().get('edit') != null &&
						 ApexPages.currentPage().getParameters().get('edit') != ''){
				 editinput();
		 }

	 }

	 //create new attachment record
	 public PageReference createNewAttachment(){
	    return new PageReference ('/apex/AttachmentUpload?pid='+inputRecord.id+'&retURL=%2Fapex%2FInputDetail?id='+inputRecord.id);
	 }

	 //get all the child attachment records
	 private List<Attachment> findAttachments(){
	     return [SELECT Id, Name, LastModifiedById, CreatedById, ContentType FROM Attachment
	             WHERE ParentId = :this.inputRecord.id];
	 }

	 public PageReference delAttachment(){
	   
	   logDelAttachment.logMessage('>>inside delAttachment method ...>> ');  
	     
	   system.debug('$$$' + attachmentIdToDelete);
	   logDelAttachment.logDebug('$$$' + attachmentIdToDelete);
	   Attachment att = new Attachment(Id=attachmentIdToDelete);
	   delete att;
	   logDelAttachment.logDebug('Deleted Attachment ' + att);
	   attachmentIdToDelete = '';
	   listChildAttachments = findAttachments();
	   logDelAttachment.saveLogs();
	   return null;
	 }

	 public Pagereference customDelete() {
	    logCustomDelete.logMessage('>>inside customDelete method ...>> ');  
	 	
	 	try{
	 		delete inputRecord;
	 		logCustomDelete.logDebug('Deleted inputRecord ' + inputRecord);
	 		if(inputOptionRecord.Id != null){
	 			delete inputOptionRecord;
	 			logCustomDelete.logDebug('Deleted inputOptionRecord ' + inputOptionRecord);
	 		}
	 		if(system.currentPageReference().getParameters().get('retURL') != null) {
	 			logCustomDelete.saveLogs();
	 			return new Pagereference(system.currentPageReference().getParameters().get('retURL'));
	 		}
	 		logCustomDelete.saveLogs();
	 		return Page.EDGE_ManageCashInput;
	 	} catch(Exception ex) {
	 	    logCustomDelete.logDebug('Exception :' + ex.getMessage());
			logCustomDelete.saveLogs();
			return null;
	 	}
	 }
	 public Pagereference customCancel() {
	 	try{
	 		if(returnURL != null) {
	 			return new Pagereference(returnURL);
	 		}
	 		return Page.EDGE_ManageCashInput;
	 	} catch(Exception ex) {
			return null;
	 	}
	 }

	 public List<SelectOption> CurrencyOptions{
    	get{
    		if(CurrencyOptions==null || CurrencyOptions.size()==0){
        		CurrencyOptions= Utility.picklistValues('Input__c','Custom_Currency__c', true);
    		}
    		return CurrencyOptions;
    	}
    	set;
    }

	 public map<String, string> LabelMaps{
  		get{
  			//String apiName = Utility.changeTransactionType(selectedTranscation);
  			if(!String.isEmpty(fieldset)){
  				try{
  				return Utility.getFieldLabel('input__c', selectedTranscation);
  				}catch(Exception Ex){

  				}
  			}
  			return new map<String, string>();
  		}
  	}

  	// Get fields from fieldset
	public List<Schema.FieldSetMember> getFields() {

        return Schema.SObjectType.Input__c.fieldSets.getMap().get(fieldset).getFields();
    }
    // GetInvoice with all the fields in the fieldset
    private Input__c getInput(String id) {
        String query = 'SELECT ';
        system.debug('#####' + this.getFields());
        for(Schema.FieldSetMember f : this.getFields()) {
            query += f.getFieldPath() + ', ';
        }
        query += 'Id,Name,Input_Type__c,Type__c,CreatedById,OwnerId,Input_Beneficiary__r.Name, LastModifiedById,Option_Protection_Obligation__c,Input_Beneficiary__r.OwnerId, Input_Beneficiary__r.Buyer_Supplier_Number__c, LastModifiedBy.Name, CreatedBy.Name, CreatedDate, LastModifiedDate, Owner.Name FROM Input__c Where Id =: id LIMIT 1';
        system.debug('#####' + query);
        return Database.query(query);
    }


    public PageReference save(){
    	logSave.logMessage('>>inside save method ...>> ');  
    	
    	boolean hasError = false;
    	try{
    		hasError = Utility.fieldSetValidation(inputRecord,fieldset, inputRecord.Input_Type__c, true);
    		logSave.logDebug('hasError : ' + hasError);
    		if(!hasError) {
		    	if(ApexPages.hasMessages(ApexPages.Severity.Error)){
		    		hasError = true;
		    		logSave.saveLogs();
		    		return ApexPages.currentPage();
		    	}
		    	if(inputRecord.Input_Type__c == 'Option - Other' || inputRecord.Input_Type__c == 'Option - WUBS'){
		    		inputOptionRecord.Transaction_Date__c = inputRecord.Transaction_Date__c;
		    		inputOptionRecord.Buy_Currency__c = inputRecord.Buy_Currency__c;
		    		inputOptionRecord.Sell_Currency__c = inputRecord.Sell_Currency__c;
		    		inputOptionRecord.Reference_Number__c = inputRecord.Reference_Number__c;
		    		inputOptionRecord.Counter_Party__c = inputRecord.Counter_Party__c;
		    		update inputOptionRecord;
		    		logSave.logDebug('updated inputOptionRecord ' + inputOptionRecord);
		    	}
		    	update inputRecord;
		    	logSave.logDebug('updated inputRecord ' + inputRecord);
		    	inputRecord =  getInput(inputRecord.id);
    	    	if(inputOptionRecord.Id != null){
    	    		inputOptionRecord = getInput(inputOptionRecord.id);
    	    	}
				edit = false;
		    	if(returnURL != null) {
		    	    logSave.saveLogs();
		 			return new Pagereference(returnURL);
				}
				logSave.saveLogs();
		 		return null;
	      }
	      logSave.saveLogs();
		  return ApexPages.currentPage();
    	}
    	catch(DMLException dmlEx){
    	    logSave.logDebug('Exception :' + dmlEx.getMessage());
    		logSave.saveLogs();
    		return ApexPages.currentPage();
    	}

    }

		public Pagereference editInput(){
			for(FeedItem fPost : [SELECT RelatedRecordId FROM FeedItem
																	WHERE ParentId=:inputRecord.Id AND Type='ContentPost' ORDER BY CreatedDate desc]) {
					latestContentId = fPost.RelatedRecordId;
					break;
			}
			edit = true;
			String encodedInputId = Utility.doEncryption(inputrecord.Id);
			PageReference pg = new PageReference(CMP_Administration__c.getInstance().CM_Community_BaseURL__c+'/EDGE_InputDetail?Id='+encodedInputId+'&edit=true');
			return pg;
		}

		public PageReference cancel(){
			edit = false;
			if(returnurl!=null) {
			    PageReference pgRef = new PageReference(returnurl);
                pgRef.setRedirect(true);
                return pgRef;
            }
			return null;
		}
}