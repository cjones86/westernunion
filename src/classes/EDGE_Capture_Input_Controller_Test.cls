/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_Capture_Input_Controller_Test
 * Description: Test class for Controller EDGE_Capture_Input_Controller
 * Created Date: 01 Mar' 2016
 * Created By: Rohit Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update                                           
 =====================================================================*/
@isTest
private class EDGE_Capture_Input_Controller_Test {

	static testMethod void captureInput() {
		Test.startTest();
		EDGE_Capture_Input_Controller edgeInput = new EDGE_Capture_Input_Controller();
		system.assertNotEquals(edgeInput.transactionTypes.size(),0);
		Test.stopTest();
	}
}