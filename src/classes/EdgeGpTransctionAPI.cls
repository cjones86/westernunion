//Created By   : Ranjeet Singh
//Created date : March/11/2016
//Description  : T-482897, T-482225, Retreive H2H Get Holding Currencies from WUBS 2.0

public without sharing class EdgeGpTransctionAPI {
        //Declare members
        static final String certificateName = Utility_Security.wubsIntegrationAdmin.certificate_Key__c;
        static final Integer timeout = 30000;  // 30,000 Milliseconds = 30 Seconds 
        public static boolean isMockupResponse = false;
        
        public String calloutResponseStatus{get;set;}
        public String calloutResponseResult{get;set;}
        public String reqHeaderCustomerId{get;set;}
        public String errorTxt{get;set;}
        Boolean isAddErrorToPage{get;set;}
        
        //Init Methods
        public EdgeGpTransctionAPI(){
            isAddErrorToPage =false;
            Init();
        }
        
        //If error needs to be added to apex-page messages.
        public EdgeGpTransctionAPI(boolean isAddErrorToPg){
            isAddErrorToPage = isAddErrorToPg;
            Init();
        }
        
        public void Init(){
            errorTxt = '';
            calloutResponseStatus = '';
            calloutResponseResult = '';
            reqHeaderCustomerId = '';
        }
        
        //End Init Methods
        
        //Common method to make callouts
        boolean performAction(String mockResponse, String method, String endPoint, String postData){
            HTTPResponse res ;
            try{                
                //System.Debug('########## 1 EdgeGpTransctionAPI - performAction: isMockupResponse ' + isMockupResponse + ' #####');
                HttpRequest req = new HttpRequest();
                req.setEndpoint(endPoint);
                req.setMethod(method);
                if(method.equalsIgnoreCase('POST')){
                    if(!String.isBlank(postData)){
                        req.setBody(postData);
                    }
                }
                req.setTimeout(timeout);
                req.setClientCertificateName(certificateName); 
                    
                //System.Debug('########## 3 EdgeGpTransctionAPI - performAction() HttpRequest.endPoint:  ' + endPoint + ' #####');
                //System.Debug('########## 4 EdgeGpTransctionAPI - performAction() HttpRequest.method:  ' + method + ' #####');
                //System.Debug('########## 5 EdgeGpTransctionAPI - performAction() HttpRequest.timeout:  ' + timeout + ' #####');
                //System.Debug('########## 6 EdgeGpTransctionAPI - performAction() HttpRequest.certificateName:  ' + certificateName + ' #####');

                //if(String.isNotBlank(reqHeaderCustomerId))
                //    req.setHeader('customerid',reqHeaderCustomerId);

                Http http = new Http();
                if(isMockupResponse==true){
                    //System.Debug('########## 2 EdgeGpTransctionAPI - performAction:  MOCKUP #####');
                    calloutResponseStatus = '200 OK';
                    calloutResponseResult = mockResponse;   
                }else{
                 	res = http.send(req);
                    calloutResponseStatus = res.getStatusCode() + ': Detail : ' + res.getStatus() ;
                    calloutResponseResult = res.getBody();
                }
                //System.Debug('########## 7 EdgeGpTransctionAPI - performAction() HttpRequest.calloutResponseStatus:  ' + calloutResponseStatus + ' #####');
                //System.Debug('########## 8 EdgeGpTransctionAPI - performAction() HttpRequest.calloutResponseResult:  ' + calloutResponseResult + ' #####');
                //System.Debug('########## 9 EdgeGpTransctionAPI - performAction() HttpRequest.res.getBody(): ' + res.getBody() + ' #####');
                return true;
            }catch(Exception Ex){
                errorTxt = Ex+'';
                if(isAddErrorToPage){
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Ex+',\r\n Stack Trace:'+ Ex.getStackTraceString()+'\r\n res:'+res));
                    //System.Debug('########## 10 EdgeGpTransctionAPI - performAction() HttpRequest.errorTxt:  ' + errorTxt + ' #####');
                }
            }        
            return false;
        } 

    public boolean getHoldingCurrencies(String CustomerId){
        //Sample Result Test
        //calloutResponseResult = ;return true;      
        Init();
        WUBS_Integration_Administration__c gCs = WUBS_Integration_Administration__c.getInstance();
        System.debug('********1'+gCs+'  '+gCs.Integration_Enabled__c);
        
        if(gCs != null && gCs.Integration_Enabled__c){
        	                    System.debug('********2');
        	
            if(String.isEmpty(gCs.WUBS_API_GetHoldingCurrencies__c)){
                errorTxt = 'Please set configuration at: WUBS_Integration_Administration__c.WUBS_API_GetHoldingCurrencies__c';
                if(isAddErrorToPage){
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, errorTxt));
                    //System.Debug('########## 11 EdgeGpTransctionAPI - getHoldingCurrencies() HttpRequest.errorTxt: ' + errorTxt + ' #####');
                    System.debug('********3');
                }
                return false;
            } else{
            	                    System.debug('********4');
            	
                String endPoint = gCs.WUBS_API_GetHoldingCurrencies__c;
                if(!String.isEmpty(CustomerId)){
                	                    System.debug('********5');
                	
                    endPoint = endPoint+CustomerId;
                    System.debug('########## 12 CustomerID:  ' + CustomerId);
                    System.debug('########## 13 endPoint:  ' + endPoint);
                }else{
                	                    System.debug('********6');
                	
                    errorTxt = 'CustomerId not provided for GetHoldingCurrencies API. CustomerId= '+CustomerId;
                    if(isAddErrorToPage){
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, errorTxt));
                    }
                    return false;
                }
                
                return performAction(
                    '[{"code":"121","description": "des"}]',//Mock Response 
                    'GET',                                  //Request type
                    gCs.WUBS_API_GetHoldingCurrencies__c,   //End point
                    ''                                      //Request Post body
                    );
            }
        }
        return false;
    }

    //T-476811, Retrieve Holding Balances from WUBS 2.0
    public boolean getHoldingBalances(String CustomerId){
        //System.Debug('########## MP EdgeGpTransctionAPI: ENTERING getHoldingBalances #####');
        Init();
        WUBS_Integration_Administration__c gCs = WUBS_Integration_Administration__c.getInstance();

        //System.debug('gcs::: '+ gCs + ' >>>>1 '+ gCs.Integration_Enabled__c + ' >>>>2 '+ gCs.WUBS_API_GetHoldingBalance__c);

        if(gCs != null && gCs.Integration_Enabled__c){
            if(String.isEmpty(gCs.WUBS_API_GetHoldingBalance__c)){
                errorTxt = 'Please set configuration at: WUBS_Integration_Administration__c.WUBS_API_GetHoldingBalance__c';
                if(isAddErrorToPage){
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, errorTxt));
                    //System.Debug('########## 14 EdgeGpTransctionAPI - getHoldingBalances() HttpRequest.errorTxt: ' + errorTxt + ' #####');
                }
                return false;
            } else{
                String endPoint = gCs.WUBS_API_GetHoldingBalance__c;
                reqHeaderCustomerId = CustomerId;
                if(!String.isEmpty(CustomerId)){
                    endPoint = endPoint+CustomerId;
                    //System.debug('########## 15 CustomerID:  ' + CustomerId);
                    //System.debug('########## 16 endPoint:  ' + endPoint);
                }else{
                    errorTxt = 'CustomerId not provided for GetHoldingCurrencies API. CustomerId= '+CustomerId;
                    if(isAddErrorToPage){
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, errorTxt));
                        //System.Debug('########## 17 EdgeGpTransctionAPI - getHoldingBalances() HttpRequest.errorTxt: ' + errorTxt + ' #####');
                    }
                    return false;
                }
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Check'));
                return performAction(
                     getDummyResponseData(), //Mock Response 
                    'GET',                   //Request type
                    endPoint,   //End point
                    ''                       //Request Post body
                    );
            }
        }
        //System.Debug('########## MP EdgeGpTransctionAPI: EXITING getHoldingBalances #####');
        return false;
    }
    
    public String getDummyResponseData(){
        String jsonResStr = '[{"customerId":"4","currency":"AED","bookBalance":1025717.0800,"availableBalance":630553.1500},'+
                             '{"customerId":"4","currency":"AUD","bookBalance":59767.8100,"availableBalance":7517.8100},'+
                             '{"customerId":"4","currency":"BHD","bookBalance":2291450.0000,"availableBalance":1902690.0000},'+
                             '{"customerId":"4","currency":"CAD","bookBalance":2291450.0000,"availableBalance":1902690.0000},'+
                             '{"customerId":"4","currency":"CHF","bookBalance":2291450.0000,"availableBalance":1902690.0000},'+
                             '{"customerId":"4","currency":"DKK","bookBalance":2291450.0000,"availableBalance":1902690.0000},'+
                             '{"customerId":"4","currency":"EUR","bookBalance":2291450.0000,"availableBalance":1902690.0000},'+
                             '{"customerId":"4","currency":"FJD","bookBalance":2291450.0000,"availableBalance":1902690.0000},'+
                             '{"customerId":"4","currency":"HKD","bookBalance":2291450.0000,"availableBalance":1902690.0000},'+
                             '{"customerId":"4","currency":"INR","bookBalance":2291450.0000,"availableBalance":1902690.0000},'+
                             '{"customerId":"4","currency":"JPY","bookBalance":2291450.0000,"availableBalance":1902690.0000},'+
                             '{"customerId":"4","currency":"PHP","bookBalance":2291450.0000,"availableBalance":1902690.0000},'+
                             '{"customerId":"4","currency":"XPF","bookBalance":2291450.0000,"availableBalance":1902690.0000},'+
                             '{"customerId":"4","currency":"ABC","bookBalance":2291450.0000,"availableBalance":1902690.0000}]';
       return jsonResStr;
    }
}