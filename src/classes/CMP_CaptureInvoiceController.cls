/**=====================================================================
 * Appirio, Inc
 * Name: CMP_CaptureInvoiceController
 * Description: Controller to capture Invoice from CMP user
 * Created Date: 02 Feb' 2016
 * Created By: Rohit Sharma (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
public without sharing class CMP_CaptureInvoiceController {

    public Invoice__c invoice{get;set;}
    public FeedItem uploadedAttachment{get;set;}
    //public List<FeedItem> posts{get;set;}
    public Boolean isDisplayBenePopUp{get;set;}
    public boolean canCreateTransaction{get;set;}
    public Id recordTypeId{get;set;}
    public Id accId {get;set;}
    public integer deleteIndex{get;set;}
    public List<SelectOption> currencyOptions{get;set;}
    public String allowedFileType{get;set;}
    public InsightsAndInputTabCtrl InsightsAndInputCtl{get;set;}
    public boolean isUserH2H{get;set;}
    public PageReference returnurl{get;set;}
    public boolean isInvSaved{get;set;}
    public boolean hasError{get;set;}
    private boolean isSaveClicked;
    public String recordTypeName{get;set;}
    public CMP_Administration__c cmpAdmin{get;set;}
    public boolean beneInNetwork{get;set;}

    public CMP_CaptureInvoiceController() {
        beneInNetwork = false;
        canCreateTransaction = Utility_Security.canCreateNetworkTransaction;
        uploadedAttachment = new FeedItem();
        returnurl = Page.EDGE_MyDashboard;
        isInvSaved = false;
        isSaveClicked = false;
        /*if(ApexPages.currentPage().getParameters().get('retUrl') != null && ApexPages.currentPage().getParameters().get('retUrl') != ''){
        	returnurl = new PageReference(ApexPages.currentPage().getParameters().get('retUrl'));
        }*/
        //posts = new List<FeedItem>();
        cmpAdmin = CMP_Administration__c.getInstance();
        recordTypeName = cmpAdmin.CMP_GP_Default_Invoice_Record_Type__c;
        if(!canCreateTransaction) {
            recordTypeName = cmpAdmin.CMP_Default_Invoice_Record_Type__c;
        }
        allowedFileType = cmpAdmin.CMP_Allowable_File_Types__c;
        recordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        invoice = new Invoice__c(RecordTypeId=recordTypeId);
        accId = utility.currentAccount;
        if(canCreateTransaction){
            if(Utility_Security.canPayInvoices_SP){
                isUserH2H = false;
                invoice.Delivery_Method__c = 'Standard Payment';
            }
            if(Utility_Security.canPayInvoices_H2H){
                isUserH2H = true;
                invoice.Delivery_Method__c = 'Holding-to-Holding';
            }
        }
		invoice.Custom_Currency__c = Utility.loggedInUser.DefaultCurrencyIsoCode;
        updateCurrencyList();
    }

    public void updateCurrencyList() {
        currencyOptions = new List<SelectOption>();
        for(CMP_Currency_ISO_Mapping__c gpCurrency : [SELECT Currency_ISO_Code__c,Name,Id FROM CMP_Currency_ISO_Mapping__c ORDER BY Name]) {
            currencyOptions.add(new SelectOption(gpCurrency.Currency_ISO_Code__c,gpCurrency.Name));
        }
    }

    public void closeBenePopup() {
        if(!String.isEmpty(invoice.Beneficiary_GP_ID__c)) {
            Network__c relatesNetwork = null;
            Global_Pay_ID_Management__c gPIdM = null;
            for(Global_Pay_ID_Management__c gPId : [SELECT Global_Pay_ID__c,Creation_Type__c,User_Beneficiary__c,User_Beneficiary__r.Email,User_Beneficiary__r.ContactId,User_Beneficiary__r.Contact.AccountId FROM Global_Pay_ID_Management__c WHERE Global_Pay_ID__c=:invoice.Beneficiary_GP_ID__c]) {
                gPIdM = gPId;
                break;
            }
            if(gPIdM!=null) {
                for(Network__c networks : [SELECT Id FROM Network__c WHERE (Account_Invitee__c=:gPIdM.User_Beneficiary__r.Contact.AccountId AND Account_Inviter__c=:Utility.currentAccount) OR 
                                          (Account_Inviter__c=:gPIdM.User_Beneficiary__r.Contact.AccountId AND Account_Invitee__c=:Utility.currentAccount)]) {
                    relatesNetwork = networks;
                    break;
                }
                if(String.isEmpty(invoice.Beneficiary_GP_Email_Address__c)) {
                    invoice.Beneficiary_GP_Email_Address__c = gPIdM.User_Beneficiary__r.Email;
                }
            }
            if(relatesNetwork!=null) {
               invoice.Invite_to_my_network__c = false;
               beneInNetwork = true; 
            }
        }
        isDisplayBenePopUp = false;
     }
     public void showBenePopUp() {
        isDisplayBenePopUp = true;
     }

     private void saveInvoice() {
        isDisplayBenePopUp = false;
        hasError = false;
        invoice.Status__c = 'Draft';
        invoice.Initiated_By__c = 'Buyer';
        /*if(invoice.Name == null || invoice.Name == '') {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.CM_Validation_Input_InvoiceNumberShouldNotBeBlank));
            hasError = true;
        }*/
        if(invoice.Due_Date__c == null) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.CM_Validation_Input_FieldPaymentDateShouldNotBeBlank));
            hasError = true;
        }
        if(invoice.Amount__c == null ) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.CM_Validation_Input_FieldAmountShouldNotBeBlank));
            hasError = true;
        }
        if(invoice.Custom_Currency__c == '' || invoice.Custom_Currency__c == 'Select' || invoice.Custom_Currency__c == null) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.CM_Validation_Input_FieldSelectedCurrencyIsNotValid));
            hasError = true;
        }
        if(recordTypeName == cmpAdmin.CMP_GP_Default_Invoice_Record_Type__c && invoice.Invite_to_my_network__c && String.isEmpty(invoice.Beneficiary_GP_Email_Address__c)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.CM_Error_Message_Invoice_Email_Required));
            hasError = true;
        }
        if(uploadedAttachment.ContentFileName!=null && !String.isBlank(uploadedAttachment.ContentFileName)) {
                String filetype = uploadedAttachment.ContentFileName.subString(uploadedAttachment.ContentFileName.lastIndexOfIgnoreCase('.')+1);
                if((allowedFileType==null || String.isBlank(allowedFileType)) || !allowedFileType.containsIgnoreCase(filetype)) {
                	ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.CM_Alert_File_InvalidFileFormat));
            		hasError = true;
                }
        }
        if(!hasError) {
            invoice.Account__c = Utility.currentAccount;
            invoice.Buyer__c = UserInfo.getUserId();
            insert invoice;
            if(uploadedAttachment.ContentFileName!=null && !String.isBlank(uploadedAttachment.ContentFileName)){
            	uploadedAttachment.Title = uploadedAttachment.ContentFileName;
                uploadedAttachment.Body = uploadedAttachment.ContentFileName;
                uploadedAttachment.Type ='ContentPost';
                uploadedAttachment.Visibility = 'AllUsers';
                uploadedAttachment.ParentId = invoice.Id;
                insert uploadedAttachment;
            }
            /*if(!posts.isEmpty()) {
                for(FeedItem att : posts) {
                    att.ParentId = invoice.Id;
                }
                insert posts;
            }*/
        }
     }

     public PageReference redirectToHome(){
        if(isSaveClicked){
            returnurl = new PageReference (CMP_Administration__c.getInstance().CM_Community_BaseURL__c+'/Edge_Invoices?Id='+invoice.Id);
            return returnurl;
        }
        else{
            isSaveClicked = false;
            isInvSaved = false;
            return null;
        }
     }
     public PageReference save() {
        Savepoint sp = Database.setSavepoint();
        try {
            saveInvoice();
            isSaveClicked = true;
            isInvSaved = true;
           // if(invoice.Id!=null)
	           // return returnurl;
        } catch(DMLException ex) {
        	Database.rollback( sp );
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getdmlMessage(0)));
        } catch (Exception ex) {
            Database.rollback( sp );
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.CM_Alert_UnableToSaveInvoice + ex.getMessage()));
        }
        return null;
     }

     public PageReference saveNew() {
        Savepoint sp = Database.setSavepoint();
        try {
            saveInvoice();
            isInvSaved = true;
            invoice = new Invoice__c(RecordTypeId=recordTypeId);
            uploadedAttachment = new FeedItem();
        } catch(DMLException ex) {
        	Database.rollback( sp );
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getdmlMessage(0)));
        } catch (Exception ex) {
            Database.rollback( sp );
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.CM_Alert_UnableToSaveInvoice + ex.getMessage()));
        }
        return null;
     }
     /*
     public PageReference attachmentUpdate() {
        try {
            if(uploadedAttachment.ContentFileName!=null && !String.isBlank(uploadedAttachment.ContentFileName)) {
                String filetype = uploadedAttachment.ContentFileName.subString(uploadedAttachment.ContentFileName.lastIndexOf('.')+1);
                if((allowedFileType==null || String.isBlank(allowedFileType)) || allowedFileType.indexOf(filetype)!=-1) {
                    FeedItem fI = new FeedItem();
                    fI.Title = uploadedAttachment.ContentFileName;
                    fI.Body = uploadedAttachment.ContentFileName;
                    fI.Type ='ContentPost';
                    fI.ContentData = uploadedAttachment.ContentData;
                    fI.ContentFileName = uploadedAttachment.ContentFileName;
                    fI.Visibility = 'AllUsers';
                    posts.add(fI);
                    uploadedAttachment = new FeedItem();
                }else {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.CM_Alert_File_InvalidFileFormat));
                }
            }
        }catch(Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error in Attachment' + ex.getMessage()));
        }
        return null;
     }

     public PageReference delAttachment() {
     	if(posts!=null && posts.size()>0 && deleteIndex<posts.size()) {

        posts.remove(deleteIndex);
     	}
        return null;
     }*/
}