/**=====================================================================
 * Name: EDGE_Payment_ApprovalsCtrl
 * Description: Controller class for EDGE_Payment_Approvals page
 * Created Date: 25th Jun'16
 * Created By: Nikhil Sharma
 =====================================================================*/

 public without sharing class EDGE_Payment_ApprovalsCtrl{
    public Boolean isPaymentApprovals{
		get{
			return Utility_Security.isPaymentApprovals;
		}
	}
	
	public pageReference redirectToHome(){
	    if(!isPaymentApprovals){
	        pageReference pgref = new pageReference('/edge_activity_dashboard');
	        pgref.setRedirect(true);
	        return pgref;
	    }return null;
	}
 }