/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_CaptureinvoiceController_Test
 * Description: Controller Test class for EDGE_CaptureinvoiceController
 * Created Date: 01 Apr 2016
 * Created By: Nikhil Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
@isTest
private class EDGE_CaptureinvoiceController_Test {

	private static testMethod void test() {
        test_Utility.createWUEdgeSharingAdmin();
	    Invoice__c parentInv = test_Utility.createInvoice(true);
        test_Utility.createCMPAdministrationWithInvoice(parentInv.Id);
		test_Utility.createCMPAlert();
		test_Utility.createWubsIntAdministration();
		test_Utility.createGPH2HCurrencies();
		test_Utility.createGPIntegrationAdministration();
		test_Utility.currencyISOMapping();
		
		CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.CCT_Client_ID__c = '3232112';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount.CMP_Create_Transactions__c = true;
        beneAccount.CMP_Enabled__c = true;
        beneAccount.CMP_Holding_Enabled__c = true;
        beneAccount.CMP_H2H_Transaction_Enabled__c = true;
        beneAccount.BillingStreet = 'testStreet';
        beneAccount.BillingCity = 'city';
        beneAccount.BillingState = 'state';
        beneAccount.BillingCountry = 'USA';
        beneAccount.BillingPostalCode = '231231';
        beneAccount.Billing_Address_ISO_Country_Code__c = 'USA';
        insert beneAccount;
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact;
        Supplier__c supp = test_Utility.createSupplier(beneAccount.Id,beneAccount.id,false);
        supp.Supplier_Name__c = 'Test';
        supp.Address_line_1__c = 'Test';
        supp.Address_Line_2__c = 'Test';
        supp.City__c = 'Test';
        supp.State_Province__c = 'Test';
        supp.Post_Code__c = 'Test';
        insert supp;
        
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
		commUser.timezonesidkey='America/Indiana/Indianapolis';
		commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        commUser.CMP_Network_Invitation_Enabled__c = true;
        commUser.CMP_Create_Transactions__c = true;
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        insert commUser;    
        
        ApexPages.currentPage().getParameters().put('retUrl','/testRetUrl');
        ApexPages.currentPage().getParameters().put('Referer','/testReferer');
        
        Global_Pay_ID_Management__c gPIM = new Global_Pay_ID_Management__c(Global_Pay_ID__c='12345',
                                                                               User_Beneficiary__c=commUser.Id,
                                                                               Creation_Type__c='Automatic Email Match');
        insert gPIM;
        
        Invoice__c invoice = test_Utility.createInvoice(false);
        invoice.RecordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('GP Invoice - Active').getRecordTypeId();
        invoice.Custom_Currency__c = 'USD';
        invoice.Invoice_Number__c = 'INVTest-123';
        invoice.Status__c = 'Draft';
        invoice.Initiated_By__c='Buyer';
        //invoice.Due_Date__c = Date.today().addDays(30);
        //invoice.Amount__c = 2000;
        invoice.Account__c = beneAccount.Id;
        invoice.Buyer__c = commUser.Id;
        invoice.Supplier__c = supp.id;
        insert invoice;
        FeedItem feedPost = new FeedItem();
        feedPost.ParentId = invoice.Id; //eg. Opportunity id, custom object id..
        feedPost.ContentData = Blob.valueof('Test data for pdf file');
        feedPost.Title = 'test.txt';
        feedPost.Body = feedPost.Title;
        feedPost.ContentFileName = feedPost.Title;
        feedPost.Visibility = 'AllUsers';
        insert feedPost;
        
        
        System.RunAs(commUser){
        	Test.startTest();
            ApexPages.currentPage().getParameters().put('id',EncryptionManager.doEncrypt(invoice.id));
        		EDGE_CaptureinvoiceController ctrl = new EDGE_CaptureinvoiceController();
        		ctrl.deleteAttachmentFeed();
        		ctrl.updateCurrencyList();
        		ctrl.getdelTypes();
        		ctrl.getopenPresentationOptions();
        		ctrl.isSaveAndAddBeneClicked = false;
        		ctrl.isSaveAndPayClicked = false;
        	    ctrl.save();
    			ctrl.saveAndPay();
        	    ctrl.saveAndAddNew();
        	    ctrl.selectedSuppId = supp.id;
        	    ctrl.saveAndAddBene();
        	    ctrl.invoice = invoice;
        		ctrl.invoice = null;
        		System.assert(true, ' '+ ctrl.isDisplayBenePopUp +  ctrl.recordTypeId +''+ ctrl.deleteIndex + ctrl.isUserH2H + ctrl.beneNumber + ctrl.deliveryType);
        		ctrl = new EDGE_CaptureinvoiceController();
                ctrl.invoice.Beneficiary_GP_ID__c = '12345';
                //EDGE_CaptureinvoiceController.BeneinNetwork('12345',beneAccount.Id);
        		ctrl.invoice.Delivery_Method__c = 'Holding-to-Holding';
        		ctrl.hbCurrencyISO = 'USD';
        		ctrl.beneName = 'testBene';
        		ctrl.beneIdH2H = '3232112';
        		ctrl.invoice.Due_Date__c = System.Today().addDays(10);
        		ctrl.invoice.Amount__c = 111;
        		ctrl.recordTypeName = 'GP Invoice - Active';
        		ctrl.invoice.Invite_to_my_network__c = true;
        		ctrl.invoice.Beneficiary_GP_Email_Address__c = 'testdfs@dfdlds.com';
        		ctrl.invoice.Beneficiary_GP_ID__c = '3232112';
        		ctrl.filename = 'testFile.docx';        		
        		//ctrl.save();
        		ctrl.fileName = 'test.docx';
        		ctrl.allowedFileType = '.docx';
        		ctrl.fileId = feedPost.Id;
                ctrl.fileData = Blob.valueof('myString test string');
                ctrl.uploadAttachmentFeed();
               //  ctrl.uploadedAttachment = feedPost;
                ctrl.saveNew();
                ctrl = new EDGE_CaptureinvoiceController();
                ctrl.deleteAttachmentFeed();
                ctrl.updateCurrencyList();
        		ctrl.invoice.Beneficiary_GP_ID__c = '12345';
                ctrl.invoice.Delivery_Method__c = 'Standard Payment';
                ctrl.invoice.Due_Date__c = System.Today().addDays(10);
                ctrl.invoice.Amount__c = 111;
                ctrl.recordTypeName = 'GP Invoice - Active';
                ctrl.invoice.Invite_to_my_network__c = true;
                ctrl.invoice.Beneficiary_GP_Email_Address__c = 'testdfs@dfdlds.com';
                ctrl.invoice.Beneficiary_GP_ID__c = '12345';
        		ctrl.save();
        		ctrl.invoice.Beneficiary_GP_ID__c = '12345';
            	System.assertEquals([Select id,balance__c from invoice__C where id= :invoice.id][0].balance__c,111);
        	Test.stopTest();
        }
	}

}