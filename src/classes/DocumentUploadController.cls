//
// (c) 2014 Appirio, Inc.
//
// DocumentUploadController
// Used as controller for DocumentEdit and DocumentUpload page.
//
// May 17, 2015    Ashish Goyal  			Original (Ref. T-393811)
public without sharing class DocumentUploadController {

	public Account_Document__c accDoc {get; set;}
	public DocumentWrapper accDocWrap {get; set;}
	public Attachment attach{get;set;}
	public String retURL;
	public String accId{get;set;}
	public List<AttachmentWrapper> listChildAttachments {get;set;}
	public String dealer {get; set;}
	public String accountName {get; set;}
	public String accountDetailPageURL {get; set;}
	public String tabName {get; set;}
	public String accountDocName {get; set;}
	public Boolean isSufficient {get; set;}


	public String localeDate{
        get{
            Utility utility = new Utility();
            return utility.localeDate;
        }
    }

	public String localeDateRegx{
        get{
            String dtStr = localeDate.replace('yyyy', '(\\d{2,4})');
            dtStr = dtStr.replace('mm', '(\\d{1,2})\\');
            dtStr = dtStr.replace('dd', '(\\d{1,2})\\');
            return '/'+dtStr+'( (\\d{1,2}):(\\d{1,2}))? ?(am|pm|AM|PM|Am|Pm)?/';
        }
    }


	// @description : Constructor of class
	// @param : none
	// @return : none
	public DocumentUploadController(){
		accId = ApexPages.currentPage().getParameters().get('aId');
		accountDetailPageURL = '/apex/AccountOverrideViewDetail?id='+accId+'&accOwn=&accFil=&tabSel=detailTab&tabEdit=&userType=CRM';
		accountName = '';
		tabName = '';
		attach = new Attachment();
		isSufficient = false;
		listChildAttachments = new List<AttachmentWrapper>();
		String docId = ApexPages.currentPage().getParameters().get('Id');
		tabName = ApexPages.currentPage().getParameters().get('tab');
		retURL = ApexPages.currentPage().getParameters().get('retURL');

		if(!String.isEmpty(docId)){
			docId = EncryptionManager.doDecrypt(docId);
			for(Account_Document__c accDocc  : [SELECT Id,Name, Name__c, Account__c,Account__r.Name, CreatedDate, CreatedBy.Name, LastModifiedDate,
							CreatedById, Owner.Name, RecordTypeId, RecordType.Name, Edge_Account_Document_Link__c
							from Account_Document__c
							WHERE Id =: docId order by CreatedDate desc limit 1]){
					accDoc = accDocc;
							}
			findAttachments();
			User tUser = [SELECT Id, ContactId, Contact.AccountId FROM User Where Id =: UserInfo.getUserId()];
			accountName = accDoc.Account__r.Name;
			accDocWrap = new DocumentWrapper(accDoc.Name__c,accDoc.Owner.Name,accDoc.CreatedDate,accDoc.LastModifiedDate,accDoc.Edge_Account_Document_Link__c);
			accountDocName = accDoc.Name__c;
			if(tUser.Contact.AccountId != accDoc.Account__c){
				isSufficient = true;
			}
		}
		else{
			accDoc = new Account_Document__c();
		}
		system.debug('===================================='+ accId);
		if(accId == null){
			accId = Utility.currentAccount;
			List<Account> accList = new List<Account>([SELECT OwnerId from Account Where Id =:Utility.currentAccount]);
			if(accList.size() > 0){
				dealer = accList.get(0).OwnerId;
			}
		}
		// accDoc.Account__c = accId;

	}


	// @description : Save the document and associated attachments
	// @param : none
	// @return : PageReference

	public pagereference saveDoc(){
		accDoc.Account__c = accId;
		insert accDoc;
		if(attach.Name != null){
			attach.ParentId = accDoc.Id;
			insert attach;
		}
		String sitePrefix = Site.getPathPrefix();
		pagereference pageref;
		if(String.IsEmpty(sitePrefix)){
			pageref = new pagereference('/apex/AddAnotherDocument?aId='+accId);
		}else{
			pageref = new pagereference(Site.getPathPrefix()+'/AddAnotherDocument');
		}
		// pagereference pageref = new pagereference('/apex/AddAnotherDocument');
		pageref.setRedirect(true);
		return pageRef;
	}


	// @description : Update the document info and Attachments related to document record
	// @param : none
	// @return : PageReference

	public pagereference updateDoc(){
		accDoc.Name__c = accountDocName;
		update accDoc;
		String strSite = Site.getPathPrefix();
		PageReference pg;

		if(retURL != null){
			if(String.IsEmpty(strSite)){
		 		retURL = retURL+'&tab=doc';
		 	}else{
		 		retURL = retURL+'&tab=docOut';
		 	}
			pg = new PageReference(retURL);
		}else{
		 	String pageName;
		 	if(String.IsEmpty(strSite)){
		 		pageName = '/apex/AccountDocument?aId='+accDoc.Account__c+'&tab=doc';
		 	}else{
		 		pageName = strSite+'/EDGE_Docuemnt_collaboration?tab=docOut';
		 	}
			pg = new PageReference(pageName);
		}
		return pg;
	}


	//get all the child attachment records
	 private void findAttachments(){
	 	 listChildAttachments = new List<AttachmentWrapper>();
	     for(Attachment attach: [SELECT Id, Name, LastModifiedById, LastModifiedDate, CreatedDate, CreatedById,CreatedBy.Name, ContentType FROM Attachment
	             WHERE ParentId = :accDoc.id order by CreatedDate desc ]){
	             	listChildAttachments.add(new AttachmentWrapper(attach));
	             }
	 }


	 // @description : Delete the existing attachment and create a new one
	// @param : none
	// @return : PageReference

	 public PageReference createNewAttachment(){
	 	String strSite = Site.getPathPrefix();
	 	String pageName;
	 	if(String.IsEmpty(strSite)){
	 		pageName = 'AccountDocumentView';
	 	}else{
	 		pageName = 'AccountDocumentCommView';
	 	}
	 	/*
	 	List<Attachment> listAttachment = new List<Attachment>([SELECT Id from Attachment WHERE ParentId =:accDoc.Id]);
	 	if(listAttachment.size() > 0){
	 		delete listAttachment;
	 	}
	 	*/
	 	if(String.IsEmpty(strSite)){
	    	return new PageReference ('/p/attach/NoteAttach?pid='+accDoc.id+'&retURL=%2Fapex%2F'+pageName+'?Id='+accDoc.Id+'&retURL=%2Fapex%2FAccountDocument?aId='+accDoc.Account__c);
	 	}else{
	    	return new PageReference ('/AttachmentUpload?pid='+accDoc.id+'&retURL=%2Fapex%2F'+pageName+'?Id='+accDoc.Id);
	 	}
	 }


	// @description : When user click on cancel, redirect according to User(Community or Dealer)
	// @param : none
	// @return : PageReference

	public pagereference cancel(){
		PageReference pg;
		String strSite = Site.getPathPrefix();
		if(retURL != null){
			if(!String.IsEmpty(strSite))
				retURL = retURL +'&tab='+tabName;
			pg = new PageReference(retURL);
		}else{

		 	String pageName;
		 	if(String.IsEmpty(strSite)){
		 		pageName = '/apex/AccountDocument?aId='+accDoc.Account__c;
		 	}else{
		 		pageName = strSite+'/EDGE_Document_collaboration';
		 	}
			pg = new PageReference(pageName);
		}
		return pg;
	}

	public String checkBoxOutgoingHtml{
		get{
			return '<div><input type="checkbox" id="outputChk" onclick="doToggleDelete(this,\'deleteInputOutgoingChk\');"/>Action</div>';
		}
	}

	// @description : delete the account document record
	// @param : none
	// @return : void

	public void customDelete() {
	 	try{
	 		List<Attachment> accDocObjList = new List<Attachment>();

	 		for(AttachmentWrapper docWrap : listChildAttachments){
	 			if(docWrap.isSelected){
	 				system.debug('-----------------------------------------------------');
	 				accDocObjList.add(docWrap.accountDoc);
	 			}
	 		}
	 		if(accDocObjList.size() > 0){
	 			delete accDocObjList;
	 			system.debug('-----------------------------------------------------');
	 		}
	 		findAttachments();
	 	}
	 	catch(Exception ex) {
	 		system.debug('--------error------'+ex.getMessage());
	 	}
	 }

	// @description : Wrapper class
	// @param : none
	// @return : none
	public class AttachmentWrapper{
		public Attachment accountDoc {get; set;}
		public Boolean isSelected {get; set;}
		public String createdDate {get ; set;}

		public AttachmentWrapper(Attachment accountDoc){
			this.accountDoc = accountDoc;
			this.isSelected = false;
			this.createdDate = accountDoc.createdDate.date().format();
		}
	}


	public class DocumentWrapper{
		public String attachmentURL {get; set;}
		// public Account_Document__c accountDoc {get; set;}
		public Boolean isSelected {get; set;}
		public DateTime createdDate {get; set;}
		public DateTime modifiedDate {get; set;}
		public String docName {get; set;}
		public String docCreatedBy {get; set;}
		public String documentlink {get;set;}

		public DocumentWrapper(String docName, String docCreatedBy, DateTime createdDate, DateTime modifiedDate, String documentlink){
			//this.attachmentURL = attachmentURL;
			this.docName = docName;
			this.createdDate = createdDate;
			this.modifiedDate = modifiedDate;
			this.docCreatedBy = docCreatedBy;
			this.documentlink = documentlink;
			// this.isSelected = false;
		}
	}

}