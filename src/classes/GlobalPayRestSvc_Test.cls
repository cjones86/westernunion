/**=====================================================================
 * Appirio, Inc
 * Name: GlobalPayRestSvc_Test
 * Description: Test class for GlobalPayRestSvc
 * Created Date: 06 Apr 2016
 * Created By: Nikhil Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/ 
@isTest 
private class GlobalPayRestSvc_Test { 
    static Global_Pay_ID_Management__c gPIM =null;
    static Invoice__c invoice  = null;
    private static testMethod void globalUpdateOrDeleteBene() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/GlobalPay';  
        req.httpMethod = 'PATCH';
        createTestData();
        
        String reqJson = '{"gp.type":"UpdateBene","gp.beneficiaryId":"'+gPIM+'","ToValue":"reset"}';
        req.requestBody = Blob.valueOf(reqJson);
        RestContext.request = req;
        RestContext.response = res;
        String result  = GlobalPayRestSvc.doPatch();
        //system.assertEquals( 'reset', [select id , Status__c, Beneficiary_GP_Name__c from Invoice__c where id=:invoice.id].Beneficiary_GP_Name__c);
        
        String reqJson2 = '{"gp.type":"DeleteBene","gp.beneficiaryId":"'+gPIM+'"}';
        req.requestBody = Blob.valueOf(reqJson2);
        RestContext.request = req;
        RestContext.response = res;
        String result2  = GlobalPayRestSvc.doPatch();
        //system.assertEquals( 'Pending Review', [select id , Status__c, Beneficiary_GP_Name__c from Invoice__c where id=:invoice.id].Status__c);
    }
    private static testMethod void GlobalPayService_H2HConfirmation(){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/GlobalPay';  
        req.httpMethod = 'PATCH';
        createTestData();
        
        String reqJson = '{"amount":"121","gp.beneficiaryId":"'+gPIM+'","gp.totalAmount":"323"'+
        ',"gp.settlementCurrency":"USD","currency":"INR","gp.lineItemId":"424", "gp.orderId":"ewew112"'+
    	',"gp.paymentAmount":"100", "gp.paymentCurrency":"USD", "gp.paymentReference":"2332", "gp.settlementMethod":"POST"'+
    	',"gp.type":"H2HConfirmation", "gp.orderCommittedDate":"2016-04-12T13:27:18.000Z", "objectId":"'+invoice.id+'"}';
        req.requestBody = Blob.valueOf(reqJson);
        RestContext.request = req;
        RestContext.response = res;
        String result  = GlobalPayRestSvc.doPatch();
        //system.assertEquals( 'reset', [select id , Status__c, Beneficiary_GP_Name__c from Invoice__c where id=:invoice.id].Beneficiary_GP_Name__c);
    	
    }   
    private static testMethod void GlobalPayService_OrderConfirmation(){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/GlobalPay';  
        req.httpMethod = 'PATCH';
        createTestData();
        
        String reqJson = '{"amount":"121","gp.beneficiaryId":"'+gPIM.Id+'","gp.totalAmount":"323"'+
        ',"gp.settlementCurrency":"USD","currency":"INR","gp.lineItemId":"424", "gp.orderId":"ewew112"'+
    	',"gp.paymentAmount":"100", "gp.paymentCurrency":"USD", "gp.paymentReference":"2332", "gp.settlementMethod":"POST"'+
    	',"gp.type":"OrderConfirmation", "gp.orderCommittedDate":"2016-04-12T13:27:18.000Z", "objectId":"'+invoice.id+'"'+
    	',"gp.beneficiaryCity":"jaipur","gp.beneficiaryCountry":"USA","gp.beneficiaryState":"RAJ", "gp.beneficiaryName":"hami"'+
    	',"gp.beneficiaryPostCode":"232123","gp.beneficiaryStreetAddress":"down street","gp.notesToBene":"bene Me"'+
    	',"gp.beneficiaryEmail":"jjdd@gmail.com", "gp.accountNumber":"121","gp.orderStatus":"COMMITTED"}';

        req.requestBody = Blob.valueOf(reqJson);
        RestContext.request = req;
        RestContext.response = res;
        String result  = GlobalPayRestSvc.doPatch();
        //system.assertEquals( 'reset', [select id , Status__c, Beneficiary_GP_Name__c from Invoice__c where id=:invoice.id].Beneficiary_GP_Name__c);
    	
    }   
     
    static void createTestData(){
        
        test_Utility.createCMPAdministration();
        test_Utility.createCMPAlert();
        test_Utility.globalPayOpenInvoiceStatuses(); 
        test_Utility.createWUEdgeSharingAdmin();
        CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        insert beneAccount;
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact;
        Account beneAccount1 = test_Utility.createAccount(false);
        beneAccount1.RecordTypeId=accountRecordType;
        beneAccount1.ExternalId__c='12345';
        beneAccount1.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        insert beneAccount1;
        Contact newContact1 = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact1;
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
        commUser.timezonesidkey='America/Indiana/Indianapolis';
        commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        commUser.CommunityNickname = commUser.alias;
        insert commUser;     
        invoice = test_Utility.createInvoice(false);
        invoice.RecordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('GP Invoice - Active').getRecordTypeId();
        invoice.Custom_Currency__c = 'USD';
       // invoice.Name = 'INVTest-123';
        invoice.Status__c = 'Accepted';
        invoice.Initiated_By__c='Seller';
        invoice.Due_Date__c = Date.today().addDays(30);
        invoice.Amount__c = 2000;
        invoice.Account__c = Utility.currentAccount;
        invoice.Buyer__c = commUser.Id;
        insert invoice;        
        system.debug('****invoice:'+invoice.id);
        if (commUser.Id != null) { 
            gPIM = new Global_Pay_ID_Management__c(
                Global_Pay_ID__c='12345',
                Source_Invoice_ID__c=invoice.id,
                User_Beneficiary__c=commUser.Id,
                Creation_Type__c='Automatic Email Match');
           insert gPIM;
        }

    }

}