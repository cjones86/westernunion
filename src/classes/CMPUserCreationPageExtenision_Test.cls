@isTest
public class CMPUserCreationPageExtenision_Test {
    static String contactId;
    static String accountId;
    static User usr;
    static testMethod void UnitTest() {
        createTestData();
        Test.startTest();
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(usr);
        CMPUserCreationPageExtenision ctrl = new CMPUserCreationPageExtenision(stdCtrl);
        
        ApexPages.currentPage().getParameters().put('contactId', contactId);
        ApexPages.StandardController stdCtrl1 = new ApexPages.StandardController(usr);
        CMPUserCreationPageExtenision ctrl1 = new CMPUserCreationPageExtenision(stdCtrl1);
        ctrl1.custSave();
        PageReference pref1 = ctrl1.custCancel();
        ctrl1.setPassword();   
        System.assertEquals(new PageReference('/'+contactId).getURL() , pref1.getURL());
        
        ApexPages.currentPage().getParameters().put('Id', usr.Id);
        ApexPages.StandardController stdCtrl2 = new ApexPages.StandardController(usr);
        CMPUserCreationPageExtenision ctrl2 = new CMPUserCreationPageExtenision(stdCtrl2);
        ctrl2.custSave();
        PageReference pref2 = ctrl2.custCancel();
        ctrl2.setPassword();   
        System.assertEquals(new PageReference('/'+usr.ContactId).getURL() , pref2.getURL());
        
        Test.stopTest();
                
        
    }
    
    private static void createTestData() {
        Account newAccount = new Account(Name = 'Test Account', OwnerId = UserInfo.getUserId());
        insert newAccount;
        Contact newContact = new Contact(AccountId = newAccount.Id, LastName = 'Test Contact',FirstName='test', Email='test@test1243.com');
        insert newContact;
        contactId = newContact.Id;
        User testUser = new User();
         //testUser = new User();
        testUser.alias = 'testuser';
        testUser.Email = 'testuser@test123.com';
        testUser.EmailEncodingKey = 'ISO-8859-1';
        testUser.LanguageLocaleKey = 'en_US';
        testUser.LastName = 'Test User567';
        testUser.LocaleSidKey = 'en_AU';
        testUser.Account_Administrator__c = false;
        testUser.ProfileId = [SELECT Id FROM Profile WHERE Name LIKE 'Cash Management Customers - CCT' LIMIT 1].Id;
        testUser.TimeZoneSidKey = 'Australia/Sydney';
        testUser.UserName = 'testuser@travelex.com.au';
        testUser.ContactId = newContact.Id;
        testUser.DefaultCurrencyIsoCode = 'INR';
        insert testUser;
        usr = testUser;
        
        CMPUserFieldDefaultValue__c defaultVal1 = new CMPUserFieldDefaultValue__c(Name = 'emailencodingkey', Default_Value__c= 'UTF-8');
        CMPUserFieldDefaultValue__c defaultVal2 = new CMPUserFieldDefaultValue__c(Name = 'languagelocalekey', Default_Value__c= 'en_US');
        CMPUserFieldDefaultValue__c defaultVal3 = new CMPUserFieldDefaultValue__c(Name = 'localesidkey', Default_Value__c= 'en_US');
        CMPUserFieldDefaultValue__c defaultVal4 = new CMPUserFieldDefaultValue__c(Name = 'timezonesidkey', Default_Value__c= 'America/Indiana/Indianapolis');
        List<CMPUserFieldDefaultValue__c> listDefaultVals = new List<CMPUserFieldDefaultValue__c> {defaultVal1,defaultVal2,defaultVal3,defaultVal4};
        insert listDefaultVals;
    }
}