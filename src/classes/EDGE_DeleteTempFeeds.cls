/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_DeleteTempFeeds
 * Description: Batch class to delete default feeds from invoice
 * Created Date: 3 Mar' 2016
 * Created By: Rohit Sharma (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
global without sharing class EDGE_DeleteTempFeeds implements Database.Batchable<sObject> {

  static apexLogHandler.apexLog logExecute = new apexLogHandler.apexLog('EDGE_DeleteTempFeeds','execute');
  
  global Database.QueryLocator start(Database.BatchableContext BC){
      CMP_Administration__c cmpAdmin = CMP_Administration__c.getInstance();
      return Database.getQueryLocator('SELECT Id,Type,RelatedRecordId FROM FeedItem WHERE ParentId=\''+cmpAdmin.EDGE_Default_Feed_Invoice__c+'\'');
  }  

  global void execute(Database.BatchableContext BC, List<sObject> scope){
      logExecute.logMessage('>>inside execute method ...>> ');
      //delete (new ContentDocument(Id='069n0000000A1OnAAK'));
      List<FeedItem> feeds = new List<FeedItem>();
      String relatedRecords ='';
      For(FeedItem feedI : (List<FeedItem>)scope){
        if(feedI.Type=='ContentPost')
             relatedRecords = relatedRecords+'\''+feedI.RelatedRecordId+'\',';
        feeds.add(feedI);
      }
      relatedRecords = relatedRecords.removeEnd(',');
      //system.debug(' attachment : '+Database.query('SELECT Id,RecordId FROM FeedAttachment WHERE FeedEntityId IN ('+relatedRecords+')'));
      
      system.debug('relatedRecords : '+relatedRecords);
      logExecute.logDebug('relatedRecords : '+relatedRecords);
      List<ContentDocument> relatedDocument = new List<ContentDocument>();
      if(relatedRecords!='') {
          for(ContentVersion cV : Database.query('SELECT Id,ContentDocumentId FROM ContentVersion WHERE Id IN ('+relatedRecords+')')) {
              relatedDocument.add(new ContentDocument(Id=cV.ContentDocumentId));
          }
          system.debug('relatedDocument : '+relatedDocument);
          logExecute.logDebug('relatedDocument : '+relatedDocument);
          if(relatedDocument.size()>0)
          delete relatedDocument;
      }
      if(feeds.size()>0)
        delete feeds;
      
      logExecute.saveLogs();
  }
  
  global void finish(Database.BatchableContext BC){
   
  }
}