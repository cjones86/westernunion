public with sharing class Edge_Activity_DashboardCtrl {

    public DateTime lastlogin{get;set;}

    public Boolean canShowHoldingBalance{
        get{
            return Utility_Security.canShowHoldingBalance; 
        }
    }

    Public Edge_Activity_DashboardCtrl(){
        User u = [SELECT LastLoginDate FROM User WHERE Id =:UserInfo.getUserId()];
        lastlogin = u.LastLoginDate;
    }
}