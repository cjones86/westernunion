/**=====================================================================
 * Appirio, Inc
 * Name: CMP_SubmittedInvoiceDetailController
 * Description: Controller with business logic for Buyer user to Accept/Reject Invoice
 * Created Date: 12 Feb' 2016
 * Created By: Rohit Sharma (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
 public without sharing class CMP_SubmittedInvoiceDetailController {

 	public Invoice__c invoiceRecord {get;set;}
 	public Boolean displayRejectPopup {get; set;}
 	public string rejectReason{get;set;}
 	public List<FeedItem> contentPost{get;set;}
 	public FeedItem feedPost{get;set;}
 	public String latestContentId{get;set;}
 	public boolean canEdit{get;set;}
 	public boolean showActions{get;set;}
 	public string selectFeed{get;set;}
 	public string feedId{get;set;}

 	public CMP_SubmittedInvoiceDetailController() {
 		if(ApexPages.currentPage().getParameters().get('id') != null && ApexPages.currentPage().getParameters().get('id') != ''){
            for(Invoice__c tmpInv : getInvoice(EncryptionManager.doDecrypt(ApexPages.currentPage().getParameters().get('id')))){
                this.invoiceRecord = tmpInv;
        	}
 		}
 		canEdit = false;
		if(invoiceRecord.OwnerId == UserInfo.getUserId() || invoiceRecord.OwnerId == Utility.currentAccountOwner){
        	canEdit = true;
        }
        showActions = false;
        if((invoiceRecord.Status__c=='Invoice Submitted' || invoiceRecord.Status__c=='Rejected') && Utility_Security.canCreateTransaction && invoiceRecord.Account__c == Utility.CurrentAccount) {
        	showActions = true;
        }
 		fillContentPost();
 		feedPost = new FeedItem();
 	}

 	public void closeRejectPopup() {
    	displayRejectPopup = false;
    }

    public void showRejectPopup() {
    	displayRejectPopup = true;
        Pagereference pref = new Pagereference('?id='+invoiceRecord.Id);
    }

 	// Get fields from fieldset
    public List<Schema.FieldSetMember> getFields() {
        return SObjectType.Invoice__c.FieldSets.Detail.getFields();
    }

    // GetInvoice with all the fields in the fieldset
    private List<Invoice__c> getInvoice(String id) {
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : this.getFields()) {
            query += f.getFieldPath() + ', ';
        }
        query += 'Name,Id,Initiated_By__c,Account__c, Currency_Code__c, Input_Beneficiary__r.Name, Input_Beneficiary__r.OwnerId, Input_Beneficiary__r.Buyer_Supplier_Number__c,Buyer__r.ContactId,Buyer__r.Id FROM Invoice__c Where Id =: id LIMIT 1';
        return Database.query(query);
    }

	//NS: 9th Feb'16 (T-470007)
     public Pagereference accept(){
        try{
            invoiceRecord.Status__c = 'Acknowledged';
            update invoiceRecord;
            String encodedInvoiceId = Utility.doEncryption(invoiceRecord.Id);
            PageReference returnurl = new PageReference('/EDGE_InvoiceDetail2?Id='+encodedInvoiceId+'&edit=true');
         	return returnurl;
        }catch(Exception ex){
            ApexPages.addMessages(ex);
            return ApexPages.currentPage();
        }
     }

     //NS: 9th Feb'16 (T-470007)
     public Pagereference reject(){
        try{
            invoiceRecord.Status__c = 'Rejected';
            update invoiceRecord;
            closeRejectPopup();
            FeedItem fi = new FeedItem();
            fi.ParentId = invoiceRecord.Id;
            fi.Body = System.Label.CM_ReasonToReject+ ' '+rejectReason;
            fi.Visibility = 'AllUsers';
            insert fi;
            showActions = false;
            Pagereference pref = new Pagereference(ApexPages.currentPage().getParameters().containsKey('retUrl')?ApexPages.currentPage().getParameters().get('retUrl'):null);
            pref.setRedirect(true);
            return pref;
        }catch(Exception ex){
            ApexPages.addMessages(ex);
            return ApexPages.currentPage();
        }
     }

     public void fillContentPost() {
     	latestContentId = null;
		contentPost = [SELECT Id,ContentFileName,ContentType,Title,RelatedRecordId,CreatedById,CreatedBy.Name,CreatedDate FROM FeedItem WHERE ParentId=:invoiceRecord.Id AND Type='ContentPost' ORDER BY CreatedDate desc];
		if(contentPost.size()>0) {
			latestContentId = contentPost.get(0).RelatedRecordId;
		}
     }

     public Pagereference uploadAttachment() {
     	try {
        	if(feedPost.Title!=null && !String.isBlank(feedPost.Title)) {
	            feedPost.ParentId = invoiceRecord.Id; //eg. Opportunity id, custom object id..
                feedPost.Body = feedPost.Title + '-'+invoiceRecord.Name;
                feedPost.ContentFileName = feedPost.Title;
                feedPost.Visibility = 'AllUsers';
                insert feedPost;
        	}
        	return redirectURL();
        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
            return null;
        }
     }

     public Pagereference redirectURL() {
     	String encodedInvoiceId = EncryptionManager.doEncrypt(invoiceRecord.Id);
     	PageReference returnurl = new PageReference('/EDGE_Invoice_submitted_Detail?Id='+encodedInvoiceId);
        return returnurl;
     }

     public void viewContent(){
     	latestContentId = selectFeed;
     }

     public void deleteFeed() {
     	if(feedId!=null && !String.isBlank(feedId)) {
     		delete (new FeedItem(Id=feedId));
     	}
     	fillContentPost();
     }
}