/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_InvoiceManagement_Test
 * Description: Controller Test class for BS_ColdPaymentCtrl
 * Created Date: 05 Apr 2016
 * Created By: Nikhil Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
@isTest
private class BS_ColdPaymentCtrl_Test {

  private static testMethod void test() {
        test_Utility.createCMPAdministration();
    test_Utility.createCMPAlert();
    test_Utility.createWubsIntAdministration();
    test_Utility.createGPH2HCurrencies();
    test_Utility.createGPIntegrationAdministration();
    test_Utility.currencyISOMapping();
    
    CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.CCT_Client_ID__c = '3232112';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount.CMP_Create_Transactions__c = true;
        beneAccount.CMP_Enabled__c = true;
        beneAccount.CMP_Holding_Enabled__c = true;
        beneAccount.CMP_H2H_Transaction_Enabled__c = true;
        beneAccount.BillingStreet = 'testStreet';
        beneAccount.BillingCity = 'city';
        beneAccount.BillingState = 'state';
        beneAccount.BillingCountry = 'USA';
        beneAccount.BillingPostalCode = '231231';
        beneAccount.Billing_Address_ISO_Country_Code__c = 'USA';
        insert beneAccount;
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact;
        
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
    commUser.timezonesidkey='America/Indiana/Indianapolis';
    commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        commUser.CMP_Network_Invitation_Enabled__c = true;
        commUser.CMP_Create_Transactions__c = true;
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        insert commUser;    
        
        ApexPages.currentPage().getParameters().put('retUrl','/testRetUrl');
        ApexPages.currentPage().getParameters().put('Referer','/testReferer');
        
        Global_Pay_ID_Management__c gPIM = new Global_Pay_ID_Management__c(Global_Pay_ID__c='12345',
                                                                               User_Beneficiary__c=commUser.Id,
                                                                               Creation_Type__c='Automatic Email Match');
        insert gPIM;
        
        /*Invoice__c inv = new Invoice__c(Beneficiary_GP_ID__c='3232112',Beneficiary_GP_Email_Address__c='testuser@test123.com',
            Delivery_Method__c='Holding-to-Holding');
        inv.Amount__c = 25;
        insert inv;*/
        
        
        
        System.RunAs(commUser){
            
            Test.StartTest();
            BS_ColdPaymentCtrl coldPayment = new BS_ColdPaymentCtrl();
            coldPayment.showPay();
            coldPayment.showBenePopUp();
            coldPayment.closeBenePopUp();
            coldPayment.closePopup();
            String cctClientID = coldPayment.cctClientID;
            coldPayment.invWrp.inv.Delivery_Method__c='Holding-to-Holding';
            coldPayment.invWrp.inv.Beneficiary_GP_ID__c='3232112';
            coldPayment.invWrp.inv.Beneficiary_GP_Email_Address__c='testuser@test123.com';
            //BS_ColdPaymentCtrl.InvoiceWrapper wrap = new BS_ColdPaymentCtrl.InvoiceWrapper(inv);
            //wrap.inv = inv;
            //coldPayment.invWrp = wrap;
            coldPayment.payInvoice();
            Test.StopTest();
            
        }
    }    
  }