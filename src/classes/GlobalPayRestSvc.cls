//Updated By : Ranjeet Singh
//Updated date : 5th April 2016
//Description : T-490096
@RestResource(urlMapping='/GlobalPay/*')
global class GlobalPayRestSvc {

    @HttpPatch

    global static String doPatch() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        System.debug('req:: ' + req);
        System.debug('********** A) Global Class GlobalPayRestSvc body: ' + req.requestBody.toString() + ' *****');
        
        String requestString = req.requestBody.toString(); 
        System.debug('********** B) processInvoicePayment requestString:  ' + requestString + ' *****');
        
        Map<String, Object> gpRequestString = (Map<String, Object>) JSON.deserializeUntyped(requestString);
        System.debug('********** B1) processInvoicePayment gpRequestString:  ' + gpRequestString + ' *****');  
              
        String Transactiontype = (String)gpRequestString.get('gp.type');
        System.debug('********** C) Transactiontype:' + Transactiontype + '*****');
        GlobalPayServiceBase bseService = null;
        
        if('UpdateBene'.equalsIgnoreCase(Transactiontype) || 'DeleteBene'.equalsIgnoreCase(Transactiontype)){
            System.debug('********** D1) Entering if: TransactionType = Update or Delete *****');
            bseService = new GlobalUpdateOrDeleteBeneService(); 
        }else{
            System.debug('********** D2) Entering if else: TransactionType <> Update or Delete *****');
            bseService = new GlobalPayService();    
        }
         
        bseService.payLoad = requestString;
        bseService.gpRequestString = gpRequestString;
        System.debug('********** E) bseService:  ' + bseService + ' *****'); 
        return bseService.processRequest();
    }

}