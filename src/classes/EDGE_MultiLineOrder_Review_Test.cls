/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_InvoiceManagement_Test
 * Description: Controller Test class for EDGE_MultiLineOrder_Review_Controller
 * Created Date: 19 Aug 2016
 * Created By: Abhishek Agrawal (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
@isTest
private class EDGE_MultiLineOrder_Review_Test {

    private static testMethod void test() {
        test_Utility.createCMPAdministration();
        test_Utility.createCMPAlert();
        test_Utility.createWubsIntAdministration();
        test_Utility.createGPH2HCurrencies();
        test_Utility.createGPIntegrationAdministration();
        test_Utility.currencyISOMapping();
        test_Utility.createWUEdgeSharingAdmin();
        CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.CCT_Client_ID__c = '3232112';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount.CMP_Create_Transactions__c = true;
        beneAccount.CMP_Enabled__c = true;
        beneAccount.CMP_Holding_Enabled__c = true;
        beneAccount.CMP_H2H_Transaction_Enabled__c = true;
        beneAccount.BillingStreet = 'testStreet';
        beneAccount.BillingCity = 'city';
        beneAccount.BillingState = 'state';
        beneAccount.BillingCountry = 'USA';
        beneAccount.BillingPostalCode = '231231';
        beneAccount.Billing_Address_ISO_Country_Code__c = 'USA';
        
        Account beneAccount1 = test_Utility.createAccount(false);
        beneAccount1.RecordTypeId=accountRecordType;
        beneAccount1.ExternalId__c='12345678912';
        beneAccount1.CCT_Client_ID__c = '3232112';
        beneAccount1.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount1.CMP_Create_Transactions__c = true;
        beneAccount1.CMP_Enabled__c = true;
        beneAccount1.CMP_Holding_Enabled__c = true;
        beneAccount1.CMP_H2H_Transaction_Enabled__c = true;
        beneAccount1.BillingStreet = 'testStreet';
        beneAccount1.BillingCity = 'city';
        beneAccount1.BillingState = 'state';
        beneAccount1.BillingCountry = 'USA';
        beneAccount1.BillingPostalCode = '231231';
        beneAccount1.Billing_Address_ISO_Country_Code__c = 'USA';
        
        List<Account> listAccount = new List<Account>{beneAccount,beneAccount1};
        insert listAccount;
        
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact;
        
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
        commUser.timezonesidkey='America/Indiana/Indianapolis';
        commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        commUser.CMP_Network_Invitation_Enabled__c = true;
        commUser.CMP_Create_Transactions__c = true;
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        insert commUser;    
        
        ApexPages.currentPage().getParameters().put('retUrl','/testRetUrl');
        ApexPages.currentPage().getParameters().put('Referer','/testReferer');

        
        Global_Pay_ID_Management__c gPIM = new Global_Pay_ID_Management__c(Global_Pay_ID__c='12345',
                                                                               User_Beneficiary__c=commUser.Id,
                                                                               Creation_Type__c='Automatic Email Match');
        insert gPIM;
        
        Supplier__c supp = test_Utility.createSupplier(beneAccount.Id,beneAccount1.Id,true);
        Invoice__c inv1 = test_Utility.createInvoice('GP Invoice - Active', beneAccount.Id, commUser.Id,'Pending Approval (Open)', false);
        Invoice__c inv2 = test_Utility.createInvoice('GP Invoice - Active', null, commUser.Id,'Pending Approval (Open)', false);
        
        Batch__c bh = new Batch__c(Batch_Submitted__c=System.Now(),Contact__c=newContact.id,Status__c='Inactive',
                                    GP2_Last_Submitted_By__c = UserInfo.getUserId(),GP2_Last_Order_Submitted_Date_Time__c = System.Now());        
        insert bh;
        ApexPages.currentPage().getParameters().put('orderId',bh.id);

        Invoice__c invoice = test_Utility.createInvoice('GP Invoice - Active', beneAccount.Id, commUser.Id,'Draft', false);
        invoice.Beneficiary_GP_Name__c = 'testGP';
        invoice.Supplier__c = supp.Id;
        invoice.Beneficiary_GP_ID__c = '123131131';
        invoice.Delivery_Method__c = 'Standard Payment';
        invoice.GP2_Last_Submitted_By__c = commUser.id;
        invoice.GP2_Last_Order_Submitted_Date_Time__c = System.now();
        invoice.Batch_Id__c =  bh.id;
        insert invoice;
        
        inv2.Supplier__c = supp.Id;
        List<Invoice__c> listInvoices = new List<Invoice__c>{inv1,inv2};
        insert listInvoices;
        
        System.RunAs(commUser){
            Holding_Balance__c hb =  test_Utility.createHoldingBalance(beneAccount.Id ,true);
            Test.startTest();
            ApexPages.currentPage().getParameters().put('xlsType','Receivables');
            EDGE_MultiLineOrder_Review_Controller ctrl = new EDGE_MultiLineOrder_Review_Controller();
            List<InvoiceUIWrapper_MultiLine> multiList = new List<InvoiceUIWrapper_MultiLine>();
            Invoice__c invo = [Select Id, Name,GP2_Last_Order_Submitted_Date_Time__c,GP2_Last_Submitted_By__r.Name, Batch_Id__c ,Status__c,RecordType.Name,Custom_Currency__c,RecordTypeId,Invoice_Number__c,Delivery_Method__c,
                                        Initiated_By__c,Due_Date__c,Amount__c,Account__c,Buyer__c,Buyer__r.Contact.AccountId,Beneficiary_GP_ID__c,Supplier__r.Buyer__c,
                                        Beneficiary_GP_Name__c,Reference_Number__c,Type__c,Account__r.Name,TypeTrans__c,Supplier__c,Supplier__r.Supplier__c
                                        From Invoice__c where Id = :invoice.Id];
            InvoiceUIWrapper_MultiLine  multiLineOb = new InvoiceUIWrapper_MultiLine(invo);
            multiLineOb.selected = true;
            multiList.add(multiLineOb);
            ctrl.invoices = multiList;
            String d  = ctrl.LocaleDate;
            d = ctrl.localeDateRegx;
            //ctrl.batchName = 'Test' ;
            ctrl.resetInvoices();
            ctrl.status = 'Pending Approval (Open)';
            ctrl.currencyString = 'USD';
            ctrl.requestType = 'Receivables';
            ctrl.refreshInvoices();
            ctrl.toggleSortAction();
            ctrl.step2 = true;
            System.assert(ctrl.batchName != Null);
            Test.stopTest();
        }
    }// END testMethod
}//END EDGE_MultiLineOrder_Review_Test