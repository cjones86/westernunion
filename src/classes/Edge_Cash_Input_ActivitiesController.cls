public without sharing class Edge_Cash_Input_ActivitiesController {
	public String selectedCurrency{get;set;}
	public Input__c input{get;set;}
	public transient List<InvoiceUIWrapper> inInvoices{get;set;}
	public transient List<InvoiceUIWrapper> outInvoices{get;set;}
	public List<inputWpr> hedgingInputs{get;set;}
	public List<inputWpr> foreignCurrencyInputs{get;set;}
	public integer allInvoices{get;set;}
	public integer allInvoicesBene{get;set;}
	public List<String> gPId{get;set;}
	public Boolean createTransactionPermForSP{get;set;}
	public Boolean createTransactionPermForH2H{get;set;}
	public string currentAccount{get;set;}
	public Boolean displayPopup {get; set;}
	public String popupURL{get;set;}
	public String selectedInvoice{get;set;}
	public Set<String> currSet = new Set<String>();
	public Set<String> allCurrSet;

	public String localeDate{
	      get{
	          Utility utility = new Utility();
	          return utility.localeDate;
	      }
	}
	public String localeDateRegx{
	      get{
	          String dtStr = localeDate.replace('yyyy', '(\\d{2,4})');
	      dtStr = dtStr.replace('mm', '(\\d{1,2})\\');
	      dtStr = dtStr.replace('dd', '(\\d{1,2})\\');
	          return '/'+dtStr+'( (\\d{1,2}):(\\d{1,2}))? ?(am|pm|AM|PM|Am|Pm)?/';
	      }
	  }

	public void closePopup() {
    	displayPopup = false;
    }
    public PageReference showPay() {
    	popupURL = CMP_Administration__c.getInstance().Base_Invite_URL__c+'/apex/GPAppTest?app=payBeneficiary&id='+selectedInvoice;
    	displayPopup = true;
    	return null;
    }

	public Edge_Cash_Input_ActivitiesController() {
		input = new Input__c();
		gPId = new List<String>();
		for(Global_Pay_ID_Management__c gpRecord : [SELECT Global_Pay_ID__c FROM Global_Pay_ID_Management__c WHERE User_Beneficiary__c=:UserInfo.getUserId()]) {
			gPId.add(gpRecord.Global_Pay_ID__c);
		}
		currentAccount = Utility.currentAccount;
		createTransactionPermForSP = Utility_Security.canPayInvoices_SP;
		createTransactionPermForH2H = Utility_Security.canPayInvoices_H2H;
		PopulateLists();
		allCurrSet = getAllCurrencies();
		system.debug('all curr list>>>'+allCurrSet);
	}

	public Set<String> getAllCurrencies(){
		 for(InvoiceUIWrapper wrap : inInvoices){
	    currSet.add(wrap.curr);
	    system.debug('inv>>>'+wrap);
	  }

	   for(InvoiceUIWrapper wrap : outInvoices){
	    currSet.add(wrap.curr);
	    system.debug('out>>>'+wrap);
	  }

	  return currSet;
	}

	public List<SelectOption> getaccountTradedCurrency(){
        list<Selectoption> lstCurrency = new list<Selectoption>();
        lstCurrency.add(new Selectoption('', Label.CM_Community_Text_FieldFilter_Currency_Item_ShowAll));
        //lstCurrency.addAll(Utility.getaccountTradedCurrencySearch());
        //lstCurrency.addAll(allCurrList);
        /*for(String curr : allCurrSet){
        	system.debug('curr'+curr);
        	if(curr != null)
        	 lstCurrency.add(new Selectoption(curr, curr));

        }
        system.debug('lstCurrency'+lstCurrency);
        return lstCurrency;*/

				lstCurrency.addAll(Utility.getaccountTradedCurrency());
				system.debug('----------ashish---------'+lstCurrency);
				for(SelectOption sOpt : Utility.getCurrencyForFC()){
						Boolean isMatch = false;
						for(SelectOption sOpt2 : lstCurrency){
								if(sOpt.getValue() == sOpt2.getValue()){
										isMatch = true;
										break;
								}
						}
						if(!isMatch){
								lstCurrency.add(sOpt);
						}
				}
				for(SelectOption sOpt : Utility.getCurrencyForHB()){
						Boolean isMatch = false;
						for(SelectOption sOpt2 : lstCurrency){
								if(sOpt.getValue() == sOpt2.getValue()){
										isMatch = true;
										break;
								}
						}
						if(!isMatch){
								lstCurrency.add(sOpt);
						}
				}
				return lstCurrency;
    }

    public void PopulateLists() {
        //PopulateLists(true);
        String invoiceQueries = generateInvoiceFilterQuery('Invoice');
        inInvoices = getPayableInvoice(invoiceQueries);
        outInvoices = getReceivables(invoiceQueries);

        String inputQueries = generateInvoiceFilterQuery('Input');
        System.debug('>>>>>inputQuery '+inputQueries);
        hedgingInputs = getHedgingInputs(inputQueries);
        foreignCurrencyInputs = getHoldingInputs(inputQueries);

        inInvoices.addAll(getPayableInputs(inputQueries));
        outInvoices.addAll(getReceivablesInputs(inputQueries));

        //System.debug('>>>>> '+getPayableInputs(inputQueries));
    }

    private String generateInvoiceFilterQuery(String objectType) {
		String query = '';
		if(objectType != 'Input' && !String.isEmpty(selectedCurrency)) {
			query = 'Custom_Currency__c=\''+selectedCurrency+'\'';
		}
		if(objectType == 'Input' && !String.isEmpty(selectedCurrency)){
			query = '(Sell_Currency__c=\''+selectedCurrency+'\' OR Custom_Currency__c=\''+selectedCurrency+'\')';
			//query = 'Sell_Currency__c=\''+selectedCurrency+'\'';
		}
		    /*if(!String.isEmpty(selectedCurrency)) {
      query = 'Custom_Currency__c=\''+selectedCurrency+'\'';
    }*/

		String objTypeField = objectType=='Invoice'? 'Due_Date__c':'Transaction_Date__c';
        if(input.Window_Start_Date__c!=null){
        	String startDateL = input.Window_Start_Date__c+'' ;//'yyyy-MM-dd\'T\'hh:mm:ss\'Z\''
            startDateL = startDateL.replace('00:00:00', '');
            query +=(String.isBlank(query)?'':' AND ')+objTypeField+'>='+startDateL;
        }
        if(input.Window_End_Date__c!=null){
        	String endDateL = input.Window_End_Date__c+'' ;//'yyyy-MM-dd\'T\'hh:mm:ss\'Z\''
            endDateL = endDateL.replace('00:00:00', '');
            query +=(String.isBlank(query)?'':' AND ')+objTypeField+'<='+endDateL;
        }
        system.debug('Query>>>>'+query);
		return query;
	}

	public List<InvoiceUIWrapper> getReceivablesInputs(String filterQuery){
		String userId = UserInfo.getUserId();
		String currentAccId = Utility.currentAccount;
		List<InvoiceUIWrapper> latestInputs = new List<InvoiceUIWrapper>();
		String soql = inputBaseQuery();
		soql += ' Parent_account__c=\''+Utility.currentAccount+'\'';
		soql += ' AND ( Input_Type__c IN (\'Forecast Inflow\') or Type__c IN (\'Forecast Inflow\')) ';
        soql += (String.isBlank(filterQuery)?'':' AND ')+filterQuery+' ORDER BY Transaction_Date__c DESC';

        for(Input__c inp: Database.query(soql)){
        	latestInputs.add(new InvoiceUIWrapper(inp));
        }
        return latestInputs;
	}

	public List<InvoiceUIWrapper> getPayableInputs(String filterQuery){
		String userId = UserInfo.getUserId();
		String currentAccId = Utility.currentAccount;
		List<InvoiceUIWrapper> latestInputs = new List<InvoiceUIWrapper>();
		String soql = inputBaseQuery();
		soql += ' Parent_account__c=\''+Utility.currentAccount+'\'';
		soql += ' AND ( Input_Type__c IN (\'Purchase Order\',\'Forecast Outflow\') or Type__c IN (\'Purchase Order\',\'Forecast Outflow\')) ';
        soql += (String.isBlank(filterQuery)?'':' AND ')+filterQuery+' ORDER BY Transaction_Date__c DESC';

        for(Input__c inp: Database.query(soql)){
        	latestInputs.add(new InvoiceUIWrapper(inp));
        }
        return latestInputs;
	}

	public List<InvoiceUIWrapper> getPayableInvoice(String filterQuery) {
		String userId = UserInfo.getUserId();
		String currentAccId = Utility.currentAccount;
		List<InvoiceUIWrapper> latestInvoices = new List<InvoiceUIWrapper>();

		for(Invoice__c Invoice: database.query('SELECT Name,Initiated_By__c,RecordTypeId,Account__r.Name,Reference_Number__c,Account__c,RecordType.Name,Custom_Currency__c,Beneficiary_Name__c,Beneficiary_GP_Name__c,Delivery_Method__c,Type__c,Due_Date__c,Amount__c,tolabel(Status__c),CreatedBy.Name,Id,Buyer__r.Contact.AccountId,Beneficiary_GP_ID__c,Invoice_Number__c, TypeTrans__c, Supplier__c  FROM Invoice__c WHERE Account__c=:currentAccId'+(String.isBlank(filterQuery)?'':' AND ')+filterQuery+' ORDER BY Due_Date__c DESC')){
			if(Invoice.RecordTypeId != null){

			InvoiceUIWrapper inv = new InvoiceUIWrapper(Invoice);
			inv.reffrenceBasedOnRecordType();
			latestInvoices.add(inv);
		}}
		return latestInvoices;
	}

	public List<InvoiceUIWrapper> getReceivables(String filterQuery) {
		String userId = UserInfo.getUserId();
		String currentAccId = Utility.currentAccount;
		String accountString = '(Seller_Account__c=\''+currentAccId+'\' ' + 'OR (Supplier__c != null AND Supplier__r.Supplier__c=\''+currentAccId+'\'))';
		List<InvoiceUIWrapper> latestInvoices = new List<InvoiceUIWrapper>();
		//NS: 03/17 - Commented Record Type and gpId condition same as of Edge_Activity_MyInvoicesController (HomePage Invoices Controller)
		//List<String> rtAllowed = new List<String>{'Cold Payment - Active','Cold Payment - Complete','GP Invoice - Active','GP Invoice - Complete'};
		//if(gPId.size()>=1) {
		//tolabel(Status__c)StatusTrans,
		    InvoiceUIWrapper invWrapper;
			for(Invoice__c Invoice: database.query('SELECT Name,Account__c,Initiated_By__c,Account__r.Name,Reference_Number__c,RecordTypeId,RecordType.Name,Custom_Currency__c,Delivery_Method__c,Beneficiary_Name__c,Beneficiary_GP_Name__c,Type__c,Due_Date__c,Amount__c,tolabel(Status__c),Buyer__c,Buyer__r.Name,CreatedBy.Name,Id, Invoice_Number__c,TypeTrans__c, Supplier__c FROM Invoice__c WHERE '+ accountString +(String.isBlank(filterQuery)?'':' AND ')+filterQuery+' ORDER BY Due_Date__c DESC')){
				if(Invoice.RecordTypeId != null){

                invWrapper = new InvoiceUIWrapper(Invoice);
                 if(Invoice.TypeTrans__c.equalsIgnoreCase(Label.CM_SectionHeading_InvoiceDetail_Payables)){
                     invWrapper.getCategoryType = Label.CM_EDGE_Receivables_Others;
                }
                invWrapper.reffrenceBasedOnRecordType();
               latestInvoices.add(invWrapper);
               }

			}
			return latestInvoices;
		//}
		//return latestInvoices;
	}

	private String inputBaseQuery() {//tolabel(Type__c) TypeTrans,tolabel(Input_Type__c) InputTypeTrans,
		String soql = 'SELECT id, Input_Beneficiary__c, tolabel(Input_Type__c), Window_Start_Date__c, Window_End_Date__c, Counter_Party__c,Rate__c,Settlement_Amount__c,';
        soql += 'LastModifiedDate, LastModifiedBy.Name, Input_Beneficiary__r.Name, Input_Beneficiary__r.OwnerId, Input_Beneficiary__r.Buyer_Supplier_Number__c,Name, Custom_Currency__c, Amount__c, Show_Input__c, ';
        soql += 'ShowAlert__c, Beneficiary_Customer__c, Description__c, Paid__c, Invoice_Number__c, Payment_Date__c, PO_Number__c, Sort_Order__c, ';
        soql += 'Transaction_Date__c, tolabel(Type__c), Reference_Number__c, Option_Protection_Obligation__c, Option_Protection_Obligation__r.Id, ';
        soql += 'Option_Protection_Obligation__r.Amount__c, CreatedDate, Parent_Account__c, OwnerId, Currency_Code__c,Sell_Currency__c, Buy_Currency__c';
        soql += ' FROM Input__c WHERE ';
        return soql;
	}

	public List<inputWpr> getHedgingInputs(String filterQuery) {
		List<inputWpr> lstInputWpr = new List<inputWpr> ();
		String soql = inputBaseQuery();
        soql += ' Parent_account__c=\''+Utility.currentAccount+'\'';
		soql += ' AND ( Input_Type__c IN '+Utility.getCriteriaFromValues(Utility.inputHedging)+' or '+
                        ' Type__c IN '+Utility.getCriteriaFromValues(Utility.inputHedging)+') ';
        soql += (String.isBlank(filterQuery)?'':' AND ')+filterQuery+' ORDER BY Transaction_Date__c DESC';
        system.debug('soql query>>>'+soql);
        for (Input__c input: database.query(soql)){
        	lstInputWpr.add(new inputWpr('name', 'asc',  input));
        }
        return lstInputWpr;
	}

    public List<inputWpr> getHoldingInputs(String filterQuery) {
    	List<inputWpr> lstInputWpr = new List<inputWpr> ();
		String soql = inputBaseQuery();
        soql += ' Parent_account__c=\''+Utility.currentAccount+'\'';
		soql += ' AND ( Input_Type__c = \'Foreign Currency Balance\' or '+' Type__c = \'Foreign Currency Balance\') ';
        soql += (String.isBlank(filterQuery)?'':' AND ')+filterQuery+' ORDER BY Transaction_Date__c DESC';
        for (Input__c input: database.query(soql)){
        	lstInputWpr.add(new inputWpr('name', 'asc',  input));
        }
        return lstInputWpr;
	}
}