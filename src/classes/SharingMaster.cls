/**
 * @author Moahmmad Swaleh/Appirio, Inc.
 * @date 25MAY2016
 * @decsription This class is master class for all the sharing related logic.
 *              To call this class from anywhere use this syntax. "SharingMaster.doShare()"
 *
 */
public without sharing class SharingMaster {
    
    public static final String ACTION_INSERT = 'insert';
    public static final String ACTION_UPDATE = 'update';
    public static final String ADMIN_FIELD_PREFIX = 'Sharing_Enabled_';
    
    //values to be set or read from outside this class (other than a trigger call)
    public static String objectName;
    public static Set<Id> recordIds = new Set<Id>();
    public static String runStatus;
        
    static Boolean refresh = false;
    static Set <Id> sObjIdsSet = new Set <Id>();
    static String triggeringObject;
    
    //get custom setting data to put control/check when and where to run or not run sharing code
    static WU_Edge_Sharing_Administration__c sharingAdmin = WU_Edge_Sharing_Administration__c.getInstance();
    static Set<Id> byPassUserIdsSet = new Set<Id>();//user ids to bypass sharing code for
    static Set<Id> runUserProfilesSet = new Set<Id>();
    static List<String> invRTNamesList = new List<String>();
    static {
        //get user ids to bypass
        String byPassUserIdsStr = (String)sharingAdmin.get('Bypass_Sharing_User_IDs__c');
        List<Id> byPassUserIdsList = byPassUserIdsStr != null && byPassUserIdsStr.length() >= 15 ? byPassUserIdsStr.deleteWhitespace().split(',') : new List<Id>();
        byPassUserIdsSet.addAll(byPassUserIdsList);
        
        //get profile ids who get sharing thru sharing logic
        List<String> runUserProfilesList = new List<String>();
        System.debug('### WU_Edge_Sharing_Administration__c ' + sharingAdmin);
        for(String st: ((String)sharingAdmin.get('Sharing_Enabled_Profiles__c')).split(',')) {
            runUserProfilesList.add(st.trim());
        }
        
        System.debug('runUserProfilesSet**'+runUserProfilesSet);
        
        //get invoice record type names to share
        invRTNamesList = ((String)sharingAdmin.get('Sharing_Enabled_Invoice_Record_Types__c')).split(', ');
        System.debug('invRTNamesList**'+invRTNamesList);
    }
        
    /**
     * @date 25MAY2016
     * @param
     * @return void
     */
    public static void doShare() {        
        
        //exit if user or the profile is not enabled to run sharing
        if(byPassUserIdsSet.contains(UserInfo.getUserId())) return;
                
        SObject sObj;        
        
        //check if the method is being called from a trigger or elsewhere
        if(Trigger.isExecuting && sharingAdmin.Sharing_Enabled__c) {
            if(Trigger.isInsert) {
                sObj = Trigger.new[0];
                sObjIdsSet.addAll(Trigger.newMap.keySet());
            } else if(Trigger.isUpdate) {
                sObj = Trigger.new[0];
            }
            /*No action on delete event because if a record (Invoice, Invite, Supplier or Network) 
            is deleted, there will no share to delete and if User is deactivated or Account/Contact is deleted
            the associated users will go away, so no shares to delete in this case either*/
            
            triggeringObject = sObj.getSObjectType().getDescribe().getName();
            //whether sharing is enabled for the object
            if(!(Boolean)sharingAdmin.get(ADMIN_FIELD_PREFIX + triggeringObject)) {
                return;
            }                       
        } else { //if from VF manual refresh
            triggeringObject = objectName;
            sObjIdsSet.addAll(recordIds);
            refresh = true;
        }
                
        //run sharing logic based on specific triggering objects
        if(triggeringObject == 'Account') {//From VF refresh: this would need to refresh sharing on all objects              
            shareInvoices();
            /* Supplier, Network and Invites are set Private at object evel sharing, yet.*/
            //shareSuppliers();
            //shareNetworks();
            //shareInvites();           
        /** Invoice Object Sharing*/
        } else if(triggeringObject == 'Invoice__c') {
            shareInvoices();
        /** Supplier Object Sharing*/ 
        } else if(triggeringObject == 'Supplier__c') {            
            //shareSuppliers();
        /** Network Object Sharing*/
        } else if(triggeringObject == 'Network__c') {
            //shareNetworks();
        /** Invites Object Sharing*/
        } else if(triggeringObject == 'Invites__c') {
            //shareInvites();
        }
    }
    
    //recordId to Buyer/Seller Account map
    static Map<Id,Id> buyerRecordMap = new Map<Id,Id>();
    static Map<Id,Id> sellerRecordMap = new Map<Id,Id>();
    
    //Invoice object logic
    static void shareInvoices() {
        System.debug('invRTNamesList**'+invRTNamesList);
        //records to consider if from trigger update
        if(Trigger.isExecuting && Trigger.isUpdate) {
            refresh = true;
            sObjIdsSet = new Set <Id>();
            for(Invoice__c inv: (List<Invoice__c>)Trigger.new) {
                if(inv.RecordTypeId != ((Map<Id,Invoice__c>)Trigger.oldMap).get(inv.Id).RecordTypeId 
                || inv.Account__c != ((Map<Id,Invoice__c>)Trigger.oldMap).get(inv.Id).Account__c
                || inv.Seller_Account__c != ((Map<Id,Invoice__c>)Trigger.oldMap).get(inv.Id).Seller_Account__c) {
                    sObjIdsSet.add(inv.Id);
                }
            }            
        }
        //remove existing sharing if Trigger criteria changes or is manual refresh
        if(!sObjIdsSet.isEmpty() && refresh) removeSharing('Invoice__c', sObjIdsSet);//remove sharing if one of the criteria change
        
        String qString = 'SELECT Id, Account__c, Seller_Account__c, RecordTypeId, RecordType.Name FROM Invoice__c WHERE RecordType.Name IN: invRTNamesList';
        if(triggeringObject == 'Account') qString += ' AND (Account__c IN: sObjIdsSet OR Seller_Account__c IN: sObjIdsSet)';
        else qString += ' AND Id IN: sObjIdsSet';
                
        buyerRecordMap = new Map<Id,Id>();
        sellerRecordMap = new Map<Id,Id>();
        for(Invoice__c inv: Database.query(qString)) {
            System.debug('inv rt**'+inv.RecordType.Name);
            buyerRecordMap.put(inv.Account__c, inv.Id);
            sellerRecordMap.put(inv.Seller_Account__c, inv.Id);
        }           
        if(!buyerRecordMap.isEmpty() || !sellerRecordMap.isEmpty()) addSharing('Invoice__c'/*, buyerRecordMap, sellerRecordMap*/);
    }
    
    //Supplier object logic
    static void shareSuppliers() {
        //records to consider if from trigger update
        if(Trigger.isExecuting && Trigger.isUpdate) {
            refresh = true;
            sObjIdsSet = new Set <Id>();
            for(Supplier__c sup: (List<Supplier__c>)Trigger.new) {
                if(sup.Buyer__c != ((Map<Id,Supplier__c>)Trigger.oldMap).get(sup.Id).Buyer__c
                || sup.Supplier__c != ((Map<Id,Supplier__c>)Trigger.oldMap).get(sup.Id).Supplier__c) {
                    sObjIdsSet.add(sup.Id);
                }
            }            
        }       
        //remove existing sharing if Trigger criteria changes or is manual refresh
        if(!sObjIdsSet.isEmpty() && refresh) removeSharing('Supplier__c', sObjIdsSet);//remove sharing if one of the criteria change
        
        String qString = 'SELECT Id, Buyer__c, Supplier__c FROM Supplier__c WHERE';
        if(triggeringObject == 'Account') qString += ' (Buyer__c IN: sObjIdsSet OR Supplier__c IN: sObjIdsSet)';
        else qString += ' Id IN: sObjIdsSet';
        
        buyerRecordMap = new Map<Id,Id>();
        sellerRecordMap = new Map<Id,Id>();
        for(Supplier__c sup: Database.query(qString)) {
            buyerRecordMap.put(sup.Buyer__c, sup.Id);
            sellerRecordMap.put(sup.Supplier__c, sup.Id);
        }
        if(!buyerRecordMap.isEmpty() || !sellerRecordMap.isEmpty()) addSharing('Supplier__c'/*, buyerRecordMap, sellerRecordMap*/);
    }
    
    //Network object logic
    static void shareNetworks() {
        //records to consider if from trigger update
        if(Trigger.isExecuting && Trigger.isUpdate) {
            refresh = true;
            sObjIdsSet = new Set <Id>();
            for(Network__c net: (List<Network__c>)Trigger.new) {
                if(net.Account_Invitee__c != ((Map<Id,Network__c>)Trigger.oldMap).get(net.Id).Account_Invitee__c
                || net.Account_Inviter__c != ((Map<Id,Network__c>)Trigger.oldMap).get(net.Id).Account_Inviter__c) {
                    sObjIdsSet.add(net.Id);
                }
            }            
        }       
        //remove existing sharing if Trigger criteria changes or is manual refresh
        if(!sObjIdsSet.isEmpty() && refresh) removeSharing('Network__c', sObjIdsSet);//remove sharing if one of the criteria change
        
        String qString = 'SELECT Id, Account_Invitee__c, Account_Inviter__c FROM Network__c WHERE';
        if(triggeringObject == 'Account') qString += ' (Account_Invitee__c IN: sObjIdsSet OR Account_Inviter__c IN: sObjIdsSet)';
        else qString += ' Id IN: sObjIdsSet';
        
        buyerRecordMap = new Map<Id,Id>();
        sellerRecordMap = new Map<Id,Id>();
        for(Network__c net: Database.query(qString)) {
            buyerRecordMap.put(net.Account_Invitee__c, net.Id);
            sellerRecordMap.put(net.Account_Inviter__c, net.Id);
        }
        if(!buyerRecordMap.isEmpty() || !sellerRecordMap.isEmpty()) addSharing('Network__c'/*, buyerRecordMap, sellerRecordMap*/);
    }
    
    //Invites object logic
    static void shareInvites() {
        //records to consider if from trigger update
        if(Trigger.isExecuting && Trigger.isUpdate) {
            refresh = true;
            sObjIdsSet = new Set <Id>();
            for(Invites__c ivt: (List<Invites__c>)Trigger.new) {
                if(ivt.Invitee_Contact__c != ((Map<Id,Invites__c>)Trigger.oldMap).get(ivt.Id).Invitee_Contact__c
                || ivt.Inviter_User__c != ((Map<Id,Invites__c>)Trigger.oldMap).get(ivt.Id).Inviter_User__c) {
                    sObjIdsSet.add(ivt.Id);
                }
            }            
        }
        //remove existing sharing if Trigger criteria changes or is manual refresh
        if(!sObjIdsSet.isEmpty() && refresh) removeSharing('Invites__c', sObjIdsSet);//remove sharing if one of the criteria change
        
        String qString = 'SELECT Id, Invitee_Parent_Account__c, Inviter_Parent_Account__c FROM Invites__c WHERE';
        if(triggeringObject == 'Account') qString += ' (Invitee_Parent_Account__c IN: sObjIdsSet OR Inviter_Parent_Account__c IN: sObjIdsSet)';
        else qString += ' Id IN: sObjIdsSet';
        
        buyerRecordMap = new Map<Id,Id>();
        sellerRecordMap = new Map<Id,Id>();
        for(Invites__c ivt: Database.query(qString)) {
            buyerRecordMap.put(ivt.Invitee_Parent_Account__c, ivt.Id);
            sellerRecordMap.put(ivt.Inviter_Parent_Account__c, ivt.Id);
        }
        if(!buyerRecordMap.isEmpty() || !sellerRecordMap.isEmpty()) addSharing('Invites__c'/*, buyerRecordMap, sellerRecordMap*/);
    }
    
    //add sharing records
    static void addSharing(String sharingObject/*, Map<Id,Id> buyerRecordMap, Map<Id,Id> sellerRecordMap*/) {                
        
        Map<Id,Id> accId2RoleIdBuyerMap = new Map<Id,Id>();
        Map<Id,Id> accId2RoleIdSellerMap = new Map<Id,Id>();
        for(User u: [SELECT Id, UserRoleId, UserRole.Name, Contact.AccountId FROM User WHERE UserRoleId != null /*AND ProfileId IN: runUserProfilesSet*/ AND (Contact.AccountId IN: buyerRecordMap.keySet() OR Contact.AccountId IN: sellerRecordMap.keySet()) ]) {
            if(buyerRecordMap.containsKey(u.Contact.AccountId)) {
                accId2RoleIdBuyerMap.put(u.Contact.AccountId,u.UserRoleId);
            }
            if(sellerRecordMap.containsKey(u.Contact.AccountId)) {
                accId2RoleIdSellerMap.put(u.Contact.AccountId,u.UserRoleId);
            }
        }
        
        //Groups aasociated with Roles
        Map<Id,Id> roleId2GroupIdMap = new Map<Id,Id>();
        for(Group g: [SELECT Id, RelatedId FROM Group WHERE Type = 'Role' AND (RelatedId IN: accId2RoleIdBuyerMap.values() OR RelatedId IN: accId2RoleIdSellerMap.values())]) {
            roleId2GroupIdMap.put(g.RelatedId,g.Id);
        }
        
        //which object record is to be shared
        List<Sobject> sharesToCreate;
        Sobject shareRec;
        if(sharingObject == 'Invoice__c') {
            sharesToCreate = new List<Invoice__Share>();
        } else if(sharingObject == 'Supplier__c') {
            //sharesToCreate = new List<Supplier__Share>();
        } if(sharingObject == 'Network__c') {
            //sharesToCreate = new List<Network__Share>();
        } if(sharingObject == 'Invites__c') {
            //sharesToCreate = new List<Invites__Share>();
        }
        
        Boolean mapIdContainsKey;
        //buyer or invitee shares
        for(Id bAccId: buyerRecordMap.keySet()) {
            
            mapIdContainsKey = accId2RoleIdBuyerMap.containsKey(bAccId) && roleId2GroupIdMap.containsKey(accId2RoleIdBuyerMap.get(bAccId));
            if( !mapIdContainsKey) continue;
            
            if(sharingObject == 'Invoice__c') {
                shareRec = new Invoice__Share();
                shareRec.put('RowCause', Schema.Invoice__Share.RowCause.Buyer_Account_Role_Share__c);
            } else if(sharingObject == 'Supplier__c') {
                //shareRec = new Supplier__Share();
            } if(sharingObject == 'Network__c') {
                //shareRec = new Network__Share();
                //shareRec.put('RowCause', Schema.Network__Share.RowCause.Invitee_Account_Role_Share__c);
            } if(sharingObject == 'Invites__c') {
                //shareRec = new Invites__Share();
                //shareRec.put('RowCause', Schema.Invites__Share.RowCause.Invitee_Account_Role_Share__c);
            }
            
            shareRec.put('ParentId', buyerRecordMap.get(bAccId));
            System.debug('buyerRecordMap.get(bAccId)**'+buyerRecordMap.get(bAccId));
            if(mapIdContainsKey) shareRec.put('UserOrGroupId', roleId2GroupIdMap.get(accId2RoleIdBuyerMap.get(bAccId)) );
            System.debug('accId2RoleIdBuyerMap.get(bAccId)**'+accId2RoleIdBuyerMap.get(bAccId));         
            shareRec.put('AccessLevel', 'Edit');
            
            sharesToCreate.add(shareRec);
        }
        
        //seller or inviter shares
        for(Id sAccId: sellerRecordMap.keySet()) {
            
            mapIdContainsKey = accId2RoleIdSellerMap.containsKey(sAccId) && roleId2GroupIdMap.containsKey(accId2RoleIdSellerMap.get(sAccId));
            if( !mapIdContainsKey ) continue;
            
            if(sharingObject == 'Invoice__c') {
                shareRec = new Invoice__Share();
                shareRec.put('RowCause', Schema.Invoice__Share.RowCause.Seller_Account_Role_Share__c);
            } else if(sharingObject == 'Supplier__c') {
                //shareRec = new Supplier__Share();
            } if(sharingObject == 'Network__c') {
                //shareRec = new Network__Share();
                //shareRec.put('RowCause', Schema.Network__Share.RowCause.Inviter_Account_Role_Share__c);
            } if(sharingObject == 'Invites__c') {
                //shareRec = new Invites__Share();
                //shareRec.put('RowCause', Schema.Invites__Share.RowCause.Inviter_Account_Role_Share__c);
            }
            
            shareRec.put('ParentId', sellerRecordMap.get(sAccId) );
            System.debug('sellerRecordMap.get(sAccId)**'+sellerRecordMap.get(sAccId));
            if(mapIdContainsKey) shareRec.put('UserOrGroupId', roleId2GroupIdMap.get(accId2RoleIdSellerMap.get(sAccId)) );
            System.debug('accId2RoleIdSellerMap.get(sAccId)**'+accId2RoleIdSellerMap.get(sAccId));
            shareRec.put('AccessLevel', 'Edit');
            
            sharesToCreate.add(shareRec);
        }
        
        System.debug('sharesToCreate**'+sharesToCreate);
        
        if(!sharesToCreate.isEmpty()) {
            try {
                insert sharesToCreate; //insert shares
                runStatus = 'Success';
            }
            catch(Exception e) {
                runStatus = e.getMessage();
            }
        }
    }
    
    //remove sharing records
    static void removeSharing(String triggObject, Set<Id> sObjectIdsSet) {
        
        List<String> rowCauses = new List<String>();
        if(triggObject == 'Invoice__c') {
            rowCauses.add(Schema.Invoice__Share.RowCause.Seller_Account_Role_Share__c);
            rowCauses.add(Schema.Invoice__Share.RowCause.Buyer_Account_Role_Share__c);
        } /*else if(triggObject == 'Supplier__c') {
            rowCauses.add(Schema.Supplier__Share.RowCause.Supplier_Account_Role_Share__c);
            rowCauses.add(Schema.Supplier__Share.RowCause.Buyer_Account_Role_Share__c);
        } else if(triggObject == 'Network__c') {
            rowCauses.add(Schema.Network__Share.RowCause.Invitee_Account_Role_Share__c);
            rowCauses.add(Schema.Network__Share.RowCause.Invite7_Account_Role_Share__c);
        } else if(triggObject == 'Invites__c') {
            rowCauses.add(Schema.Invites__Share.RowCause.Invitee_Account_Role_Share__c);
            rowCauses.add(Schema.Invites__Share.RowCause.Invite7_Account_Role_Share__c);
        }*/
        
        String qString = 'SELECT Id FROM ' + triggObject.replace('__c', '__Share') + ' WHERE ParentId IN: sObjectIdsSet AND RowCause IN: rowCauses';
                
        try {
            delete Database.query(qString);
        }
        catch(Exception e) {
            runStatus = e.getMessage();
        }
    }
}