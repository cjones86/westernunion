/**=====================================================================
 * Appirio, Inc
 * Name: inputWprTest
 * Description: Test class for inputWpr Wrapper class
 * Created Date: 06 Apr 2016
 * Created By: Nikhil Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
@isTest
private  class inputWprTest {
	@isTest         
    static void test_Wpr() {
    	Input__c input = test_Utility.createInput(true);
    	test_Utility.createCMPAdministration();
		test_Utility.createCMPAlert();
		test_Utility.createWubsIntAdministration();
		test_Utility.createGPH2HCurrencies();
		test_Utility.createGPIntegrationAdministration();
		test_Utility.createCMPLanguageSettings();
		test_Utility.createWUEdgeSharingAdmin();
		CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.CCT_Client_ID__c = '3232112';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        insert beneAccount;
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact;
        
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
		commUser.timezonesidkey='America/Indiana/Indianapolis';
		commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        insert commUser;  
        
        Invoice__c invoice1 = test_Utility.createInvoice('GP Invoice - Active', beneAccount.Id, commUser.Id,'Pending Approval (Open)', true);
        
        Test.startTest();
 
         /*   Input__c inp = test_Utility.createInputWithType(true,'Purchase Order', beneAccount.Id);
            inputWpr inpWrp = new inputWpr('test sort','test dir', inp);
            boolean isSelected = inpWrp.isSelected;
            String invDate = inpWrp.invDate;
            Decimal settlementCurrency = inpWrp.settlementCurrency;
            String settlementCurrencyFormatted = inpWrp.settlementCurrencyFormatted;
            Decimal amount = inpWrp.amount;
            String amountFormatted = inpWrp.amountFormatted; */
            
        System.RunAs(commUser){
            Input__c inp = test_Utility.createInputWithType(true,'Purchase Order', beneAccount.Id);
            Input__c inpNull = null;
            inputWpr inpWrp = new inputWpr('test sort','test dir', inp);
            inputWpr inpWrp2 = new inputWpr('test sort','test dir', invoice1);
            boolean isSelected = inpWrp.isSelected;
            String invDate = inpWrp.invDate;
            
            String invBuyerName = inpWrp.invBuyerName;
            String invBuyerNumber = inpWrp.invBuyerNumber;
            String invStatus = inpWrp.invStatus;
            String invRefNumber = inpWrp.invRefNumber;
            Decimal settlementCurrency = inpWrp.settlementCurrency;
            String settlementCurrencyFormatted = inpWrp.settlementCurrencyFormatted;
            Decimal amount = inpWrp.amount;
            String amountFormatted = inpWrp.amountFormatted;
            
            inpWrp.sortField = 'Amount__c';
            inpWrp.compareTo(inpWrp);
            inpWrp.sortField = 'Input_Beneficiary__r.Name';
            inpWrp.compareTo(inpWrp);
            inpWrp.sortField = 'Reference_Number__c';
            inpWrp.compareTo(inpWrp);
            inpWrp.sortField = 'Type__c';
            inpWrp.compareTo(inpWrp);
            inpWrp.sortField = 'Status__c';
            inpWrp.compareTo(inpWrp);
            inpWrp.sortField = 'settlementCurrency';
            inpWrp.compareTo(inpWrp);
            inpWrp.sortField = 'Transaction_Date__c';
            inpWrp.compareTo(inpWrp);
            
            
            inpWrp2.sortField = 'Amount__c';
            inpWrp2.compareTo(inpWrp2);
            inpWrp2.sortField = 'Input_Beneficiary__r.Name';
            inpWrp2.compareTo(inpWrp2);
            inpWrp2.sortField = 'Reference_Number__c';
            inpWrp2.compareTo(inpWrp2);
            inpWrp2.sortField = 'Type__c';
            inpWrp2.compareTo(inpWrp2);
            inpWrp2.sortField = 'Status__c';
            inpWrp2.compareTo(inpWrp2);
            inpWrp2.sortField = 'settlementCurrency';
            inpWrp2.compareTo(inpWrp2);
            inpWrp2.sortField = 'Transaction_Date__c';
            inpWrp2.compareTo(inpWrp2);
        }
        Test.stopTest();
    }
}