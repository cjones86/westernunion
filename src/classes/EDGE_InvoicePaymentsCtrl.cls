/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_InvoicePaymentsCtrl
 * Description: Controller class for EDGE_InvoicePayments component
 * Created Date: 19 Feb' 2016
 * Created By: Nikhil Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update                                                  
 =====================================================================*/
 
public without sharing class EDGE_InvoicePaymentsCtrl{
    public string invId{get;set;}
    public string buyerAccId{get;set;}
    public boolean isCurrentAccountBuyer{
        get{
            if(buyerAccId == Utility.currentAccount)
                return true;
            else
                return false;
        }set;}
    public list<Payment__c> payments{
        get{
            if(payments == null)
                return [Select Id, Name, Order_ID__c,Payment_Date_Time__c,Payment_Amount__c,
                        Payment_Currency__c,Settlement_Amount__c,Settlement_Currency__c,Settlement_Method__c,Payment_Status__c From Payment__c Where Invoice_ID__c = :invId];
             return payments;
        }set;    
    }
    
    public EDGE_InvoicePaymentsCtrl(){
    
    }
}