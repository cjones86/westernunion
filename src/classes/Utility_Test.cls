/**=====================================================================
 * Name: Utility_Test
 * Description: Test class for Utility.cls
 * Created Date: Mar 12, 2015
 * Created By: Aashita Sharma 
 =====================================================================*/

@isTest
private class Utility_Test {
    
    private static User testUser; 
    
    // method  to cover utility class
    static testMethod void testUtility() {  
        createTestData();       
          
         
        System.runAs(testUser){ 
           Test.startTest(); 
            PageReference pageRef = Page.AccountApproval;
            Test.setCurrentPage(pageRef);
            Utility.getaccountTradedCurrency(); 
            Utility util = new Utility();
            String Dates = util.localeDate;
            boolean patterResult = Utility.matchPattern('Test');
            boolean CollebratioinUser = Utility.isCollaborationUser ;
            String pageName = Utility.getPageNamefromUrl(pageRef.getUrl());
            Set<String> ccPages = Utility.getCCpages();
            List<SelectOption> netCashCurreny = Utility.getaccountNetcashCurrency();
            netCashCurreny = Utility.getaccountTradedCurrencySearch();
            Utility.getCurrencyForFCOther(Utility.getCurrencyForFC());
            Utility.createTestUser(true);
            Utility.getBoolean('YES');
            Utility.getBoolean('No');
            Utility.isValidSfdcId(testUser.id);
            String scvrowElimainated = Utility.CSVRowDeliminator;
            Id id = Utility.cctClientID;
            String currencies = Utility.userCurrency;
            Utility.getCurrencyForHB();
            

            
            
                        //Utility.getCurrentUsers();    
            Utility.getCurrencyForFC();  
            String test1 = Utility.getCriteriaFromValues('Test, Test123, Test567');
            String test2 = Utility.getCriteriaFromValues('  '); 
            input__c input = new input__c();
            // Boolean testValidation = Utility.fieldSetValidation(input, 'Option_Other', true);
            String testVF = Utility.getVFPageName();
            Map <String, String> testFieldlabel1 = Utility.getFieldLabel('input__c', 'Fixed Forward Contract - Other');
            Map <String, String> testFieldlabel2 = Utility.getFieldLabel('input__c', 'Outflow - Other');
            Map <String, String> testFieldlabel3 = Utility.getFieldLabel('input__c', 'Forecast Inflow');
            Map <String, String> testFieldlabel4 = Utility.getFieldLabel('input__c', 'Window Forward Contract - Other');
            
            Utility.picklistValues('Invoice__c', 'Currency__c', true);
            Utility.picklistValues('Invoice__c', 'Currency__c');
            Utility.picklistValueSet('Invoice__c', 'Currency__c');
            Integer testSOQL = Utility.getRemainingSOQLRows();
            Decimal testCurrencyAmount = Utility.getSettlementCurrencyAmount(1.0,'USD','INR');
            Boolean testValidtest = Utility.isValidText('   test  ');
            
            System.assertEquals(testValidtest, true);
            
            String testgetValidText = Utility.getValidText('  test12');
            //to verify getvalidtext method result should be  'test12'
            System.assertEquals(testgetValidText, 'test12');
             
            
            Boolean testValidDecimal = Utility.isValidDecimal(' test123');
             //to verify isValidDecimal method result should be flase
            System.assertEquals(testValidDecimal, false);
            
            Boolean testValidDecimal1 = Utility.isValidDecimal(' 20.0');
           //to verify isValidDecimal method result should be true
            System.assertEquals(testValidDecimal1, true);
            
            Decimal testgetDecimal = Utility.getDecimal(' 20.1 ');
           //to verify getDecimal method result should be 20.1
            System.assertEquals(testgetDecimal, 20.1);
            
            Date testgetDate1 = Utility.getDate('10/03/2015');
            System.assertNotEquals(testgetDate1, date.newInstance(10,03,2015));
            
            String testgetMonthName = Utility.getMonthName(10 );
            System.assertNotEquals(testgetMonthName, '10' );
            
            Boolean testValidCsvRow = Utility.isValidCsvRow('test,test12,test123');
            
            System.assertNotEquals(Utility.loggedInMarketRate, null);
            
            // checking values of page properties
            
            System.assertNotEquals(Utility.inputIncoming, null);
            
            System.assertNotEquals(Utility.inputHedging, null);
            
            System.assertNotEquals(Utility.inputTypeFfcWubs, null);
            
            System.assertNotEquals(Utility.inputLink, null);
            
            System.assertNotEquals(Utility.currentAccount, null);
           
            System.assertNotEquals(Utility.loggedInUser, null);
           
            System.assertNotEquals(Utility.lastUpdateMarketDatetime, null);
            
            String testgetCriteriaFromValues = Utility.getCriteriaFromValues('test1,test11');
            Test.stopTest();
        }
    }
   
    static testMethod void testUtility_updateOptionInput() {  
        createTestData();       
        System.runAs(testUser){ 
           Test.startTest(); 
	        System.assertNotEquals(Utility.inputLink, null);
	        String fieldSet = 'Option_Other';
	        String selectedTranscation = 'Option - Other';
	        Input__c input = new input__c();
	        Input__c optionInput = new input__c();
			Utility.updateOptionInput(input, optionInput, selectedTranscation, fieldSet);
          	
          	
			System.assertEquals(input.Type__c, 'Option Protection - Other');
			System.assertEquals(optionInput.Type__c, 'Option Obligation - Other');
			selectedTranscation = 'Option - WUBS';   
			Utility.updateOptionInput(input, optionInput, selectedTranscation, fieldSet);
			System.assertEquals(input.Type__c, 'Option Protection - WUBS');
			System.assertEquals(optionInput.Type__c, 'Option Obligation - WUBS');
          	
          	System.assertEquals(Utility.isCCTuser , false);
          	System.assertEquals(Utility.isNonCCTuser , false);
        //   	System.assertNotEquals(Utility.inputNOT_Outgoing , null);
          	
           Test.stopTest(); 
        }
    }   
   
    // creating test data for coverage    
    private static void createTestData(){
        Campaign camp = new Campaign(Name= 'Bronze Trigger Emails');
        insert camp; 
         
        Account acc = new Account();
        acc.Name  = 'Test Account';
        insert acc; 
        
        Contact contact = new Contact();
        contact.LastName = 'Test Contact';
        contact.AccountId = acc.Id;
        insert contact;
        
        // creating a test user
        testUser = new User();
        testUser.alias = 'testuser';
        testUser.Email = 'testuser@test123.com';
        testUser.EmailEncodingKey = 'ISO-8859-1';
        testUser.LanguageLocaleKey = 'en_US';
        testUser.LastName = 'Test User567';
        testUser.LocaleSidKey = 'en_AU';
        testUser.Account_Administrator__c = false;
        testUser.ProfileId = [SELECT Id FROM Profile WHERE Name LIKE 'Partner community - FX management tool' LIMIT 1].Id;
        testUser.TimeZoneSidKey = 'Australia/Sydney';
        testUser.UserName = 'testuser@travelex.com.au';
        testUser.ContactId = contact.Id;
        testUser.DefaultCurrencyIsoCode = 'INR';
        insert testUser;
        
        System.runAs(testUser){
        /*
            Invoice__c invoice = new Invoice__c();
            invoice.Account__c = acc.id;
            invoice.CurrencyIsoCode = 'USD';
            insert invoice;
            */
            
            Input__c input = new Input__c();
            input.CurrencyIsoCode = 'INR'; 
            insert input;
            
             Forward_Contracts__c forwardContracts = new  Forward_Contracts__c();
             forwardContracts.Account_ID__c = acc.Id;
    
             forwardContracts.Settlement_Amount__c = 20; 
             forwardContracts.Maturity_Date__c =  Date.today().addDays(5);
             forwardContracts.Start_Date__c = Date.today();
             forwardContracts.Transaction_Number__c = 25;
             forwardContracts.Custom_Currency__c = 'USD';
             insert forwardContracts;            
            
            list<Market_Rate__c> listMarketRateToInsert = new  list<Market_Rate__c>();
            Market_Rate__c marketRate = new Market_Rate__c();
            marketRate.Currency_Code__c= 'USD';
            marketRate.Currency_Name__c = 'USD';
            marketRate.Currency_Value__c = 20;
            marketRate.last_updated__c =  Datetime.newInstance(2015, 03, 13);
            listMarketRateToInsert.add(marketRate);
                        
            Market_Rate__c marketRate1 = new Market_Rate__c();
            marketRate1.Currency_Name__c = 'INR';
            marketRate1.Currency_Code__c= 'INR';
            marketRate1.Currency_Value__c = 20;
            marketRate1.last_updated__c =  Datetime.newInstance(2015, 03, 13);
            listMarketRateToInsert.add(marketRate1);
            insert listMarketRateToInsert;         
            system.debug('***listMarketRateToInsert:'+listMarketRateToInsert); 
        }            
    }
}