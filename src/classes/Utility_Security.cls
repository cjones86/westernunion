/**=====================================================================
 * Name: Utility_Security
 * Description: utility class for security or accessibility purpose
 * Created Date: 5th Feb'16
 * Created By: Nikhil Sharma
 =====================================================================*/

 public without sharing class Utility_Security{

    Public static User us{
        get{
            if(us == null)
                return Utility.loggedInUser;
            else
                return us;
        }set;
    }

    public static WUBS_Integration_Administration__c wubsIntegrationAdmin{
        get{
            if(wubsIntegrationAdmin==null){
                wubsIntegrationAdmin = WUBS_Integration_Administration__c.getInstance();
            }
            System.debug('********** WUBS_Integration_Administration__c wubsIntegrationAdmin:  ' + wubsIntegrationAdmin + '**********');
            return wubsIntegrationAdmin;
        }
        set;
    }


    public static Boolean isIntegrationEnabled{
        get{
            Global_Pay_Integration_Administration__c gPIA = Global_Pay_Integration_Administration__c.getInstance();
            if(gpIA != null && gPIA.Integration_Enabled__c)
                return true;
            else
                return false;
        }set;
    }

    //NS: 5th Feb'16 - T-468990 (Added method to validate whether user/client is CMP Enabled or not)
    public static boolean isCMPEnabled{
        get{
            if(isIntegrationEnabled && us.CMP_Enabled__c && us.Contact.Account.CMP_Enabled__c)
                return true;
            else
                return false;
        }set;
    }


    //NS: 5th Feb'16 - T-468990 (Added method to validate whether user/client can create transaction or not)
    public static boolean canCreateTransaction{
        get{
            if(isIntegrationEnabled && us.CMP_Create_Transactions__c && us.Contact.Account.CMP_Create_Transactions__c)
                return true;
            else
                return false;
        }set;
    }

    public static boolean isNetworkEnabled{
        get{
            if(us.CMP_Network_Invitation_Enabled__c)
                return true;
            else
                return false;
        }set;
    }

    //CJ 16/3 Added method to validate whether user can submit transactions
    public static boolean canSubmitInvoices{
        get{
            if(isIntegrationEnabled && us.EDGE_Submit_Invoice_enabled__c)
                return true;
            else
                return false;
        }set;
    }

    //NS: 5th Feb'16 - T-468990 (Added method to validate whether client has holding or not )
    public static boolean hasHolding{
        get{
            if(isIntegrationEnabled && us.Contact.Account.CMP_Holding_Enabled__c)
                return true;
            else
                return false;
        }set;
    }

    //NS: 5th Feb'16 - T-468990 (Added method to validate whether client is enabled for H2H transaction or not)
    public static boolean canH2HTransaction{
        get{
            if(isIntegrationEnabled && us.Contact.Account.CMP_H2H_Transaction_Enabled__c)
                return true;
            else
                return false;
        }set;
    }

    //NS: 5th Feb'16 - T-468990 (Added method to validate whether user can view and create cashflow data)
    public static boolean canViewAndCreateCashFlowData{
        get{
            if(isCMPEnabled)
                return true;
            else
                return false;
        }set;
    }

    //NS: 5th Feb'16 - T-468990 (Added method to validate whether user can create payments by wire, draft or ACH)
    public static boolean canCreatePaymentsByWireDraftOrACH{
        get{
            if(isCMPEnabled && canCreateTransaction)
                return true;
            else
                return false;
        }set;
    }

    //NS: 5th Feb'16 - T-468990 (Added method to validate whether user can settle payments from Holding)
    public static boolean canSettlePaymentFromHolding{
        get{
            if(isCMPEnabled && canCreateTransaction && hasHolding)
                return true;
            else
                return false;
        }set;
    }

    //NS: 5th Feb'16 - T-468990 (Added method to validate whether user can receive payments into Holding via H2H transaction)
    public static boolean canReceivePaymentsIntoHolding_H2H{
        get{
            if(isCMPEnabled && canCreateTransaction && canH2HTransaction)
                return true;
            else
                return false;
        }set;
    }

    //NS: 5th Feb'16 - T-468990 (Added method to validate whether user can Manage Holding Balances)
    public static boolean canManageHoldingBalances{
        get{
            if(isCMPEnabled && canCreateTransaction && hasHolding && !Utility.isNonCCTuser)
                return true;
            else
                return false;
        }set;
    }

    //NS: 5th Feb'16 - T-468990 (Added method to validate whether user can create H2H transactions)
    public static boolean canCreateH2HTransactions{
        get{
            if(isCMPEnabled && canCreateTransaction && hasHolding && canH2HTransaction)
                return true;
            else
                return false;
        }set;
    }

    //NS: 5th Feb'16 - T-468990 (Added method to validate whether user can show holding balance details)
    public static boolean canShowHoldingBalance{
        get{
            if(isCMPEnabled && canCreateTransaction && hasHolding)
                return true;
            else
                return false;
        }set;
    }

    //NS: 5th Feb'16 - T-468990 (Added method to validate whether user can manage beneficiaries(Supplier) in cmp)
    public static boolean canManageBeneficiaryInCMP{
        get{
            if(isCMPEnabled)
                return true;
            else
                return false;
        }set;
    }

    //NS: 5th Feb'16 - T-468990 (Added method to validate whether user can manage beneficiaries in GP 2.0)
    public static boolean canManageBeneficiaryInGP{
        get{
            if(isCMPEnabled && canCreateTransaction )
                return true;
            else
                return false;
        }set;
    }

    //NS: 5th Feb'16 - T-468990 (Added method to validate whether user can create invoice or not)
    public static boolean canCreateInvoice{
        get{
            if(isCMPEnabled && canCreateTransaction )
                return true;
            else
                return false;
        }set;
    }

    //NS: 5th Feb'16 - T-468990 (Added method to validate whether user can manage beneficiaries)
    public static boolean canManageBeneficiary{
        get{
            if(isCMPEnabled && canCreateTransaction )
                return true;
            else
                return false;
        }set;
    }

    //NS: 5th Feb'16 - T-468990 (Added method to validate whether user can Pay Invoices(Bene) Standard Payment)
    public static boolean canPayInvoices_SP{
        get{
            if(isCMPEnabled && canCreateTransaction )
                return true;
            else
                return false;
        }set;
    }

    //NS: 5th Feb'16 - T-468990 (Added method to validate whether user can Pay Invoices(Bene) Holding to Holding)
    public static boolean canPayInvoices_H2H{
        get{
            if(isCMPEnabled && canCreateTransaction && hasHolding && canH2HTransaction)
                return true;
            else
                return false;
        }set;
    }

    //NS: 5th Feb'16 - T-468990 (Added method to validate whether user can make payments(cold) - Standard)
    public static boolean canMakeStandardColdPayments{
        get{
            if(isCMPEnabled && canCreateTransaction)
                return true;
            else
                return false;
        }set;
    }

    //NS: 5th Feb'16 - T-468990 (Added method to validate whether user can make payments(cold) - HB Payment)
    public static boolean canMakeHBColdPayments{
        get{
            if(isCMPEnabled && canCreateTransaction && hasHolding && canH2HTransaction)
                return true;
            else
                return false;
        }set;
    }

    // RS : 8th Feb'16 - T-468990 (User Can create invoice with GP only if following conditions match)
    public static boolean canCreateNetworkTransaction {
        get{
            if(isCMPEnabled && canCreateTransaction && us.CMP_Network_Invitation_Enabled__c)
                return true;
            else
                return false;
        }set;
    }

    public static boolean isPaymentApprovals {
    	get{
            if(us.EDGE_Payment_Approvals__c)
                return true;
            else
                return false;
        }set;
    }

    public static boolean isReporting {
    	get{
    		if(us.EDGE_Reporting__c)
    		        return true;
            else
                return false;

    	}set;
    }
 }