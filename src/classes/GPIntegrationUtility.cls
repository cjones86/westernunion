/**=====================================================================
 * Appirio, Inc
 * Name: GPIntegrationUtility
 * Description: Utility class for Global Pay intration with invoive,invites, networks
 * Created Date: 19 Jan' 2014
 * Created By: Rohit Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
public without sharing class GPIntegrationUtility {

	// Generate 36 character UNIQUE TOKEN
    public static String generateInvitesTOKEN(String tokenField){  
        Boolean unique = false;
        String token;
        while (!unique){
            Blob key = Crypto.GenerateAESKey(256);
            String hex = EncodingUtil.ConvertTohex(key);
            token = hex.substring(0, 36);       
            unique = (Database.countQuery('Select Count() From Invites__c Where ' + String.escapeSingleQuotes(tokenField) + ' = \'' + token + '\'') == 0);
        }
        return token;
    }
}