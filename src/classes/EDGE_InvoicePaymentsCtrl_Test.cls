/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_InvoicePaymentsCtrl_Test
 * Description: Controller Test class for EDGE_InvoicePaymentsCtrl
 * Created Date: 14th July 2016
 * Created By: Nikhil Sharma (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
@isTest
private class EDGE_InvoicePaymentsCtrl_Test {

	private static testMethod void test() {
        test_Utility.createCMPAdministration();
        test_Utility.createCMPAlert();
        test_Utility.createWUEdgeSharingAdmin();
        CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        insert beneAccount;
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact;
        Account beneAccount1 = test_Utility.createAccount(false);
        beneAccount1.RecordTypeId=accountRecordType;
        beneAccount1.ExternalId__c='12345';
        beneAccount1.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        insert beneAccount1;
        Contact newContact1 = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact1;
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
        commUser.timezonesidkey='America/Indiana/Indianapolis';
        commUser.CMP_Enabled__c=true;
        commUser.EDGE_Payment_Approvals__c = true;
        commUser.UserName = newContact.Email+'.cmp';
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        commUser.CommunityNickname = commUser.alias;
        insert commUser;
        
        Invoice__c invoice = test_Utility.createInvoice(false);
        invoice.RecordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('GP Invoice - Active').getRecordTypeId();
        invoice.Custom_Currency__c = 'USD';
        invoice.Invoice_Number__c = 'INVTest-123';
        invoice.Status__c = 'Invoice Submitted';
        invoice.Initiated_By__c='Seller';
        invoice.Due_Date__c = Date.today().addDays(30);
        invoice.Amount__c = 2000;
        invoice.Account__c = beneAccount.Id;
        invoice.Buyer__c = commUser.Id;
        insert invoice;
        
        //Insert Payment record
        Payment__c pmt = new Payment__c();
        pmt.Invoice_ID__c = invoice.Id;
        pmt.Notification_User__c = commUser.Id;
        pmt.Payment_Status__c = 'Pending Approval';
        pmt.Order_ID__c = '3123123113';
        pmt.Payment_Amount__c = 23123;
        pmt.Payment_Date_Time__c = System.Now();
        insert pmt;
        
        System.RunAs(commUser){
            Test.startTest();
                EDGE_InvoicePaymentsCtrl cmp = new EDGE_InvoicePaymentsCtrl();
                boolean isCurrentAccountBuyer = cmp.isCurrentAccountBuyer;
                cmp.buyerAccId = beneAccount.Id;
                cmp.invId = invoice.Id;
                isCurrentAccountBuyer = cmp.isCurrentAccountBuyer;
                list<Payment__c> payments = cmp.payments;
                System.assertEquals(1,payments.size());
            Test.stopTest();
        }
	}

}