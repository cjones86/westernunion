/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_SubmitInvoice_Test
 * Description: Controller Test class for EDGE_SubmitInvoice
 * Created Date: 01 Apr 2016
 * Created By: Nikhil Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
@isTest
private class EDGE_SubmitInvoice_Test {

	private static testMethod void test() {
        test_Utility.createWUEdgeSharingAdmin();
        Invoice__c parentInv = test_Utility.createInvoice(true);
        test_Utility.createCMPAdministrationWithInvoice(parentInv.Id);
		test_Utility.createCMPAlert();
		test_Utility.createWubsIntAdministration();
		test_Utility.createGPH2HCurrencies();
		test_Utility.createGPIntegrationAdministration();
		test_Utility.currencyISOMapping();
		
		CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.CCT_Client_ID__c = '3232112';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount.BillingStreet = 'testStreet';
        beneAccount.BillingCity = 'city';
        beneAccount.BillingState = 'state';
        beneAccount.BillingCountry = 'USA';
        beneAccount.BillingPostalCode = '231231';
        
        Account beneAccount1 = test_Utility.createAccount(false);
        beneAccount1.RecordTypeId=accountRecordType;
        beneAccount1.ExternalId__c='11111145';
        beneAccount.CCT_Client_ID__c = '32321122131';
        beneAccount1.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount1.CMP_Enabled__c = true;
        
        List<Account> listAccount = new List<Account>{beneAccount, beneAccount1};
        insert listAccount;
        
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        Contact newContact1 = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        
        List<Contact> listContacts = new List<Contact>{newContact,newContact1};
        insert listContacts;
        
        supplier__c supp = test_Utility.createSupplier(beneAccount.Id,beneAccount1.id,false);
        supp.Supplier_Name__c = 'Test';
        supp.Address_line_1__c = 'Test';
        supp.Address_Line_2__c = 'Test';
        supp.City__c = 'Test';
        supp.State_Province__c = 'Test';
        //supp.Country__c = 'ANDORRA';
        supp.Post_Code__c = 'Test';
        insert supp;
        
        
        
        User commUser1 = test_Utility.createCommUser(newContact1.Id, false);
        commUser1.ProfileId = profileId;
        commUser1.emailencodingkey='UTF-8';
        commUser1.localesidkey='en_US';
		commUser1.timezonesidkey='America/Indiana/Indianapolis';
		commUser1.CMP_Enabled__c=true;
        commUser1.UserName = newContact1.Email+'.cmp1';
        
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
		commUser.timezonesidkey='America/Indiana/Indianapolis';
		commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        
        
        list<User> listUsers = new List<User>{commUser,commUser1};
        insert listUsers;
        
        Invoice__c invoice = test_Utility.createInvoice(false);
        invoice.RecordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('GP Invoice - Active').getRecordTypeId();
        invoice.Custom_Currency__c = 'USD';
        invoice.Invoice_Number__c = 'INVTest-123';
        invoice.Status__c = 'Draft';
        invoice.Initiated_By__c='Buyer';
        invoice.Due_Date__c = Date.today().addDays(30);
        invoice.Amount__c = 2000;
        invoice.Account__c = beneAccount.Id;
        invoice.Buyer__c = commUser.Id;
        invoice.Supplier__c = supp.id;
        insert invoice;
        FeedItem feedPost = new FeedItem();
        feedPost.ParentId = invoice.Id; //eg. Opportunity id, custom object id..
        feedPost.ContentData = Blob.valueof('Test data for pdf file');
        feedPost.Title = 'test.txt';
        feedPost.Body = feedPost.Title;
        feedPost.ContentFileName = feedPost.Title;
        feedPost.Visibility = 'AllUsers';
        insert feedPost;
        
        ApexPages.currentPage().getParameters().put('buyerId',commUser1.Id);
        ApexPages.currentPage().getParameters().put('buyerName','test');
        ApexPages.currentPage().getParameters().put('accountId',beneAccount1.Id);
        ApexPages.currentPage().getParameters().put('retUrl','/testRetUrl');
        
        Global_Pay_ID_Management__c gPIM = new Global_Pay_ID_Management__c(Global_Pay_ID__c='12345',
                                                                              User_Beneficiary__c=commUser.Id,
                                                                              Creation_Type__c='Automatic Email Match');
        insert gPIM;
        
        System.RunAs(commUser){
        	Test.startTest();
        		EDGE_SubmitInvoice ctrl = new EDGE_SubmitInvoice();
        		ctrl.fileId = feedPost.id;
        		ctrl.save();
        		ctrl.saveNew();
        		PageReference pgRef1 = ctrl.uploadAttachmentFeed();
        		ctrl.fileId = '12345';
        		ctrl.invoice.Invoice_Number__c = '3423234';
        		ctrl.save();
        		ctrl.saveNew();
        		PageReference pgRef2 = ctrl.redirectToHome();
        		PageReference pgRef3 = ctrl.deleteAttachmentFeed();
        	Test.stopTest();
        	
        	 ApexPages.currentPage().getParameters().remove('buyerId');
        	 ctrl = new EDGE_SubmitInvoice();
        	 ctrl.saveNew();
        	 ctrl.fileId = feedPost.id;
        }
	}

}