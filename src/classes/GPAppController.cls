public class GPAppController {

	static apexLogHandler.apexLog logPayInvoice = new apexLogHandler.apexLog('GPAppController','payInvoice');
	static apexLogHandler.apexLog logSaveInvoice = new apexLogHandler.apexLog('GPAppController','saveInvoice');
	static apexLogHandler.apexLog logSave = new apexLogHandler.apexLog('GPAppController','save');
	
    public Input__c objInput {get;set;}
    public String baseGPUrl{get;set;}
    public String serviceUrl {get;set;}
    public String serviceParam {get;set;}
    public Boolean showLogin {get;set;}
    public Boolean showPay {get;set;}
    public Boolean showManage {get;set;}
    public Boolean showManageHolding{get;set;}
    public Boolean showCold {get;set;}
    public Boolean showApproveOrder{get;set;}
    //public Input__c input {get;set;}
    public Invoice__c objInvoice {get;set;}
    public Id stdID {get;set;}
    public Boolean inputSaved {get;set;}
    public String language {get;set;}
    public Boolean showBene{get;set;}
    public String callbackUrl{get;set;}
    public String callbackUrlFailure{get;set;}
    private Global_Pay_Integration_Administration__c gPIA;
    public String beneName{get;set;}
    public String beneId{get;set;}
    public String beneEmail{get;set;}
    public String beneCurrency{get;set;}
    public Account cctAccount{get;set;}
    public string isColdPay{get;set;}
    public string mode{get;set;}
    public string cancelCallbackURL{get;set;}
    public string action{get;set;}
    public string accountNumber{get;set;}
    public string bankName{get;set;}
    public string bankCountry{get;set;}
    public string supplierCountry{get;set;}
    public boolean showSelectBene{get;set;}
    public Map<String,String> params;
    public string suppID{get;set;}
    public string returnpageUrl{get;set;}
    public Supplier__c supplier{get;set;}
    public Boolean NoDecimalCurrency{get;set;}
    public String batchId{get;set;}
    public Boolean showMuliplePay{get;set;}
    public	batchInvoices bI {get;set;}
    
	public Map<String,Integer> vals {
	    get {
	        if (vals == null) {
	            vals = new Map<String, Integer> {
	                'Value 1' => 0, 
	                'Value 2' => 0, 
	                'Value 3' => 0};
	        }
	        return vals;
	    }
	    set;
	}

    public Boolean isCMPEnabled{
        get{
            return Utility_Security.isCMPEnabled;
        }set;}
    public void demoCallBackUrl(){
      callbackUrl += 'Ret&beneName=test&beneId=123456&beneCurr=INR';
      if(callbackUrl.containsIgnoreCase('searchBeneRet')){
          List<String> lstUrl = callbackUrl.split('&');
          for(String str: lstUrl){
              if(str.contains('beneName'))
                  beneName = str.split('=')[1];
              else if(str.contains('beneId'))
                  beneId = str.split('=')[1];
              else if(str.contains('beneCurr'))
                  beneCurrency = str.split('=')[1];
          }
      }
    }

    public pageReference saveInvoice(){
    	
    	logSaveInvoice.logMessage('>>inside saveInvoice method ...>> ');
        if(stdID != null) {
            Invoice__c inv = new Invoice__c(Id=stdID);
            populateInvoiceData(inv);
            update inv;
            logSaveInvoice.logDebug(''+inv);
            String encodedInvoiceId = Utility.doEncryption(inv.Id);
            pageReference pg = new pageReference('/EDGE_InvoiceDetail2?Id='+encodedInvoiceId);
           // pg.setRedirect(true);
           	logSaveInvoice.saveLogs();
            return pg;
        }
        logSaveInvoice.saveLogs();
        return null;
    }

    public pageReference payInvoice(){
    	
    	logPayInvoice.logMessage('>>inside payInvoice method ...>> ');
        if(stdID != null) {
            Invoice__c inv = new Invoice__c(Id=stdID);
            populateInvoiceData(inv);
            update inv;
            System.Debug('###'+ApexPages.currentPage().getHeaders().get('Host'));
            logPayInvoice.logDebug(ApexPages.currentPage().getHeaders().get('Host') + '');
            System.Debug('###'+ApexPages.currentPage().getParameters().get('currUrl'));
            logPayInvoice.logDebug(ApexPages.currentPage().getParameters().get('currUrl')+'');
            pagereference pg;
            String encodedInvoiceId = Utility.doEncryption(inv.Id);
            if(ApexPages.currentPage().getParameters().get('currUrl') != null){
             pg = new pageReference('/EDGE_Pay?invoiceId='+encodedInvoiceId+'&retURL=https://'+ApexPages.currentPage().getHeaders().get('Host')+ApexPages.currentPage().getParameters().get('currUrl'));
            }else{
             pg = new pageReference('/EDGE_Pay?invoiceId='+encodedInvoiceId);
            }//pg.setRedirect(true);
            logPayInvoice.saveLogs();
            return pg;
        }
        logPayInvoice.saveLogs();
        return null;
    }

    public void populateInvoiceData(Invoice__c inv){
      System.Debug('###'+accountNumber);
        if(beneCurrency!=null) inv.Custom_Currency__c = beneCurrency;
        if(beneId!=null) inv.Beneficiary_GP_ID__c = beneId;
        if(String.isNotBlank(bankName) && bankName != '') inv.Bank_Name__c = bankName;
        if(String.isNotBlank(accountNumber) && accountNumber != '') inv.Bank_Account_Number__c = accountNumber;
    }

    public String beneRetStatus{get;set;}

    public void updateInvoice(){
    	pageReference pgref = null;
    	returnpageUrl = '';
    	getParamValues();
    	system.debug('start***>>'+action);
    	if('Save'.equalsIgnoreCase(action))
	        pgref= saveInvoice();
	    else if('Pay'.equalsIgnoreCase(action))
	        pgref= payInvoice();
	    system.debug('***>>'+action);
		//Set URL to property onLoadUrlRedirect= pgref.getExternalURL
		if(pgref!=null)
			returnpageUrl = pgref.getUrl();

    }

    public void getParamValues(){
    	params = ApexPages.currentPage().getParameters();
        if(params.containsKey('app')) serviceParam = params.get('app');
        if(params.containsKey('Id')) stdID = EncryptionManager.doDecrypt(params.get('Id'));
        if(params.containsKey('suppId')) suppID =  EncryptionManager.doDecrypt(params.get('suppId'));
        if(params.containsKey('batchId')) batchId = params.get('batchId');        
        if(params.containsKey('gp.beneficiaryName')) beneName = params.get('gp.beneficiaryName');
        if(params.containsKey('gp.beneficiaryId')) beneId = params.get('gp.beneficiaryId');
        if(params.containsKey('gp.beneficiaryEmail')) beneEmail = params.get('gp.beneficiaryEmail');
        if(params.containsKey('gp.currencyId')) beneCurrency = params.get('gp.currencyId');
        if(params.containsKey('gp.action')) action = params.get('gp.action');
        if(params.containsKey('gp.accountNumber')) accountNumber = params.get('gp.accountNumber');
        if(params.containsKey('gp.bankName')) bankName = params.get('gp.bankName');
        if(params.containsKey('gp.bankCountry')) bankCountry = params.get('gp.bankCountry');
        if(params.containsKey('gp.supplierCountry')) supplierCountry = params.get('gp.supplierCountry');
    }
    
    


    public GPAppController()
    {
        //bI = new batchInvoices();
        beneRetStatus = '';
        PageReference pageref = new PageReference(URL.getCurrentRequestUrl().toExternalForm()+'&searchBeneRet=1');
        pageref.getParameters().put('retURL', '');
        callbackUrl = pageRef.getUrl();
        callbackUrlFailure = callbackUrl+'';
        callbackUrlFailure = callbackUrlFailure.replace('GPAppTest', 'EDGE_InvoiceDetail2');


		System.debug('>>> callbackUrl >> '+ callbackUrl);

        if(!isCMPEnabled){
            ApexPages.Message errMsg= new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.GP_Access_Error_For_Non_CMP);
            ApexPages.addmessage(errMsg);
        }

        else{
            gPIA = Global_Pay_Integration_Administration__c.getInstance();
            if(gPIA!=null) {
                baseGPUrl = gPIA.GP_API_baseURL__c;
            }
            stdID = null;

			getParamValues();
            System.debug('****2'+params);
            System.Debug('###'+beneEmail);
            serviceUrl = baseGPUrl + serviceParam;
            if('gp'.equalsIgnoreCase(serviceParam)) showLogin = true;
            else if('payBeneficiary'.equalsIgnoreCase(serviceParam)) {
              if(stdID != null){
              	showPay = true;
	            cancelCallbackURL = callbackUrl+'';
	            cancelCallbackURL = cancelCallbackURL.replace('GPAppTest', 'EDGE_InvoiceDetail2');
	            callbackUrl = cancelCallbackURL;
              }else if(batchId != null){
              	showMuliplePay = true;
	            cancelCallbackURL = callbackUrl+'';
	            cancelCallbackURL = cancelCallbackURL.replace('GPAppTest', 'EDGE_MultiLineOrderStep1');
	            callbackUrl = cancelCallbackURL.replace('EDGE_MultiLineOrderStep1', 'EDGE_MultiLine_Order_Review');
              }
              
            }
            else if('manageBeneficiary'.equalsIgnoreCase(serviceParam)) {
                showManage = true;
                if(params.get('retURL') != null){
                    cancelCallbackURL = callbackUrl+'';
                  cancelCallbackURL = cancelCallbackURL.replace('GPAppTest', params.get('retURL'));
                }
                System.debug('****4');
            }
            else if('SelectBene'.equalsIgnoreCase(serviceParam)){
                showSelectBene = true;
                cancelCallbackURL = callbackUrl+'';
                cancelCallbackURL = cancelCallbackURL.replace('GPAppTest', 'EDGE_InvoiceDetail2');
            }
            else if('manageHoldingBalance'.equalsIgnoreCase(serviceParam)) showManageHolding = true;
            else if('coldPayments'.equalsIgnoreCase(serviceParam)) showCold = true;
            else if('searchBene'.equalsIgnoreCase(serviceParam)) showBene = true;
            else if('approveTransactions'.equalsIgnoreCase(serviceParam)) showApproveOrder = true;
            if(params.containsKey('searchBeneRet')){
               beneRetStatus = 'searchBeneRet';
            }
            /*
            for (string key : params.keySet() ){
              beneName += key + ' : ' + params.get(key) + '\n';
            }*/

            language = UserInfo.getLanguage();
            
            if(batchId != null){
            	 bI = new batchInvoices();
            	for(Invoice__c inv :[SELECT  Id, Name, Custom_Currency__c,Beneficiary_GP_Name__c,
		                              Beneficiary_GP_ID__c, Amount__c,Delivery_Method__c,Reference_Number__c,
		                              Input_Beneficiary__r.Name,Beneficiary_GP_Email_Address__c,
		                              Buyer__r.Contact.AccountId, Buyer__r.Contact.MailingPostalCode,
		                              Buyer__r.Contact.MailingCity, Buyer__r.Contact.MailingCountry,
		                              Buyer__r.Contact.MailingState, Buyer__r.Contact.MailingStreet,
		                              Buyer__r.Contact.Account.CCT_Client_ID__c, Invoice_Number__c,
		                              supplier__c,supplier__r.id,Supplier__r.name,Supplier__r.country__c,Supplier__r.Supplier_Name__c,
		                              supplier__r.Address_line_1__c,supplier__r.City__c,supplier__r.State_Province__c,
		                              supplier__r.Post_Code__c,supplier__r.Country_ISO_Code__c,
		                              RecordType.Name
		                              FROM Invoice__c WHERE Batch_Id__c =: batchId]){
		                              	
		                  bI.beneIds.add(inv.Beneficiary_GP_ID__c != null ? inv.Beneficiary_GP_ID__c:'');
		                  bI.amount.add(inv.Amount__c != null ? inv.Amount__c.toPlainString() : '');
		                  bI.invoiceIds.add(inv.Reference_Number__c != null? inv.Reference_Number__c : inv.id) ;
		                  bI.suppIds.add(inv.supplier__c !=null ? (String)inv.supplier__c : '');
		                  bI.suppNames.add(inv.Supplier__r.name !=null ? inv.Supplier__r.name : '' );
		                  bI.suppSiteIds.add('');
		                  bI.suppStreets.add(inv.supplier__r.Address_line_1__c !=null ? inv.supplier__r.Address_line_1__c :'');
		                  bI.suppCities.add(inv.supplier__r.City__c !=null ? inv.supplier__r.City__c: '' );
		                  bI.suppStates.add(inv.supplier__r.State_Province__c != null ? inv.supplier__r.State_Province__c : '' );
	                      bI.suppZipCodes.add(inv.supplier__r.Post_Code__c != null ? inv.supplier__r.Post_Code__c :'');
	                      bI.suppCountries.add(inv.supplier__r.Country_ISO_Code__c !=null ? inv.supplier__r.Country_ISO_Code__c : '');
	                      bI.isColdPay.add(inv.RecordType.Name != null ? (inv.RecordType.Name.containsIgnoreCase('Cold') ? 'TRUE' : 'FALSE') : 'FALSE');
	                      bI.objectName.add('Invoice__c');
		                  //bI.objectId.add(inv.Invoice_Number__c !=null ? inv.Invoice_Number__c :'');
                  }
                  bI.convertToJson(bI);
            }

            if(stdID != null) {
                objInvoice = [SELECT  Id, Name, Custom_Currency__c,Beneficiary_GP_Name__c,
                              Beneficiary_GP_ID__c, Amount__c,Delivery_Method__c,Reference_Number__c,
                              Input_Beneficiary__r.Name,Beneficiary_GP_Email_Address__c,
                              Buyer__r.Contact.AccountId, Buyer__r.Contact.MailingPostalCode,
                              Buyer__r.Contact.MailingCity, Buyer__r.Contact.MailingCountry,
                              Buyer__r.Contact.MailingState, Buyer__r.Contact.MailingStreet,
                              Buyer__r.Contact.Account.CCT_Client_ID__c, Invoice_Number__c,
                              supplier__c,Supplier__r.name,Supplier__r.country__c,Supplier__r.Supplier_Name__c,
                              supplier__r.Address_line_1__c,supplier__r.City__c,supplier__r.State_Province__c,
                              supplier__r.Post_Code__c,supplier__r.Country_ISO_Code__c,
                              RecordType.Name
                              FROM Invoice__c WHERE Id =: stdID];
                 if(objInvoice.RecordType.Name != null && objInvoice.RecordType.Name.containsIgnoreCase('Cold')){
                    isColdPay = 'TRUE';
                }
                else{
                    isColdPay = 'FALSE';
                }

                List<Currency_Validation_Rules__mdt> listCurrencyRules = [Select id, MasterLabel, Validation_Type__c From Currency_Validation_Rules__mdt Where Validation_Type__c = 'No decimal allowed'];
                for(Currency_Validation_Rules__mdt cr:listCurrencyRules){
                  //NodecimalCurrencies.add(cr.Custom_Label__c);
                  if(objInvoice.Custom_Currency__c == cr.MasterLabel){
                    NoDecimalCurrency = true;
                    break;
                  }
                }

                if(objInvoice.Delivery_Method__c.equalsIgnoreCase('Holding-to-Holding') && !String.isEmpty(objInvoice.Beneficiary_GP_ID__c)) {
                    for(Account acc : [SELECT Id,CCT_Client_ID__c,BillingStreet,BillingCity,
                                        BillingState,BillingCountry,BillingPostalCode,Billing_Address_ISO_Country_Code__c FROM
                                        Account WHERE CCT_Client_ID__c=:objInvoice.Beneficiary_GP_ID__c]) {
                        cctAccount = acc;
                        break;
                    }
                }



            objInput = new Input__c();
            inputSaved = false;
        }
        System.debug('***3'+suppID);
        if(suppID!=null){
            for(Supplier__c supp : [SELECT  id, name, supplier_Name__c, country__c ,Address_line_1__c,
                            City__c ,State_Province__c, Post_Code__c, Country_ISO_Code__c
                            FROM supplier__c Where Id =: suppID])
                            supplier = supp;

        }
    }
}
    public PageReference save() {

		logSave.logMessage('>>inside save method ...>> ');
        try {
          upsert objInput; // inserts the new record into the database
          system.debug('======= objInput ========'+ objInput);
          logSave.logDebug(''+objInput);
          objInput = [SELECT Id, Amount__c, Custom_Currency__c, Reference_Number__c, Input_Beneficiary__r.Name FROM Input__c WHERE Id =: objInput.Id];
          inputSaved = true;
          showCold = false;
        } catch (DMLException e) {
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error creating new Input.'));
          logSave.logDebug(e.getMessage());
          logSave.saveLogs();
          return null;
        }
        logSave.saveLogs();
        return null;
    }
    
    
    public class batchInvoices{
    	public List<String> beneIds;
    	public List<String> amount;
    	public List<string> invoiceIds;
    	public List<string> suppIds;
    	public List<string> suppNames;
    	public List<string> suppSiteIds;
    	public List<string> suppStreets;
    	public List<string> suppCities;
    	public List<string> suppStates;
    	public List<string> suppZipCodes;
    	public List<string> suppCountries;
    	public List<string> isColdPay;
    	public List<string> objectName;
    	public List<string> objectId;
    	
    	public string jsonBeneIds{get;set;}
    	public string jsonAmounts{get;set;}
    	public string jsonInvoiceIds{get;set;}
    	public string jsonSuppIds{get;set;}
    	public string jsonSuppNames{get;set;}
    	public string jsonSuppSiteIds{get;set;}
    	public string jsonSuppStreets{get;set;}
    	public string jsonSuppCities{get;set;}
    	public string jsonSuppStates{get;set;}
    	public string jsonSuppZipCodes{get;set;}
    	public string jsonSuppCountries{get;set;}
    	public string jsonIsColdPay{get;set;}
	    	
    	public batchInvoices(){
             beneIds = new List<String>();
        	 amount = new List<String>();
        	 invoiceIds = new List<String>();
        	 suppIds = new List<string>();
             suppNames = new List<String>();
             suppSiteIds = new List<String>();
        	 suppStreets = new List<String>();
          	 suppCities = new List<String>();
             suppStates = new List<String>();
             suppZipCodes = new List<String>();
             suppCountries = new List<String>();
        	 isColdPay = new List<string>();
        	 objectName = new List<String>();
        	 objectId = new List<String>();
    	}
    	
    	public void convertToJson(batchInvoices bI){
    		jsonBeneIds = JSON.serialize(bI.beneIds);
	    	jsonAmounts = JSON.serialize(bI.amount);
	    	jsonInvoiceIds = JSON.serialize(bI.invoiceIds);
	    	jsonSuppIds = JSON.serialize(bI.suppIds);
	    	jsonSuppNames = JSON.serialize(bI.suppNames);
	    	jsonSuppSiteIds = JSON.serialize(bI.suppSiteIds);
	    	jsonSuppStreets = JSON.serialize(bI.suppStreets);
	    	jsonSuppCities = JSON.serialize(bI.suppCities);
	    	jsonSuppStates = JSON.serialize(bI.suppStates);
	    	jsonSuppZipCodes = JSON.serialize(bI.suppZipCodes);
	    	jsonSuppCountries = JSON.serialize(bI.suppCountries);
	    	jsonIsColdPay = JSON.serialize(bI.isColdPay);
    	}
    }

    /*@future(Callout=true)
    public static void callGP(String inputId) {
          Input__c objInput = [SELECT Id, Amount__c, Custom_Currency__c, Reference_Number__c, Input_Beneficiary__r.Name FROM Input__c WHERE Id =: inputId];

          //POST to GP
          HttpRequest req = new HttpRequest();
          HttpResponse res = new HttpResponse();
          Http http = new Http();

          String reqBody = 'amount=' + objInput.Amount__c;
          reqBody += '&currency=' + objInput.Custom_Currency__c;
          reqBody += '&refNumber=' + objInput.Reference_Number__c;
          reqBody += '&beneficiaryName=' + objInput.Input_Beneficiary__r.Name;
          reqBody += '&objectName= Input__c';
          reqBody += '&objectId=' + objInput.Id;

          req.setEndpoint(gPIA.GP_API_payBeneficiary__c);
          req.setMethod('POST');

          req.setBody(reqBody);
          req.setHeader('Content-Type', 'application/x-www-form-urlencoded');

          try {
              res = http.send(req);
          } catch(System.CalloutException e) {
              //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
              System.debug('Callout error: ' + e);
              //return null;
          }
          System.debug('body**' + res.getBody());
    }*/
}