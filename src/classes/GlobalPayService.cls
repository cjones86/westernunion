//Created By : Ranjeet Singh
//Created date : 5th April 2016
//Description : GlobaRestSvc - Combine GlobalPayResSvc & GlobalUpdateOrDeleteBeneService
//Modified by : Priyanka Kumar  20 May'16 T-504295,T-504297
//Modified by : Priyanka Kumar  T-511032  14 June'16

public without sharing class GlobalPayService extends GlobalPayServiceBase {
    Payment__c payment {get;set;}
    Invoice__c requestedInvoice{get;set;}
    public override String processBRequest(){

        System.debug('*********** GlobalPayService 1.0) Entering processBRequest *****');

        if(invoiceId != null && invoiceId != '') {
            System.debug('*********** GlobalPayService 1.0.1) -- in if -- (invoiceId != null && invoiceId != ) *****');
            //system.debug('*********** GlobalPayService 1.0.2) orderCommittedDate & userID: ' + getSafeMapValue('gp.orderCommittedDate') + ' @' + userinfo.getUserId() + ' *****');

            requestedInvoice = getInvoice(invoiceId);
            payment = new Payment__c();

            if(requestedInvoice!=null) {
                /*******************************************************************
                1.1.0 - Set Invoice record fields - Both Standard and Holding Payments
                *******************************************************************/
                System.debug('********** GlobalPayService 1.1.0) - Set Invoice record fields - Both Standard and Holding Payments *****');

                //requestedInvoice.Amount__c = (String.isNotBlank(amount))?Decimal.valueOf(amount):requestedInvoice.Amount__c;
                //requestedInvoice.Currency__c = currency1;
                //requestedInvoice.custom_currency__c = paymentCurrency;

                //Start : T-513543 : Priyanka
                if(requestedInvoice.supplier__r.Email_Address__c != null && requestedInvoice.Beneficiary_GP_Email_Address__c == null){
                	requestedInvoice.Beneficiary_GP_Email_Address__c = requestedInvoice.supplier__r.Email_Address__c;
                }
                //End : T-513543 : Priyanka

                //Commented below for new logic, T-490096.
                //requestedInvoice.Settlement_Amount__c = (String.isNotBlank(totalAmount))?Decimal.valueOf(totalAmount):requestedInvoice.Settlement_Amount__c;
                //requestedInvoice.Settlement_Amount__c = requestedInvoice.Settlement_Amount__c + ((String.isNotBlank(paymentAmount))?Decimal.valueOf(paymentAmount):requestedInvoice.Amount__c);
                //requestedInvoice.Balance__c = requestedInvoice.Amount__c - requestedInvoice.Settlement_Amount__c;
                //requestedInvoice.Balance__c = requestedInvoice.Amount__c - ((String.isNotBlank(paymentAmount))?Decimal.valueOf(paymentAmount):requestedInvoice.Amount__c);

                /*if(requestedInvoice.Balance__c>0){
                    requestedInvoice.Status__c = 'Partial Payment Submitted';
                }else{
                    requestedInvoice.Status__c = 'Full Payment Submitted';
                }*/


                /*******************************************************************
                1.3 - Process Standard Payments - Type = 'OrderConfirmation'
                *******************************************************************/
                System.debug('********** GlobalPayService 1.3) TransactionType = ' + TransactionType + ' *****');
                if(Transactiontype == 'OrderConfirmation') {
                    ProcesOrderConfirmation();
                }

                /*******************************************************************
                1.4 - Process Holding  Payments - Type = 'H2HConfirmation'
                *******************************************************************/
                else if (Transactiontype == 'H2HConfirmation') {
                    ProcessH2HConfirmation();
                }

                /*******************************************************************
                1.5 - Update Invoice Record
                *******************************************************************/
                //System.debug('********** GlobalPayService 1.5) Update requestedInvoice *****');

                //update requestedInvoice;
                //System.debug('********** GlobalPayService 1.5.1) update requestInvoice : ' + requestedInvoice + ' *****');
            }

            /*******************************************************************
            1.7 - Call processCallToAction method for additional processing
            *******************************************************************/
            System.debug('********** GlobalPayService 1.7) Call method processCallToAction(requestInvoice): ' + requestedInvoice + ' *****');
            //MP processCallToAction(requestedInvoice);

            //T-504295
            if(orderStatus.equalsIgnoreCase('COMMITTED')){
                processCallToAction(requestedInvoice, payment);
                payment.Payment_Status__c = 'Committed';
            }else if(orderStatus.equalsIgnoreCase('PENDING_APPROVAL')){
                payment.Payment_Status__c = 'Pending Approval';
            }else if(orderStatus.equalsIgnoreCase('REJECTED')){
                payment.Payment_Status__c = 'Rejected';
            }
            else{
                payment.Payment_Status__c = 'Pending Approval';
            }

            if(requestedInvoice!=null) {
                System.debug('********** GlobalPayService 1.5) Update requestedInvoice *****');

                System.Debug('###'+requestedInvoice.Payment_Count__c);
                System.debug('###'+Decimal.valueOf(paymentAmount));
                if(orderStatus.equalsIgnoreCase('COMMITTED')){
                  //requestedInvoice.Balance__c = requestedInvoice.Amount__c;
                  if(requestedInvoice.Payment_Count__c == 0)requestedInvoice.Balance__c = requestedInvoice.Amount__c - Decimal.valueOf(paymentAmount);
                  else requestedInvoice.Balance__c = requestedInvoice.Balance__c - Decimal.valueOf(paymentAmount);
                }

                setInvoiceStatus(requestedInvoice, payment);

                update requestedInvoice;
                System.debug('********** GlobalPayService 1.5.1) update requestInvoice : ' + requestedInvoice + ' *****');

                /*******************************************************************
                1.2 - Set Payment record fields - Both Standard and Holding Payments
                *******************************************************************/
                System.debug('********** GlobalPayService 1.2) - Set Payment record fields - Both Standard and Holding Payments *****');

                // payment = new Payment__c();
                payment.Order_ID__c = orderId;
                payment.Payment_Amount__c = (String.isNotBlank(paymentAmount))?Decimal.valueOf(paymentAmount):requestedInvoice.Amount__c;
                payment.Payment_Currency__c = paymentCurrency;
                payment.Payment_Reference__c = paymentReference;
                payment.Settlement_Currency__c = settlementCurrency;
                payment.Settlement_Method__c = settlementMethod;
                payment.Invoice_ID__c = requestedInvoice.Id;
                payment.Payment_Date_Time__c = orderCommittedDate;
                payment.Line_Item_ID__c = lineItemId;
                payment.Payee_Email_Address__c = requestedInvoice.Beneficiary_GP_Email_Address__c;
                payment.Additional_Email_1__c = requestedInvoice.Supplier__r.Additional_Email_1__c;
                payment.Additional_Email_2__c = requestedInvoice.Supplier__r.Additional_Email_2__c;
                payment.Settlement_Amount__c = (String.isNotBlank(totalAmount))?Decimal.valueOf(totalAmount):requestedInvoice.Amount__c;
                //if(Transactiontype == 'OrderConfirmation') {
                  //}
                system.debug('********** GlobalPayService X.X.1) processCallToAction: Payment Invite_ID__c = ' + payment.Invite_ID__c + '*****');
                system.debug('********** GlobalPayService X.X.2) processCallToAction: Payment Notification_Type__c = ' + payment.Notification_Type__c + '*****');

//MP /////////////////////////////////////////////////////////////////////
//        newPayment.Notification_Type__c = 'Invite';
//        newPayment.Invite_ID__c = invite.Id;  //Newly Inserted Invite
//        string result = invite.id;
//        system.debug('********** GlobalPayService X.X.1) processCallToAction: Inserted Payment.Id (result) = ' + result + '*****');
//        update newPayment;
//        System.debug('********** GlobalPayService X.X.3) processCallToAction: Invite.Notification_Type__c = ' + Invite.Notification_Type__c + ' *****');
//////////////////////////////////////////////////////////////////////////

                /*******************************************************************
                1.6 - Insert Payment Record
                *******************************************************************/
                System.debug('********** GlobalPayService 1.6) Insert Payment Record *****');

                upsert payment Line_Item_ID__c;

            }
        }
        return 'success';
    }

    /*******************************************************************
    2.0 Parse ProcesOrderConfirmation
    *******************************************************************/
    void ProcesOrderConfirmation(){
        System.debug('********** GlobalPayService 2.0) Entering ProcessOrder Confirmation *****');

        /*******************************************************************
        2.1 - Get Invoice record values for just Standard Payments
        *******************************************************************/
        System.debug('********** GlobalPayService 2.1) - Get Invoice record values for just Standard Payments *****');

        String city = (String)getSafeMapValue('gp.beneficiaryCity');
        String beneficiaryCountry = (String)getSafeMapValue('gp.beneficiaryCountry');
        String beneficiaryState = (String)getSafeMapValue('gp.beneficiaryState');
        String beneficiaryName = (String)getSafeMapValue('gp.beneficiaryName');
        String beneficiaryPostCode = (String)getSafeMapValue('gp.beneficiaryPostCode');
        String beneficiaryStreetAddress = (String)getSafeMapValue('gp.beneficiaryStreetAddress');
        String notesToBene = (String)getSafeMapValue('gp.notesToBene');
        String beneficiaryEmail = (String)getSafeMapValue('gp.beneficiaryEmail');
        String paymentBankAccountLastFour = (String)getSafeMapValue('gp.accountNumber');

        System.debug('********** GlobalPayService 2.1.1) Variables - city: ' + city + ' *****');
        System.debug('********** GlobalPayService 2.1.2) Variables - beneficiaryCountry: ' + beneficiaryCountry + ' *****');
        System.debug('********** GlobalPayService 2.1.3) Variables - beneficiaryState: ' + beneficiaryState + ' *****');
        System.debug('********** GlobalPayService 2.1.4) Variables - beneficiaryName: ' + beneficiaryName + ' *****');
        System.debug('********** GlobalPayService 2.1.5) Variables - beneficiaryPostCode: ' + beneficiaryPostCode + ' *****');
        System.debug('********** GlobalPayService 2.1.6) Variables - beneficiaryStreetAddress: ' + beneficiaryStreetAddress + ' *****');
        System.debug('********** GlobalPayService 2.1.7) Variables - notesToBene: ' + notesToBene + ' *****');
        System.debug('********** GlobalPayService 2.1.8) Variables - beneficiaryEmail: ' + beneficiaryEmail + ' *****');
        System.debug('********** GlobalPayService 2.1.9) Variables - paymentBankAccountLastFour: ' + paymentBankAccountLastFour + ' *****');

        /*******************************************************************
        2.2 - Set Invoice fields to update for just Standard Payments
        *******************************************************************/
        System.debug('********** GlobalPayService 2.2) - Update Invoice Record for just Standard Payments *****');

        requestedInvoice.Beneficiary_GP_ID__c = GlobalPayId;
        requestedInvoice.Beneficiary_GP_Name__c = beneficiaryName;
        //if(requestedInvoice.Beneficiary_GP_Email_Address__c == null) requestedInvoice.Beneficiary_GP_Email_Address__c = beneficiaryEmail;
        requestedInvoice.Beneficiary_GP_Country__c = beneficiaryCountry;

        /*******************************************************************
        2.3 - Set Payment fields to insert for just Standard Payments
        *******************************************************************/
        System.debug('********** GlobalPayService 2.3) - Set Payment fields to insert for just Standard Payments *****');

        payment.Payment_Bank_Account_Last_Four__c = paymentBankAccountLastFour;
        payment.Notes_to_Bene__c = notesToBene;

    }

    /*******************************************************************
    3.0 Parse H2HConfirmation
    *******************************************************************/
    void ProcessH2HConfirmation(){
        System.debug('********** GlobalPayService 3.0) Entering ProcessH2HConfirmation  *****');

        /*******************************************************************
        3.1 - Get Invoice record values for just Holding Payments
        *******************************************************************/
        System.debug('********** GlobalPayService 3.1) - Get Invoice record values for just Holding Payments *****');
        String cctClientId = (String)getSafeMapValue('payeeCctClientId');
        String payeeName = (String)getSafeMapValue('payeeName');
        String payeeAccountId = (String)getSafeMapValue('payeeAccountId');
        String beneficiaryStreetAddress = (String)getSafeMapValue('gp.street');
        String city = (String)getSafeMapValue('gp.city');
        String beneficiaryState = (String)getSafeMapValue('gp.state');
        String beneficiaryPostCode = (String)getSafeMapValue('gp.zip');
        String beneficiaryCountry = (String)getSafeMapValue('gp.country');

        System.debug('********** GlobalPayService 3.1.1) Variables - cctClientId: ' + cctClientId + ' *****');
        System.debug('********** GlobalPayService 3.1.2) Variables - payeeName: ' + payeeName + ' *****');
        System.debug('********** GlobalPayService 3.1.3) Variables - payeeAccountId: ' + payeeAccountId + ' *****');
        System.debug('********** GlobalPayService 3.1.4) Variables - beneficiaryStreetAddress: ' + beneficiaryStreetAddress + ' *****');
        System.debug('********** GlobalPayService 3.1.5) Variables - city: ' + city + '**********');
        System.debug('********** GlobalPayService 3.1.6) Variables - beneficiaryState: ' + beneficiaryState + ' *****');
        System.debug('********** GlobalPayService 3.1.7) Variables - beneficiaryPostCode: ' + beneficiaryPostCode + ' *****');
        System.debug('********** GlobalPayService 3.1.8) Variables - beneficiaryCountry: ' + beneficiaryCountry + ' *****');

        /*******************************************************************
        3.2 - Set Invoice fields to update for just Holding Payments
        *******************************************************************/
        System.debug('********** GlobalPayService 3.2) Set Invoice fields to update for just Holding Payments *****');

        //NOTE:  Nothing different at this point to update for just holding payments

       /*******************************************************************
       3.3 - Set Payment fields to insert for just Holding Payments
       *******************************************************************/
       System.debug('********** GlobalPayService 3.3) Set Payment fields to insert for just Holding Payments *****');

       // NOTE:  Nothing different at this point to insert for just holding payments…

    }

    /*******************************************************************
    4.0 Get fields from fieldset
    *******************************************************************/
    private static Map<String,Schema.SObjectField> getFields() {
        System.debug('********** GlobalPayService 4.0) Entering Map: Get fields from fieldset *****');

        SObjectType invoiceType = Schema.getGlobalDescribe().get('Invoice__c');
        return invoiceType.getDescribe().fields.getMap();
    }

    /*******************************************************************
    5.0 Get Invoice with all the fields in the field set
    *******************************************************************/
    private static Invoice__c getInvoice(String id) {
        System.debug('********** GlobalPayService 5.0) Entering List<Invoice__c>: Get Invoice with all fields in field set *****');

        String query = 'SELECT ';
        for(String field : getFields().KeySet()) {
            query += field + ', ';
        }
        query += 'Input_Beneficiary__r.Name, Input_Beneficiary__r.OwnerId, Input_Beneficiary__r.Buyer_Supplier_Number__c,Buyer__r.Id,Buyer__r.ContactId,Buyer__r.contact.AccountId,Supplier__r.Email_Address__c, Supplier__r.Additional_Email_1__c,Supplier__r.Additional_Email_2__c FROM Invoice__c Where Id =: id LIMIT 1';
        List<Invoice__c> invoices =  Database.query(query);
        if(invoices.size()>0){
            return invoices[0];
        }
        return null;
    }

    /*******************************************************************
    6.0 processCallToAction() to take action on Invoice as necessary
    *******************************************************************/
    //MP public static void processCallToAction(Invoice__c requestInvoice) {
    public static void processCallToAction(Invoice__c requestInvoice, Payment__c newPayment) {
        System.debug('********** GlobalPayService 6.0) Entering Global Class GlobalPayRestSvc: processCallToAction(Invoice__c requestInvoice, Payment__c newPayment)' + newPayment + ' *****');

        //if(('Full Payment Submitted'.equalsIgnoreCase(requestInvoice.Status__c) || 'Partial Payment Submitted'.equalsIgnoreCase(requestInvoice.Status__c)) && requestInvoice.Beneficiary_GP_ID__c!=null ) {
            System.debug('********** GlobalPayService 6.1) processCallToAction - Entering If: Status__c = Partial or Full Payment Submitted *****');

            if(!String.isEmpty(requestInvoice.supplier__r.Email_address__c) && !requestInvoice.Invite_to_my_network__c){
              newPayment.Notification_Type__c = 'Payment Only';
            }
            else{
            // Check to see if Global Pay ID Management record exists for Buyer/Seller
            Global_Pay_ID_Management__c gPIdM = null;
            for(Global_Pay_ID_Management__c gPId : [SELECT Global_Pay_ID__c, Creation_Type__c, User_Beneficiary__c, User_Beneficiary__r.ContactId, User_Beneficiary__r.Contact.AccountId FROM
                                                    Global_Pay_ID_Management__c WHERE Global_Pay_ID__c = :requestInvoice.Beneficiary_GP_ID__c]) {
                gPIdM = gPId;
                System.debug('********** GlobalPayService 6.2) processCallToAction - gPIdM: ' + gPIdM + ' *****');
                break;
            }

            // Global Pay ID Management record exists - Send Payment
            if(gPIdM != null) {
                System.debug('********** GlobalPayService 6.3) processCallToAction - Entering If: gPIdM != null) *****');
                requestInvoice.Seller_Account__c = gPIdM.User_Beneficiary__r.Contact.AccountId;
                newPayment.Notification_User__c = gPIdM.User_Beneficiary__c;
                //MP sendInvitation(gPIdM, requestInvoice);
                sendInvitation(gPIdm, requestInvoice, newPayment);
                System.debug('********** GlobalPayService 6.4) processCallToAction: sendInvitation(gPIdM, requestInvoice); *****');

            } else{
                // Global Pay ID Management record does not exist but Email is provided and Invite = Yes
                //if(!String.isEmpty(requestInvoice.supplier__r.Email_address__c) && requestInvoice.Invite_to_my_network__c) {
                    System.debug('********** GlobalPayService 6.5) processCallToAction - Entering else if: gPIdM == null) & !String.isEmpty(requestInvoice.Beneficiary_GP_Email_Address__c)*****');
                    System.debug('********** GlobalPayService 6.5.1) processCallToAction requestInvoice.Beneficiary_GP_Email_Address__c: ' + requestInvoice.Beneficiary_GP_Email_Address__c + ' *****');
                    System.debug('********** GlobalPayService 6.5.2) processCallToAction requestInvoice.Invite_to_my_network__c: ' + requestInvoice.Invite_to_my_network__c + ' *****');
                    System.debug('********** GlobalPayService 6.5.3) processCallToAction requestInvoice.supplier__r.Email_address__c: ' + requestInvoice.supplier__r.Email_address__c + ' *****');

                    //MP - Review this section
                    User cmpUser = null;
                    for(User userTemp : [SELECT Id FROM User WHERE Email = :requestInvoice.supplier__r.Email_address__c AND CMP_Enabled__c = TRUE]) {
                        cmpUser = userTemp;
                        break;
                    }

                    // User Match found for Email Address - Invite to Network
                    if(cmpUser != null) {
                        System.debug('********** GlobalPayService 6.6) processCallToAction - Entering if: cmpUser != null *****');


                        Global_Pay_ID_Management__c gPIM = new Global_Pay_ID_Management__c(Global_Pay_ID__c = requestInvoice.Beneficiary_GP_ID__c,
                                                                               Source_Email_Match__c=requestInvoice.Supplier__r.Email_Address__c,
                                                                               Source_Invoice_ID__c = requestInvoice.Id,
                                                                               User_Beneficiary__c = cmpUser.Id,
                                                                               Creation_Type__c = 'Automatic Email Match');
                        insert gPIM;

                        gPIdM = [SELECT Global_Pay_ID__c,Creation_Type__c,User_Beneficiary__c,User_Beneficiary__r.ContactId,User_Beneficiary__r.Contact.AccountId FROM
                                                        Global_Pay_ID_Management__c WHERE Id = :gPIM.Id];

                        requestInvoice.Seller_Account__c = gPIdM.User_Beneficiary__r.Contact.AccountId;
                        newPayment.Notification_User__c = gPIdM.User_Beneficiary__c;
                        System.debug('********** GlobalPayService 6.7) processCallToAction: Call Method sendInvitation(gPIdM, requestInvoice) *****');
                        //MP sendInvitation(gPIdM, requestInvoice);
                        sendInvitation(gPIdM, requestInvoice, newPayment);

                    } else {
                        // User Match not found for Email Address - Invite to WU Edge
                        System.debug('********** GlobalPayService 6.8) processCallToAction - Entering else: cmpUser = null *****');
                        Invites__c relatedInvites = null;
                        for(Invites__c invites : [SELECT Id, Status__c,Last_Invitation_Sent_Date_Time__c FROM Invites__c WHERE Benne_Email__c=:requestInvoice.Beneficiary_GP_Email_Address__c AND Inviter_User__c =: requestInvoice.Buyer__r.Id]) {
                            relatedInvites = invites;
                            break;
                        }
                        if(relatedInvites==null) {
                            Invites__c invite = new Invites__c(Invitation_Sent_Count__c = 1, Benne_Email__c = requestInvoice.Beneficiary_GP_Email_Address__c, Inviter_User__c = requestInvoice.Buyer__r.Id,
                                                               Status__c = 'Sent', Invoice__c = requestInvoice.Id, Invitation_Token__c = GPIntegrationUtility.generateInvitesTOKEN('Invitation_Token__c'),
                                                               Last_Sent_By_User__c = requestInvoice.OwnerId, Status_DateTime__c = system.now());

                            System.debug('********** GlobalPayService 6.9) Global processCallToAction: insert invite ' + invite + ' *****');
                            insert invite;
                            relatedInvites = invite;
                        } else {
                            relatedInvites.Last_Invitation_Sent_Date_Time__c = system.now();
                            update relatedInvites;
                        }

                        //MP 2016-04-05 ////////////////////////////
                        newPayment.Notification_Type__c = 'Invite';
                        newPayment.Invite_ID__c = relatedInvites.Id;  //Newly Inserted Invite
                        system.debug('********** GlobalPayService X.X.3) processCallToAction: newPayment.Notification_Type__c = ' + newPayment.Notification_Type__c + '*****');
                        system.debug('********** GlobalPayService X.X.4) processCallToAction: newPayment.Invite_ID__c = ' + newPayment.Invite_ID__c + '*****');
                        system.debug('********** GlobalPayService X.X.5) processCallToAction: newPayment.Payee_Email_Address__c = ' + newPayment.Payee_Email_Address__c + '*****');
//                        update newPayment;
                        //////////////////////////////////////////////
                    }
                //}
              //}
            }
        //}
    }
    }

    /*******************************************************************
    7.0 sendInvitation() to determine which invites/emails to send
    *******************************************************************/
    //MP private static void sendInvitation(Global_Pay_ID_Management__c gPIdM, Invoice__c requestInvoice) {
    private static void sendInvitation(Global_Pay_ID_Management__c gPIdM, Invoice__c requestInvoice, Payment__c newPayment) {
        System.debug('********** GlobalPayService 7.0) Entering Global Class GlobalPayRestSvc: sendInvitation(Global_Pay_ID_Management__c gPIdM,Invoice__c requestInvoice, Payment__c newPayment) ) *****');


        Network__c relatedNetworks = null;
        /*
        Invites__c relatedInvites = null;
        // See if any WUBS EDGE invitations already exist for the Bene...
        for(Invites__c invites : [SELECT Id, Status__c FROM Invites__c WHERE Benne_Email__c=:requestInvoice.Beneficiary_GP_Email_Address__c]) {
            relatedInvites = invites;
            break;
        }

        System.debug('********** GlobalPayService 7.1) relatedInvites: ' + relatedInvites + ' *****');*/

        // See if any Network invitations already exist for the Bene...
        for(Network__c networks : [SELECT Id, Status__c FROM Network__c WHERE (Account_Invitee__c = :gPIdM.User_Beneficiary__r.Contact.AccountId AND Account_Inviter__c = :requestInvoice.Buyer__r.Contact.AccountId) OR
                              (Account_Inviter__c = :gPIdM.User_Beneficiary__r.Contact.AccountId AND Account_Invitee__c = :requestInvoice.Buyer__r.Contact.AccountId)]) {
            relatedNetworks = networks;
            break;
        }

        System.debug('********** GlobalPayService 7.2) relatedNetworks: ' + relatedNetworks + ' *****');

        // If neither WUBS EDGE Invite or Network Invite, then Invite Bene to WUBS EDGE
        if(/*relatedInvites != null && */relatedNetworks == null) {
            if(requestInvoice.Invite_to_my_network__c){
                System.debug('********** GlobalPayService 7.3) sendInvitation - Entering if: relatedInvite == null && relatedNetworks == null *****');

                /*Invites__c invite = new Invites__c(Invitation_Sent_Count__c = 1, Benne_Email__c = requestInvoice.Beneficiary_GP_Email_Address__c, Inviter_User__c = requestInvoice.Buyer__r.Id,
                                                   Invitee_Contact__c = gPIdM.User_Beneficiary__r.ContactId,Status__c = 'Sent',Invoice__c = requestInvoice.Id,
                                                   Invitation_Token__c = GPIntegrationUtility.generateInvitesTOKEN('Invitation_Token__c'),
                                                   Last_Sent_By_User__c = requestInvoice.OwnerId, Status_DateTime__c = system.now());

                insert invite;
                relatedInvites = invite;*/

                //System.debug('********** GlobalPayService 7.4.1) invite: ' + invite + ' *****');
                //System.debug('********** GlobalPayService 7.4.2) relatedInvites: ' + relatedInvites + ' *****');

                Network__c newNetwork = new Network__c(Invitee_Contact__c = gPIdM.User_Beneficiary__r.ContactId, Account_Invitee__c = gPIdM.User_Beneficiary__r.Contact.AccountId,
                                                       Inviter_User__c = requestInvoice.Buyer__r.Id, Account_Inviter__c = requestInvoice.Buyer__r.Contact.AccountId,
                                                       Status__c='1 - Pending (Sent)', Source_Invoice__c = requestInvoice.Id, Status_DateTime__c = system.now());

                insert newNetwork;
                relatedNetworks = newNetwork;

                System.debug('********** GlobalPayService 7.5.1) newNetwork: ' + newNetwork + ' *****');
                System.debug('********** GlobalPayService 7.5.2) relatedNetworks: ' + relatedNetworks + ' *****');

                //MP 2016-04-05 ////////////////////////////
                newPayment.Notification_Type__c = 'Network';
                //newPayment.Invite_ID__c = invite.Id;  //Newly Inserted Invite
                system.debug('********** GlobalPayService 7.5.3) processCallToAction: newPayment.Notification_Type__c = ' + newPayment.Notification_Type__c + '*****');
                system.debug('********** GlobalPayService 7.5.4) processCallToAction: newPayment.Invite_ID__c = ' + newPayment.Invite_ID__c + '*****');
//                update newPayment;
                //////////////////////////////////////////////
            }

        } else {
            //if(relatedNetworks != null )
                //  Already on network

                if (relatedNetworks.Status__c.contains('Accepted')) {
                    //  already on network, send Payment_Notification
                    System.debug('********** GlobalPayService 7.6.1) sendInvitation - Entering else if: relatedNetworks != null && relatedNetworks.Status__c != 3 – Active (Accepted) *****');
                    /*requestInvoice.Send_Payment_Notification__c = true;
                    update requestInvoice;*/
                    System.debug('********** GlobalPayService 7.6.2) Update requestInvoice ' + requestInvoice + ' *****');
                    //MP 2016-04-05 ////////////////////////////
                    newPayment.Notification_Type__c = 'Payment Only';
                    system.debug('********** GlobalPayService 7.6.3) processCallToAction: newPayment.Notification_Type__c = ' + newPayment.Notification_Type__c + '*****');
//                    update newPayment;
                    //////////////////////////////////////////////
                }
                //T-490096, If a Network__c record is found for Kwong & Jennifer but the status is not '3 - Active (Accepted)', then set it to "0 - Resend".
                if(!relatedNetworks.Status__c.contains('Accepted')){
                    /*relatedNetworks.Status__c = '0 - Resend';
                    update relatedNetworks;*/
                    System.debug('********** GlobalPayService 7.6.2) Update relatedNetworks Record ' + relatedNetworks + ' *****');
                    newPayment.Notification_Type__c = 'Network';
                }
            }
            /*
            //T-490096, If an Invites__ record is found for Kwong:, If Status is not "Accepted", then set it to "Resend"
            if(relatedInvites!=null && !('Accepted'.equalsIgnoreCase(relatedInvites.Status__c))){
                relatedInvites.Status__c = 'Resend';
                update relatedInvites;
                System.debug('********** GlobalPayService 7.6.2) Update relatedInvites Record ' + relatedInvites + ' *****');
            }*/

            /* Priyanka : Changes Start : T-511032 */
            if(newPayment.Notification_Type__c == 'Network' || newPayment.Notification_Type__c == 'Payment Only'){
            	 //get the supplier record and check if it not populated
            	 Supplier__c sup = [SELECT Id,Supplier__c,Buyer__c FROM Supplier__c WHERE Id = :requestInvoice.Supplier__c LIMIT 1];
            	 if(sup.Supplier__c == null){
            	 	 sup.Supplier__c = gPIdM.User_Beneficiary__r.Contact.AccountId;
            	 	 update sup;
            	 }
            }
            /* Priyanka : Changes End : T-511032 */
    }

    /**************************************************************************************
    setInvoiceStatus : Update Invoice Status based on following scenarios of payment status
    T-504297
    **************************************************************************************/
     public static void setInvoiceStatus(Invoice__c requestInvoice, Payment__c newPayment) {
          if((requestInvoice.status__c != 'Partial Payment Submitted' && requestInvoice.status__c != 'Full Payment Submitted') && newPayment.Payment_Status__c == 'Pending Approval'){
            requestInvoice.status__c = 'Payment Approval Pending';
          }else if((requestInvoice.status__c != 'Partial Payment Submitted' && requestInvoice.status__c != 'Full Payment Submitted') && newPayment.Payment_Status__c == 'Rejected'){
            requestInvoice.status__c = 'Payment Approval Rejected';
          }else if((requestInvoice.status__c != 'Partial Payment Submitted' && requestInvoice.status__c != 'Full Payment Submitted') && newPayment.Payment_Status__c == 'Committed'){
                if(requestInvoice.Balance__c > 0){
                    requestInvoice.Status__c = 'Partial Payment Submitted';
                }else{
                    requestInvoice.Status__c = 'Full Payment Submitted';
                }
          }
        //   else if((requestInvoice.status__c == 'Partial Payment Submitted' || requestInvoice.status__c != 'Full Payment Submitted') &&
        //           (newPayment.Payment_Status__c == 'Pending Approval' || newPayment.Payment_Status__c == 'Rejected')){
        //         if(requestInvoice.Balance__c > 0){
        //             requestInvoice.Status__c = 'Partial Payment Submitted';
        //         }else{
        //             requestInvoice.Status__c = 'Full Payment Submitted';
        //         }
        //   }
         //I-233602  Modification
         else if((requestInvoice.status__c == 'Partial Payment Submitted' || requestInvoice.status__c == 'Full Payment Submitted') && newPayment.Payment_Status__c =='Committed'){
  	     	 if(requestInvoice.Balance__c > 0){
                    requestInvoice.Status__c = 'Partial Payment Submitted';
 			 }else{
                    requestInvoice.Status__c = 'Full Payment Submitted';
             }
          }
      }


}