public class CM_EDGE_EDGE_Network_Invite_Email_Ctrl
{
    public Network__C relatedTo{get;set;}
    public void CM_EDGE_EDGE_Network_Invite_Email_Ctrl(){
 //      for(user u1 :[Select id from user where contact.id =: contactId] ){
      ////    System.assert(false ,u1.id);
       // }
    }
    public String RecipentLang{get
    {
      // System.assert(false,relatedTo.Invitee_Contact__c );
        User recipentUser ;
        Network__C recipentContact;
        if(relatedTo != null){
        for(network__C c  :[Select id ,Invitee_Contact__c from network__C where id = : relatedTo.id])
        {
            recipentContact = c;
        }}
        if(recipentContact != null){
            for(User c : [Select id, LanguageLocaleKey from user where contactId = :recipentContact.Invitee_Contact__c]){
                recipentUser =  c;
            }
             if(recipentUser != null){
                return recipentUser.LanguageLocaleKey;
             }
            else {
                return 'en';
            }
        }
        else
        {
             return 'en';
        }
    }

}
}