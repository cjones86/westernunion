/**=====================================================================
 * Name: EDGE_SupplierTriggerHandler
 * Description: Trigger Class to update the Supplier network records
 * Created Date: May 11, 2016
 * Created By: Priyanka Kumar
 * Task      : T-501534
 =====================================================================*/
public without sharing class EDGE_SupplierTriggerHandler {


    public void onBeforeInsertUpdate(list<Supplier__c> newList,Map<Id,Supplier__c> oldMap) {
    	/*List<Supplier__c> filteredRecords = new List<Supplier__c>();
      for(Supplier__c sup : newList){
				 if(sup.Supplier__c == null && sup.Email_Address__c != null && (oldMap != null && (sup.Email_Address__c != oldMap.get(sup.Id).Email_Address__c)  || oldMap == null)){
					 emailFilteredList.put(sup.Email_Address__c,sup);
				 }
      	 if(sup.Supplier__c != null && (oldMap != null && (sup.Supplier__c != oldMap.get(sup.Id).Supplier__c)  || oldMap == null)){
      	 	 filteredRecords.add(sup);
      	 }
      }
			System.Debug('###'+emailFilteredList);*/
      populateSupplieronEmail(newList,oldMap);
      populateNetworkLookUp(newList,oldMap);
    }

		private void populateSupplieronEmail(list<Supplier__c> newList,Map<Id,Supplier__c> oldMap){
			Map<String, Supplier__c> emailFilteredList = new Map<String, Supplier__c>();

			for(Supplier__c sup : newList){
				 if(sup.Supplier__c == null && sup.Email_Address__c != null && (oldMap != null && (sup.Email_Address__c != oldMap.get(sup.Id).Email_Address__c)  || oldMap == null)){
					 emailFilteredList.put(sup.Email_Address__c,sup);
				 }
      }
			System.Debug('###'+emailFilteredList);
      if(emailFilteredList.size() > 0){
				List<Contact> listContacts = new List<Contact>([Select id, Email, AccountId From Contact Where Email IN :emailFilteredList.keySet()]);
				System.Debug('###'+listContacts);
				System.Debug('###'+emailFilteredList.keyset());
				Supplier__c Sup;
				for(Contact c:listContacts){
					if(c.AccountId != null){
						sup = emailFilteredList.get(c.Email);
						sup.Supplier__c = c.AccountId;
					}
				}
			}
		}

     private void populateNetworkLookUp(list<Supplier__c> newList,Map<Id,Supplier__c> oldMap) {
			 List<Supplier__c> filteredRecords = new List<Supplier__c>();
	    Set<Id> supplierSet = new Set<Id>();
	    Set<Id> buyerSet = new Set<Id>();
			for(Supplier__c sup : newList){
      	 if(sup.Supplier__c != null && (oldMap != null && (sup.Supplier__c != oldMap.get(sup.Id).Supplier__c)  || oldMap == null)){
      	 	 filteredRecords.add(sup);
      	 }
      }

	    for(Supplier__c record : filteredRecords){
	      supplierSet.add(record.Supplier__c);
	      buyerSet.add(record.Buyer__c);
	    }

     	//get all the network records
     	List<Network__c> networkList = [SELECT Id,Account_Invitee__c,Account_Inviter__c
     	                                FROM Network__c WHERE Account_Invitee__c != null
     	                                AND Account_Inviter__c != null
     	                                AND (Account_Inviter__c IN :supplierSet OR Account_Inviter__c IN :buyerSet)];

     	  for(Supplier__c sup : filteredRecords){
     	  	if(sup.Supplier__c != null){
     	  		  for(Network__c net : networkList){
     	  		  	if((net.Account_Invitee__c == sup.Supplier__c && net.Account_Inviter__c == sup.Buyer__c) ||
                  (net.Account_Invitee__c == sup.Buyer__c && net.Account_Inviter__c == sup.Supplier__c)){
                	   sup.network__c = net.Id;
                	   break;
                }
     	  		  }
     	  	}
     	  }
     }

}