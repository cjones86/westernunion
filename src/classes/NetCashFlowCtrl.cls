/**=====================================================================
 * Name: NetCashFlowCtrl
 * Description: Related to NetCashFlow Page
 * Created Date: Jan 28, 2015
 * Created By: Nishant Bansal (JDC)
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
global without sharing class NetCashFlowCtrl {
 //without sharing
    public static Integer selectedMonth{get;set;}
    public static String SelectedForwardType{get;set;}
    public static String selectedCurrency{get;set;}
    public List<wrapperInvoice> wrapperList{get;set;}
    public String chartLanguage{get;set;}
    public String userCurrency{get;set;}
    public static AggregateResult[] aggResult;
    public static AggregateResult[] aggResultInvoice;
    //public static String colorSet{get;set;}
    public String PickListValues { get; set; }
    public Id accId{get;set;}
    public String userLanguage{get;set;}

    private static String TRADING_RECEIPTS = Label.CM_Community_Text_FieldFilter_TransactionTypeFilter_Item_ForecastInflow;
    private static String PURCHASE_ORDER = Label.CM_Edge_Purchase_Order;
    private static String FORECAST_ORDER = Label.CM_Community_Text_FieldFilter_TransactionTypeFilter_Item_ForecastOutflow;

    private static String invoiceLabel = Label.CM_Edge_Invoice;
    private static String voidLabel = Label.CM_Community_Text_FieldFilter_InvoiceStatus_Item_Void;
    private static String paidLabel = Label.CM_FieldLabel_Input_Outgoing_Paid;
    private static String draftLabel = Label.CM_Community_Text_FieldFilter_InvoiceStatus_Item_Draft;
    private static String ackLabel = Label.CM_Community_Text_FieldFilter_InvoiceStatus_Item_Acknowledged;
    private static String invPaidLabel = Label.CM_Community_Text_FieldFilter_InvoiceStatus_Item_Paid;


    private static String partialPymntSubmittedLabel = Label.CM_Edge_Partial_Payment;
    private static String fullPymntSubmittedLabel = Label.CM_Edge_Full_Payment;
    private static String invoiceSubmittedLabel = Label.CM_Edge_Invoice_Submitted;
    private static String sellerLabel = Label.CM_Edge_Seller;
    private Static String fcType ;


    public NetCashFlowCtrl(){
        selectedMonth = 6;
        selectedCurrency = Label.CM_Edge_Total;//UserInfo.getDefaultCurrency();UserInfo.getDefaultCurrency();
        //={!selectedCurrency}&month={!selectedMonth}
        SelectedForwardType = 'Earliest';
        userCurrency = Utility.userCurrency;

        if(system.currentPageReference()!=null && !String.isBlank( system.currentPageReference().getParameters().get('currency'))) {
            selectedCurrency = system.currentPageReference().getParameters().get('currency');
        }
        if(system.currentPageReference()!=null && !String.isBlank( system.currentPageReference().getParameters().get('month'))) {
            selectedMonth = Integer.valueOf(system.currentPageReference().getParameters().get('month'));
        }

        wrapperList = loadOpps(selectedCurrency, selectedMonth ,SelectedForwardType);
        User userLoggedIn = Utility.loggedInUser;
        accId = Utility.currentAccount;
        userLanguage = UserInfo.getLanguage();
        chartLanguage = UserInfo.getLanguage();
    }

    //Getting data from exising record only for netcashFlow.
    public List<SelectOption> getaccountNetcashCurrency(){
        list<Selectoption> lstCurrency = new list<Selectoption>();
        lstCurrency.add(new Selectoption(Label.CM_Edge_Total,System.Label.CM_Community_Text_FieldFilter_CurrencyFilter_Item_TotalIn+' ' + Utility.userCurrency));
        lstCurrency.addAll(Utility.getaccountNetcashCurrency());
        return lstCurrency;
    }


    //Getting data from exising record I-149395, This select netcashFlow+Summary Report currency.
    public List<SelectOption> getaccountTradedCurrency(){
        list<Selectoption> lstCurrency = new list<Selectoption>();

        //lstCurrency.add(new Selectoption(Label.CM_Edge_Total,System.Label.CM_Community_Text_FieldFilter_CurrencyFilter_Item_TotalIn+' ' + Utility.userCurrency));
        system.debug('### utility loggedInUser ' + Utility.loggedInUser);
        lstCurrency.addAll(Utility.getaccountTradedCurrency());
        system.debug('----------ashish---------'+lstCurrency);
        for(SelectOption sOpt : Utility.getCurrencyForFC()){
            Boolean isMatch = false;
            for(SelectOption sOpt2 : lstCurrency){
                if(sOpt.getValue() == sOpt2.getValue()){
                    isMatch = true;
                    break;
                }
            }
            if(!isMatch){
                lstCurrency.add(sOpt);
            }
        }
        system.debug('----------raghu1---------'+lstCurrency);
        for(SelectOption sOpt : Utility.getCurrencyForHB()){
            Boolean isMatch = false;
            for(SelectOption sOpt2 : lstCurrency){
                if(sOpt.getValue() == sOpt2.getValue()){
                    isMatch = true;
                    break;
                }
            }
            if(!isMatch){
                lstCurrency.add(sOpt);
            }
        }
        system.debug('----------raghu2---------'+lstCurrency);
        lstCurrency = Utility.getCurrencyForFCOther(lstCurrency);
        system.debug('----------raghu3---------'+lstCurrency);

        //To put Total option on top for I-234535
        Selectoption total =  new Selectoption(Label.CM_Edge_Total,Label.CM_Edge_Total);
        list<Selectoption> cloneList = lstCurrency.clone();
        lstCurrency.clear();
        lstCurrency.add(total);
        lstCurrency.addAll(cloneList);
        system.debug('----------raghu4---------'+lstCurrency);
        return lstCurrency;
    }


    public PageReference search() {
        String selectedCur = selectedCurrency;
        return null;
    }

    public static List<wrapperInvoice> loadOpps(String selectedCurrency1, Integer selectedMonth1, Id accId ) {
        //set<Id> owners = Utility.getCurrentUsers();
        User userLoggedIn = Utility.loggedInUser;
        List<wrapperInvoice> wrapperList = new List<wrapperInvoice>();
        map<String, wrapperInvoice>mapWpr = new map<String, wrapperInvoice>();
        try{
            if(accId==null){
                accId = Utility.currentAccount;
            }
            system.debug('************aid :'+ accId+'-----------selectedCurrency1--------'+selectedCurrency1);
            selectedCurrency = selectedCurrency1;
            wrapperInvoice wrapInv;
            String inputMonth;
            Double inputAmount;
            set<string>mthYear = new set<string>();
            Double maxAmt = 0;
            Date startdt = Date.today().toStartOfMonth();
            //Date lastdt = startdt.addMonths(selectedMonth1-1);
            Date lastdt = startdt.addMonths(selectedMonth1).addDays(-1);
            CashFlowComponentClone cashFlowCln = new CashFlowComponentClone();
            cashFlowCln.cur = selectedCurrency;
            CashFlowComponentClone.selMonth = selectedMonth1;
            CashFlowComponentClone.wfctype = fcType;
            System.debug('helloController'+CashFlowComponentClone.wfctype);
            LIST<String> months = cashFlowCln.getMonths();

            //Fix the missing month data
            Integer month =0;
            system.debug('***cashFlowCln.netOperatingFXCashFlow >'+cashFlowCln.netOperatingFXCashFlow);
            System.Debug('***cashFlowCln.netHedgeTotal'+cashFlowCln.netHedgeTotal);
            if(cashFlowCln.netHedgeTotal.size()>0){
                 while(startdt<=lastdt){
                    Decimal amt =0;
      /*
                     //commented negative values.
                     if(maxAmt>0){
                        amt = (maxAmt-(2*maxAmt))/2;
                     }
    */
                    wrapInv = new wrapperInvoice(cashFlowCln.netHedgeTotal[month], startdt.month(), startdt.year(), '');
                    if(!mthYear.contains(wrapInv.invoiceMonth)){
                        mthYear.add(wrapInv.invoiceMonth);
                        wrapperList.add(wrapInv);
                        wrapInv.init();
                    }
                    startdt = startdt.addMonths(1);
                    month++;
                }
            }
            //Build color map
            wrapperList.sort();


        }catch(Exception Ex){
           system.debug('******Ex::'+Ex.getMessage()+'>>'+Ex.getStackTraceString());
        }
        System.debug('---------> wrapperList : '  + wrapperList);
        return wrapperList;
    }
    @RemoteAction
    global static List<wrapperInvoice> loadOpps(String selectedCurrency1, Integer selectedMonth1 ,String SelectForwardType) {
        Id AccId;
        System.debug('checking' + SelectForwardType);
        fcType = SelectForwardType;
        return loadOpps(selectedCurrency1, selectedMonth1, AccId );
    }

    //Update data for forward looking
    @RemoteAction
    global static List<InvoicByCurrencyWrap> loadAllTimeData() {
        Id accId = Utility.currentAccount;
        User userLoggedIn = Utility.loggedInUser;
        List<InvoicByCurrencyWrap> newWrapperList = new List<InvoicByCurrencyWrap>();
        List<InvoicByCurrencyWrap> newWrapperListTemp1 = new List<InvoicByCurrencyWrap>();
        List<InvoicByCurrencyWrap> newWrapperListTemp2 = new List<InvoicByCurrencyWrap>();
        Map<String, InvoicByCurrencyWrap> invoiceMap = new map<String, InvoicByCurrencyWrap>();
        try{
        	date td = date.Today().toStartOfMonth();
        	//Inflow
            for(AggregateResult agResult : [SELECT SUM(Amount__c) s, Custom_Currency__c c FROM Input__c WHERE Parent_Account__c =: accId AND Input_Type__c != :invoiceLabel  and ( Type__c in :Utility.inputIncoming.split(',') or Input_Type__c in :Utility.inputIncoming.split(',')) and Paid__c = false and Transaction_Date__c >= :td GROUP BY Custom_Currency__c]) {
                addRecordToInvoiceCurrencyMap(agResult, invoiceMap, true);
            }
            //outgoing,
            for(AggregateResult agResult : [SELECT SUM(Amount__c) s, Custom_Currency__c c FROM Input__c WHERE paid__c =false And Parent_Account__c =: accId AND Input_Type__c != :invoiceLabel and (Type__c in :Utility.input_Outgoing.split(',') or Input_Type__c in :Utility.input_Outgoing.split(',')) and Transaction_Date__c >= :td GROUP BY Custom_Currency__c]) {
                addRecordToInvoiceCurrencyMap(agResult, invoiceMap, false);

            }
            //Outgoing
            for(AggregateResult agResult : [SELECT SUM(Amount__c) s, Custom_Currency__c c From Invoice__c WHERE
                                                Status__c not in (:Label.CM_Community_Text_FieldFilter_InvoiceStatus_Item_Void, :Label.CM_Community_Text_FieldFilter_InvoiceStatus_Item_Paid , :Label.CM_Edge_Partial_Payment,:Label.CM_Edge_Full_Payment,:Label.CM_Edge_Invoice_Submitted)
                                                And Account__c =: accId and Due_Date__c >= :td GROUP BY Custom_Currency__c]) {
                addRecordToInvoiceCurrencyMap(agResult, invoiceMap, false);
            }
            //Inflow
            for(AggregateResult agResult : [SELECT SUM(Amount__c) s, Custom_Currency__c c From Invoice__c WHERE
                                                 Status__c  in (:Label.CM_Community_Text_FieldFilter_InvoiceStatus_Item_Draft, :Label.CM_Community_Text_FieldFilter_InvoiceStatus_Item_Acknowledged)
                                                 And (Seller_Account__c=:accId
                                                 OR (Supplier__c != null AND Supplier__r.Supplier__c=:accId))
                                                 and Initiated_By__c = :sellerLabel
                                                 and Due_Date__c >= :td GROUP BY Custom_Currency__c]) {
                addRecordToInvoiceCurrencyMap(agResult, invoiceMap, true);
            }
        }catch(Exception Ex){
           system.debug('******Ex::'+Ex.getMessage());
        }

        newWrapperList = invoiceMap.values();
        newWrapperList.sort();
        /*Integer countRec = newWrapperList.size();
        if(countRec < 5){
            countRec = 0;
        }else{
            countRec = newWrapperList.size() - 5;
        }
        for(; countRec < newWrapperList.size(); countRec++){
            InvoicByCurrencyWrap invWrap = newWrapperList.get(countRec);
            if(invWrap.amount >= 0){
                newWrapperListTemp1.add(invWrap);
            }else{
                newWrapperListTemp2.add(invWrap);
            }
        }
        newWrapperList.clear();
        if(newWrapperListTemp1.size() > 0){
            newWrapperList.addAll(newWrapperListTemp1);
        }
        if(newWrapperListTemp2.size() > 0){
            newWrapperList.addAll(newWrapperListTemp2);
        }*/
        return newWrapperList;
    }

    // Add record to map based on ifFlow or outflow type
    public static void addRecordToInvoiceCurrencyMap(AggregateResult agResult, Map<String, InvoicByCurrencyWrap> invoiceMap, boolean inFlow) {
           InvoicByCurrencyWrap wrap = new InvoicByCurrencyWrap((Decimal)agResult.get('s') , String.valueof(agResult.get('c')));
           if(!inFlow){
                wrap.amount = (wrap.amount - (2*wrap.amount));
                wrap.init();
           }
            if(!invoiceMap.containsKey(wrap.currencyCode)){
                invoiceMap.put(wrap.currencyCode, wrap);
            }else{
                InvoicByCurrencyWrap wrapInv = invoiceMap.get(wrap.currencyCode);
                wrapInv.amount = wrapInv.amount + wrap.amount;
                wrapInv.init();
            }

    }


    global class wrapperInvoice implements Comparable {

        public Decimal amount{get;set;}
        public String invoiceMonth{get;set;}
        private Integer invMonth{get;set;}
        private Integer invYear{get;set;}
        public string colorSet{get;set;}
        public String toolTipAmount{get;set;}
        public String currencyCode{get;set;}

        public Integer compareTo(Object compareTo) {
            wrapperInvoice compareToEmp = (wrapperInvoice)compareTo;
            if (invYear > compareToEmp.invYear) return 1;
            if (invYear == compareToEmp.invYear)
            {
                if (invMonth == compareToEmp.invMonth) return 0;
                if (invMonth > compareToEmp.invMonth) return 1;
                return -1;
            }
            return -1;
        }
        public wrapperInvoice(Decimal amountP, Integer invMonthP, Integer invYearP, String currencyCode){
            invYear =invYearP;
            invMonth= invMonthP;
            amount = amountP;
            this.currencyCode = currencyCode;
            init();
            initColor();
        }
        public void initColor(){
            if(colorSet == null) colorSet = '';
            if(amount == null){
                colorSet = colorSet + '#000';
            }
            if(amount!=null){
                if(String.isEmpty(colorSet)){
                    if(amount>=0){
                        colorSet = colorSet + '#15BB87';
                    }else{
                        colorSet = colorSet + '#C9626E';
                    }
                }
            }
        }
        public void init(){
            if(amount==null){
                amount = 0;
            }
            List<String> args = new String[]{'0','number','###,###,##0.00'};
            toolTipAmount = String.format( amount.divide(1000, 0, System.RoundingMode.HALF_UP).format(), args);
            string inputMonth ='';
            if(invMonth == 1)
                inputMonth = Label.Jan+' ';
            else if(invMonth == 2)
                inputMonth = Label.Feb+' ';
            else if(invMonth == 3)
                inputMonth = Label.Mar+' ' ;
            else if(invMonth == 4)
                inputMonth = Label.Apr+' ';
            else if(invMonth == 5)
                inputMonth = Label.May+' ';
            else if(invMonth == 6)
                inputMonth = Label.Jun+' ';
            else if(invMonth == 7)
                inputMonth = Label.Jul+' ' ;
            else if(invMonth == 8)
                inputMonth = Label.Aug+' ' ;
            else if(invMonth == 9)
                inputMonth = Label.Sep+' ' ;
            else if(invMonth == 10)
                inputMonth = Label.Oct+' ' ;
            else if(invMonth == 11)
                inputMonth = Label.Nov+' ' ;
            else if(invMonth == 12)
                inputMonth = Label.Dec+' ' ;
            invoiceMonth = inputMonth+ invYear;
        }

    }

    global class InvoicByCurrencyWrap implements Comparable{
        public Decimal amount{get;set;}
        public String toolTipAmount{get;set;}
        public String currencyCode{get;set;}

        public InvoicByCurrencyWrap(Decimal amount, String currencyCode) {
            this.amount = amount;
            this.currencyCode = currencyCode;
            init();
        }
         public void init(){
            if(amount==null){
                amount = 0;
            }
            List<String> args = new String[]{'0','number','###,###,##0.00'};
            toolTipAmount = String.format( amount.divide(1000, 0, System.RoundingMode.HALF_UP).format(), args);
         }

         public Integer compareTo(Object ObjToCompare) {
            InvoicByCurrencyWrap ObjToCompareObj = (InvoicByCurrencyWrap)ObjToCompare;
            if(Math.abs(amount) > Math.abs(ObjToCompareObj.amount)){
                return -1;
            }
            else{
                return 1;
            }
        }
    }

    public class HedgedDonutData{
      public String name { get; set; }
      public Decimal data { get; set; }

      public HedgedDonutData(String name, Decimal data) {
        this.name = name;
        this.data = data;
      }

    }

    public Map<String , Decimal> mapToStoreExposeAmount{get;set;}


    public Map<String,List<HedgedDonutData>> getDonutData(){
          mapToStoreExposeAmount =  new Map<String,Decimal>();

        Map<string,List<HedgedDonutData>> mapDonutData = new Map<string,List<HedgedDonutData>>();
        List<HedgedDonutData> data = new List<HedgedDonutData>();

        List<SelectOption> listCurrencies = getaccountTradedCurrency();
        Map<String,Decimal> mapNetExposure = getNetExposure();
        Map<String,Decimal> mapHedgingInstruments = getHedgingInstruments();
        Decimal NetExposure;
        Decimal HedgingInstruments;
        Decimal HedgePercent;
        System.Debug('###DonutMapExposure'+mapNetExposure);
        System.Debug('###DonutMapHedging'+mapHedgingInstruments);
        for(SelectOption so:listCurrencies){
          if(so.getLabel() != 'Total' && so.getLabel() != 'null'){
            data = new List<HedgedDonutData>();
            NetExposure = mapNetExposure.get(so.getLabel());
            HedgingInstruments = mapHedgingInstruments.get(so.getLabel());

            /*if(HedgingInstruments != null && NetExposure == null){
              NetExposure = 0;
            }*/
            if(NetExposure != null && HedgingInstruments == null && NetExposure != 0){
              HedgingInstruments = 0;
            }
            System.Debug('###DonutDataCurrency'+so.getLabel());
            System.Debug('###DonutDataExposure'+NetExposure);
            System.Debug('###DonutDataHedging'+HedgingInstruments);
            if(HedgingInstruments != null && NetExposure != null) {
              HedgePercent = ((HedgingInstruments/NetExposure)*100).setScale(0);
            System.Debug('###DonutDataHedging'+(100-HedgePercent)*NetExposure);

            if(HedgePercent < 0){
              HedgePercent = HedgePercent * -1;
            }
            if(HedgePercent > 100){
              data.add(new HedgedDonutData(Label.CM_EDGE_ExposedAmount, 0));
              data.add(new HedgedDonutData(Label.Hedging_Instruments, 100));
            }
            else{
              data.add(new HedgedDonutData(Label.CM_EDGE_ExposedAmount, 100-HedgePercent));
              data.add(new HedgedDonutData(Label.Hedging_Instruments, HedgePercent));
            }

            Double Amount = ((100-HedgePercent)*NetExposure)/100;
            Double HomeAmount = Utility.getSettlementCurrencyAmount(Amount,so.getLabel(),userCurrency);

            //System.debug('###Hello' + so.getLabel()   + (100-HedgePercent));
            mapDonutData.put(so.getLabel(),data);
            mapToStoreExposeAmount.put(so.getLabel() , HomeAmount);

            }
            //mapDonutData.put('GBP',data);
          }
        }
        return mapDonutData;
    }

    public Map<String,Decimal> getNetExposure(){
      List<AggregateResult> lstInvoices = [SELECT Custom_Currency__c,SUM(Amount__c )tot FROM Invoice__c WHERE Status__c NOT IN ('Void','Full Payment Submitted') and Account__c = :accId and Amount__c != null Group By Custom_Currency__c];
      List<AggregateResult> lstInputs = [SELECT Custom_Currency__c,SUM(Amount__c )tot FROM Input__c WHERE Type__c IN ('Purchase Order', 'Forecast Outflow') and Parent_Account__c = :accId and Amount__c != null Group By Custom_Currency__c];
      List<AggregateResult> lstInvReceivables = [SELECT Custom_Currency__c,SUM(Amount__c )tot FROM Invoice__c WHERE Seller_Account__c = :accId and Status__c NOT IN ('Full Payment Submitted','Void') and Amount__c != null Group By Custom_Currency__c];
      List<AggregateResult> lstInflows = [SELECT Custom_Currency__c,SUM(Amount__c )tot FROM Input__c WHERE Type__c IN ('Forecast Inflow') and Parent_Account__c = :accId and Amount__c != null Group By Custom_Currency__c];
      Map<String,Decimal> mapTot=new Map<String,Decimal>();
      for(AggregateResult agr :lstInvoices){
          String InvoiceCurrency = (String)agr.get('Custom_Currency__c');
          Decimal amnt = (Decimal)agr.get('tot');
          System.DEbug('###invs'+InvoiceCurrency+amnt);

          mapTot.put(InvoiceCurrency,amnt);
      }
      for(AggregateResult agr :lstInputs){
          String InputCurrency = (String)agr.get('Custom_Currency__c');
          Decimal amnt = (Decimal)agr.get('tot');
          System.DEbug('###inputs'+InputCurrency+amnt);
          if(mapTot.containsKey(InputCurrency)){
            Decimal amnt2 = mapTot.get(InputCurrency);
            amnt = amnt+amnt2;
          }
          mapTot.put(InputCurrency,amnt);
      }
      for(AggregateResult agr :lstInvReceivables){
          String InvReceivables = (String)agr.get('Custom_Currency__c');
          Decimal amnt = (Decimal)agr.get('tot');
          System.DEbug('###Invsrevs'+InvReceivables+amnt);
          if(mapTot.containsKey(InvReceivables)){
            Decimal amnt2 = mapTot.get(InvReceivables);
            amnt = amnt-amnt2;
          }
          else{
            amnt = 0 - amnt;
          }
          mapTot.put(InvReceivables,amnt);
      }
      for(AggregateResult agr :lstInflows){
          String InflowCurrency = (String)agr.get('Custom_Currency__c');
          Decimal amnt = (Decimal)agr.get('tot');
          System.DEbug('###Inflow'+InflowCurrency+amnt);
          if(mapTot.containsKey(InflowCurrency)){
            Decimal amnt2 = mapTot.get(InflowCurrency);
            amnt = amnt-amnt2;
          }
          else{
            amnt = 0 - amnt;
          }
          mapTot.put(InflowCurrency,amnt);
      }
      return mapTot;
    }

    public Map<String,Decimal> getHedgingInstruments(){
      List<AggregateResult> lstHBs = [SELECT Custom_Currency__c,SUM(Amount__c )tot FROM Holding_Balance__c WHERE Account__c = :accId and amount__c != null Group By Custom_Currency__c];
      List<AggregateResult> lstInputs = [SELECT Custom_Currency__c,SUM(Amount__c )tot FROM Input__c WHERE Parent_Account__c = :accId and Type__c IN ('Option - Other','Fixed Forward Contract - Other','Window Forward Contract - Other') and amount__c != null Group By Custom_Currency__c];
      List<AggregateResult> lstForwards = [SELECT Custom_Currency__c,SUM(Amount__c )tot FROM Forward_Contracts__c WHERE Account_ID__c = :accId and Amount__c != null Group By Custom_Currency__c];
      List<AggregateResult> lstInputSell = [SELECT Sell_Currency__c,SUM(Settlement_Amount__c )tot FROM Input__c WHERE Parent_Account__c = :accId and Type__c IN ('Fixed Forward Contract - Other','Window Forward Contract - Other') and Settlement_Amount__c != null Group By Sell_Currency__c];
      List<AggregateResult> lstForwardsell = [SELECT Sell_Currency__c,SUM(Settlement_Amount__c )tot FROM Forward_Contracts__c WHERE Account_ID__c = :accId and Settlement_Amount__c != null Group By Sell_Currency__c];

      Map<String,Decimal> mapTot=new Map<String,Decimal>();
      for(AggregateResult agr :lstInputs){
          String InputCurrency = (String)agr.get('Custom_Currency__c');
          Decimal amnt = (Decimal)agr.get('tot');
          System.DEbug('###inp'+InputCurrency+amnt);
          if(mapTot.containsKey(InputCurrency)){
            Decimal amnt2 = mapTot.get(InputCurrency);
            amnt = amnt+amnt2;
          }
          mapTot.put(InputCurrency,amnt);

      }
      for(AggregateResult agr :lstHBs){
          String HBCurrency = (String)agr.get('Custom_Currency__c');
          Decimal amnt = (Decimal)agr.get('tot');
          System.DEbug('###HB'+HBCurrency+amnt);
          if(mapTot.containsKey(HBCurrency)){
            Decimal amnt2 = mapTot.get(HBCurrency);
            amnt = amnt+amnt2;
          }
          mapTot.put(HBCurrency,amnt);

      }
      for(AggregateResult agr :lstForwards){
          String FowardCurrency = (String)agr.get('Custom_Currency__c');
          Decimal amnt = (Decimal)agr.get('tot');
          System.DEbug('###Forward'+FowardCurrency+amnt);
          if(mapTot.containsKey(FowardCurrency)){
            Decimal amnt2 = mapTot.get(FowardCurrency);
            amnt = amnt+amnt2;
          }
          mapTot.put(FowardCurrency,amnt);

      }
      for(AggregateResult agr :lstInputSell){
          String FowardCurrency = (String)agr.get('Sell_Currency__c');
          Decimal amnt = (Decimal)agr.get('tot');
          System.DEbug('###ForwardSell'+FowardCurrency+amnt);
          if(mapTot.containsKey(FowardCurrency)){
            Decimal amnt2 = mapTot.get(FowardCurrency);
            amnt = amnt-amnt2;
          }else{
            amnt = 0 - amnt;
          }
          mapTot.put(FowardCurrency,amnt);

      }
      for(AggregateResult agr :lstForwardsell){
          String FowardCurrency = (String)agr.get('Sell_Currency__c');
          Decimal amnt = (Decimal)agr.get('tot');
          System.DEbug('###WUForwardSell'+FowardCurrency+amnt);
          if(mapTot.containsKey(FowardCurrency)){
            Decimal amnt2 = mapTot.get(FowardCurrency);
            amnt = amnt-amnt2;
          }else{
            amnt = 0 - amnt;
          }
          mapTot.put(FowardCurrency,amnt);

      }
      return mapTot;
    }

}