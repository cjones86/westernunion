/**=====================================================================
 * Appirio, Inc
 * Name: GPAppInModalController_Test
 * Description: Test class for GPAppInModalController
 * Created Date: 06 Apr 2016
 * Created By: Nikhil Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
 
@isTest
private class GPAppInModalController_Test {

	private static testMethod void test() {
	    test_Utility.createGPIntegrationAdministration();
	    GPAppInModalController gpAppCtrl = new GPAppInModalController();
	    gpAppCtrl.closePopup();
	    gpAppCtrl.showLogin();
	    gpAppCtrl.showPay();
	}
}