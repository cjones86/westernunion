/**=====================================================================
 * Appirio, Inc
 * Name: Edge_Cash_Input_ActivitiesControllerTest
 * Description: Controller Test class for Edge_Cash_Input_ActivitiesController
 * Created Date: 05 Apr 2016
 * Created By: Nikhil Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
@isTest
private class Edge_Cash_Input_ActivitiesControllerTest {

	private static testMethod void test() {
        test_Utility.createCMPAdministration();
		test_Utility.createCMPAlert();
		test_Utility.createWubsIntAdministration();
		test_Utility.createGPH2HCurrencies();
		test_Utility.createGPIntegrationAdministration();
		test_Utility.currencyISOMapping();
		test_Utility.createWUEdgeSharingAdmin();
		CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.CCT_Client_ID__c = '3232112';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount.CMP_Create_Transactions__c = true;
        beneAccount.CMP_Enabled__c = true;
        beneAccount.CMP_Holding_Enabled__c = true;
        beneAccount.CMP_H2H_Transaction_Enabled__c = true;
        beneAccount.BillingStreet = 'testStreet';
        beneAccount.BillingCity = 'city';
        beneAccount.BillingState = 'state';
        beneAccount.BillingCountry = 'USA';
        beneAccount.BillingPostalCode = '231231';
        beneAccount.Billing_Address_ISO_Country_Code__c = 'USA';
        
        Account beneAccount1 = test_Utility.createAccount(false);
        beneAccount1.RecordTypeId=accountRecordType;
        beneAccount1.ExternalId__c='12345678912';
        beneAccount1.CCT_Client_ID__c = '3232112';
        beneAccount1.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount1.CMP_Create_Transactions__c = true;
        beneAccount1.CMP_Enabled__c = true;
        beneAccount1.CMP_Holding_Enabled__c = true;
        beneAccount1.CMP_H2H_Transaction_Enabled__c = true;
        beneAccount1.BillingStreet = 'testStreet';
        beneAccount1.BillingCity = 'city';
        beneAccount1.BillingState = 'state';
        beneAccount1.BillingCountry = 'USA';
        beneAccount1.BillingPostalCode = '231231';
        beneAccount1.Billing_Address_ISO_Country_Code__c = 'USA';
        
        List<Account> listAccount = new List<Account>{beneAccount,beneAccount1};
        insert listAccount;
        
        
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact;
        
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
		commUser.timezonesidkey='America/Indiana/Indianapolis';
		commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        commUser.CMP_Network_Invitation_Enabled__c = true;
        commUser.CMP_Create_Transactions__c = true;
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        insert commUser;    
        
        ApexPages.currentPage().getParameters().put('retUrl','/testRetUrl');
        ApexPages.currentPage().getParameters().put('Referer','/testReferer');
        
        Global_Pay_ID_Management__c gPIM = new Global_Pay_ID_Management__c(Global_Pay_ID__c='12345',
                                                                               User_Beneficiary__c=commUser.Id,
                                                                               Creation_Type__c='Automatic Email Match');
        insert gPIM;
        
        
        Supplier__c supp = test_Utility.createSupplier(beneAccount.Id,beneAccount1.Id,true);
        Invoice__c inv1 = test_Utility.createInvoice('GP Invoice - Active', beneAccount.Id, commUser.Id,'Pending Approval (Open)', false);
        Invoice__c inv2 = test_Utility.createInvoice('GP Invoice - Active', null, commUser.Id,'Pending Approval (Open)', false);
        inv2.Supplier__c = supp.Id;
        List<Invoice__c> listInvoices = new List<Invoice__c>{inv1,inv2};
        insert listInvoices;
        
       Forward_Contracts__c Fc = new Forward_Contracts__c();
        Fc.Account_ID__c = beneAccount.ID;
        Fc.Maturity_Date__c = System.today();
        Fc.Buy_Currency__c = 'AUD';
        Fc.Sell_Currency__c = 'AUD';
        insert Fc;
        
        System.RunAs(commUser){
            
        	Test.startTest();
        		Edge_Cash_Input_ActivitiesController ctrl = new Edge_Cash_Input_ActivitiesController();
        		String DateRegex = ctrl.localeDate;
        		       DateRegex = ctrl.localeDateRegx;
        		System.assertEquals(1, ctrl.inInvoices.size());
        		System.assertEquals(1, ctrl.outInvoices.size());
        		
        		ctrl.closePopup();
        		ctrl.showPay();
        		List<SelectOption> lstCurr = ctrl.getaccountTradedCurrency();
        		ctrl.selectedCurrency = 'USD';
        		ctrl.input.Window_Start_Date__c = System.Today();
        		ctrl.input.Window_End_Date__c = System.Today().addDays(5);
        		ctrl.PopulateLists();
        		
        	Test.stopTest();
        }
	}

}