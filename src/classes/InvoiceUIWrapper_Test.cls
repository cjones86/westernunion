/**=====================================================================
 * Appirio, Inc
 * Name: InvoiceUIWrapper_Test
 * Description: Test class for InvoiceUIWrapper
 * Created Date: 06 Apr 2016
 * Created By: Nikhil Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
 
@isTest
private class InvoiceUIWrapper_Test {

	private static testMethod void test() {
        test_Utility.createCMPAdministration();
		test_Utility.createCMPAlert();
		test_Utility.createWubsIntAdministration();
		test_Utility.createGPH2HCurrencies();
        test_Utility.createWUEdgeSharingAdmin();
		CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.CCT_Client_ID__c = '3232112';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        insert beneAccount;
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact;
        
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
		commUser.timezonesidkey='America/Indiana/Indianapolis';
		commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        insert commUser;    
        
        Invoice__c invoice = test_Utility.createInvoice('GP Invoice - Active', beneAccount.Id, commUser.Id,'Partial Payment Submitted', false);
        invoice.Beneficiary_GP_Name__c = 'testGP';
        insert invoice;
        Input__c inp = test_Utility.createInputWithType(true,'Purchase Order', beneAccount.Id);
        
        Test.startTest();
        System.runAs(commUser){
            InvoiceUIWrapper invWrp2 = new InvoiceUIWrapper(inp);
            // System.debug('>>>> invoice>>> '+ invoice.RecordTypeId);
            // try{
            //     InvoiceUIWrapper invWrp3 = new InvoiceUIWrapper(invoice);
            // }Catch(exception ex){}
            
            InvoiceUIWrapper invWrp1 = new InvoiceUIWrapper();
            Invoice__c invo = [Select Id, Name, Status__c,RecordType.Name,Custom_Currency__c,RecordTypeId,Invoice_Number__c,Delivery_Method__c,
                                        Initiated_By__c,Due_Date__c,Amount__c,Account__c,Buyer__c,Buyer__r.Contact.AccountId,Beneficiary_GP_ID__c,
                                        Beneficiary_GP_Name__c,Reference_Number__c,Type__c,Account__r.Name,TypeTrans__c
                                From Invoice__c where Id = :invoice.Id];
            System.debug('invo:::: '+ invo.RecordType.Name);
            invWrp1.Invoice = invo;
            InvoiceUIWrapper invWrp3 = new InvoiceUIWrapper(invo);
            
            String categoryTrns = invWrp3.categoryTrns;
            Boolean isViewSubmitAccess = invWrp3.isViewSubmitAccess;
            Boolean isViewAccess = invWrp3.isViewAccess;
            Boolean isEditAccess = invWrp3.isEditAccess;
            Boolean isPayNowAccess = invWrp3.isPayNowAccess;            
        }

        
        Test.stopTest();
	}

}