@isTest
private class DocumentUploadController_Test {

    static User testUser;

    static testMethod void createNewAttachment() {
			createTestData();
			Id accId = [Select Id From Account].Id;
			Id accDocId = [Select Id From Account_Document__c].Id;
			PageReference pageRef = Page.DocumentUpload;
			Test.setCurrentPage(pageRef);
			ApexPages.currentPage().getParameters().put('Id',accDocId);
			ApexPages.currentPage().getParameters().put('aId',accId);
			ApexPages.currentPage().getParameters().put('tab','doc');
			
			DocumentUploadController ctrl = new DocumentUploadController();
			String localeDate = ctrl.localeDate;
			String convertedDate = ctrl.localeDateRegx;
			//PageReference pgRef1 = ctrl.redirectHome();
			//PageReference pgRef2 = ctrl.saveDoc();       
			PageReference pgRef3 = ctrl.createNewAttachment();
			PageReference pgRef4 = ctrl.cancel();
			ctrl.customDelete();
    }
    
    static testMethod void createNewDocument() {
	    createTestData();
	    Id accId = [Select Id From Account].Id;
	    Id accDocId = [Select Id From Account_Document__c].Id;
	    PageReference pageRef = Page.DocumentUpload;
	    Test.setCurrentPage(pageRef);
      
	    System.RunAs(testUser){
	     ApexPages.currentPage().getParameters().put('retURL','/testRetUrl');	
	     DocumentUploadController ctrl = new DocumentUploadController();
	     PageReference pageRef1 = ctrl.saveDoc();
       ctrl.accountDocName = 'testDoc';
	     ctrl.updateDoc();
	     
	    }

    }
    
    
    static testMethod void saveDocument() {
		createTestData();
		Id accId = [Select Id From Account].Id;
	//	Id accDocId = [Select Id From Account_Document__c].Id;
		PageReference pageRef = Page.DocumentUpload;
		Test.setCurrentPage(pageRef);
	//	ApexPages.currentPage().getParameters().put('Id',accDocId);
		ApexPages.currentPage().getParameters().put('aId',accId);
		ApexPages.currentPage().getParameters().put('tab','doc');
		
		DocumentUploadController ctrl = new DocumentUploadController();
		PageReference pageRef1 = ctrl.saveDoc();
		ctrl.accountDocName = 'testDoc';
		PageReference pageRef2 = ctrl.updateDoc();       
		//ctrl.customDelete();
    }
    
    private static void createTestData() {
    	ID naInvestigationCaseRecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Operations' AND SobjectType = 'Case'].Id;
        ID ausCFXCaseRecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'AUS CFX Case Reason' AND SobjectType = 'Case'].Id;
        Account newAccount = new Account(Name = 'Test Account', OwnerId = UserInfo.getUserId());
        insert newAccount;
        Account_Document__c accD = new Account_Document__c();
		accD.Account__c = newAccount.Id;
		accD.Name__c = 'Test Doc';
		insert accD;
		Attachment attach = new Attachment();
		attach.Name = 'Test.txt';
		attach.Body = Blob.valueOf('ashishs shiweeovch');
		attach.ParentId = accD.Id;
		insert attach;
        Contact newContact = new Contact(AccountId = newAccount.Id, LastName = 'Test Contact');
        insert newContact;
        testUser = new User();
	     //testUser = new User();
		testUser.alias = 'testuser';
		testUser.Email = 'testuser@test123.com';
		testUser.EmailEncodingKey = 'ISO-8859-1';
		testUser.LanguageLocaleKey = 'en_US';
		testUser.LastName = 'Test User567';
		testUser.LocaleSidKey = 'en_AU';
		testUser.Account_Administrator__c = false;
		testUser.ProfileId = [SELECT Id FROM Profile WHERE Name LIKE 'Partner community - FX management tool' LIMIT 1].Id;
		testUser.TimeZoneSidKey = 'Australia/Sydney';
		testUser.UserName = 'testuser@travelex.com.au';
		testUser.ContactId = newContact.Id;
		testUser.DefaultCurrencyIsoCode = 'INR';
		insert testUser;
        Case cs = new Case(ContactId = newContact.Id, RecordTypeId = naInvestigationCaseRecordTypeId);
        cs.CMP_Public__c = true;
        insert cs;
        CaseComment newCaseComment1 = new CaseComment(ParentId = cs.Id, CommentBody = 'Test');
        newCaseComment1.IsPublished = true;
        insert newCaseComment1;
    }
}