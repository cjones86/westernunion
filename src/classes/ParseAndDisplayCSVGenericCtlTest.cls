/**=====================================================================
 * Name: ParseAndDisplayCSVGenericCtlTest
 * Description: Test class for ParseAndDisplayCSVGenericCtl controller.
 * Created Date: 24 March, 2015
 * Created By: Ranjeet Singh (JDC)
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
 
@isTest
private class ParseAndDisplayCSVGenericCtlTest {
//!! 
//

    static User testUser = null;
    static testMethod void unitTestController() {
        PageReference pgGenericCsv = Page.ParseAndDisplayCSVGeneric;
        testUser = test_Utility.createTestUser(true);
        pgGenericCsv.getParameters().put('sourcePage', 'InsightsAndInputTab');
        System.runAs(testUser){
            Input__c input = test_Utility.createInput(true);
            Attachment att = test_Utility.addAttachmentToParent(input.id, 'test2.csv', '01/13/2015,USD,5,2,test 8822,123\n01/13/2015,USD,5,2,test 8822,123');
            system.debug('*****input>>'+input.id+'..att :'+att.id);
            String attachmentid = '';//pgGenericCsv.getparameters().put('attid', att.id);
            String[] tmpFields =  new List<String> { '123344','1234','han','12-05-2015','IN','Approved' };
            ParseAndDisplayCSVGenericCtl ctl =new ParseAndDisplayCSVGenericCtl();
            String transtype = pgGenericCsv.getparameters().put('type', 'Option - WUBS');
            //ctl.transtype = transtype;
             GenericBaseCsvWrapper inv = ctl.getGenericCsvWrapper(tmpFields, transtype);
             List<GenericBaseCsvWrapper> csWrappList = new  List<GenericBaseCsvWrapper>();
             csWrappList.add(inv);
            
            Test.setCurrentPage(pgGenericCsv);
           
            ctl.Init();
            ctl.parseCsv('123344,1234,han,12-05-2015,IN,Approved');
            ctl.parsedInput = csWrappList;
            ctl.inputList = csWrappList;
            ctl.parseAttachment(att.Body, 'Test');
            
 
            
            
            Account acc = new Account(name='Test Account');
            Utility.currentAccount = acc.Id;
            User usr = [Select   Id, Profile.Name, ContactId, DefaultCurrencyIsoCode, Name, IsPrmSuperUser,
                	CurrencyIsoCode, Contact.AccountId, Contact.Account.CurrencyIsoCode, LanguageLocaleKey FROM  User
                  	WHERE  Id = :UserInfo.getUserId() LIMIT 1];
            Utility.loggedInUser.id = usr.Id;
          //  GenericBaseCsvWrapper inv = new GenericBaseCsvWrapper(tmpFields, 'Option - Other');
        //   List<SelectOption> StatusOptions = ctl.StatusOptions;
          ParseAndDisplayBaseCSVGenericCtl ps = ctl.self;
             ctl.OnSaveUpdate(inv);
            ctl.parsedInput = csWrappList;
            ctl.inputList = csWrappList;
            ctl.updateBeneficiaries();
            ctl.attachmentid = att.Id;
            // ctl.showNextRecordToInsert();
            ctl.parsedInput = csWrappList;
            ctl.isValidCsv = true;
            ctl.saveAndSubmit();
            ctl.save();
            // ctl.cancel();
            // pgGenericCsv.getparameters().put('attid', att.id);
            //ctl.parsedInput = null;
            //ctl.Init();
            
            
        }
        
    }

}