/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_DeleteTempFeedsScheduler
 * Description: scheduler class for EDGE_DeleteTempFeeds Batch
 * Created Date: 3 Mar' 2016
 * Created By: Rohit Sharma (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
global class EDGE_DeleteTempFeedsScheduler implements Schedulable {
   
   global void execute(SchedulableContext SC) {
      EDGE_DeleteTempFeeds deleteFeeds = new EDGE_DeleteTempFeeds(); 
      database.executebatch(deleteFeeds,10);
   }
}