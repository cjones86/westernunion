/**
 * An apex page controller that exposes the change password functionality ///
 */
public with sharing class ChangePasswordController {

    public String username{get; set;}
    public String oldPassword {get; set;}
    public String newPassword {get; set;}
    public String verifyNewPassword {get; set;}
    public String selectedLanguage{get;set;}
    public List<SelectOption> languageOptions{get;set;}
    public pageReference pgRef{get;set;}
    public String errorMessage{get;set;}
    public boolean isShowValidtionScreen{get;set;}
    public boolean isAccountLocked{get;set;}
    public integer validateFailCounter{get;set;}
    private EDGE_Custom_Auth__c eca;
    public String successMessage{get;set;}
    Public String email{get;set;}
    Public String encodedEmail{get;set;}
    Public String inputToken{get;set;}
    Private String secToken;
    Private User currUser;
    public static boolean firstRun = true;
    PageReference ref;
    private static boolean run = true;
    private boolean resendtokenVar = false;
    public boolean showEULAPopup{get;set;}
    public Boolean agreeEndUser{get;set;}
    public Integer countMe{get;set;}

    public ChangePasswordController(){
        showEULAPopup = false;
        agreeEndUser = false;
    }
    
    public PageReference changeSitePassword1(){
        if(firstRun){
        	firstRun = false;
        	countMe = 0;
        try{
        	system.debug('>>>> new pwd dummy>>>>'+newPassword + '>>>'+verifyNewPassword +'>>oldpassword '+oldpassword +'>>>site pwSite.IsPasswordExpired>>'+Site.IsPasswordExpired());
        	pgRef = Site.changePassword(newPassword, verifyNewPassword, oldpassword);
        	system.debug('>>>exception dummy>>>'+ApexPages.getMessages());
        	
            String usrProfile;
                for(User us :[Select Id, Name, Email, Profile.Name From User Where UserName = :userInfo.getUserName()]){
                    email = us.Email;
                    getMaskedEmail(email);
                    currUser = us;
                    usrProfile = us.Profile.Name;
                    break;
                }

              String allowedTFAProfiles = CMP_Administration__c.getInstance().EDGE_TFA_Enabled_Profiles__c;
              system.debug('allowedTFAProfiles>>>'+allowedTFAProfiles);
              system.debug('usrProfile>>>'+usrProfile);
              if(!allowedTFAProfiles.containsIgnoreCase(usrProfile)){
                        //pgRef = Site.changePassword(newPassword, verifyNewPassword, oldpassword);
                        //boolean oldVal = Site.isPasswordExpired();
                        //system.debug('pgRef>>>'+pgRef + 'oldVal'+oldVal);
                        if(ApexPages.getMessages().size() > 0){
	                        pgRef.setredirect(true);
	                        return pgRef;
                        }else{
                        	return null;
                        }
              }
            system.debug('ApexPages.getMessages().size()+++'+ApexPages.getMessages().size());
            if(ApexPages.getMessages().size() > 0)
            	return null;
            else{
	            isShowValidtionScreen = true;
	            countMe = 1;
	            system.debug('isShowValidtionScreen>>>+'+isShowValidtionScreen); 
	            custAuth();
            }

        }catch(Exception e){
            system.debug('>>>> inside exception sendOTP>>>'+pgRef+' >>> exception >>>'+e);
        }
        }
        return null;
        
    }

    public PageReference changeSitePassword() {

        try{

            //if(firstRun){
            system.debug('>>>> new pwd >>>>'+newPassword + '>>>'+verifyNewPassword);
            //ref = Site.changePassword(newPassword, verifyNewPassword, null);
            system.debug('>>>> eca >>>>'+eca);
            system.debug('>>>> ref 1>>>>'+ref);
            //String regexExpr = '^(?=.*\d+)(?=.*[a-zA-Z])[0-9a-zA-Z!@#$%]$';
            //Pattern MyPattern = Pattern.compile(regexExpr);

            //if(ref != null){
                //there is no error, call custom auth screen
            if(newPassword != verifyNewPassword){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.Edge_Password_Match);
                ApexPages.addMessage(myMsg);
                return null;
            }
            if(newPassword.length() < 8 || verifyNewPassword.length() < 8){
                 ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.Edge_Password_Length);
                 ApexPages.addMessage(myMsg);
                return null;
            }

            //check if it contains digits and letters
            if(!(Pattern.matches('([a-zA-Z]+[!@#$%^&*]*[0-9]+[a-zA-Z0-9!@#$%^&*]*)',newPassword))){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.Edge_Password_Verify);
                    ApexPages.addMessage(myMsg);
                    return null;
             }
             
             system.debug('Site pwd expired'+Site.isPasswordExpired());
             /*if(Site.isPasswordExpired()){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'You cannot reuse old password');
                    ApexPages.addMessage(myMsg);
                    return null;
             }*/

             PageReference pg = sendOTP();
             if(pg != null){
                 return pg;
             }else{
                 return null;
             }

            /*if(!newPassword.isAlphanumeric()){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Password should be alphanumeric');
                ApexPages.addMessage(myMsg);
                return null;
            }*/

            /*Matcher MyMatcher = MyPattern.matcher(newPassword);
            if(MyMatcher.matches() == false){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Password must be a combination of letters and digits');
                ApexPages.addMessage(myMsg);
                return null;
            }*/



            //}

            // Check if there already exists custom auth record
            /*List<EDGE_Custom_Auth__c> auth = [Select Id, Name, Email_Address__c, Login_Status__c, OwnerId, Recipient_User__c, Security_Token__c,
                                        Login_Attempts__c,Last_Attempt_Date_Time__c
                                        From EDGE_Custom_Auth__c Where Recipient_User__c = :currUser.Id
                                        And Login_Status__c != 'Expired' Limit 1];



             if(auth.size() > 0){
                delete auth[0];
            }*/
                //firstRun = false;
            //}
            //return null; //stay on the same page
        }catch(Exception e){
            system.debug('>>>> error in changing password >>>>'+e);
            ApexPages.addMessages(e);
            return null;
        }
    }

    /*public PageReference changePassword() {

    pgRef = null;
    PageReference pg = null;
    try{
          pgRef = Site.changePassword(newPassword, verifyNewPassword, oldpassword);
          system.debug('>>>>>>'+userInfo.getUserName());
          String usrProfile;
                for(User us :[Select Id, Name, Email, Profile.Name From User Where UserName = :userInfo.getUserName()]){
                    email = us.Email;
                    getMaskedEmail(email);
                    currUser = us;
                    usrProfile = us.Profile.Name;
                    break;
                }
                String allowedTFAProfiles = CMP_Administration__c.getInstance().EDGE_TFA_Enabled_Profiles__c;
                if(pgRef!=null){
                    if(!allowedTFAProfiles.containsIgnoreCase(usrProfile)){
                        pgRef.setredirect(true);
                        return pgRef;
                    }
            //isShowValidtionScreen = true;
            custAuth();
            system.debug('>>>> priyanka :::'+pgRef);
            system.debug('>>>> priyanka >>>>');
        }
       return null;
        }catch(Exception e){
            system.debug('>>>> error in changing password >>>>'+e);
            ApexPages.addMessages(e);
            return null;
        }
    } */

    public PageReference sendOTP() {
        try{
            String usrProfile;
                for(User us :[Select Id, Name, Email, Profile.Name From User Where UserName = :userInfo.getUserName()]){
                    email = us.Email;
                    getMaskedEmail(email);
                    currUser = us;
                    usrProfile = us.Profile.Name;
                    break;
                }

              String allowedTFAProfiles = CMP_Administration__c.getInstance().EDGE_TFA_Enabled_Profiles__c;
              system.debug('allowedTFAProfiles>>>'+allowedTFAProfiles);
              system.debug('usrProfile>>>'+usrProfile);
              if(!allowedTFAProfiles.containsIgnoreCase(usrProfile)){
                        pgRef = Site.changePassword(newPassword, verifyNewPassword, oldpassword);
                        boolean oldVal = Site.isPasswordExpired();
                        system.debug('pgRef>>>'+pgRef + 'oldVal'+oldVal);
                        pgRef.setredirect(true);
                        return pgRef;
              }

            isShowValidtionScreen = true;
            custAuth();

        }catch(Exception e){
            system.debug('>>>> inside exception sendOTP>>>'+pgRef+' >>> exception >>>'+e);
        }
        return null;
    }

        Public void getMaskedEmail(String Email){
        encodedEmail = '';
        if(String.isNotBlank(Email)){
           // Let's say Email = 'nikhil.sharma@appirio.com';
            encodedEmail = Email.left(2); // encodedEmail = 'ni'
            String s1 = '*';
            List<String> lst = Email.split('@'); // lst = {'nikhil.sharma', 'appirio.com'}
            Integer n1 = lst[0].length()-2; // n1 = Length('nikhil.sharma') - 2
            encodedEmail += s1.repeat(n1)+'@'; // encodedEmail = 'ni' + '****... n1 times' + '@'
            String str1 = lst[1].substringBefore('.'); // Str1 = 'appirio' (lst[1] = 'appirio.com')
            encodedEmail += s1.repeat(str1.length()-2); // encodedEmail = 'ni' + '****..*' + '@' + '*****'
            encodedEmail += str1.right(2); // encodedEmail = 'ni' + '****..*' + '@' + '*****' + 'io'
            String str = lst[1].substringAfter('.'); // str = 'com'
            encodedEmail += '.'+str;
        }
    }

     public void custAuth(){
        // Check if there already exists custom auth record
        for(EDGE_Custom_Auth__c auth: [Select Id, Name, Email_Address__c, Login_Status__c, OwnerId, Recipient_User__c, Security_Token__c,
                                              Login_Attempts__c,Last_Attempt_Date_Time__c
                                            From EDGE_Custom_Auth__c Where Recipient_User__c = :currUser.Id
                                            And Login_Status__c != 'Expired' Limit 1]){
            eca = auth;
            validateFailCounter = (Integer)auth.Login_Attempts__c;
            if(auth.Login_Status__c == 'Locked'){
                isAccountLocked = true;
                errorMessage = Label.Edge_Login_TFA_Account_Locked_Message;
            }
            return;
        }

        // Create new Custom auth record if there doesn't exist any.
        eca = new EDGE_Custom_Auth__c();
        eca.Email_Address__c = email;
        eca.Login_Status__c = 'Active';
        eca.OwnerId = currUser.Id;
        eca.Recipient_User__c = currUser.Id;
        decimal tkn = getSecurityToken();
        String token = tkn.toPlainString();
        if(token.length() < 8){
            String s1 = '0';
            Integer n1 = 8 - token.length();
            token += s1.repeat(n1);
        }
        eca.Security_Token__c = token;
        secToken = eca.Security_Token__c;
        insert eca;
    }

    public Integer getSecurityToken(){
        Integer r = Math.round(Math.random()*100000000);
        return r;
    }

    public void resendToken(){
        System.debug('eca::: ' + eca);
        try{
            if(run){
                isShowValidtionScreen = true;
                eca.Login_Status__c = 'Resend';
                update eca;
                run = false;
                resendtokenVar = true;
             }
        }catch(exception ex){
            System.debug('ex:: '+ ex.getMessage());
            custAuth(); // create new custom auth record
        }
        successMessage = Label.EDGE_Login_TFA_ResendCodeInstructions;
        errorMessage = '';
        //return null;
    }



    public pageReference doVerify(){
        if(firstRun){
            System.debug('firstRun>>> ' + firstRun);
        System.debug('inputToken>>> ' + inputToken);
        System.debug('secToken>>> ' + secToken);
        boolean isUserAlreadyLoggedIn = true; // if user is logged in from another browser then auth record will not exist
        if(String.isEmpty(inputToken)|| inputToken.trim().length() != 8){
            errorMessage = Label.Edge_Login_TFA_Enter_8_Digit_Token;
            successMessage = '';
            return null;
        }
         inputToken = inputToken.trim();

        for(EDGE_Custom_Auth__c ec : [Select Id, Name, Email_Address__c, Login_Status__c, OwnerId, Recipient_User__c, Security_Token__c,
                                              Login_Attempts__c,Last_Attempt_Date_Time__c
                                            From EDGE_Custom_Auth__c Where Recipient_User__c = :currUser.Id
                                            And Login_Status__c != 'Expired' Limit 1]){

            eca = ec;
            secToken = eca.Security_Token__c;
            validateFailCounter = (Integer)ec.Login_Attempts__c;
            isUserAlreadyLoggedIn = false;
            system.debug('>>>>'+eca+'>>>>sec token>>'+secToken);
         //   System.assert(false, '>>>> ');

        }

        /*if(isUserAlreadyLoggedIn){
            errorMessage = Label.Edge_Login_TFA_Invalid_Token_Message;
            //errorMessage = 'Random';
            successMessage = '';
            return null;
        }*/

        if(validateFailCounter >= 4){
            isAccountLocked = true;
            eca.Login_Attempts__c = 5;
            eca.Last_Attempt_Date_Time__c = System.now();
            eca.Login_Status__c = 'Locked';
            update eca;
            errorMessage = Label.Edge_Login_TFA_Account_Locked_Message;
            successMessage = '';
            return null;
        }
        if(inputToken.equalsIgnoreCase(secToken)){
            system.debug('>>>> new pwd 111>>>>'+newPassword + '>>>'+verifyNewPassword +'>>oldpassword '+oldpassword +'>>>site pwSite.IsPasswordExpired>>'+Site.IsPasswordExpired());

            /*try{
            pgRef = Site.changePassword(newPassword, verifyNewPassword, null);
            system.debug('>>>exception>>>'+ApexPages.getMessages());
            

            
            system.debug('>>>pgRef>>>'+pgRef);
            }catch(Exception e){
                system.debug('>>>> inside exception>>>'+pgRef);
              //pgRef = new PageReference('/managecash/CommunityLogin');
              //pgRef.setredirect(true);
              isShowValidtionScreen = false;
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.Edge_No_Old_Password));

              return null;
            }*/

            /*finally{
                if(pgRef != null){
                    delete eca;
                }

                //isShowValidtionScreen = true;
            }*/
            //return null;
            system.debug('>>>check'+checkEULA());
            if(checkEULA()){
                if(pgRef != null){
                    pgRef.setredirect(true);
                    return pgRef;
                }else{
                    isShowValidtionScreen = false;
                    showEULAPopup = false;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a new password which you have not used earlier'));
                return null;
                }
            }else{
                system.debug('>>>showEULAPopup'+showEULAPopup);
                showEULAPopup = true;
                //isShowValidtionScreen = false;
                return null;
            }

        }
        else{
            validateFailCounter += 1;
            eca.Login_Attempts__c = validateFailCounter;
            eca.Last_Attempt_Date_Time__c = System.now();
            eca.Login_Status__c = 'Failed';
            update eca;
            errorMessage = Label.Edge_Login_TFA_Invalid_Token_Message;
            successMessage = '';
            return null;
        }
        firstRun = false;
        }
        return null;
    }

    public boolean checkEULA(){
        DateTime versionDate = CMP_Administration__c.getInstance().EDGE_Current_EULA_Version_Date__c;
        String versionEULA = CMP_Administration__c.getInstance().EDGE_Current_EULA_Version__c ;
        List<User> userList = [Select EDGE_EULA_Accepted_Date_Time__c,EDGE_EULA_Accepted_Version__c FROM User Where Id = :UserInfo.getUserId()];
        if(userList[0].EDGE_EULA_Accepted_Date_Time__c != null && versionDate != null && userList[0].EDGE_EULA_Accepted_Date_Time__c >= versionDate){
            if(userList[0].EDGE_EULA_Accepted_Date_Time__c == versionDate){
              //if date is same, check the version.Technically the 2nd one is the most current version.So, show the popup in this case
              if(versionEULA != null && userList[0].EDGE_EULA_Accepted_Version__c != null && versionEULA > userList[0].EDGE_EULA_Accepted_Version__c ){
                return false;
              }
            }
            return true;
        }else{
            //show popup
            return false;
        }
    }

    public pageReference acceptEULA(){
      system.Debug('###inside accept EULA');   
      /* Priyanka : Start : T-542707 */
      String versionEULA = CMP_Administration__c.getInstance().EDGE_Current_EULA_Version__c ;

      //Raghu Rankawat : update EDGE_EULA_Accepted_Date_Time__c when user accept EULA
      List<User> userList = [Select EDGE_EULA_Accepted_Date_Time__c,EDGE_EULA_Accepted_Version__c FROM User Where Id = :UserInfo.getUserId()];
      //if(userList[0].EDGE_EULA_Accepted_Date_Time__c == null){
         userList[0].EDGE_EULA_Accepted_Date_Time__c = System.Now();

      //}
      if(versionEULA != null)
      userList[0].EDGE_EULA_Accepted_Version__c = versionEULA;
      update userList[0];
      /* Priyanka : End : T-542707 */

            //pgRef = Site.changePassword(newPassword, verifyNewPassword, oldpassword);
            System.Debug('###'+pgRef);   
      if(pgRef != null){
      delete eca;
      pgRef.setredirect(true);
      return pgRef;
      }else{
        return null;
      }
    }


    public pageReference changeResendPassword(){
        return Site.changePassword(newPassword, verifyNewPassword, null);
    }



    /*@IsTest(SeeAllData=true) public static void testChangePasswordController() {
        // Instantiate a new controller with all parameters in the page
        ChangePasswordController controller = new ChangePasswordController();
        controller.oldPassword = '123456';
        controller.newPassword = 'qwerty1';
        controller.verifyNewPassword = 'qwerty1';

        System.assertEquals(controller.changePassword(),null);
    }  */
}