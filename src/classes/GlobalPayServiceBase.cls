//Created By : Ranjeet Singh
//Created date : 5th April 2016
//Description : GlobaRestSvc - Combine GlobalPayResSvc & GlobalUpdateOrDeleteBeneService
public abstract without sharing class GlobalPayServiceBase {
    public String payLoad{get;set;}
    public String Transactiontype{get;set;}
    public String GlobalPayId{get;set;}
    public Map<String, Object> gpRequestString{get;set;}

    public String amount {get;set;}
    public String totalAmount{get;set;}
    public String settlementCurrency{get;set;}
    public String currency1{get;set;}
    public String lineItemId{get;set;}
    public String orderId{get;set;}
    public String paymentAmount{get;set;}
    public String paymentCurrency{get;set;}
    public String paymentReference{get;set;}
    public String settlementMethod{get;set;}
    public String committeddate{get;set;}
    public DateTime orderCommittedDate{get;set;}
    public String invoiceId{get;set;}
    //T-504295
    public String orderStatus{get;set;}

    //Must override at drived class
    public abstract String processBRequest();

    //Implement common parser code.
    public virtual String processRequest(){
        /*******************************************************************
         1 - Standard and Holding Payments
        *******************************************************************/
        System.debug('********** 1) - Standard and Holding Payments *****');

        /*******************************************************************
         1A - Get common/shared values - Both Standard and Holding Payments
        *******************************************************************/

        System.debug('********** 1A) - Get common/shared values - Both Standard and Holding Payments *****');

        amount = (String)getSafeMapValue('amount');
        totalAmount = (String)getSafeMapValue('gp.totalAmount');
        GlobalPayId = (String)getSafeMapValue('gp.beneficiaryId');
        settlementCurrency = (String)getSafeMapValue('gp.settlementCurrency');
        currency1 = (String)getSafeMapValue('currency');
        lineItemId = (String)getSafeMapValue('gp.lineItemId');
        orderId = (String)getSafeMapValue('gp.orderId');
        paymentAmount = (String)getSafeMapValue('gp.paymentAmount');
        paymentCurrency = (String)getSafeMapValue('gp.paymentCurrency');
        // If CP then paymentReference, else Invoice Number
        paymentReference = (String)getSafeMapValue('gp.paymentReference');
        settlementMethod = (String)getSafeMapValue('gp.settlementMethod');
        Transactiontype = (String)getSafeMapValue('gp.type');
        //T-504295
        orderStatus = (String)getSafeMapValue('gp.paymentStatus');
        if(Transactiontype == 'OrderConfirmation' || Transactiontype == 'H2HConfirmation') {
          committeddate = (String)getSafeMapValue('gp.orderCommittedDate');
          if(committeddate != null && committeddate != ''){
            committeddate = committeddate.replace('T',' ');
            committeddate = committeddate.substring(0,committeddate.length()-4);
            orderCommittedDate = DateTime.Valueof(committeddate);
          }
        }
        invoiceId = (String)getSafeMapValue('objectId');
        System.debug('********** 1A) GlobalPayServiceBase invoiceId>objectId:  ' + invoiceId + ' *****');
        System.debug('********** 1A) Variables - amount: ' + amount + ' *****');
        System.debug('********** 1A) Variables - totalAmount: ' + totalAmount + ' *****');
        System.debug('********** 1A) Variables - beneficiaryId: ' + GlobalPayId + ' *****');
        System.debug('********** 1A) Variables - settlementCurrency: ' + settlementCurrency + ' *****');
        System.debug('********** 1A) Variables - currency1: ' + currency1 + ' *****');
        System.debug('********** 1A) Variables - lineItemId: ' + lineItemId + ' *****');
        System.debug('********** 1A) Variables - orderId: ' + orderId + ' *****');
        System.debug('********** 1A) Variables - paymentAmount: ' + paymentAmount + ' *****');
        System.debug('********** 1A) Variables - paymentCurrency: ' + paymentCurrency + ' *****');
        System.debug('********** 1A) Variables - paymentReference: ' + paymentReference + ' *****');
        System.debug('********** 1A) Variables - settlementMethod: ' + settlementMethod + ' *****');
        System.debug('********** 1A) orderCommittedDate: ' + getSafeMapValue('gp.orderCommittedDate') + ' *****');
        System.debug('********** 1A) Variables - committeddate: ' + committeddate + ' *****');
        System.debug('********** 1A) Variables - Transactiontype: ' + Transactiontype + ' *****');
        System.debug('********** 1A) Variables - orderStatus: ' + orderStatus + ' *****');

        //Call specific service API
        return processBRequest();
    }
    public object getSafeMapValue(String key){
        if(gpRequestString.containsKey(key)){
            return gpRequestString.get(key);
        }
        return '';
    }

}