/**=====================================================================
 * Appirio, Inc
 * Name: BS_CustomLookupCtrl_Test
 * Description: Controller Test class for BS_CustomLookupCtrl
 * Created Date: 04 Apr 2016
 * Created By: Nikhil Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
@isTest
private class BS_CustomLookupCtrl_Test {

	private static testMethod void test() {

		BS_CustomLookupCtrl ctrl = new BS_CustomLookupCtrl();
		String lookupTitle = ctrl.lookupTitle;
		String deleteTitle = ctrl.deleteTitle;
		ctrl.fieldName = 'test';
		ctrl.objName = 'testObj';
		ctrl.selectedId = '231121';
		ctrl.fieldPopulatingId = 'beneId';
		ctrl.fieldPopulatingBeneId = 'testBeneId';
		ctrl.fieldPopulateBuyerId = 'testBuyerId';
		ctrl.fieldPopulateAccountId = 'testAccId';
		ctrl.lookBuyer = true;
		ctrl.deliveryType = 'Standard Payment';
		ctrl.H2HSearch = true;
	}

}