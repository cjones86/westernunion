public without sharing class EDGE_Becomeacustomercontroller {

    static apexLogHandler.apexLog logSave = new apexLogHandler.apexLog('Becomeacustomerctrl','save');
    
	public Account userAccount {get;set;}
	public Contact userContact {get;set;}
	public Map<String,String> fieldLabels {get;set;}

	public EDGE_Becomeacustomercontroller() {
		User currentUser = Utility.loggedInUser;
		//userAccount = getAccount(currentUser.Contact.AccountId);
		fieldLabels = new Map<string,string>();
		if(currentUser.ContactId != null)userContact = getContact(currentUser.ContactId);

	}

	public pagereference save(){
	    logSave.logMessage('>>inside Save method ...>> ');
		System.Debug('###'+usercontact);
		logSave.logDebug('###'+usercontact);
		update userContact;
		System.Debug('###'+userContact.Account.EDGE_Become_Customer_Interest__c);
		logSave.logDebug('###'+userContact.Account.EDGE_Become_Customer_Interest__c);
		if(!userContact.Account.EDGE_Become_Customer_Interest__c){
			usercontact.account.EDGE_Become_Customer_Interest__c = true;
			usercontact.account.EDGE_Become_Customer_Interest_Date__c = System.Now();
		}
		System.Debug('###'+userContact.Account);
		logSave.logDebug('###'+userContact.Account);
		update userContact.Account;
		logSave.saveLogs();
		return null;
	}

	public List<Schema.FieldSetMember> getFields() {
				return Schema.SObjectType.Contact.fieldSets.getMap().get('EDGE_Become_a_Customer').getFields();
		}

		/*private Account getAccount(String id) {
				String query = 'SELECT ';
				system.debug('#####' + this.getFields());
				for(Schema.FieldSetMember f : this.getFields()) {
						query += f.getFieldPath() + ', ';
				}
				query += 'Id FROM Account Where Id =: id LIMIT 1';
				system.debug('#####' + query);
				return Database.query(query);
		}*/

		private Contact getContact(String id) {
				String query = 'SELECT ';
				system.debug('#####' + this.getFields());
				for(Schema.FieldSetMember f : this.getFields()) {
						query += f.getFieldPath() + ', ';
						fieldLabels.put(f.fieldPath,f.Label);
				}
				query += 'Id, account.EDGE_Become_Customer_Interest__c, account.EDGE_Become_Customer_Interest_Date__c FROM Contact Where Id =: id LIMIT 1';
				system.debug('#####' + query);
				return Database.query(query);
		}
}