/**
    @author Mohammad Swaleh
    @date 6JUN2016
    @description controller class for ManualShareRefresh which calls SharingMaster class to refresh sharing records 
                 on Invoice, Network, Supplier and Invites
*/

public class ManualShareRefreshController {    

    public String idsString { get; set; }
    public List<SelectOption> objectList { get; set; }
    public String selectedObject { get; set; }
    
    WU_Edge_Sharing_Administration__c sharingAdmin = WU_Edge_Sharing_Administration__c.getInstance();
    
    public ManualShareRefreshController() {
    
        String objectStr = (String)sharingAdmin.get('Sharing_Enabled_Object_List__c');
    
        objectList = new List<SelectOption>();
        objectList.add(new SelectOption('', '--Select--'));
        for(String s: objectStr.split(',')) {
            objectList.add(new SelectOption(s.trim(), s.trim()));
        }
    }
    
    public PageReference refreshShares() {
        SharingMaster.objectName = selectedObject;
        SharingMaster.recordIds.addAll((List<Id>)idsString.split(','));
        SharingMaster.doShare();
        if(SharingMaster.runStatus == 'Success') ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, SharingMaster.runStatus));
        else ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, SharingMaster.runStatus));
        return null;
    }
}