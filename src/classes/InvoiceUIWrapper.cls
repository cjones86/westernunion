//Created By Ranjeet Singh, T-479413 , Dashboard Component - Filter+tables
//  Review : Should use wrapper class to make common security property across development.
public without sharing class InvoiceUIWrapper {

    public InvoiceUIWrapper(){}

    public InvoiceUIWrapper(Input__c inp){
        Input = inp;
        itemType = 'Input';
        settlementCurrency = Utility.getSettlementCurrencyAmount(Input.Amount__c, Input.Custom_Currency__c, UserInfo.getDefaultCurrency());
        String s = (settlementCurrency.setScale(2) + 0.001 ).format();
        settlementCurrencyFormat = s.substring(0,s.length()-1);
        paymentDate = Input.Transaction_Date__c;
        curr = Input.Custom_Currency__c;
        amount = Input.Amount__c;
        supplier = Input.Input_Beneficiary__r.Name;
        refNumber = Input.Reference_Number__c;
        category = Input.Input_Type__c;
        status = '';
        buyer = Input.Input_Beneficiary__r.Name;
        this.isDealerAccess= true;
        if(this.Input.OwnerId == UserInfo.getUserId() ||
                    (Utility.currentAccountOwner != null && this.Input.OwnerId == Utility.currentAccountOwner)){
            isOwner = true;
        }
        else{
            isOwner = false;
        }
        if(Utility.isDealerUser && this.Input.OwnerId!= UserInfo.getUserId()){
            this.isDealerAccess= false;
        }
        encodedInputId = EncryptionManager.doEncrypt(inp.Id);
        if(encodedInputId.contains('+')){
          encodedInputId = encodedInputId.replace('+','%2B');
        }
        
    }

    public InvoiceUIWrapper(Invoice__c inv){
        Invoice = inv;
        itemType = 'Invoice';
        isSelected = false;
        settlementCurrency = Utility.getSettlementCurrencyAmount(invoice.Amount__c, invoice.Custom_Currency__c, UserInfo.getDefaultCurrency());
        String s = (settlementCurrency.setScale(2) + 0.001 ).format();
        settlementCurrencyFormat = s.substring(0,s.length()-1);
        if(String.isNotBlank(Invoice.RecordType.Name) && Invoice.RecordType.Name.containsIgnoreCase('GP') || Invoice.RecordType.Name.containsIgnoreCase('Cold'))
            beneName = Invoice.Beneficiary_GP_Name__c;
        else
            beneName = Invoice.Beneficiary_Name__c;

        paymentDate = Invoice.Due_Date__c;
        curr = Invoice.Custom_Currency__c;
        amount = Invoice.Amount__c;
        supplier = beneName;
        refNumber = Invoice.Reference_Number__c;
        category = Invoice.Type__c;
        status = Invoice.Status__c;
        buyer = Invoice.Account__r.Name;
        RecordType = Invoice.RecordType.Name;
        this.isDealerAccess = false;
        encodedInvoiceId = EncryptionManager.doEncrypt(inv.Id);
      //  encodedInvoiceId = EncodingUtil.urlEncode(encodedInvoiceId, 'UTF-8');
       // System.assert(false , 'Hello'+ encodedInvoiceId);
        if(encodedInvoiceId.contains('+')){
        	encodedInvoiceId = encodedInvoiceId.replace('+','%2B');
        }
        //getCategoryType = 'Hello';
        
    
      
            getCategoryType = inv.TypeTrans__c;
        
            
        
    }
    public String RecordType;
    public boolean isDealerAccess{get;set;}
    public boolean isOwner {get;set;}
	public boolean isSelected{get;set;}
    public Invoice__c Invoice{get;set;}
    public Input__c Input{get;set;}
    public Decimal settlementCurrency{get;set;}
    public String settlementCurrencyFormat{get;set;}
    public String beneName{get;set;}

    //T-485199 NS:03/16
    public Date paymentDate{get;set;}
    public String curr{get;set;}
    public Decimal amount{get;set;}
    public Decimal settlementAmt{get;set;}
    public String supplier{get;set;}
    public String refNumber{get;set;}
    public String category{get;set;}
    public String status{get;set;}
    public String categoryTrns{
        get{
            if('Input'.equalsIgnoreCase(itemType)){
                //return Input.Input_Type__c;
                return Input.Input_Type__c;
            }else{
               //System.assert(false, 'inside else');
                return getCategoryType ;
            }
        }
    }

    public String buyer{get;set;}
    public String itemType{get;set;}

    public boolean isViewSubmitAccess{
        get{
            return  Invoice.Status__c=='Invoice Submitted' || Invoice.Status__c=='Rejected';
        }
    }
    public boolean isViewAccess{
        get{
            return Invoice.Status__c!='Invoice Submitted' && Invoice.Status__c!='Rejected';
        }
    }
    public boolean isEditAccess{
        get{
            return (Invoice.Status__c=='Draft'|| Invoice.Status__c=='Acknowledged' || Invoice.Status__c == 'Payment Approval Rejected'|| Invoice.Status__c == 'Pending Approval (Open)' ||
                Invoice.Status__c == 'Approved' || Invoice.Status__c == 'Paid' || Invoice.Status__c == 'Void') &&
            Invoice.RecordType.Name!='GP Invoice - Complete' &&  Invoice.RecordType.Name!='Cold Payment - Complete' &&
            (Invoice.Buyer__r.Contact.AccountId== Utility.currentAccount || Invoice.Account__c==Utility.currentAccount);
        }
    }
    public boolean isPayNowAccess{
        get{
            if(invoice.Buyer__r.Contact.AccountId==Utility.currentAccount &&
                (invoice.Status__c=='Draft' || invoice.Status__c=='Acknowledged' ||
                invoice.Status__c=='Partial Payment Submitted')) {
                    if(invoice.Beneficiary_GP_ID__c!=null && invoice.Amount__c != null &&
                         invoice.Due_Date__c != null && invoice.Delivery_Method__c != null &&
                         invoice.Delivery_Method__c != '' && invoice.Supplier__c != null) {
                            if((invoice.Delivery_Method__c=='Standard Payment' && Utility_Security.canPayInvoices_SP) ||
                                (invoice.Delivery_Method__c=='Holding-to-Holding' && Utility_Security.canPayInvoices_H2H)) {
                                    return true;
                            }
                    }
            }
            return false;
        }
    }

    public void reffrenceBasedOnRecordType()
    {
        if(!this.RecordType.containsIgnoreCase('Cold Payment') )
        {
            this.refNumber = this.invoice.Invoice_Number__c;
        }
    }
    
    public String getCategoryType{get;set;}
    public String encodedInvoiceId{get;set;}
    public String encodedInputId{get;set;}
}