public without sharing class CMPCaseListController {

	public List<CaseWrapper> caseWrapperList {get; set;}
	public String caseCommentToSearch {get; set;}
	public Set<id> caseIds {get;set;}
	public String accName {get{
		 List<Account> accList = new List<Account>([SELECT Name from Account Where Id =:Utility.currentAccount]);
		 if(accList.size() > 0){
		 	return accList.get(0).Name;
		 }
		 return '';
		} set;
	}
	public CMPCaseListController(){
		populateCaseList();
	}

	public void populateCaseList(){
		caseWrapperList = new List<CaseWrapper>();
		caseIds = new Set<id>();
		String currentAccount = Utility.currentAccount;
		String caseQuery = 'SELECT Id, CaseNumber, FK_Order_Number__c, CreatedDate, toLabel(Status), toLabel(Secondary_Case_Layout__c),Book_Amount__c,Currency_Type__c,Beneficiary_Name__c,'+
							'(Select CommentBody From CaseComments';
		if(!String.isEmpty(caseCommentToSearch)){
			String caseCommentToSearchTemp = '%'+caseCommentToSearch+'%';
			caseQuery = caseQuery+' WHERE  CommentBody LIKE :caseCommentToSearchTemp';
		}
		caseQuery = caseQuery+' order by createdDate desc limit 1), LastModifiedDate '+
							'FROM Case WHERE AccountId =:currentAccount  AND CMP_Public__c = true';
		system.debug('---caseQuery---------'+caseQuery);
		for(Case cs : Database.query(caseQuery)){
			String caseNotes = '';
			if(cs.CaseComments.size() > 0){
				caseNotes = cs.CaseComments.get(0).CommentBody;
			}
			if(!String.isEmpty(caseCommentToSearch)){
				if(cs.CaseComments.size() > 0){
					caseIds.add(cs.id);
					caseWrapperList.add(new CaseWrapper(cs.Id, cs.CaseNumber, cs.FK_Order_Number__c, cs.Secondary_Case_Layout__c,
										cs.Status, caseNotes, cs.CreatedDate.date().format(), cs.LastModifiedDate.date().format(),cs.Currency_Type__c,cs.Book_Amount__c,cs.Beneficiary_Name__c));
				}
			}else{
				caseIds.add(cs.id);
				caseWrapperList.add(new CaseWrapper(cs.Id, cs.CaseNumber, cs.FK_Order_Number__c, cs.Secondary_Case_Layout__c,
										cs.Status, caseNotes, cs.CreatedDate.date().format(), cs.LastModifiedDate.date().format(),cs.Currency_Type__c,cs.Book_Amount__c,cs.Beneficiary_Name__c));
			}
		}

		caseQuery = 'SELECT Id, CaseNumber, FK_Order_Number__c, CreatedDate, toLabel(Status), toLabel(Secondary_Case_Layout__c),Book_Amount__c,Currency_Type__c,Beneficiary_Name__c,LastModifiedDate ';
		caseQuery = caseQuery+'FROM Case WHERE AccountId =:currentAccount AND CMP_Public__c = true';
		caseQuery = caseQuery+'	AND (CaseNumber LIKE :caseCommentToSearchTemp OR FK_Order_Number__c LIKE :caseCommentToSearchTemp';
		casequery = caseQuery+' OR Beneficiary_Name__c LIKE :caseCommentToSearchTemp OR Status LIKE :caseCommentToSearchTemp';
		caseQuery = caseQuery+' OR Reason LIKE :caseCommentToSearchTemp)';

		for(Case cs : Database.query(caseQuery)){
			if(!caseIds.contains(cs.Id)){
				String caseNotes = '';
				caseWrapperList.add(new CaseWrapper(cs.Id, cs.CaseNumber, cs.FK_Order_Number__c, cs.Secondary_Case_Layout__c,
										cs.Status, caseNotes, cs.CreatedDate.date().format(), cs.LastModifiedDate.date().format(),cs.Currency_Type__c,cs.Book_Amount__c,cs.Beneficiary_Name__c));
			}
		}
	}


	public void refreshCaseList(){
		caseCommentToSearch = '';
		populateCaseList();
	}

	public void searchCaseList(){
		populateCaseList();
	}

	public class CaseWrapper{

		public String caseId {get; set;}
		public String caseNumber {get; set;}
		public String caseOrderNumber {get; set;}
		public String caseStatus {get; set;}
		public String caseNote {get; set;}
		public String caseRootCause {get; set;}
		public String caseDate {get; set;}
		public String caseModifiedDate {get; set;}
		public String transactionCurrency {get;set;}
		public Decimal transactionAmount {get;set;}
		public String beneficiaryName {get;set;}
		public String encodedCaseId{get;set;}

		public CaseWrapper(String caseId, String caseNumber, String caseOrderNumber, String caseRootCause, String caseStatus, String caseNote,
                                                  String caseDate, String caseModifiedDate,String transactionCurrency,Decimal transactionAmount, String beneficiaryName){
				
				encodedCaseId = EncryptionManager.doEncrypt(caseId);
        if(encodedCaseId.contains('+')){
          encodedCaseId = encodedCaseId.replace('+','%2B');
        }
				this.caseId = encodedCaseId;
				this.caseNumber = caseNumber;
				this.caseOrderNumber = caseOrderNumber;
				this.caseRootCause = caseRootCause;
				this.caseStatus = caseStatus;
				this.caseNote = caseNote;
				this.caseDate =caseDate;
				this.caseModifiedDate = caseModifiedDate;
				this.transactionCurrency = transactionCurrency;
				this.transactionAmount = transactionAmount;
				this.beneficiaryName = beneficiaryName;
		}
	}
}