/**=====================================================================
 * Name: EDGE_SupplierTriggerHandlerTest
 * Description: Test Class of EDGE_SupplierTriggerHandler
 * Created Date: May 11, 2016
 * Created By: Priyanka Kumar
 * Task      : T-501534
 =====================================================================*/
 
@isTest 
private class EDGE_SupplierTriggerHandlerTest {
	
	static testmethod void testNetworkLookup() {
		
		
		Account beneAccount1 = test_Utility.createAccount(true);
		Account beneAccount2 = test_Utility.createAccount(true);
		Account beneAccount3 = test_Utility.createAccount(true);
		
		test_Utility.createCMPAdministration();
		CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
		String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
		Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount1.Id);
    insert newContact;
		
		Network__c net1 = test_Utility.createNetwork(newContact.Id, '', beneAccount1.Id , beneAccount2.Id , true);
		Network__c net2 = test_Utility.createNetwork(newContact.Id, '', beneAccount3.Id , beneAccount2.Id , true);
		
		Test.startTest();
		Supplier__c sup = test_Utility.createSupplier(beneAccount1.Id, beneAccount2.Id, true);
		
		List<Supplier__c> supList = [Select Id,Network__c From Supplier__c Where Id = :sup.Id];
		system.assert(supList[0].Network__c != null); 
		
		//if supplier is updated 
		sup.Supplier__c = beneAccount3.Id;
		update sup;
		
		Test.stopTest();
		
	}
	
	 static testmethod void testPopulateSupplieronEmail() {
    
    
    Account beneAccount1 = test_Utility.createAccount(true);
    Account beneAccount2 = test_Utility.createAccount(true);
    Account beneAccount3 = test_Utility.createAccount(true);
    
    test_Utility.createCMPAdministration();
    CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
    String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
    Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount1.Id);
    insert newContact;
    
    Network__c net1 = test_Utility.createNetwork(newContact.Id, '', beneAccount1.Id , beneAccount2.Id , true);
    Network__c net2 = test_Utility.createNetwork(newContact.Id, '', beneAccount3.Id , beneAccount2.Id , true);
    
    Test.startTest();
    Supplier__c sup = test_Utility.createSupplier(null, beneAccount2.Id, false);
    sup.Email_Address__c = 'test@test.com';
    insert sup;
    
    List<Supplier__c> supList = [Select Id,Supplier__c,Network__c From Supplier__c Where Id = :sup.Id];
    system.assert(supList[0].Supplier__c != null);
 
    Test.stopTest();
    
  }

}