/**=====================================================================
 * Appirio, Inc
 * Name: InviteUpdateController
 * Description: Controller to verify Invite and create Network record for InviteUpdate VF page - T-460125,T-460124
 * Created Date: 31 Dec' 2015
 * Created By: Rohit Sharma (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 * 3 Oct 2016				   Raghu Rankawat		populate EDGE_EULA_Accepted_Date_Time__c when user register for T-542453
 =====================================================================*/
public without sharing class InviteUpdateController {

    static apexLogHandler.apexLog logAcceptUser = new apexLogHandler.apexLog('InviteUpdateCtrl','acceptUser');

    public String inviteId{get;set;}
    public Invites__c invite{get;set;}
    public Boolean errorMessage{get;set;}
    public String userName{get;set;}
    public String companyName{get;set;}
    public String password{get;set;}

    public Contact newContact{get;set;}
    public String email{get;set;}
    public Boolean agreeEndUser{get;set;}
    //public Integer securityCode{get;set;}
    public boolean addtoNetworkOnly{get;set;}
    public String selectedLanguage{get;set;}
    public List<SelectOption> languageOptions{get;set;}
    public List<Invites__c> listInvites{get;set;}
    public boolean isWubsCustomer{get;set;}
    public boolean isRegSuccessful{get;set;}
    public boolean isEditEmail{get;set;}
    public List<SelectOption> sortedCountries {get;set;}
    public String getCaptchaLanguage {
        /*
        get{
            if(selectedLanguage.equalsIgnoreCase('cs') ){
            return   'cs';
        }
        else if(selectedLanguage.equalsIgnoreCase('en_US')){
            return  'en';
        }
        else if(selectedLanguage.equalsIgnoreCase('fr')){
            return  'fr';
        }
        else if(selectedLanguage.equalsIgnoreCase('de')){
            return  'nl';
        }
        else if(selectedLanguage.equalsIgnoreCase('it')){
            return  'it';
        }else if(selectedLanguage.equalsIgnoreCase('pl')){
            return  'pl';
        }
        return 'en';
  */
  get{
  Map<String,CashManagementLanguageConfiguration__c> cashMLanguages = CashManagementLanguageConfiguration__c.getAll();
  //System.debug('hELLO'+cashMLanguages.keySet());
        if(cashMLanguages!=null && cashMLanguages.size()>0) {
            for(CashManagementLanguageConfiguration__c lang :cashMLanguages.values()) {
                if(lang.reCaptchaLanguageParameter__c == selectedLanguage){
                    return lang.reCaptchaLanguageParameter__c ;
                }
            }
        }
  return 'en';
        }
    }
    public  String SiteKey {
        get {

              for(reCaptcha__mdt valmdt : [SELECT DeveloperName,Site_key__c FROM reCaptcha__mdt]){
                  return valmdt.Site_key__c;
              }
              return null;
        }
    }
        public boolean showCaptcha{
            get{
                if(ApexPages.currentPage().getParameters().get('Token') != null  && String.isNotBlank(ApexPages.currentPage().getParameters().get('Token'))  ){
                    return false;
                }
                else{
                    return true;
                }
            }
        }




    public InviteUpdateController() {
        agreeEndUser = false;
        addtoNetworkOnly = false;
        isRegSuccessful = false;
        isWubsCustomer = true;
        isEditEmail = false;
        newContact = new Contact();
        listInvites = new List<Invites__c>();
        //sortedCountries = Utility.getSortedcountries();
        inviteId = ApexPages.currentPage().getParameters().get('ID');
        String invitationToken = ApexPages.currentPage().getParameters().get('Token');
        String clientIP = ApexPages.currentPage().getParameters().get('X-Salesforce-SIP');
        String userAgent = ApexPages.currentPage().getParameters().get('User-Agent');
        //pageHeaders.get('X-Salesforce-SIP');
        //pageHeaders.get('User-Agent�);
        system.debug('Cookie :'+retriveCookie());
        selectedLanguage = retriveCookie();
        selectedLanguage =  selectedLanguage.trim().length() > 0 ? selectedLanguage : 'en_US';

        /*
        if(ApexPages.currentPage().getParameters().get('currentLang') != null && ApexPages.currentPage().getParameters().get('currentLang') != '')
            language = ApexPages.currentPage().getParameters().get('currentLang');
        */
        System.debug('invitationToken:::: ' + invitationToken);
        System.debug('inviteId:::: ' + inviteId);

        if(inviteId!=null && inviteId!='' && invitationToken!=null && invitationToken!='') {
            for(Invites__c tmpInv : getInvites(inviteId,invitationToken) ){
                invite = tmpInv;
                break;
            }
            if(invite!=null && String.isNotBlank(invite.Benne_Email__c)){
                listInvites = [Select Id, Name,Inviter_User__c,Inviter_User__r.Contact.AccountId From Invites__c Where Benne_Email__c=:invite.Benne_Email__c];
            }
        }

        errorMessage =false;
        if(invite==null){
            isEditEmail = true;
        }
        else if(invite!=null && (invite.Status__c!='Sent')) {
            errorMessage = true;
            System.debug('invite::: ' + invite);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CM_Invite_Expired));
        } else {
            invite.Invitation_Page_View_Count__c = (invite.Invitation_Page_View_Count__c!=null?invite.Invitation_Page_View_Count__c :0)+1;
            newContact.Email = invite.Benne_Email__c;
            newContact.country__c = invite.Invoice__r.Beneficiary_GP_Country__c; //changes done with Reference to task  T-505313
            if(invite.Invitee_Contact__c!=null) {
                addtoNetworkOnly = true;
            }
            invite.Last_Invitation_Viewed_Date_Time__c = System.now();
            invite.Client_IP_Address__c = clientIP;
            invite.User_Agent__c = userAgent;
        }
        getLanguageSettings();
        //getCaptchaLanguage();
    }

    public List<SelectOption> getLanguageSettings() {
        languageOptions = new List<SelectOption>();
        Map<String,CashManagementLanguageConfiguration__c> cashMLanguages = CashManagementLanguageConfiguration__c.getAll();
        if(cashMLanguages!=null && cashMLanguages.size()>0) {
            for(CashManagementLanguageConfiguration__c lang :cashMLanguages.values()) {
                if(lang.Active__c){
                languageOptions.add(new SelectOption(lang.ISO_Code__c,lang.Label__c));
                }
            }
        }
        System.debug('Hello'+ cashMLanguages.values());
        return languageOptions;
    }



        /*
          if(cashMLanguages!=null && cashMLanguages.size()>0) {
            for(CashManagementLanguageConfiguration__c lang :cashMLanguages.values()) {
                languageOptions.add(new SelectOption(lang.ISO_Code__c,lang.Label__c));
            }
        }*/
       // System.debug('Hello '+cashMLanguages.values());



    private string retriveCookie(){

        Cookie selLanguage = ApexPages.currentPage().getCookies().get('selLang');
        system.debug('****>>'+ selLanguage);
        if(selLanguage != null)
            return selLanguage.getValue();
        return '';
    }


    // Get fields from fieldset
    public Map<String,Schema.SObjectField> getFields() {
        SObjectType invitesType = Schema.getGlobalDescribe().get('Invites__c');
        return invitesType.getDescribe().fields.getMap();
    }
    // GetInvite with all the fields in the fieldset
    private List<Invites__c> getInvites(String id,String invitationToken) {
        String query = 'SELECT ';
        for(String field : this.getFields().KeySet()) {
            query += field + ', ';
        }
        query += 'Invoice__r.Beneficiary_GP_ID__c,Invoice__r.Beneficiary_GP_Name__c,Invoice__r.Beneficiary_GP_Email_Address__c,Invoice__r.Beneficiary_GP_Country__c,Invoice__r.Supplier__c,Inviter_User__r.Contact.AccountId FROM Invites__c Where Id =:id AND Invitation_Token__c=:invitationToken';
        System.debug('query:::: ' + query);
        return Database.query(query);
    }


    public PageReference cancel() {
         PageReference pg = Page.CommunityLogin;
         pg.setRedirect(true);
         return pg;
    }

    public PageReference acceptUser() {

        logAcceptUser.logMessage('>>inside acceptUser method ...>> ');
        System.debug('Hello'+ApexPages.currentPage().getParameters().get('g-recaptcha-response'));
        logAcceptUser.logDebug('Hello'+ApexPages.currentPage().getParameters().get('g-recaptcha-response'));
        if(showCaptcha && String.isBlank(ApexPages.currentPage().getParameters().get('g-recaptcha-response'))){
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR , Label.CM_EDGE_CAPTCHA_ERROR));
              return null ;
        }
        Savepoint sp = Database.setSavepoint();
        try {
        CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        Account beneAccount = new Account(Name=companyName,RecordTypeId=accountRecordType);

        if(invite != null && invite.Invoice__r != null)
            beneAccount.ExternalId__c = invite.Invoice__r.Beneficiary_GP_ID__c;

        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        System.Debug('###'+newContact);
        logAcceptUser.logDebug('###'+newContact);
        System.Debug('###'+newContact.CurrencyISOCode);
        logAcceptUser.logDebug('###'+newContact.CurrencyISOCode);
        //MAP 3/1/16 - Flag all newly created Accounts to "lead"
        beneAccount.CMP_WUBS_EDGE_Account_Status__c = '1 - New WUBS EDGE Lead';
        // MAP 3/15/16 - Flag all newly created Accounts as CMP Enabld
        beneAccount.CMP_Enabled__c = TRUE;
        beneAccount.BillingStreet = newContact.MailingStreet;
        beneAccount.BillingCity = newContact.MailingCity;
        beneAccount.BillingState = newContact.MailingState;
        beneAccount.BillingPostalCode = newContact.MailingPostalCode;
        beneAccount.BillingCountry = newContact.country__c;//changes done with Reference to task  T-505313
        beneAccount.CurrencyISOCode = newContact.CurrencyISOCode;
       // beneAccount.phone=newContact.phone;
        insert beneAccount;

        newContact.RecordTypeId=contactRecordType;
        newContact.AccountId=beneAccount.Id;

        insert newContact;

        User commUser = new User(FirstName = newContact.FirstName, LastName = newContact.LastName, ContactId = newContact.Id,
                                 Email = newContact.Email , username = userName, isActive =  true,
                                 ProfileId = profileId,CurrencyISOCode = newContact.CurrencyISOCode,
                                 DefaultCurrencyISOCode = newContact.CurrencyISOCode,
                                 emailencodingkey='UTF-8',languagelocalekey=selectedLanguage,
                                 localesidkey='en_US',timezonesidkey='Europe/London',CMP_Enabled__c=true,
                                 Already_a_WUBS_Customer__c=isWubsCustomer);

        commUser.UserName = newContact.Email;
        commUser.Street = newContact.MailingStreet;
        commUser.City = newContact.MailingCity;
        commUser.State = newContact.MailingState;
        commUser.PostalCode = newContact.MailingPostalCode;
        commUser.Country = newContact.country__c;//changes done with Reference to task  T-505313
        commUser.phone = newContact.phone;//Reference to task  T-505313

        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        //Validate if there already exists user with duplicate username
        for(User us: [Select Id From User Where UserName =:commUser.UserName limit 1]){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CM_Edge_DuplicateUserName_ErrorMessage));
            Database.rollback( sp );
            newContact = newContact.clone(false,true);
            logAcceptUser.saveLogs();
            return null;
        }

        //commUser.CommunityNickname = commUser.alias;
        // MAP 3/15/16 - Set all New User defaults
        commUser.CMP_Enabled__c = TRUE;
        commUser.CMP_Network_Invitation_Enabled__c = TRUE;
        commUser.EDGE_Submit_Invoice_Enabled__c = TRUE;

		// Raghu Rankawat : for Task T-542453
		    commUser.EDGE_EULA_Accepted_Date_Time__c = System.Now();
        commUser.EDGE_EULA_Accepted_Version__c = cmpA.EDGE_Current_EULA_Version__c;
        insert commUser;


        if (commUser.Id != null && invite != null) {
            invite.Invitee_Contact__c = newContact.Id;
            invite.Status__c ='Accepted';
            invite.Status_DateTime__c = system.now();
            update invite;

            List<Network__c> listNetworks = new List<Network__c>();

            // Network__c newNetwork = new Network__c(Invitee_Contact__c=newContact.Id,Inviter_User__c=invite.Inviter_User__c,
            //                                       Account_Invitee__c=newContact.AccountId,
            //                                       Account_Inviter__c=invite.Inviter_User__r.Contact.AccountId,
            //                                       Status__c='3 - Active (Accepted)',Status_DateTime__c = system.now());
            // insert newNetwork;

            List<Invites__c> listInvitesToDelete = new List<Invites__c>();
            for(Invites__c inv: listInvites){
                Network__c newNetwork = new Network__c(Invitee_Contact__c=newContact.Id,Inviter_User__c=inv.Inviter_User__c,
                                                   Account_Invitee__c=newContact.AccountId,
                                                   Account_Inviter__c=inv.Inviter_User__r.Contact.AccountId,
                                                   Status_DateTime__c = system.now());
                if(inv.Id != invite.Id){
                    newNetwork.isNetworkCreatedFromSelfRegPage__c = true;
                    newNetwork.Status__c='1 - Pending (Sent)';
                    listInvitesToDelete.add(inv);
                }

                else
                    newNetwork.Status__c='3 - Active (Accepted)';

                listNetworks.add(newNetwork);
            }
            insert listNetworks;
            delete listInvitesToDelete;

            System.debug('Invoice__r:::: ' + invite.Invoice__r);
            logAcceptUser.logDebug('Invoice__r:::: ' + invite.Invoice__r);
            if(invite.Invoice__r != null){
                Global_Pay_ID_Management__c gPIM = new Global_Pay_ID_Management__c(Global_Pay_ID__c=invite.Invoice__r.Beneficiary_GP_ID__c,
                                                                                    Source_Email_Match__c=invite.Invoice__r.Beneficiary_GP_Email_Address__c,
                                                                                Source_Invoice_ID__c=invite.Invoice__c,
                                                                               User_Beneficiary__c=commUser.Id,
                                                                               Creation_Type__c='Manually Created');


                     insert gPIM;

                     System.debug('Supplier:::: ' + invite.Invoice__r.Supplier__c);
                     logAcceptUser.logDebug('Supplier:::: ' + invite.Invoice__r.Supplier__c);
                if(invite.Invoice__r.Supplier__c != null){
                    Id suppId = invite.Invoice__r.Supplier__c;
                    Supplier__c supp = new Supplier__c(Id=suppId);
                    supp.Supplier__c = beneAccount.Id;
                    update supp;
                }
            }

            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO , Label.CM_Alert_SelfReg_NextSteps));
            isRegSuccessful = true;
           //PageReference pg = Page.CommunityLogin;
           //pg.setRedirect(true);
           //return pg;
        }else if(commUser.Id != null && invite == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO , Label.CM_Alert_SelfReg_NextSteps));
            isRegSuccessful = true;
        }
        }catch(Exception e) {
            logAcceptUser.logDebug('Catch Exception : ' + e.getMessage());
            logAcceptUser.saveLogs();
            Database.rollback( sp );
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
            newContact = newContact.clone(false,true);
        }
        logAcceptUser.saveLogs();
        return null;
    }

    //NS: 05/12 - Commented Following Method as we are not using it currently.
    // public PageReference acceptNetwork() {
    //     Savepoint sp = Database.setSavepoint();
    //     try {
    //         invite.Status__c ='Accepted';
    //         invite.Status_DateTime__c = system.now();
    //         update invite;
    //         Network__c newNetwork = new Network__c(Invitee_Contact__c=invite.Invitee_Contact__c,Inviter_User__c=invite.Inviter_User__c,
    //                                               Account_Invitee__c=newContact.AccountId,
    //                                               Account_Inviter__c=invite.Inviter_User__r.Contact.AccountId,
    //                                               Status__c='3 - Active (Accepted)',Status_DateTime__c = system.now());
    //         insert newNetwork;
    //         User commUser = [SELECT Id from User WHERE ContactId=:invite.Invitee_Contact__c LIMIT 1];

    //         if(invite.Invoice__r != null){
    //             Global_Pay_ID_Management__c gPIM = new Global_Pay_ID_Management__c(Global_Pay_ID__c=invite.Invoice__r.Beneficiary_GP_ID__c,
    //                                                                               Source_Email_Match__c=invite.Invoice__r.Beneficiary_GP_Email_Address__c,
    //                                                                               Source_Invoice_ID__c=invite.Invoice__c,
    //                                                                               User_Beneficiary__c=commUser.Id,
    //                                                                               Creation_Type__c='Manually Created');

    //               insert gPIM;
    //         }
    //       PageReference pg = Page.CommunityLogin;
    //       pg.setRedirect(true);
    //       return pg;
    //     }catch(Exception e) {
    //         Database.rollback( sp );
    //         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
    //     }
    //     return null;
    // }


    public PageReference changeLanguage(){

        //if(selectedLanguage.equalsIgnoreCase(English)




        Cookie selLanguage = new Cookie('selLang', selectedLanguage,null,-1,true);
        PageReference pg = new PageReference( ApexPages.currentPage().getUrl());
        pg.setCookies(new Cookie[]{selLanguage});
        system.debug('*****selLanguage:'+selLanguage+' >>'+pg);
        pg.setRedirect(true);
        //User us = new User(Id=UserInfo.getUserId());
        //us.languagelocalekey=selectedLanguage;
        //update us;
        return pg;
    }

    public void translateCountries(){
        sortedCountries = Utility.getSortedcountries();
    }


    /*
    public  pageReference Init(){
        pageReference pgRef = null;
        try{

            if(!UserInfo.getUserType().equalsIgnoreCase('Guest')){
                String profileName = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
                if(profileName.containsIgnoreCase('cct')){
                    //pgRef = Page.wuDashboard;
                    pgRef = Page.EDGE_Activity_Dashboard;
                }
            }


            if(pgRef!=null){
                pgRef.setredirect(true);
            }
        }


        catch(Exception exc){
            ApexPages.addMessages(exc);
        }

        return pgRef;
    }
    */


}