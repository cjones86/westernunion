/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_InvoiceAttachmentsController
 * Description: Controller class for Invoice Attachment relate4d list
 * Created Date: 16 Feb' 2016
 * Created By: Rohit Sharma (Appirio)
 *
 * Date Modified                Modified By                 Description of the update
 15 April 2016					Ranjeet Singh				T-493686, Documents should only be deletable by the creator
 =====================================================================*/
 public without sharing class EDGE_InvoiceAttachmentsController {

    static apexLogHandler.apexLog logUploadAttachment = new apexLogHandler.apexLog('InvoiceAttachCtrl','uploadAttachment');
    
	public String invoiceId{get;set;}
	public String invoiceName{get;set;}
    public String invoicePage{get;set;}
    public String inputId{get;set;}
    public String inputName{get;set;}
	public FeedItem feedPost{get;set;}
	public boolean showAttachmentpopUp{get;set;}
	public String feedId{get;set;}
	public String latestContentId{get;set;}
	public String selectFeed{get;set;}
	public boolean viewAttachment{get;set;}
	public id LoggedInUser{
		get{
			return Utility.loggedInUser.id;
		}
	}

	public EDGE_InvoiceAttachmentsController() {
		feedPost = new FeedItem();
	}

	public List<FeedItem> getContentFeeds() {
		if(invoiceId != null)return [SELECT Id,ContentFileName,ContentType,Title,RelatedRecordId,CreatedById,CreatedBy.Name,CreatedDate FROM FeedItem WHERE ParentId=:invoiceId AND Type='ContentPost' ORDER BY CreatedDate desc];
        else return [SELECT Id,ContentFileName,ContentType,Title,RelatedRecordId,CreatedById,CreatedBy.Name,CreatedDate FROM FeedItem WHERE ParentId=:inputId AND Type='ContentPost' ORDER BY CreatedDate desc];
	}

	public Pagereference uploadAttachment() {
	    logUploadAttachment.logMessage('>>inside uploadAttachment method ...>> ');
     	try {
        	if(feedPost.Title!=null && !String.isBlank(feedPost.Title)) {
	            if(invoiceId != null){
                feedPost.ParentId = invoiceId; //eg. Opportunity id, custom object id..
                feedPost.Body = feedPost.Title + '-'+invoiceName;
              }else{
                feedPost.ParentId = inputId; //eg. Opportunity id, custom object id..
                feedPost.Body = feedPost.Title + '-'+inputName;
              }
                feedPost.ContentFileName = feedPost.Title;
                feedPost.Visibility = 'AllUsers';
                insert feedPost;
        	}
        	showAttachmentpopUp = false;
        } catch (Exception ex) {
            logUploadAttachment.logDebug('error:: '+ ex.getMessage());
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
            logUploadAttachment.saveLogs();
        }
        if(invoiceId != null){
        	String encodedInvoiceId = Utility.doEncryption(invoiceId);
          logUploadAttachment.saveLogs();
          if(invoicePage == 'InvoiceDetail')return new PageReference ('/EDGE_InvoiceDetail2?id='+encodedInvoiceId);
          else return new PageReference ('/EDGE_Invoice_Submit_Detail?id='+encodedInvoiceId);
        }
        else{
        	String encodedInputId = Utility.doEncryption(inputId);
          logUploadAttachment.saveLogs();  
          return new PageReference ('/EDGE_InputDetail?id='+encodedInputId);
        }
	}

	public void showUploadAttachment() {
		showAttachmentpopUp = true;
	}

    public Pagereference hideUploadAttachment() {
		showAttachmentpopUp = false;
		return null;
	}

	public Pagereference viewContent(){
     	latestContentId = selectFeed;
     	viewAttachment = true;
     	return null;
     }

     public Pagereference deleteFeed() {
     	if(feedId!=null && !String.isBlank(feedId)) {
     		delete (new FeedItem(Id=feedId));
     	}
        String currentPageName =  Utility.getPageNamefromUrl(ApexPages.CurrentPage().getUrl());
        //   if(invoiceId != null)
               return new PageReference ('/'+currentPageName+'?id='+(!String.isEmpty(invoiceId)?invoiceId:inputId));
        //   else 
        //     return new PageReference ('/EDGE_InputDetail?id='+inputId);
     }

     public Pagereference hideAttachment() {
     	viewAttachment = false;
     	return null;
     }
}