/**=====================================================================
 * Name: Edge_SupplierSearchComponentCtrl_Test
 * Description: Test class for Edge_SupplierSearchComponentCtrl
 * Created Date: May 17, 2016
 * Created By: Nikhil Sharma
 =====================================================================*/
 
@isTest
private class Edge_SupplierSearchComponentCtrl_Test {

	private static testMethod void test() {
        test_Utility.createCMPAdministration();
		test_Utility.createCMPAlert();
		CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        insert beneAccount;
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact;
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
		commUser.timezonesidkey='America/Indiana/Indianapolis';
		commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        commUser.CommunityNickname = commUser.alias;
        insert commUser;
        Supplier__c sup1 = new Supplier__c(Buyer__c=beneAccount.Id,Address_line_1__c='test1',Address_Line_2__c='test2',
                                City__c='test',State_Province__c='test',Country__c='AND',Post_Code__c='123');
        Supplier__c sup2 = new Supplier__c(Buyer__c=beneAccount.Id,Address_line_1__c='test1',Address_Line_2__c='test2',
                                City__c='test',State_Province__c='test',Country__c='AND',Post_Code__c='123');
        List<Supplier__c> listsup = new List<Supplier__c>{sup1,sup2};
        insert listsup;
        System.runAs(commUser){
            Edge_SupplierSearchComponentCtrl ctrl = new Edge_SupplierSearchComponentCtrl();
            ctrl.searchTxt = 'test';
            ctrl.search();
            ctrl.refresh();
            String netStatusTrans = ctrl.listSuppliers[0].netStatusTrans;
            Map<String,String> networkStatusTranslation = Edge_SupplierSearchComponentCtrl.networkStatusTranslation;
        }
	}

}