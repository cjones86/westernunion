//Created By   : Ranjeet Singh
//Created date : March/03/2016
//Description  : T-482225, POC - Retreive H2H Get Holding Currencies from WUBS 2.0
//              Controller Clas For WUBS20_POC.page
 
global without sharing class WUBS20_POCControler {
    public String CustomerId{get;set;}
    public String calloutResponseStatus{get;set;}
    public String calloutResponseResult{get;set;}
    public String countries{get;set;}
    public WUBS20_POCControler(){
        CustomerId = '';
        Init();
    }
    void Init(){
        calloutResponseStatus = '';
        calloutResponseResult = '';
        countries = '';     
    }
    public void getHoldingCurrencies(){
        try{
            Init();
            EdgeGpTransctionAPI apiCall = new EdgeGpTransctionAPI(true);
            //  EdgeGpTransctionAPI.isMockupResponse = true;
            if(apiCall.getHoldingCurrencies(CustomerId)){
                calloutResponseResult = apiCall.calloutResponseResult;
                calloutResponseStatus = apiCall.calloutResponseStatus;
                System.Debug('########## WUBS20_POCControler: calloutResponseResult' + calloutResponseResult + ' #####');
                System.Debug('########## WUBS20_POCControler: calloutResponseStatus' + calloutResponseStatus + ' #####');
            } 
        }catch(Exception Ex){
            ApexPages.addMessages(Ex);
            System.Debug('########## WUBS20_POCControler: Ex' + Ex + ' #####');
        }
    }
    
    public List<SelectOption> getCurrency(){
        EdgeGpJsonUtils jsonUtil = new EdgeGpJsonUtils(true);
        return jsonUtil.getCurrency(calloutResponseResult);
    }

}