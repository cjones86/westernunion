/**=====================================================================
 * Name: Edge_SupplierSearchComponentCtrl
 * Description: Controller class for Edge_SupplierSearchComponent component
 * Created Date: May 09, 2016
 * Created By: Nikhil Sharma
 * Task      : T-501132
 =====================================================================*/

public without sharing class Edge_SupplierSearchComponentCtrl {

    public string searchTxt{get;set;}
    public list<SupplierWrapper> listSuppliers{get;set;}
    public Boolean isTransactionAllowedToCMP{get;set;}
    public Boolean payments{get;set;}
    public String SupplierId{get;set;}

   public static map<String, String> networkStatusTranslation{
    get{
      if(networkStatusTranslation==null){
        Schema.DescribeFieldResult F = Network__c.Status__c.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        networkStatusTranslation = new Map<String, String>();

        for(Schema.PicklistEntry e : P)
        {
            networkStatusTranslation.put(e.value, e.label);
        }
      }
      return networkStatusTranslation;
      }
      set;
    }


    public Edge_SupplierSearchComponentCtrl(){
        searchTxt = '';
        isTransactionAllowedToCMP = Utility_Security.canMakeStandardColdPayments;
        if(isTransactionAllowedToCMP){
            fetchSupplierRecords();
        }else{
        	String pageName = ApexPages.CurrentPage().getUrl();
	        	if(pageName!= null && pageName.contains('EDGE_Beneficiary')){
	        		fetchSupplierRecords();
	        	}
        }
    }

    // method to fetch supplier records based on searched text and current user's account
    public void fetchSupplierRecords(){
        listSuppliers = new list<SupplierWrapper>();
        if(String.isBlank(searchTxt)){
            for(Supplier__c sup : [SELECT Id, Name,Address_line_1__c,Address_Line_2__c,Buyer__c,City__c,Country__c,Post_Code__c,State_Province__c,
                                          Supplier__c,Network_Status__c,Supplier_Name__c,Supplier__r.CMP_H2H_Transaction_Enabled__c,
                                          CreatedDate,Created_Invoice_Date__c,Network__r.Status__c,Supplier__r.WU_EDGE_Flag__c,Beneficiary_Status__c
                                    FROM Supplier__c WHERE Buyer__c = :Utility.CurrentAccount AND Beneficiary_Status__c = 'ACTIVE' ]){
                     listSuppliers.add(new SupplierWrapper(sup));
            }
        }else{
            String mySearchString = searchTxt + '*';
            List<List<SObject>> searchList = [FIND :mySearchString IN ALL FIELDS
                                                RETURNING Supplier__c (Id,Name,Address_line_1__c,Address_Line_2__c,
                                                Supplier__r.CMP_H2H_Transaction_Enabled__c,Country__c,Post_Code__c,State_Province__c,Network__r.Status__c,
                                                Buyer__c,City__c,Supplier__c,Supplier_Name__c,Network_Status__c,CreatedDate,Created_Invoice_Date__c,
                                                Beneficiary_Status__c
                                                WHERE Buyer__c = :Utility.CurrentAccount AND Beneficiary_Status__c = 'ACTIVE')];
            List<Supplier__c> listSupp = searchList[0];
            for(Supplier__c sup: listSupp){
                listSuppliers.add(new SupplierWrapper(sup));
            }
        }
        listSuppliers.sort();
        System.debug('### listSuppliers ' + listSuppliers);
    }

    public void search(){
        fetchSupplierRecords();
    }

    public void refresh(){
        searchTxt = '';
        fetchSupplierRecords();
    }
    public  void deactivateSupplierRecord(){
      // System.assert(false, SupplierId);
        //Supplier__c suppToBeDeactivate = new Supplier__c(Id = SupplierId);
        //suppToBeDeactivate.Beneficiary_Status__c = 'In-Active';
        /* Start : T-538984 :deactivate the network connection if one exists */
        Supplier__c sup = [Select Network__c,Network__r.Status__c FROM Supplier__c WHERE Id = :SupplierId];
        if(sup.Network__c != null){
          Network__c net = new Network__c(Id = sup.Network__c,Status__c = '4 - Deactivated',Hide_from_Network_Connections__c=true);
        	update net;
        }
        sup.Beneficiary_Status__c = 'In-Active';
        /* Stop : T-538984 :deactivate the network connection if one exists */

        //update suppToBeDeactivate;
        update sup;
        fetchSupplierRecords();

    }


    public class SupplierWrapper implements Comparable{
        public String BeneficiaryStatus{get;set;}
        public Supplier__c supp{get;set;}
        public String id{get;set;}
        public String address{get;set;}
        public String name{get;set;}
        public Boolean canPayH2H{get;set;}
        public String H2HCapable{get;set;}
        public boolean isIntegrationEnabled = Utility_Security.isIntegrationEnabled;
        public DateTime dtTime{get;set;}
        public String netStatus {get;set;}
        public String encodedSuppId{get;set;}
        public SupplierWrapper(Supplier__c sup){
            this.BeneficiaryStatus = sup.Beneficiary_Status__c;
            this.supp = sup;
            this.netStatus = supp.Network__r.Status__c;
            this.dtTime = sup.CreatedDate;
            if(sup.Created_Invoice_Date__c != null){
                this.dtTime = sup.Created_Invoice_Date__c;
            }
            if(isIntegrationEnabled && sup.Supplier__c != null && sup.Network_Status__c.containsIgnoreCase('Accepted') && sup.Supplier__r.CMP_H2H_Transaction_Enabled__c)
                this.canPayH2H = true;
            else
                this.canPayH2H = false;

            if(isIntegrationEnabled && sup.Supplier__c != null && sup.Supplier__r.CMP_H2H_Transaction_Enabled__c)
                this.H2HCapable = Label.CM_Field_Value_Yes;
            else
                this.H2HCapable = Label.CM_Field_Value_No;

            //this.id=sup.id;
            encodedSuppId = EncryptionManager.doEncrypt(sup.id);
            if(encodedSuppId.contains('+')){
              this.id = encodedSuppId.replace('+','%2B');
            }else{
            	this.id = encodedSuppId;
            }
            this.name = sup.Supplier_Name__c;
            String countryLabel = returnCountryLabel(sup.Country__c);
            this.address = getFullAddress(sup.Address_line_1__c,sup.Address_Line_2__c,sup.City__c,sup.State_Province__c,countryLabel,sup.Post_Code__c);
        
            
        
        }

        public Integer compareTo(Object compareTo) {
	    	SupplierWrapper supWrpr = (SupplierWrapper)compareTo;
	        if(dtTime > supWrpr.dtTime)    return -1;
	        if(dtTime < supWrpr.dtTime)    return 1;
	        return 0;
	    }

        public string getFullAddress(String addline1,String addline2,String city,String state,String country, String postalCode){
            String address = '';
            if(String.isNotBlank(addline1))
             address = addline1 + ', ';
            if(String.isNotBlank(addline2))
             address += addline2 + ', ';
            if(String.isNotBlank(city))
             address += city + ', ';
            if(String.isNotBlank(state))
             address += state + ', ';
            if(String.isNotBlank(country))
             address += country + ', ';
            if(String.isNotBlank(postalCode))
             address += postalCode;
            return address;
        }

        public String returnCountryLabel(String countryCode){
        	  Schema.DescribeFieldResult fieldResult = Supplier__c.Country__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

			         for( Schema.PicklistEntry f : ple)
			         {
			            if(f.getValue() == countryCode){
			            	return f.getLabel();
			            }
			         }
			         return '';
        }

      public String netStatusTrans{
	      get{
	        if(networkStatusTranslation.containsKey(netStatus)){
	          return networkStatusTranslation.get(netStatus);
	        }else{
	          return netStatus;
	        }
	      }
	    }

    }

}