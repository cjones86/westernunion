/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_Capture_Input_Controller
 * Description: Controller to create inputs from Input management
 * Created Date: 01 Mar' 2016
 * Created By: Rohit Sharma (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
public with sharing class EDGE_Capture_Input_Controller {

	public map<String,List<SelectOption>> transactionTypes{get;set;}

	public EDGE_Capture_Input_Controller() {
		transactionTypes = getTransactionTypesValue();
	}

	public map<String,List<SelectOption>> getTransactionTypesValue() {
		map<String,List<SelectOption>> tansactionType = new map<String,List<SelectOption>>();
		tansactionType.put(Label.EDGE_Input_Management_Hedging,new List<SelectOption>());
		tansactionType.put(Label.EDGE_Input_Management_Payables,new List<SelectOption>());
		tansactionType.put(Label.EDGE_Input_Management_Receivables,new List<SelectOption>());
		String currentPageURL = 'https://'+ApexPages.currentPage().getHeaders().get('Host') +'/managecash'+ApexPages.currentPage().getUrl();
		List<Schema.PicklistEntry> picklistValues = Input__c.Input_Type__c.getDescribe().getPicklistValues();
        for (Schema.PicklistEntry picklistValue : picklistValues) {
        	if(picklistValue.value=='Invoice (Payable)') {
        		tansactionType.get(Label.EDGE_Input_Management_Payables).add(new SelectOption('EDGE_Capture_Invoice',picklistValue.label));
        	} else if(picklistValue.value=='Purchase Order') {
        		tansactionType.get(Label.EDGE_Input_Management_Payables).add(new SelectOption('EDGE_CaptureInput?trans=Purchase Order',picklistValue.label));
        	} else if(picklistValue.value=='Forecast Outflow') {
        		tansactionType.get(Label.EDGE_Input_Management_Payables).add(new SelectOption('EDGE_CaptureInput?trans=Forecast Outflow',picklistValue.label));
        	} else if(picklistValue.value=='Payables - Other' && Utility_Security.canCreateTransaction) {
        		tansactionType.get(Label.EDGE_Input_Management_Payables).add(new SelectOption('EDGE_MakePaymentStep1',picklistValue.label));
        	} else if(picklistValue.value=='Forecast Inflow') {
        		tansactionType.get(Label.EDGE_Input_Management_Receivables).add(new SelectOption('EDGE_CaptureInput?trans=Forecast Inflow',picklistValue.label));
        	} else if(picklistValue.value=='Invoice (Receivable)' && Utility_Security.canSubmitInvoices) {
        		tansactionType.get(Label.EDGE_Input_Management_Receivables).add(new SelectOption('EDGE_SubmitInvoice',picklistValue.label));
        	} else if(picklistValue.value=='Option - Other') {
        		tansactionType.get(Label.EDGE_Input_Management_Hedging).add(new SelectOption('EDGE_CaptureInput?trans=Option - Other',picklistValue.label));
        	} else if(picklistValue.value=='Option - WUBS') {
        		tansactionType.get(Label.EDGE_Input_Management_Hedging).add(new SelectOption('EDGE_CaptureInput?trans=Option - WUBS',picklistValue.label));
        	} else if(picklistValue.value=='Window Forward Contract - Other') {
        		tansactionType.get(Label.EDGE_Input_Management_Hedging).add(new SelectOption('EDGE_CaptureInput?trans=Window Forward Contract - Other',picklistValue.label));
        	} else if(picklistValue.value=='Fixed Forward Contract - Other') {
        		tansactionType.get(Label.EDGE_Input_Management_Hedging).add(new SelectOption('EDGE_CaptureInput?trans=Fixed Forward Contract - Other',picklistValue.label));
        	} else if(picklistValue.value=='Foreign Currency Balance') {
        		tansactionType.get(Label.EDGE_Input_Management_Hedging).add(new SelectOption('EDGE_CaptureInput?trans=Foreign Currency Balance',picklistValue.label));
        	}
        }
        return tansactionType;
	}
}