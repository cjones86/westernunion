/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_InvoiceAttachmentsControllerTest
 * Description: Test class for EDGE_InvoiceAttachmentsController class (controller class for related attachments on invoice page)
 * Created Date: 23rd Feb' 2016
 * Created By: Nikhil Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update                                           
 =====================================================================*/
 
 @isTest
private class EDGE_InvoiceAttachmentsControllerTest{

    static testMethod void captureInvoice() {
        test_Utility.createCMPAdministration();
        test_Utility.createCMPAlert();
        test_Utility.currencyISOMapping();
        test_Utility.createWUEdgeSharingAdmin();
        CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        insert beneAccount;
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact;
        Account beneAccount1 = test_Utility.createAccount(false);
        beneAccount1.RecordTypeId=accountRecordType;
        beneAccount1.ExternalId__c='12345';
        beneAccount1.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        insert beneAccount1;
        Contact newContact1 = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact1;
        Contact newContact2 = test_Utility.createContact(contactRecordType,true,beneAccount.Id);
        //insert newContact2;
        User commUser2 = test_Utility.createCommUser(newContact2.Id, true);
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
        commUser.timezonesidkey='America/Indiana/Indianapolis';
        commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        commUser.CommunityNickname = commUser.alias;
        insert commUser;
        if (commUser.Id != null) { 
            Global_Pay_ID_Management__c gPIM = new Global_Pay_ID_Management__c(Global_Pay_ID__c='1234567',
                                                                               User_Beneficiary__c=commUser.Id,
                                                                               Creation_Type__c='Automatic Email Match');
           insert gPIM;
        }
        
        Invoice__c submitInvoice = new Invoice__c(RecordTypeId=Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('GP Invoice - Active').getRecordTypeId());
       //submitInvoice.Name = 'INVTest-123';
        submitInvoice.Status__c = 'Invoice Submitted';
        submitInvoice.Initiated_By__c='Seller';
        submitInvoice.Account__c = beneAccount.Id;
        submitInvoice.Buyer__c = commUser.Id;
        submitInvoice.Beneficiary_GP_ID__c = '12345';
        insert submitInvoice;
        
        Attachment uploadedAttachment = test_Utility.addAttachmentToParent(submitInvoice.Id, 'test','test2');
        FeedItem post = new FeedItem();
          post.Title = 'test';
          post.ParentId = submitInvoice.Id; //eg. Opportunity id, custom object id..
          post.Body = uploadedAttachment.Name + '-'+submitInvoice.Name;
          post.Type = 'ContentPost';
          post.ContentData = Blob.valueOf('test data'); //uploadedAttachment.body;
          post.ContentFileName = 'test';//uploadedAttachment.Name;
          post.Visibility = 'AllUsers';
       //   post.NetworkScope = Network.getNetworkId();
          insert post;
        
        PageReference pageRef = Page.Edge_InvoiceDetail2;
        Test.setCurrentPage(pageRef);
        
        System.RunAs(commUser){
          Test.startTest();
          //Test Related list of related docs on Invoice detail page
          EDGE_InvoiceAttachmentsController attCtrl = new EDGE_InvoiceAttachmentsController();
          List<FeedItem> listFeeds = attCtrl.getContentFeeds();
          attCtrl.feedPost.Title = 'test';
          attCtrl.invoiceId = submitInvoice.Id;
          PageReference pageRef1 = attCtrl.uploadAttachment();
          attCtrl.showUploadAttachment();
          attCtrl.viewContent();
          PageReference pageRef2 = attCtrl.hideUploadAttachment();
          PageReference pageRef3 = attCtrl.hideAttachment();
          attCtrl.feedId = post.Id;
          attCtrl.deleteFeed();
          List<FeedItem> listPosts= [Select Id From FeedItem Where Id=:post.Id];
          System.assertEquals(0, listPosts.size());         
          Test.stopTest();
        }
    }
}