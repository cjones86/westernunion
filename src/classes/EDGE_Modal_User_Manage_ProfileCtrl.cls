public without sharing class EDGE_Modal_User_Manage_ProfileCtrl {

    static apexLogHandler.apexLog logCustUpdate = new apexLogHandler.apexLog('ModalUserManageProfileCtrl','custUpdate');
    
    public User commUser {get; set;}
    public String returnURL {get; set;}
    public Boolean isSaved{get;set;}
    public Map<String,String> mapCurrencyValAndLabel{get;set;}
    public Map<String,String> mapLocalValAndLabel{get;set;}
    public Map<String,String> mapLangValAndLabel{get;set;}
    public Map<String,String> mapTimeValAndLabel{get;set;}
    public List<SelectOption> languageOptions{get;set;}

    public Boolean CollabUser{
        get{
            return Utility.isCollaborationUser;
        }
    }

    public EDGE_Modal_User_Manage_ProfileCtrl (){
        String userId = UserInfo.getUserId();//ApexPages.currentPage().getParameters().get('Id');
        isSaved = true;
        mapCurrencyValAndLabel = new Map<String,String>();
        mapLocalValAndLabel = new Map<String,String>();
        mapLangValAndLabel= new Map<String,String>();
        mapTimeValAndLabel= new Map<String,String>();
       // returnURL = ApexPages.currentPage().getParameters().get('retURL');
        if(!String.isEmpty(userId)){
            commUser = [SELECT Id, FirstName, LastName,Email, Username, Alias, IsActive, ProfileId, ContactId,
                            Phone, Extension, CommunityNickname, Fax, Title, MobilePhone, CompanyName,
                            EmailEncodingKey, Department, Street, LanguageLocaleKey, State, PostalCode, Country,DefaultCurrencyISOCode,
                            FederationIdentifier, TimeZoneSidKey, LocaleSidKey, City, CurrencyIsoCode, Share_Records__c, CMP_Document_Notification__c
                            FROM User WHERE Id =:userId];
        }
        
        
        system.debug('>>>>commUser.LocaleSidKey'+commUser.LocaleSidKey);
        system.debug('>>>>commUser.LocaleSidKey'+commUser.TimeZoneSidKey);
        languageOptions = new List<SelectOption>();
        Map<String,CashManagementLanguageConfiguration__c> cashMLanguages = CashManagementLanguageConfiguration__c.getAll();
        if(cashMLanguages!=null && cashMLanguages.size()>0) {
            for(CashManagementLanguageConfiguration__c lang :cashMLanguages.values()) {
                if(lang.Active__c)
                {
                languageOptions.add(new SelectOption(lang.ISO_Code__c,lang.Label__c));
                }
            }
        }

        Schema.DescribeFieldResult fieldResult_Currency = User.CurrencyIsoCode.getDescribe();
        Schema.DescribeFieldResult fieldResult_Locale = User.LocaleSidKey.getDescribe();
        Schema.DescribeFieldResult fieldResult_Lang = User.LanguageLocaleKey.getDescribe();
        Schema.DescribeFieldResult fieldResult_Time = User.TimeZoneSidKey.getDescribe();

        List<Schema.PicklistEntry> ple_Curr = fieldResult_Currency.getPicklistValues();
        List<Schema.PicklistEntry> ple_Locale = fieldResult_Locale.getPicklistValues();
        List<Schema.PicklistEntry> ple_Lang = fieldResult_Lang.getPicklistValues();
        List<Schema.PicklistEntry> ple_Time = fieldResult_Time.getPicklistValues();

        for(Schema.PicklistEntry f: ple_Curr){
            mapCurrencyValAndLabel.put(f.getValue(), f.getLabel());
        }

        for(Schema.PicklistEntry f: ple_Locale){
            System.debug('f.getLabel(): ' + f.getLabel());
            System.debug('f: ' + f);
            mapLocalValAndLabel.put(f.getValue(), f.getLabel());
        }

        for(Schema.PicklistEntry f: ple_Lang){
              System.debug('lang f.getLabel(): ' + f.getLabel());
            mapLangValAndLabel.put(f.getValue(), f.getLabel());
        }

        for(Schema.PicklistEntry f: ple_Time){
            mapTimeValAndLabel.put(f.getValue(), f.getLabel());
        }
        
        if(!mapLocalValAndLabel.containsKey(commUser.LocaleSidKey)){
        	
        }
        
        System.debug('mapLocalValAndLabel: ' + mapLocalValAndLabel.get('en_HK'));
    }

    public void edit(){
        isSaved = false;
    }

    public void close(){
        isSaved = false;
    }

    public void custUpdate(){
        logCustUpdate.logMessage('>>inside custUpdate method ...>> ');
        try{
        	
        	//******* for Issue I-237714 ********
        	if(commUser.LastName == null || String.isBlank(commUser.LastName)){
        		CustomException c = new CustomException(Label.CM_FieldLabel_CMPUser_LastName_Error_Message);
            	ApexPages.addMessages(c);
        	}
        	else{	
	            update commUser;
	            logCustUpdate.logDebug('Updated User ' + commUser);
	            isSaved = true;
        	}
            
        }catch(Exception ex){
            logCustUpdate.logDebug('Exception ' + ex.getMessage());
            isSaved = false;
            ApexPages.addMessages(ex);
        }
        logCustUpdate.saveLogs();
    }
	//To create custom exception and show custom error message
	public class CustomException extends Exception{} 
	
    public PageReference custCancel(){
        if(returnURL != null) {
            return new Pagereference(returnURL);
        }
        return null;
    }
}