/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_CaptureCashInputsController
 * Description: Controller Test class for EDGE_CaptureCashInputsController_Test
 * Created Date: 04 Apr 2016
 * Created By: Nikhil Sharma (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
@isTest
private class EDGE_CaptureCashInputsController_Test {

	private static testMethod void test() {
		test_Utility.createWUEdgeSharingAdmin();
	    Invoice__c parentInv = test_Utility.createInvoice(true);
        test_Utility.createCMPAdministrationWithInvoice(parentInv.Id);
		test_Utility.createCMPAlert();
		test_Utility.createWubsIntAdministration();
		test_Utility.createGPH2HCurrencies();
		test_Utility.createGPIntegrationAdministration();
		test_Utility.currencyISOMapping();
		
		CMP_Administration__c cmpA = CMP_Administration__c.getInstance();
        String accountRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Account_RecordType__c AND SobjectType='Account' Limit 1]).Id;
        String contactRecordType = ([SELECT Id FROM RecordType WHERE DeveloperName=:cmpA.CMP_Default_Contact_RecordType__c AND SobjectType='Contact' Limit 1]).Id;
        String profileId = ([SELECT Id, Name FROM Profile WHERE Name=:cmpA.Default_User_Self_Registration_Profile__c Limit 1]).Id;
        
        Account beneAccount = test_Utility.createAccount(false);
        beneAccount.RecordTypeId=accountRecordType;
        beneAccount.ExternalId__c='12345';
        beneAccount.CCT_Client_ID__c = '3232112';
        beneAccount.OwnerId = cmpA.CMP_Default_Account_ParentId__c;
        beneAccount.CMP_Create_Transactions__c = true;
        beneAccount.CMP_Enabled__c = true;
        beneAccount.CMP_Holding_Enabled__c = true;
        beneAccount.CMP_H2H_Transaction_Enabled__c = true;
        beneAccount.BillingStreet = 'testStreet';
        beneAccount.BillingCity = 'city';
        beneAccount.BillingState = 'state';
        beneAccount.BillingCountry = 'USA';
        beneAccount.BillingPostalCode = '231231';
        beneAccount.Billing_Address_ISO_Country_Code__c = 'USA';
        insert beneAccount;
        Contact newContact = test_Utility.createContact(contactRecordType,false,beneAccount.Id);
        insert newContact;
        
        User commUser = test_Utility.createCommUser(newContact.Id, false);
        commUser.ProfileId = profileId;
        commUser.emailencodingkey='UTF-8';
        commUser.localesidkey='en_US';
		commUser.timezonesidkey='America/Indiana/Indianapolis';
		commUser.CMP_Enabled__c=true;
        commUser.UserName = newContact.Email+'.cmp';
        commUser.CMP_Network_Invitation_Enabled__c = true;
        commUser.CMP_Create_Transactions__c = true;
        if(commUser.FirstName != null){
            commUser.alias = commUser.FirstName.subString(0,1);
        }else{
            commUser.alias = '';
        }
        if(commUser.lastName != null){
            commUser.alias += commUser.lastName.length() > 4 ? commUser.LastName.subString(0,4) : commUser.lastName;
        }
        insert commUser;    
        
        ApexPages.currentPage().getParameters().put('retUrl','/testRetUrl');
        ApexPages.currentPage().getParameters().put('Referer','/testReferer');
        
        Global_Pay_ID_Management__c gPIM = new Global_Pay_ID_Management__c(Global_Pay_ID__c='12345',
                                                                               User_Beneficiary__c=commUser.Id,
                                                                               Creation_Type__c='Automatic Email Match');
        insert gPIM;
        
        
        
        System.RunAs(commUser){
            Invoice__c inv1 = test_Utility.createInvoice('GP Invoice - Active', beneAccount.Id, commUser.Id,'Pending Approval (Open)', false);
            Invoice__c inv2 = test_Utility.createInvoice('GP Invoice - Active', beneAccount.Id, commUser.Id,'Pending Approval (Open)', false);
            List<Invoice__c> listInvoices = new List<Invoice__c>{inv1,inv2};
            insert listInvoices;
            
        	Test.startTest();
        	    EDGE_CaptureCashInputsController ctrl1 = new EDGE_CaptureCashInputsController();
        	    ctrl1.saveNew();
        	    ctrl1.isSaveClicked = true;
        	    ctrl1.redirectToHome();
            	ApexPages.currentPage().getParameters().put('trans','Purchase Order');
                ApexPages.currentPage().getParameters().put('retUrl','/testRetUrl');
        		EDGE_CaptureCashInputsController ctrl = new EDGE_CaptureCashInputsController();
        		List<SelectOption> listCurr =  ctrl.getInputCurrency();
        		Map<String, string> LabelMaps = ctrl.LabelMaps;
        		PageReference pgRef1 = ctrl.uploadAttachmentFeed();
        		ctrl.fileName = 'test.docx';
        		ctrl.allowedFileType = '.docx';
        		ctrl.uploadAttachmentFeed();
        		ctrl.currentTabAction();
        		ctrl.saveinputs();
        		ctrl.saveNew();
        		ctrl.redirectToHome();
        		ctrl.deleteAttachmentFeed();
        		ctrl.fieldset = 'Option_Other';
        		ctrl.insertInputs();
        		ctrl.fieldset = 'Window_Forward_Contract_Other';
        		ctrl.inputUpload.Window_Start_Date__c = System.Today();
        		ctrl.inputUpload.Window_End_Date__c = System.Today().addDays(-10);
        		ctrl.insertInputs();
        		ctrl.isSaveClicked = true;
        		ctrl.redirectToHome();
        		ctrl.fieldset = 'Option_Other';
        		ctrl.inputUpload.Amount__c = 100;
        		ctrl.inputUpload.Buy_Currency__c = 'USD';
        		ctrl.inputUpload.Sell_Currency__c = 'INR';
        		ctrl.inputUpload.Settlement_Date__c = System.Today();
        		ctrl.insertInputs();
        		ctrl.selectedTranscation = 'Purchase Order';
        		ctrl.changeTransactionType();
        		ctrl.inputUpload.Custom_Currency__c = 'USD';
        		ctrl.inputUpload.Amount__c = 100;
        		ctrl.inputUpload.Transaction_Date__c = System.Today();
        		ctrl.insertInputs();
        	Test.stopTest();
        }
	}

}