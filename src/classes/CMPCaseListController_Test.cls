@isTest
public class CMPCaseListController_Test {
    static Id accountId;
	static testMethod void myUnitTest() {
        createTestData();
        Test.startTest();
        ApexPages.currentPage().getParameters().put('aId', accountId);
        CMPCaseListController ctrl =  new CMPCaseListController();
        ctrl.caseCommentToSearch = 'test';
        ctrl.refreshCaseList();
        String accName = ctrl.accName;
        System.assertEquals('Test Account', accName);
        Test.stopTest();
    }
    
    private static void createTestData() {
    	ID naInvestigationCaseRecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Operations' AND SobjectType = 'Case'].Id;
        ID ausCFXCaseRecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'AUS CFX Case Reason' AND SobjectType = 'Case'].Id;
        Account newAccount = new Account(Name = 'Test Account', OwnerId = UserInfo.getUserId());
        insert newAccount;
        accountId = newAccount.Id;
        Contact newContact = new Contact(AccountId = newAccount.Id, LastName = 'Test Contact');
        insert newContact;
        User testUser = new User();
	     //testUser = new User();
		testUser.alias = 'testuser';
		testUser.Email = 'testuser@test123.com';
		testUser.EmailEncodingKey = 'ISO-8859-1';
		testUser.LanguageLocaleKey = 'en_US';
		testUser.LastName = 'Test User567';
		testUser.LocaleSidKey = 'en_AU';
		testUser.Account_Administrator__c = false;
		testUser.ProfileId = [SELECT Id FROM Profile WHERE Name LIKE 'Partner community - FX management tool' LIMIT 1].Id;
		testUser.TimeZoneSidKey = 'Australia/Sydney';
		testUser.UserName = 'testuser@travelex.com.au';
		testUser.ContactId = newContact.Id;
		testUser.DefaultCurrencyIsoCode = 'INR';
		insert testUser;
        Case cs = new Case(ContactId = newContact.Id, RecordTypeId = naInvestigationCaseRecordTypeId);
        cs.CMP_Public__c = true;
        insert cs;
        CaseComment newCaseComment1 = new CaseComment(ParentId = cs.Id, CommentBody = 'Test');
        newCaseComment1.IsPublished = true;
        insert newCaseComment1;
    }
}