/**=====================================================================
 * Appirio, Inc
 * Name: EDGE_CaptureCashInputsController
 * Description: Controller to create inputs based on transaction type.
 * Created Date: 04 Mar' 2016
 * Created By: Rohit Sharma (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 // not used now : Priyanka
 =====================================================================*/
public without sharing class EDGE_CaptureCashInputsController {

    public String selectedTranscation{get;set;}
    public String selectedTranscation1{get;set;}
    public Input__c inputUpload{get;set;}
    public String fieldset{get;set;}
    public List<SelectOption> transactionTypes{get;set;}
    public String errorMessage{get;set;}
    public String parentInvoiceId{get;set;}
    public String invoiceOwnerId{get;set;}
    public String latestContentId{get;set;}
    public String allowedFileType{get;set;}
    public PageReference returnurl{get;set;}
    public boolean isSaveClicked{get;set;}
    public boolean isInpSaved{get;set;}
    public String beneNumber{get;set;}
    public Id accId {get;set;}
    public String fileName{get;set;}
    public String fileId{get;set;}
    public transient Blob fileData{get;set;}
    public Input__c inputOther{get;set;}
    public String currentTab{get;set;}
    //public FeedItem uploadedAttachment{get;set;}

    public EDGE_CaptureCashInputsController() {
        accId = utility.currentAccount;
        //uploadedAttachment = new FeedItem();
        CMP_Administration__c cmpAdmin = CMP_Administration__c.getInstance();
        parentInvoiceId = cmpAdmin.EDGE_Default_Feed_Invoice__c;
        allowedFileType = cmpAdmin.CMP_Allowable_File_Types__c;

        if(!String.isEmpty(parentInvoiceId)) {
            invoiceOwnerId = [SELECT OwnerId FROM Invoice__c WHERE Id=:parentInvoiceId].OwnerId;
        }
        String prm = ApexPages.currentPage().getParameters().get('trans');
        if(!String.isEmpty(prm)){
            if(prm.indexOf('%20') != -1){
                system.debug('-----------prm before-----------'+prm);
                prm = prm.replaceAll('%20', ' ');
                system.debug('-----------prm after-----------'+prm);
            }
            system.debug('-----------prm -----------'+prm);
            selectedTranscation = prm;
            selectedTranscation1 = prm;
        }
        inputUpload = new Input__c();
        inputOther = new Input__c();
        transactionTypes = getTransactionTypesValues();
        if(String.isEmpty(selectedTranscation)){
            selectedTranscation = transactionTypes.get(0).getValue();
            selectedTranscation1 = transactionTypes.get(0).getValue();
        }
        changeTransactionType();
        //returnurl = Page.EDGE_MyDashboard;
        if(ApexPages.currentPage().getParameters().get('retUrl') != null && ApexPages.currentPage().getParameters().get('retUrl') != ''){
            returnurl = new PageReference(ApexPages.currentPage().getParameters().get('retUrl'));
        }
    }

    public List<SelectOption> getTransactionTypesValues() {
        List<SelectOption> tansactionType = new List<SelectOption>();
        for (Schema.PicklistEntry picklistValue : Input__c.Input_Type__c.getDescribe().getPicklistValues()) {
            if(!picklistValue.value.contains('Invoice') && picklistValue.value!='Payables - Other') {
                tansactionType.add(new SelectOption(picklistValue.value,picklistValue.label));
            }
        }
        return tansactionType;
    }

    public List<SelectOption> getInputCurrency(){
        return Utility.picklistValues('Input__c','Custom_Currency__c', true);
    }

    public void changeTransactionType() {
        if(!String.isEmpty(selectedTranscation)) {
            fieldset = Utility.changeTransactionType(selectedTranscation);
        }
        inputUpload = new Input__c();
        inputOther = new Input__c();
    }

    //map<String, string> getFieldLabel(String sObjectApiName, String selectedTranscation)
    // To show the correct field set according to the selected transaction type in "Add Manual Input" Section
    public map<String, string> LabelMaps{
        get{
            if(!String.isEmpty(fieldset)){
                try{
                System.debug('selTrans:::::: '+ selectedTranscation);
                return Utility.getFieldLabel('input__c', selectedTranscation);
                }catch(Exception Ex){

                }
            }
            return new map<String, string>();
        }
    }

    public PageReference uploadAttachmentFeed() {
     	try {
     	    errorMessage ='';
     		if(fileName!=null && !String.isBlank(fileName)) {
                String filetype = fileName.subString(fileName.lastIndexOfIgnoreCase('.')+1);
            
                if((allowedFileType==null || String.isBlank(allowedFileType)) || !allowedFileType.containsIgnoreCase(filetype)) {
                	errorMessage = System.Label.CM_Alert_File_InvalidFileFormat;
            		return null;
                }
                
        	}
        	FeedItem uploadedAttachment = new FeedItem();
        	uploadedAttachment.ContentFileName = fileName;
	        uploadedAttachment.Title = uploadedAttachment.ContentFileName;
	        uploadedAttachment.ParentId = parentInvoiceId;//'a1d1b0000008jhw'; //eg. Opportunity id, custom object id..
	        uploadedAttachment.Body = uploadedAttachment.ContentFileName + '-submitinvoice';
	        uploadedAttachment.Visibility = 'AllUsers';
	        uploadedAttachment.ContentData = fileData;
	        insert uploadedAttachment;
	        fileId = uploadedAttachment.Id;
	        latestContentId = [SELECT RelatedRecordId FROM FeedItem WHERE Id=:fileId limit 1].RelatedRecordId;
	        update (new ContentVersion(Id=latestContentId,OwnerId=invoiceOwnerId));
     	}catch (Exception ex) {
     		errorMessage = ''+ex;
     	}
     	fileData = null;
        return null;
     }

     public PageReference deleteAttachmentFeed() {
       fileId = null;
       fileName = null;
       latestContentId = null;
       return null;
     }

    public boolean insertInputs() {
            boolean hasError = false;
            errorMessage = '';
            map<String, string> mapLabelNames = Utility.getFieldLabelByFieldsetAPIname('input__c', fieldset, selectedTranscation);
            for(Schema.FieldSetMember f : inputUpload.getSObjectType().getDescribe().fieldSets.getMap().get(fieldset).getFields()) {
                if(f.required || f.dbrequired){
                  if(inputUpload.get(f.getFieldPath())==null){
                    errorMessage =  (String.isEmpty(errorMessage)? '' : '<br/>') + mapLabelNames.get(f.FieldPath) + ' ' + System.Label.CM_Validation_Input_ShouldNotBeBlank;
                    hasError = true;
                  }
                }
            }
            if(fieldset=='Option_Other' && inputUpload.get('amount__c')==null){
                errorMessage =  (String.isEmpty(errorMessage)? '' : '<br/>') + System.Label.CM_Validation_Input_FieldObligationAmountShouldNotBeBlank;
                hasError = true;
            }
            if(fieldset=='Window_Forward_Contract_Other' && inputUpload.get('Window_Start_Date__c')!=null && inputUpload.get('Window_End_Date__c')!=null){
                Datetime Window_Start_Date = (Datetime)inputUpload.get('Window_Start_Date__c');
                Datetime Window_End_Date = (Datetime)inputUpload.get('Window_End_Date__c');
                if(Window_End_Date<Window_Start_Date){
                    errorMessage =  (String.isEmpty(errorMessage)? '' : '<br/>') + System.Label.CM_Validation_Input_DateShouldBeGreaterOrEqual;
                    hasError = true;
                }
           }
           if(!hasError) {
            List<Input__c> inputs = new List<Input__c>();
            inputUpload.Type__c = selectedTranscation;
            inputUpload.input_Type__c = selectedTranscation;
            inputUpload.Parent_Account__c = Utility.currentAccount;
            inputUpload.User__c = Utility.loggedInUser.id;
            inputs.add(inputUpload);
            if(fieldSet=='Option_Other'){
                //inputOther = new Input__c();
                Utility.updateOptionInput(inputUpload, inputOther, selectedTranscation, fieldSet);
                inputs.add(inputOther);
            }
            insert inputs;
            if(inputs.size()>1) {
                if(Utility.updateRefOptionInput(inputs.get(0), inputs.get(1), fieldSet)){
                    update inputs;
                }
            }
            if(fileId!=null) {
                FeedItem uploadedAttachment = [SELECT Id,ContentData,ContentFileName FROM FeedItem WHERE Id=:fileId];
                    if(uploadedAttachment!=null) {
                        FeedItem post = new FeedItem();
                        post.Title = uploadedAttachment.ContentFileName;
                        post.ParentId = inputUpload.Id; //eg. Opportunity id, custom object id..
                        post.Body = uploadedAttachment.ContentFileName + '-'+inputUpload.Name;
                        post.ContentData = uploadedAttachment.ContentData;
                        post.ContentFileName = uploadedAttachment.ContentFileName;
                        post.Visibility = 'AllUsers';
                        insert post;
                    }
                fileId = null;
            }
           }
           return hasError;
    }
    public PageReference saveinputs() {
        Savepoint sp = Database.setSavepoint();
        try {
            if(!insertInputs()) {
                isSaveClicked = true;
                isInpSaved = true;

            }
        } catch(Exception ex) {
            errorMessage =  (String.isEmpty(errorMessage)? '' : '<br/>') + System.Label.CM_Alert_UnableToSaveInput + ex.getMessage();
            Database.rollback( sp );
        }
        for(ApexPages.Message message : ApexPages.getMessages()) {
            if( message.getSeverity() == ApexPages.Severity.ERROR){
                //message.getSummary()
                errorMessage += '<br/>'+message.getDetail();
            }
        }
        return null;
    }

    public PageReference saveNew() {
        Savepoint sp = Database.setSavepoint();
        try {
            if(!insertInputs()) {
                isInpSaved = true;
                inputUpload = new Input__c();
                inputOther = new Input__c();
            }
        } catch (Exception ex) {
            errorMessage =  (String.isEmpty(errorMessage)? '' : '<br/>') + System.Label.CM_Alert_UnableToSaveInput + ex.getMessage();
            Database.rollback( sp );
        }
        for(ApexPages.Message message : ApexPages.getMessages()) {
            if( message.getSeverity() == ApexPages.Severity.ERROR){
                //message.getSummary()
                errorMessage += '<br/>'+message.getDetail();
            }
        }
        
       	return null;
     }

     public PageReference redirectToHome(){
        if(isSaveClicked!=null && isSaveClicked==true){
            if(returnurl != null){
            return returnurl;
          }
          else{
            returnurl = new PageReference('/EDGE_Input_Management');
            return returnurl;
          }
        }
        else{
        	PageReference pgRef;
        	
        	if(String.isNotBlank(selectedTranscation)){
        		pgRef = new PageReference('/EDGE_CaptureInput?trans=' + EncodingUtil.urlEncode(selectedTranscation,'UTF-8')); //Raghu Rankawat : for Issue I-237981
        	}
        	else
            	pgRef = new PageReference('/EDGE_CaptureInput');
            
            return pgRef;
        }
     }
     
     public void currentTabAction() {
         System.debug(currentTab);
     }
}