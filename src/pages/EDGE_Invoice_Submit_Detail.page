<!--Author        :   Shreerath Nair(Appirio)-->
<!-- Created Date :   12 March' 2016-->
<!-- Task         :   T-484693-->
<!-- Description  :   New Invoice_Submit_detail Page-->
<apex:page showHeader="false"
	controller="CMP_SubmittedInvoiceDetailController" sidebar="false"
	title="{!$Label.CM_SectionHeading_InvoiceDetail_InvoiceDetail}"
	label="{!$Label.CM_SectionHeading_InvoiceDetail_InvoiceDetail}"
	name="{!$Label.CM_SectionHeading_InvoiceDetail_InvoiceDetail}"
	standardStylesheets="false" docType="html-5.0" applyHtmlTag="false" >

	<apex:composition template="EDGE_Site_Template">

		<apex:define name="title">
			<title>{!$Label.CM_EDGE_Invoice_Submitted_Detail}</title>
		</apex:define>
		<apex:define name="navigation">
			<c:EDGE_Site_Navigation menuName="invoiceManagement" />
		</apex:define>

		<apex:define name="body">
			<apex:includeScript value="{!URLFOR($Resource.EDGE_Template, '/js/ckeditor.js')}" />
			<apex:includeScript value="{!URLFOR($Resource.EDGE_Template, '/js/config.js')}" />
			<div class="container">
				<div class="row">
					<!-- content - invoice-submit-detail  -->
					<div class="invoice-submit-detail">
						<div class="col-xs-12 col-md-12 mt85">
						      <br/>
						 <apex:outputPanel layout="none" rendered="{!IF($CurrentPage.parameters.retUrl!=null,true,false)}">
	                		<a href="{!$Setup.CMP_Administration__c.CM_Community_BaseURL__c}{!$CurrentPage.parameters.retUrl}">
	                	  		 {!$Label.CM_EDGE_Back_to_previous_page}
	                		</a>
                		 </apex:outputPanel>
							<h2 class="page-header">{!$Label.CM_SectionHeading_InvoiceDetail_Invoice} {!invoiceRecord.Name}</h2>
							<br /> <br />
							<div class="block">
								<h3 class="block-header">
									<!--<apex:OutputPanel rendered="{!showActions}"> Invite Edge Users</apex:OutputPanel>-->
									{!$Label.CM_EDGE_Invoice_Collaboration}
								</h3>
								<div class="iblk">
									<div class="col-md-12 col-sm-12 col-xs-12 text-center">
										<apex:form id="frm">
											<apex:commandButton action="{!accept}" id="acceptId"
												styleClass="btn btn-yellow"
												value="{!$Label.CM_Button_Label_Acknowledge}"
												rendered="{!showActions}" />
											&nbsp;
											<!-- <apex:commandButton action="{!showRejectPopup}" id="rejectId"
												styleClass="btn btn-light"
												value="Reject"
												rendered="{!showActions}" /> -->
										 <apex:outputPanel rendered="{!showActions}" layout="none">
											<button class="btn btn-light" data-toggle="modal" data-target=".rejection-reason" onclick="return false;">{!$Label.CM_Edge_Reject_Button}</button>
										 </apex:outputPanel>

												<br/>
										</apex:form>
									</div>
								</div>
								<div class="iblk">
									<div class="col-md-12 col-sm-12 col-xs-12 text-center">
										<apex:outputpanel id="flashPanel"
											rendered="{!latestContentId!=null}">
											<apex:flash id="theFlash"
												src="/_swf/121310/sfc/flex/DocViewer.swf"
												flashvars="shepherd_prefix={!$Site.Prefix}/sfc/servlet.shepherd&v={!latestContentId}&mode=bubble"
												height="500px" width="600px" />
										</apex:outputpanel>
									</div>

								</div>

							</div>
							<!-- /preview section-->

							<c:EDGE_NewChatterPostComponent c_parentId="{!invoiceRecord.Id}" />

							<apex:outputPanel layout="none" id="attachmentsText">
								<c:EDGE_InvoiceAttachment edge_invoiceId="{!invoiceRecord.Id}"
									edge_invoiceName="{!invoiceRecord.Name}"  edge_invoicePage="InvoiceSubmit"/>
							</apex:outputPanel>

						</div>
					</div>
				</div>
			</div>

			<!-- Reject modal -->
	 <apex:form id="rejectModalForm" html-data-toggle="validator" html-role="form">
		<div class="modal fade rejection-reason" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document" style="left: -20%; width: auto; position: relative;">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="{!$Label.CM_Community_Button_Close}"><span aria-hidden="true">&times;</span></button>
		        <!-- <h4 class="modal-title" id="myModalLabel">Invoice Rejection</h4> -->
		        <div class="mb10">
                    <span class="req">*</span> {!$Label.CM_Edge_Indicates_required}
                </div>
		      </div>
		      <div class="modal-body">
		      	<div class="col-xs-12">
			    	 <b> {!$Label.CM_Edge_FieldLabel_RejectReason} <span class="req">&nbsp;*</span> </b>
			    	<apex:inputTextarea styleclass="form-control" rows="3"  id="rejectTxt"
			    					  html-placeholder="{!$Label.CM_Edge_Field_Placeholder_EnterReasonHere}"
			    					  value="{!rejectReason}"
			    					  html-data-minlength="1"
                                      html-data-rule-required="true"
                                      html-data-msg-required="{!$Label.CM_Edge_Required_Field}"/>
		    	</div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-light" data-dismiss="modal">{!$Label.CM_Community_Button_Close}</button>
		        <apex:commandButton styleClass="btn btn-yellow" value="{!$Label.CM_Community_Button_Save}" onclick="validateRej();" action="{!reject}"/>
		      </div>
		      <script>
		      	function validateRej(){
		      		$('[id$="rejectModalForm"]').validate();
		      		return false;
		      	}
		      </script>
		    </div>
		  </div>
		</div>
	  </apex:form>
	 </apex:define>
	</apex:composition>

	<!--Mohammad Swaleh-->
	<apex:form >
		<apex:outputPanel id="rejectModal" rendered="{!displayRejectPopUp}">
			<apex:outputPanel styleClass="popupRejectBackground" layout="block"
				rendered="{!displayRejectPopUp}" />
			<apex:outputPanel styleClass="custRejectPopup" layout="block"
				rendered="{!displayRejectPopUp}">
				<center>
					<apex:outputLabel value="Reject Reason" styleClass="popupClass" />
					<br />
					<apex:inputTextarea id="rejectText" value="{!rejectReason}"
						style="width: 350px; height: 141px;" required="true" />
					<br />
					<apex:commandButton value="{!$Label.CM_Community_Button_Close}"
						action="{!closeRejectPopup}"
						style="background: #FFDF1A !important;box-shadow: 0px 1px 1px 0px rgba(0, 0, 0, 0.10) !important"
						immediate="true" />
					<apex:commandButton value="{!$Label.CM_Community_Button_Save}"
						style="background: #FFDF1A !important;box-shadow: 0px 1px 1px 0px rgba(0, 0, 0, 0.10) !important" />
				</center>
				<script>
                    function doReject(){
                        var rejectTxt = $('[id$="rejectText"]').val();
                       // alert('>> '+ rejectTxt);
                        if(rejectTxt== null || rejectTxt == ''){
                            alert('{!$Label.CM_RejectReason_RequiredMessage}');
                            return false;
                        }
                        else{
                            reject1();
                            return false;
                        }
                    }
                </script>
				<apex:actionFunction name="reject1" action="{!reject}" />
			</apex:outputPanel>
		</apex:outputPanel>
	</apex:form>
	<script>
        $('[id$=viewButton]').on("click", function() {
                        $("body").scrollTop(0);
                    });
    </script>
	<style type="text/css">
.custRejectPopup {
	background-color: white;
	border-width: 2px;
	border-style: solid;
	z-index: 9999;
	margin-left: 17%;
	padding: 10px;
	position: absolute;
	width: 929px;
	height: 32%;
	top: 215px;
	color: rgba(0, 0, 0, 0.75) !important;
}

.popupRejectBackground {
	background-color: black;
	opacity: 0.20;
	filter: alpha(opacity = 20);
	position: fixed;
	width: 90%;
	height: 100%;
	top: 10px;
	left: 53px;
	z-index: 9998;
}
</style>

</apex:page>