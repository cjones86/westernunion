//2105-06-03 Trigger that updates the field User_CMP_Email__c on the Contact object if the User is new or the email address was changed.
//2015-07-10 The trigger will update the CMP_Activation_Date__c on the Contact with the creation date of the user
//Luis Merinero

trigger UserUpdateEmailTrigger on User (after insert, before update) {
    Map<id, String> contactMap = new Map<id, String>();
    Map<id, DateTime> contactMapCreateDate = new Map<id, DateTime>();
    User uOld = new User();
    For(User u: trigger.new){
        System.debug(u.Email + u.contactId);
        If(Trigger.isUpdate){
            uOld = trigger.oldMap.get(u.id);
        }
        If(u.contactId != null && uOld.Email != u.Email){
        	contactMap.put(u.ContactId, String.valueof(u.Email));
        	contactMapCreateDate.put(u.ContactId, u.CreatedDate);
        }
    }
    
    If(contactMap.size() > 0){
        ContactUpdateUserEmail.UpdateUserEmail(contactMap,contactMapCreateDate);
    }
    
}