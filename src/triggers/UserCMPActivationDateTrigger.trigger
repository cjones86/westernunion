//JUL102105 Luis Merinero
//Trigger that updated the CMP Activation Date on the Contact related to the user.
//CMP Project

trigger UserCMPActivationDateTrigger on User (after insert) {
    List<Contact> ContactsToUpdate = new List<Contact>();
    List<user> newCMPUsers = new List<User>();
    For(User newUser:trigger.new){
        If(newUser.Contact != null && newUser.UserType == 'CspLitePortal'){
            newCMPUsers.add(newUser);
        }
    }
    if(newCMPUsers.size() <> 0){
        For(User u:newCMPUsers){
            Contact c = new Contact(id = u.Contact.id, CMP_Activation_Date__c = u.CreatedDate);
            ContactsToUpdate.add(c);
        }
        update ContactsToUpdate;
    }
}