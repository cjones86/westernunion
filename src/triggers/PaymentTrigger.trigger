/**================================================================      
* Appirio, Inc
* Name: PaymentTrigger
* Description: Trigger on Payment Object
* Created Date: 6-Oct-2016
* Created By: Raghu Rankawat (Appirio)
*
* Date Modified      Modified By      Description of the update
* 
==================================================================*/

trigger PaymentTrigger on Payment__c (before insert) {
  
  PaymentTriggerHandler objHandler = new PaymentTriggerHandler();

  if(Trigger.isInsert && Trigger.isBefore){
    objHandler.triggerHandler_BeforeInsert(Trigger.new);      
  }    

}