/*
29 May 2009 - Kevin Lam (CR-000059)
For users with a GBP UK profile, only GBP UK Administrators and GBP UK Accreditation Manager can deleted Attachments from Accounts.

4 Jun 2009 - Kevin Lam
Rule applied to all object types, not just Accounts.
*/
trigger AttachmentDelete on Attachment (before delete) {
    String userProfile = [SELECT Profile.Name FROM User WHERE Id = :UserInfo.getUserId()].Profile.Name;
    if (userProfile.startsWith('Agent') || (userProfile.startsWith('GBP UK') && !userProfile.startsWith('GBP UK System Administrator') && userProfile != 'GBP UK Accreditation Manager')) {
//      Map<ID, Attachment> attachmentParentIDs = new Map<ID, Attachment>();    // 4 Jun 2009 - Kevin Lam
        for (Attachment attachmentDeleted : Trigger.old) {
            attachmentDeleted.addError('Insufficient privileges to delete attachments.');
//          attachmentParentIDs.put(attachmentDeleted.ParentId, attachmentDeleted); // 4 Jun 2009 - Kevin Lam
        }
/*  4 Jun 2009 - Kevin Lam
        for (Account acct : [SELECT Id FROM Account WHERE Id IN :attachmentParentIDs.keySet()]) {
            attachmentParentIDs.get(acct.Id).addError('Insufficient privileges to delete attachments from accounts.');
        }
*/
    }
}