/*  19 Dec 2008 - Kevin Lam
    Trigger to update the end date of the GBP Employee record whenever a user record is deactivated
    Need to set API version to 12.0 in order to update GBP Employee table

    25 May 2009 - Kevin Lam
    - Changed to API 15.0
    - Added code for deleting records from Authority_Matrix__c
    - Moved DML operations on GBP_Employee__c and Authority_Matrix__c to userJobs.cls
    
    22 June 2012 - Adrian Chiew
    - Updated to exclude users where Do Not Delete GBP Employee Record is ticked.
*/
trigger UserDeactivated on User (before update) {

    List<ID> usersDeactivatedAuthorityMatrix = new List<ID>();
    List<ID> usersDeactivatedGBPEmployeeRecord = new List<ID>();

    for (User userUpdated : Trigger.new) {
        if(Trigger.oldMap.get(userUpdated.Id).IsActive == true && userUpdated.IsActive == false) {
            usersDeactivatedAuthorityMatrix.add(userUpdated.Id);
            if(userUpdated.Do_No_Delete_GBP_Employee_Record__c == false){
                usersDeactivatedGBPEmployeeRecord.add(userUpdated.Id);
            }
        }
    }
    
    if (usersDeactivatedGBPEmployeeRecord.size() > 0) {
        userJobs.deactivateGBPEmployeeRecord(usersDeactivatedGBPEmployeeRecord);
    }
    if (usersDeactivatedAuthorityMatrix.size() > 0) {
        userJobs.removeFromAuthorityMatrix(usersDeactivatedAuthorityMatrix);
    }    
    
}