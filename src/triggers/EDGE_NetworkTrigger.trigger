/**=====================================================================
 * Name: EDGE_NetworkTrigger
 * Description: Trigger Class to update the Supplier network records 
 * Created Date: May 11, 2016
 * Created By: Priyanka Kumar
 * Task      : T-501534
 =====================================================================*/
trigger EDGE_NetworkTrigger on Network__c (after insert,after update) {
	
  if(trigger.isInsert && trigger.isAfter){
  	EDGE_NetworkTriggerHandler handler = new EDGE_NetworkTriggerHandler();
  	handler.onAfterInsert(Trigger.new);
  }
  if(trigger.isUpdate && trigger.isAfter){
  	EDGE_NetworkTriggerHandler handler = new EDGE_NetworkTriggerHandler();
  	handler.onAfterUpdate(Trigger.new,Trigger.oldMap);
  }
}