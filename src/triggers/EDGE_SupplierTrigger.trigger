/**=====================================================================
 * Name: EDGE_SupplierTrigger
 * Description: Trigger Class to update the Supplier network records 
 * Created Date: May 11, 2016
 * Created By: Priyanka Kumar
 * Task      : T-501534
 =====================================================================*/
trigger EDGE_SupplierTrigger on Supplier__c (before insert, before update) {
	
	EDGE_SupplierTriggerHandler handler = new EDGE_SupplierTriggerHandler();
	
	if(trigger.isBefore){
		handler.onBeforeInsertUpdate(Trigger.new, Trigger.oldMap);
	}

}