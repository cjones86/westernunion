// 
// (c) 2014 Appirio, Inc.
//
// CaseCommentNotificationTrigger
// Trigger on CaseComment to send Email Alert when CaseComment is marked as public
//
// Oct 07, 2015    Ashish Goyal             Original (Ref. T-438682)
//
trigger CaseCommentNotificationTrigger on CaseComment (after insert, after update, before insert, before update) {
	if(Trigger.isAfter){
		CaseCommentNotificationTriggerManager.sendEmailToCMPUser(Trigger.new, Trigger.oldMap);
		//CaseCommentNotificationTriggerManager.checkForScripts(Trigger.new);
	}
	if(Trigger.isBefore){
		CaseCommentNotificationTriggerManager.checkCaseCommentPublicStatus(Trigger.new, Trigger.oldMap);
	}
}