/**=====================================================================
 * Appirio, Inc
 * Name: GlobalPayManagementTrigger
 * Description: Trigger class for Global_Pay_ID_Management__c object after insert actions
 * Created Date: 14 Apr' 2016
 * Created By: Rohit Sharma (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
trigger GlobalPayManagementTrigger on Global_Pay_ID_Management__c (after insert) {

	Map<String,String> gpAccpuntIdWithgPId = new Map<String,String>();
	Map<String,String> gpAccpuntIdWithEmailId = new Map<String,String>();
	for(Global_Pay_ID_Management__c gpIM : [SELECT Global_Pay_ID__c,Account_Beneficiary__c,
											Source_Email_Match__c,User_Beneficiary__r.Email FROM 
											Global_Pay_ID_Management__c WHERE Id IN :Trigger.new]) {
		gpAccpuntIdWithgPId.put(gpIM.Global_Pay_ID__c,gpIM.Account_Beneficiary__c);
		if(!String.isBlank(gpIM.Source_Email_Match__c)) 
			gpAccpuntIdWithEmailId.put(gpIM.Source_Email_Match__c,gpIM.Account_Beneficiary__c);
	}

	Map<Id,Invoice__c> invoiceToUpdate = new Map<Id,Invoice__c>();
	for(Invoice__c invoice : [SELECT Id,Seller_Account__c,Beneficiary_GP_ID__c,
							  Beneficiary_GP_Email_Address__c FROM Invoice__c WHERE 
							  Beneficiary_GP_ID__c IN:gpAccpuntIdWithgPId.keySet() OR 
							  Beneficiary_GP_Email_Address__c IN:gpAccpuntIdWithEmailId.keySet()]) {
		if(gpAccpuntIdWithgPId.containsKey(invoice.Beneficiary_GP_ID__c)) {
			invoice.Seller_Account__c = gpAccpuntIdWithgPId.get(invoice.Beneficiary_GP_ID__c);
		} else if(invoice.Beneficiary_GP_Email_Address__c!=null && 
			gpAccpuntIdWithEmailId.containsKey(invoice.Beneficiary_GP_Email_Address__c)) {
			invoice.Seller_Account__c = gpAccpuntIdWithEmailId.get(invoice.Beneficiary_GP_Email_Address__c);
		}		
		invoiceToUpdate.put(invoice.Id,invoice);
	}

	if(invoiceToUpdate.size()>0) {
		update invoiceToUpdate.values();
	}
}