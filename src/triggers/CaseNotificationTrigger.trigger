// 
// (c) 2014 Appirio, Inc.
//
// CaseNotificationTrigger
// Trigger on Case to send Email Alert when Case is marked as public
//
// Oct 07, 2015    Ashish Goyal             Original (Ref. T-438682)
//
trigger CaseNotificationTrigger on Case (after insert, after update, before insert, before update) {
    /* Commented as per Task T-521781
	if(Trigger.isAfter){
		CaseNotificationTriggerManager.sendEmailToContact(Trigger.new, Trigger.oldMap);
	}
	*/
	if(Trigger.isBefore){
		if(Trigger.isInsert)
			CaseNotificationTriggerManager.checkCasePublicStatusBI(Trigger.new);
		if(Trigger.isUpdate)
			CaseNotificationTriggerManager.checkCasePublicStatusBU(Trigger.new, Trigger.oldMap);
	}
}