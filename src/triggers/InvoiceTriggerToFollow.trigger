trigger InvoiceTriggerToFollow on Invoice__c (after insert ,before insert ,before update, after update) {
    InvoiceAutoFollowHandler objHandler=new InvoiceAutoFollowHandler();
    // if insert
    if(trigger.isInsert && trigger.isAfter){        
        objHandler.triggerHandler_AfterInsert(trigger.new);                
    }
    
    if(trigger.isBefore && trigger.isUpdate) {
        objHandler.triggerHandler_BeforeUpdate(trigger.new,trigger.oldMap);
    }
    
    if(trigger.isBefore && trigger.isInsert) {
        objHandler.triggerHandler_BeforeInsert(trigger.new);
    }
    
    
    // if update
    /* if(trigger.isUpdate){       
        objHandler.triggerHandler_AfterUpdate(trigger.oldMap,trigger.new);
    }*/
    
    //MS 5/30: Temp Sharing call START
    if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)) {                
        SharingMaster.doShare();        
    }
    //MS 5/30: Temp Sharing call END
}