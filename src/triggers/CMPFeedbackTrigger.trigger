// 
// (c) 2014 Appirio, Inc.
//
// CMPFeedbackTrigger
// Used to create approval process when record is created
//
// May 28, 2015    Ashish Goyal  			Original (Ref. T-399983)
// Modified : June 02, 2015    Ashish Goyal  			Ref. T-405363
//
trigger CMPFeedbackTrigger on CMP_Feedback__c (after insert, before insert, after update) {
	
	if(Trigger.isBefore){
		CMPFeedbackTriggerManager.beforeInsert(Trigger.new);
	}
	
	if(Trigger.isAfter){
		if(Trigger.isInsert){
			CMPFeedbackTriggerManager.afterInsert(Trigger.new);
		}
		if(Trigger.isUpdate){
			CMPFeedbackTriggerManager.afterUpdate(Trigger.new, Trigger.oldMap);
		}
	}

}